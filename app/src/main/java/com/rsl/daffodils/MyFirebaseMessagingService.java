package com.rsl.daffodils;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.rsl.daffodils.activity.FeedbackActivity;
import com.rsl.daffodils.activity.admin.FeedbackListActivity;
import com.rsl.daffodils.activity.student.CalenderActivity;
import com.rsl.daffodils.activity.student.DownloadsActivity;
import com.rsl.daffodils.activity.student.NotificationActivity;
import com.rsl.daffodils.activity.student.StudentsHomeActivity;
import com.rsl.daffodils.activity.teacher.TeachersHomeActivity;
import com.rsl.daffodils.model.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.*;

import java.text.ParseException;

import static android.content.Intent.FLAG_ACTIVITY_CLEAR_TOP;
import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;


/**
 * Created by admin on 8/30/2016.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    SharedPreferences sp;
    SharedPreferences.Editor editor;

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.app_logo);
        Log.i("onMessage/////", "Received message");
        Intent notificationIntent = null;
        String msg = "You Have Message";
        String type = "status";
        String message_display;

        sp = getSharedPreferences("daffodils", 0);
        editor = sp.edit();
        editor.apply();

        // TODO(developer): Handle FCM messages here.
        // If the application is in the foreground handle both data and notification messages here.
        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
        Log.e(TAG, "From: " + remoteMessage.getFrom());
        //Log.e(TAG, "Notification Message Body: " + remoteMessage.getNotification().getBody());
      //  Log.e(TAG, "Notification Message data: " + remoteMessage.getData());
        //Log.e(TAG, "Notification Message Tag: " + remoteMessage.getNotification().getTag());

        try {
            JSONObject jsonObject = new JSONObject(remoteMessage.getData());
            Log.e(TAG, "JSONObject: " + jsonObject.toString());
            sendNotification(jsonObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    // [END receive_message]

    /**
     * Create and show a simple notification containing the received FCM message.
     */
    private void sendNotification(JSONObject jo) {
        Intent notificationIntent = null;
        int random_number = (int) Math.round(Math.random() * 999999);
        Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.app_logo_main);
        Log.e("onMessage/////", "Received message");
        try {
            String action = jo.get("action").toString();
            if (action != null) {
                if (Constants.studentHomeActivity) {
                    notificationIntent = new Intent(this, StudentsHomeActivity.class);
                    notificationIntent.setFlags(FLAG_ACTIVITY_NEW_TASK | FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(notificationIntent);
                } else if (Constants.teacherHomeActivity) {
                    notificationIntent = new Intent(this, TeachersHomeActivity.class);
                    notificationIntent.setFlags(FLAG_ACTIVITY_NEW_TASK | FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(notificationIntent);
                }
                if (action.toLowerCase().equals("news")) {
                    notificationIntent = new Intent(this, NotificationActivity.class);
                    notificationIntent.putExtra("type", "News");

                    String str_stud = jo.getString("student_info");


                    Object boj = jo.getString("student_info");
                    if (boj instanceof JSONObject) {
                        Log.e("JSONObject", "JSONObject");
                    } else if (boj instanceof JSONArray) {
                        Log.e("JSONArray", "JSONArray");
                    }

                    JsonParser parser = new JsonParser();
                    JsonElement tradeElement = parser.parse(str_stud);
                    JsonArray trade = tradeElement.getAsJsonArray();
                    Log.e("trade", "" + trade.toString());



                    JsonObject jsonObject = trade.get(0).getAsJsonObject();

                    Log.e("jarray", "" + jsonObject);
                  //  JsonObject jsonObject = jarray.getAsJsonObject();
                    Log.e("str_stud", "==" + jsonObject.get("student_id").getAsString());

//                    notificationIntent.putExtra("student_id", jsonObject.get("student_id").getAsString());
//                    notificationIntent.putExtra("account_id", jsonObject.get("account_id").getAsString());
//                    notificationIntent.putExtra("student_image", jsonObject.get("image").getAsString());
//                    notificationIntent.putExtra("parent_mobile", jsonObject.get("parent_mobile").getAsString());
//                    notificationIntent.putExtra("student_name1", jsonObject.get("student_name").getAsString());
//                    notificationIntent.putExtra("parent_email", jsonObject.get("parent_email").getAsString());
//                    notificationIntent.putExtra("date_of_birth", jsonObject.get("dob").getAsString());
//                    notificationIntent.putExtra("gender_id", jsonObject.get("gender_id").getAsString());
//                    notificationIntent.putExtra("student_class1", jsonObject.get("class").getAsString());
//                    notificationIntent.putExtra("class_id", jsonObject.get("class_id").getAsString());


                    if (jo.get("type").equals("announcement")) {
                        notificationIntent.putExtra("type", "announcement");
                        notificationIntent.putExtra("student_id", jsonObject.get("student_id").getAsString());
                        notificationIntent.putExtra("account_id", jsonObject.get("account_id").getAsString());
                        notificationIntent.putExtra("student_image", jsonObject.get("image").getAsString());
                        notificationIntent.putExtra("parent_mobile", jsonObject.get("parent_mobile").getAsString());
                        notificationIntent.putExtra("student_name1", jsonObject.get("student_name").getAsString());
                        notificationIntent.putExtra("parent_email", jsonObject.get("parent_email").getAsString());
                        notificationIntent.putExtra("date_of_birth", jsonObject.get("dob").getAsString());
                        notificationIntent.putExtra("gender_id", jsonObject.get("gender_id").getAsString());
                        notificationIntent.putExtra("student_class1", jsonObject.get("class").getAsString());
                        notificationIntent.putExtra("class_id", jsonObject.get("class_id").getAsString());
                    } else if (jo.get("type").equals("homework")) {
                        notificationIntent.putExtra("type", "homework");
                        notificationIntent.putExtra("student_id", jsonObject.get("student_id").getAsString());
                        notificationIntent.putExtra("account_id", jsonObject.get("account_id").getAsString());
                        notificationIntent.putExtra("student_image", jsonObject.get("image").getAsString());
                        notificationIntent.putExtra("parent_mobile", jsonObject.get("parent_mobile").getAsString());
                        notificationIntent.putExtra("student_name1", jsonObject.get("student_name").getAsString());
                        notificationIntent.putExtra("parent_email", jsonObject.get("parent_email").getAsString());
                        notificationIntent.putExtra("date_of_birth", jsonObject.get("dob").getAsString());
                        notificationIntent.putExtra("gender_id", jsonObject.get("gender_id").getAsString());
                        notificationIntent.putExtra("student_class1", jsonObject.get("class").getAsString());
                        notificationIntent.putExtra("class_id", jsonObject.get("class_id").getAsString());
                    } else {
                        notificationIntent.putExtra("type", "message");
                        notificationIntent.putExtra("student_id", jsonObject.get("student_id").getAsString());
                        notificationIntent.putExtra("account_id", jsonObject.get("account_id").getAsString());
                        notificationIntent.putExtra("student_image", jsonObject.get("image").getAsString());
                        notificationIntent.putExtra("parent_mobile", jsonObject.get("parent_mobile").getAsString());
                        notificationIntent.putExtra("student_name1", jsonObject.get("student_name").getAsString());
                        notificationIntent.putExtra("parent_email", jsonObject.get("parent_email").getAsString());
                        notificationIntent.putExtra("date_of_birth", jsonObject.get("dob").getAsString());
                        notificationIntent.putExtra("gender_id", jsonObject.get("gender_id").getAsString());
                        notificationIntent.putExtra("student_class1", jsonObject.get("class").getAsString());
                        notificationIntent.putExtra("class_id", jsonObject.get("class_id").getAsString());
                    }
                    if (Constants.activity_notification) {
                        notificationIntent.setFlags(FLAG_ACTIVITY_NEW_TASK | FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(notificationIntent);
                    } else {
                        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                        notificationIntent.setFlags(FLAG_ACTIVITY_NEW_TASK
                                | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                         notificationIntent.putExtra("from_fcm", "true");
                        PendingIntent intent1 = PendingIntent.getActivity(this, random_number,
                                notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                        NotificationCompat.Builder notification = new NotificationCompat.Builder(this)
                                .setContentTitle(getString(R.string.app_name))
                                .setContentText(jo.get("msg").toString())
                                .setLargeIcon(largeIcon)
                                .setSmallIcon(R.drawable.app_logo_main)
                                .setContentIntent(intent1)
                                .setAutoCancel(true)
                                .setSound(alarmSound);
                        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                        notificationManager.notify(random_number, notification.build());
                    }
                } else if (action.toLowerCase().equals("event")) {
                    Log.v("msggh...............", jo.get("msg").toString());
                    notificationIntent = new Intent(this, CalenderActivity.class);
                    String str_stud = jo.getString("student_info");

                    Object boj = jo.getString("student_info");
                    if (boj instanceof JSONObject) {
                        Log.e("JSONObject", "JSONObject");
                    } else if (boj instanceof JSONArray) {
                        Log.e("JSONArray", "JSONArray");
                    }

                    JsonParser parser = new JsonParser();
                    JsonElement tradeElement = parser.parse(str_stud);
                    JsonArray trade = tradeElement.getAsJsonArray();
                    Log.e("trade", "" + trade.toString());


                    JsonObject jsonObject = trade.get(0).getAsJsonObject();

                    Log.e("jarray", "" + jsonObject);
                    //  JsonObject jsonObject = jarray.getAsJsonObject();
                    Log.e("str_stud", "==" + jsonObject.get("student_id").getAsString());


                    if (jo.get("type").equals("event")) {
                        notificationIntent.putExtra("type", "event");
                        notificationIntent.putExtra("student_id", jsonObject.get("student_id").getAsString());
                        notificationIntent.putExtra("account_id", jsonObject.get("account_id").getAsString());
                        notificationIntent.putExtra("student_image", jsonObject.get("image").getAsString());
                        notificationIntent.putExtra("parent_mobile", jsonObject.get("parent_mobile").getAsString());
                        notificationIntent.putExtra("student_name1", jsonObject.get("student_name").getAsString());
                        notificationIntent.putExtra("parent_email", jsonObject.get("parent_email").getAsString());
                        notificationIntent.putExtra("date_of_birth", jsonObject.get("dob").getAsString());
                        notificationIntent.putExtra("gender_id", jsonObject.get("gender_id").getAsString());
                        notificationIntent.putExtra("student_class1", jsonObject.get("class").getAsString());
                        notificationIntent.putExtra("class_id", jsonObject.get("class_id").getAsString());
                    } else {
                        notificationIntent.putExtra("type", "holiday");
                        notificationIntent.putExtra("student_id", jsonObject.get("student_id").getAsString());
                        notificationIntent.putExtra("account_id", jsonObject.get("account_id").getAsString());
                        notificationIntent.putExtra("student_image", jsonObject.get("image").getAsString());
                        notificationIntent.putExtra("parent_mobile", jsonObject.get("parent_mobile").getAsString());
                        notificationIntent.putExtra("student_name1", jsonObject.get("student_name").getAsString());
                        notificationIntent.putExtra("parent_email", jsonObject.get("parent_email").getAsString());
                        notificationIntent.putExtra("date_of_birth", jsonObject.get("dob").getAsString());
                        notificationIntent.putExtra("gender_id", jsonObject.get("gender_id").getAsString());
                        notificationIntent.putExtra("student_class1", jsonObject.get("class").getAsString());
                        notificationIntent.putExtra("class_id", jsonObject.get("class_id").getAsString());
                    }

                    if (Constants.activity_calender) {
                        notificationIntent.setFlags(FLAG_ACTIVITY_NEW_TASK | FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(notificationIntent);
                    } else {
                        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                        notificationIntent.setFlags(FLAG_ACTIVITY_NEW_TASK
                                | Intent.FLAG_ACTIVITY_SINGLE_TOP);

                         notificationIntent.putExtra("from_fcm", "true");
                        PendingIntent intent1 = PendingIntent.getActivity(this, 0,
                                notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                        NotificationCompat.Builder notification = new NotificationCompat.Builder(this)
                                .setContentTitle(getString(R.string.app_name))
                                .setContentText(jo.get("msg").toString())
                                .setLargeIcon(largeIcon)
                                .setSmallIcon(R.drawable.app_logo_main)
                                .setContentIntent(intent1)
                                .setAutoCancel(true)
                                .setSound(alarmSound);
                        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                        notificationManager.notify(random_number, notification.build());
                    }
                } else if (action.toLowerCase().equals("downloads")) {
                    Log.v("msggh...............", jo.get("msg").toString());
                    notificationIntent = new Intent(this, DownloadsActivity.class);

                    String str_stud = jo.getString("student_info");

                    Object boj = jo.getString("student_info");
                    if (boj instanceof JSONObject) {
                        Log.e("JSONObject", "JSONObject");
                    } else if (boj instanceof JSONArray) {
                        Log.e("JSONArray", "JSONArray");
                    }

                    JsonParser parser = new JsonParser();
                    JsonElement tradeElement = parser.parse(str_stud);
                    JsonArray trade = tradeElement.getAsJsonArray();
                    Log.e("trade", "" + trade.toString());

                    JsonObject jsonObject = trade.get(0).getAsJsonObject();

                    Log.e("jarray", "" + jsonObject);
                    //  JsonObject jsonObject = jarray.getAsJsonObject();
                    Log.e("str_stud", "==" + jsonObject.get("student_id").getAsString());
                    notificationIntent.putExtra("type", "downloads");
                    notificationIntent.putExtra("from_fcm","true");
                    notificationIntent.putExtra("student_id", jsonObject.get("student_id").getAsString());
                    notificationIntent.putExtra("account_id", jsonObject.get("account_id").getAsString());
                    notificationIntent.putExtra("student_image", jsonObject.get("image").getAsString());
                    notificationIntent.putExtra("parent_mobile", jsonObject.get("parent_mobile").getAsString());
                    notificationIntent.putExtra("student_name1", jsonObject.get("student_name").getAsString());
                    notificationIntent.putExtra("parent_email", jsonObject.get("parent_email").getAsString());
                    notificationIntent.putExtra("date_of_birth", jsonObject.get("dob").getAsString());
                    notificationIntent.putExtra("gender_id", jsonObject.get("gender_id").getAsString());
                    notificationIntent.putExtra("student_class1", jsonObject.get("class").getAsString());
                    notificationIntent.putExtra("class_id", jsonObject.get("class_id").getAsString());

                    Log.e("fcm", "fcm without if");
                    if (Constants.activity_downlosds) {
                        notificationIntent.setFlags(FLAG_ACTIVITY_NEW_TASK | FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(notificationIntent);
                        Log.e("fcm", "fcm with if");
                    } else {
                        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                        Log.e("fcm", "fcm with else");
                        notificationIntent.setFlags(FLAG_ACTIVITY_NEW_TASK
                                | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        PendingIntent intent1 = PendingIntent.getActivity(this, 0,
                                notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                        NotificationCompat.Builder notification = new NotificationCompat.Builder(this)
                                .setContentTitle(getString(R.string.app_name))
                                .setContentText(jo.get("msg").toString())
                                .setLargeIcon(largeIcon)
                                .setSmallIcon(R.drawable.app_logo_main)
                                .setContentIntent(intent1)
                                .setAutoCancel(true)
                                .setSound(alarmSound);
                        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                        notificationManager.notify(random_number, notification.build());
                    }
                }else if (action.toLowerCase().equals("feedback")) {
                    Log.v("msggh...............", jo.get("msg").toString());
                    notificationIntent = new Intent(this, FeedbackListActivity.class);

                   // String str_stud = jo.getString("student_info");

                 /*   Object boj = jo.getString("student_info");
                    if (boj instanceof JSONObject) {
                        Log.e("JSONObject", "JSONObject");
                    } else if (boj instanceof JSONArray) {
                        Log.e("JSONArray", "JSONArray");
                    }*/

              /*      JsonParser parser = new JsonParser();
                    JsonElement tradeElement = parser.parse(str_stud);
                    JsonArray trade = tradeElement.getAsJsonArray();
                    Log.e("trade", "" + trade.toString());*/

                   // JsonObject jsonObject = trade.get(0).getAsJsonObject();

                   // Log.e("jarray", "" + boj);
                    //  JsonObject jsonObject = jarray.getAsJsonObject();
                /*    Log.e("str_stud", "==" + jsonObject.get("student_id").getAsString());
                    notificationIntent.putExtra("type", "downloads");
                    notificationIntent.putExtra("from_fcm", "true");
                    notificationIntent.putExtra("student_id", jsonObject.get("student_id").getAsString());
                    notificationIntent.putExtra("account_id", jsonObject.get("account_id").getAsString());
                    notificationIntent.putExtra("student_image", jsonObject.get("image").getAsString());
                    notificationIntent.putExtra("parent_mobile", jsonObject.get("parent_mobile").getAsString());
                    notificationIntent.putExtra("student_name1", jsonObject.get("student_name").getAsString());
                    notificationIntent.putExtra("parent_email", jsonObject.get("parent_email").getAsString());
                    notificationIntent.putExtra("date_of_birth", jsonObject.get("dob").getAsString());
                    notificationIntent.putExtra("gender_id", jsonObject.get("gender_id").getAsString());
                    notificationIntent.putExtra("student_class1", jsonObject.get("class").getAsString());
                    notificationIntent.putExtra("class_id", jsonObject.get("class_id").getAsString());*/

                    Log.e("fcm", "fcm without if");
                    if (Constants.teacherFeedBackActivity) {
                        notificationIntent.setFlags(FLAG_ACTIVITY_NEW_TASK | FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(notificationIntent);
                        Log.e("fcm", "fcm with if");
                    } else {
                        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                        Log.e("fcm", "fcm with else");
                        notificationIntent.setFlags(FLAG_ACTIVITY_NEW_TASK
                                | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        PendingIntent intent1 = PendingIntent.getActivity(this, 0,
                                notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                        NotificationCompat.Builder notification = new NotificationCompat.Builder(this)
                                .setContentTitle(getString(R.string.app_name))
                                .setContentText(jo.get("msg").toString())
                                .setLargeIcon(largeIcon)
                                .setSmallIcon(R.drawable.app_logo_main)
                                .setContentIntent(intent1)
                                .setAutoCancel(true)
                                .setSound(alarmSound);
                        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                        notificationManager.notify(random_number, notification.build());
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


}