package com.rsl.daffodils.model;

import java.util.ArrayList;
import java.util.HashMap;

public class Constants {
    public static boolean activity_chat = false;
    public static boolean activity_references = false;
    public static boolean activity_notification = false;
    public static boolean activity_downlosds = false;
    public static boolean activity_calender = false;
    public static boolean activity_advertisement = false;
    public static boolean studentHomeActivity = false;
    public static boolean teacherHomeActivity = false;
    public static boolean teacherFeedBackActivity = false;

    public static String getChat_id() {
        return chat_id;
    }

    public static void setChat_id(String chat_id) {
        Constants.chat_id = chat_id;
    }

    public static String chat_id = "";


    public static ArrayList<HashMap<String, String>> getImage_data() {
        return image_data;
    }

    public static void setImage_data(ArrayList<HashMap<String, String>> image_data) {
        Constants.image_data = image_data;
    }

//    public static boolean isWorkActivityOpen = false;

    public static ArrayList<HashMap<String, String>> image_data = new ArrayList<>();



}
