package com.rsl.daffodils.model;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Developmenttwelve on 9/6/2017.
 */

public class Gallery{

    public ArrayList<HashMap<String, String>> getGallery_images() {
        return gallery_images;
    }

    public void setGallery_images(ArrayList<HashMap<String, String>> gallery_images) {
        this.gallery_images = gallery_images;
    }

    private ArrayList<HashMap<String, String>> gallery_images;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    private String title;
    private String description;
    private String date;
}
