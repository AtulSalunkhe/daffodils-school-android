package com.rsl.daffodils.model;

/**
 * Created by Admin on 6/29/2017.
 */

public class MenuItemGroupName {

    public String getMenu_item_group_name() {
        return menu_item_group_name;
    }

    public void setMenu_item_group_name(String menu_item_group_name) {
        this.menu_item_group_name = menu_item_group_name;
    }

    private String menu_item_group_name;

    public String getMenu_item_group_id() {
        return Menu_item_group_id;
    }

    public void setMenu_item_group_id(String menu_item_group_id) {
        Menu_item_group_id = menu_item_group_id;
    }

    private String Menu_item_group_id;

}
