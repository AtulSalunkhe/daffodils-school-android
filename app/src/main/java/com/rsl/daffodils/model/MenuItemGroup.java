package com.rsl.daffodils.model;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Admin on 6/30/2017.
 */

public class MenuItemGroup {


    public String getStudentclass() {
        return Studentclass;
    }

    public void setStudentclass(String studentclass) {
        Studentclass = studentclass;
    }

    private String Studentclass;

    public ArrayList<HashMap<String, String>> getMenu_items() {
        return menu_items;
    }

    public void setMenu_items(ArrayList<HashMap<String, String>> menu_items) {
        this.menu_items = menu_items;
    }

    private ArrayList<HashMap<String, String>> menu_items;


}
