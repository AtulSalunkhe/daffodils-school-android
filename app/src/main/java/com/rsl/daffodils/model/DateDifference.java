package com.rsl.daffodils.model;

import android.util.Log;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by admin on 6/29/2017.
 */

public class DateDifference {
    public static String printDifference(Date startDate, Date endDate) {

        // milliseconds

        Log.e("startDate",""+startDate);
        Log.e("endDate",""+endDate);

        long different = endDate.getTime() - startDate.getTime();

        Log.e("datetime",""+different);

        System.out.println("startDate : " + startDate);
        System.out.println("endDate : " + endDate);
        System.out.println("different : " + different);
        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;
        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;
        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;
        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;
        long elapsedSeconds = different / secondsInMilli;
        System.out.printf("%d days, %d hours, %d minutes, %d seconds%n",
                elapsedDays, elapsedHours, elapsedMinutes, elapsedSeconds);

        if (elapsedDays >= 1) {
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            return df.format(startDate);
        } else if (elapsedDays > 0)
            return elapsedDays + " days ago";
        else if (elapsedHours > 0)
            return elapsedHours + " hours ago";
        else if (elapsedMinutes > 0)
            return elapsedMinutes + " minutes ago";
        else if (elapsedSeconds > 0)
            return elapsedSeconds + " seconds ago";
        else
            return "now";
    }
}
