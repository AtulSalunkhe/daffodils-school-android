package com.rsl.daffodils.model;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by pratikvelani on 9/26/15.
 */
public class Event {
    public String title;

//    public Event(String event_id,String title, String description, String date,ArrayList<HashMap<String, String>> event_images) {
//        this.event_id = event_id;
//        this.title = title;
//        this.description = description;
//        this.date = date;
//        this.event_images = event_images;
//    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String description;
    public String type;

    public int day;
    public int year;
    public int month;
    public String event_id;
    public String date;

    public String getEvent_image() {
        return event_image;
    }

    public void setEvent_image(String event_image) {
        this.event_image = event_image;
    }

    public String event_image;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String time;

    public String getPresent() {
        return present;
    }

    public void setPresent(String present) {
        this.present = present;
    }

    public String present;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }



    public String getEvent_id() {
        return event_id;
    }

    public void setEvent_id(String event_id) {
        this.event_id = event_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }


    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    private ArrayList<HashMap<String, String>> event_images;

    public ArrayList<HashMap<String, String>> getEvent_images() {
        return event_images;
    }

    public void setEvent_images(ArrayList<HashMap<String, String>> event_images) {
        this.event_images = event_images;
    }


}