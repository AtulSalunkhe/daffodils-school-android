package com.rsl.daffodils.fragment.student;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.rsl.daffodils.R;
import com.rsl.daffodils.adapter.HoildayAdapter;
import com.rsl.daffodils.adapter.MessageAdapter;
import com.rsl.daffodils.adapter.NewsAdapter;
import com.rsl.daffodils.model.Constants;
import com.rsl.daffodils.utils.ConnectionDetector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MessageFragment extends Fragment  implements SwipeRefreshLayout.OnRefreshListener{

    View view;
    RecyclerView rl_news_list;
    private ConnectionDetector internet;
    private MessageAdapter adapter;
    ArrayList<HashMap<String, String>> list_data;
    private SwipeRefreshLayout swipeRefreshLayout;
    TextView txt_message;
    SharedPreferences sp;
    SharedPreferences.Editor editor;
    TextView text_msg;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_message, container, false);

        sp = getActivity().getSharedPreferences("daffodils", 0);
        editor = sp.edit();
        editor.apply();
        Constants.activity_notification = true;
        text_msg = (TextView)view.findViewById(R.id.text_msg);
        swipeRefreshLayout = (SwipeRefreshLayout)view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary, R.color.colorAccent, R.color.event_holiday_color, R.color.event_color);
        initView();
        initrecycleView();
        internet = new ConnectionDetector(getActivity());
        if (internet.isConnectingToInternet()) {
            getMessege(sp.getString("student_id", ""));
            Log.e("oncreateview",""+sp.getString("student_id", ""));
        } else {
            internet.showAlertDialog(getActivity(), false);
            txt_message.setText(getString(R.string.str_internet_title));
            txt_message.setVisibility(View.VISIBLE);
            rl_news_list.setVisibility(View.GONE);
        }

       // getupdatecount("MESSAGE");
        return view;
    }//onCreateView

    @Override
    public void onRefresh() {
        if (internet.isConnectingToInternet()) {
            getMessege(sp.getString("student_id", ""));
            Log.e("oncreateview",""+sp.getString("student_id", ""));
        } else {
            swipeRefreshLayout.setRefreshing(false);
            internet.showAlertDialog(getActivity(), false);
        }
    }

    public void initrecycleView() {
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rl_news_list.setLayoutManager(layoutManager);
        final Activity object = getActivity();
        list_data = new ArrayList<>();
        new Thread(new Runnable() {
            @Override
            public void run() {
                adapter = new MessageAdapter(list_data, object);
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        rl_news_list.setAdapter(adapter);
                    }
                });
            }
        }).start();
    }

    private void initView() {
        rl_news_list = (RecyclerView) view.findViewById(R.id.rl_news_list);
        txt_message = (TextView) view.findViewById(R.id.txt_message);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Constants.activity_notification = false;
    }


    private void getMessege(final String id) {
        swipeRefreshLayout.setRefreshing(false);
        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.setMessage(getString(R.string.str_requesting));
        dialog.setCancelable(false);
        dialog.show();
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        String requestURL = sp.getString("new_link","") + "get_message/"+ id;
        Log.e("requestURL","URL"+requestURL);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", "code=" + response);
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if(response!=null){
                    if(response.equals("204")){
                        text_msg.setVisibility(View.VISIBLE);
                        text_msg.setText("No Records Available");
                       // alertDialog1("No Records Available");
                        Log.e("Error message", "No Records Available");
                    }else {
//                        text_no_data.setVisibility(View.GONE);
//                        adapter = new CarCompanyAdapter(car_company, CarComapnyActivity.this);
//                        recyclerView_company.setAdapter(adapter);
//                        adapter.notifyDataSetChanged();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
              //  Toast.makeText(getActivity(),"Poor Internet Coonection",Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                Log.e("param", "=" + params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                    VolleyError error = new VolleyError(new String(volleyError.networkResponse.data));
                    volleyError = error;
                    dialog.dismiss();
                }
                //  Toast.makeText(getActivity(),"Poor Internet Coonection",Toast.LENGTH_SHORT).show();
                Log.e("parseNetworkError","=="+volleyError);
                Log.e("parseNetworkError","string=="+volleyError.toString());
                Log.e("parseNetworkError","msg=="+volleyError.getMessage());
                return volleyError;
            }

            @Override
            protected Response<String> parseNetworkResponse(final NetworkResponse response) {
                String responseString = "";
                if (response != null) {
                    responseString = String.valueOf(response.statusCode);
                    // can get more details such as response.headers
                }
                Map<String, String> responseHeaders = response.headers;
                if (response.statusCode == 200) {
                    Response<String> result = Response.success(responseHeaders.get("Content-Type"), HttpHeaderParser.parseCacheHeaders(response));
                    Log.e("Response", "==" + parseToString(response));
                    rl_news_list.setVisibility(View.VISIBLE);
                    txt_message.setVisibility(View.GONE);
                    list_data.clear();
                    dialog.dismiss();

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            // This code will always run on the UI thread, therefore is safe to modify UI elements.
                            try {
                                JSONArray JArray = new JSONArray(parseToString(response));

                                for (int i = 0; i < JArray.length(); i++) {
                                    JSONObject jobj_Data = JArray.getJSONObject(i);

                                    HashMap<String, String> grp_data = new HashMap<>();
                                    grp_data.put("notification_id", jobj_Data.getString("notification_id"));
                                    grp_data.put("class_id", jobj_Data.getString("class_id"));
                                    grp_data.put("student_id", jobj_Data.getString("student_id"));
                                    grp_data.put("notification_title", jobj_Data.getString("notification_title"));
                                    grp_data.put("notification_data", jobj_Data.getString("notification_data"));
                                    grp_data.put("notification_img", jobj_Data.getString("notification_img"));
                                    grp_data.put("notification_date", jobj_Data.getString("notification_date"));
                                    grp_data.put("is_active", jobj_Data.getString("is_active"));
                                    grp_data.put("type", jobj_Data.getString("type"));
                                    sp.edit().putString("type", jobj_Data.getString("type")).apply();
                                    list_data.add(grp_data);
                                }
                                adapter.notifyDataSetChanged();

                            }//for
                            catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });




                    return result;
                } else {

//                    rl_news_list.setVisibility(View.GONE);
//                   //txt_message.setVisibility(View.VISIBLE);
//                    //txt_message.setText(jObj.getString("msg"));
//                    dialog.dismiss();
                    Log.e("Error message", "Login Failed");
                }
                Log.e("responseString",""+responseString);
                return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
            }

            public String parseToString(NetworkResponse response) {
                String parsed;
                try {
                    parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                } catch (UnsupportedEncodingException e) {
                    parsed = new String(response.data);
                }
                return parsed;
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }//getMessege



    private void getupdatecount(final String type) {

        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.setMessage(getString(R.string.str_requesting));
        dialog.setCancelable(false);
        dialog.show();
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        String requestURL = sp.getString("new_link","") + "update_student_notification_count";
        Log.e("requestURL", "URL" + requestURL);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", "code=" + response);
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response != null) {
                    if (response.equals("204")) {
                        // txt_message.setVisibility(View.VISIBLE);
                        // txt_message.setText("No Records Available");
                       // alertDialog1("No Records Available");
                        Log.e("Error message", "No Records Available");
                    } else {
//                        text_no_data.setVisibility(View.GONE);
//                        adapter = new CarCompanyAdapter(car_company, CarComapnyActivity.this);
//                        recyclerView_company.setAdapter(adapter);
//                        adapter.notifyDataSetChanged();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
              //  Toast.makeText(getActivity(), "Poor Internet Coonection", Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
//                type=ANNOUNCEMENT&student_id=73
                params.put("student_id", sp.getString("student_id", ""));
                params.put("type",type);


                Log.e("param", "=" + params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                    VolleyError error = new VolleyError(new String(volleyError.networkResponse.data));
                    volleyError = error;
                    dialog.dismiss();
                }
                //  Toast.makeText(getActivity(),"Poor Internet Coonection",Toast.LENGTH_SHORT).show();
                Log.e("parseNetworkError", "==" + volleyError);
                Log.e("parseNetworkError", "string==" + volleyError.toString());
                Log.e("parseNetworkError", "msg==" + volleyError.getMessage());
                return volleyError;
            }

            @Override
            protected Response<String> parseNetworkResponse(final NetworkResponse response) {
                String responseString = "";
                if (response != null) {
                    responseString = String.valueOf(response.statusCode);
                    // can get more details such as response.headers
                }
                Map<String, String> responseHeaders = response.headers;
                if (response.statusCode == 200) {
                    Response<String> result = Response.success(responseHeaders.get("Content-Type"), HttpHeaderParser.parseCacheHeaders(response));
                    Log.e("Response", "= count =" + parseToString(response));

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

//                            try {
//
//
//                                }
//                            }//for
//                            catch (JSONException e) {
//                                e.printStackTrace();
//                            }

                        }
                    });

                    return result;
                } else {


                    Log.e("Error message", "Login Failed");
                }
                Log.e("responseString", "" + responseString);
                return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
            }

            public String parseToString(NetworkResponse response) {
                String parsed;
                try {
                    parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                } catch (UnsupportedEncodingException e) {
                    parsed = new String(response.data);
                }
                return parsed;
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }//getcount


    private void alertDialog1(String error) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                getActivity());
        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setIcon(R.drawable.daffodils_logo);
        alertDialog.setMessage(error);
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton(getString(R.string.dlg_ok),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }


}//MessageFragment
