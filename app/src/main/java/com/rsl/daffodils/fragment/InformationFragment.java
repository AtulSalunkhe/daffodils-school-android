package com.rsl.daffodils.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.rsl.daffodils.R;
import com.rsl.daffodils.adapter.InformationExpandableListAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class InformationFragment extends Fragment {

    View view;
    ExpandableListView expListView;
    InformationExpandableListAdapter listAdapter;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;
    private int lastExpandedPosition = -1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_information, container, false);
        expListView=(ExpandableListView)view.findViewById(R.id.listView1);

        enableExpandableList();

        return view;
    }//onCreateView

    private void enableExpandableList() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();

        prepareListData(listDataHeader, listDataChild);
        listAdapter = new InformationExpandableListAdapter(getActivity(), listDataHeader, listDataChild);
        // setting list adapter
        expListView.setAdapter(listAdapter);

        expListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {

                return false;
            }
        });
        // Listview Group expanded listener
        expListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {

            }
        });

        // Listview Group collasped listener
        expListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {


            }
        });

        // Listview on child click listener
        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                // TODO Auto-generated method stub

                return false;
            }

        });
    }

    private void prepareListData(List<String> listDataHeader, Map<String,
            List<String>> listDataChild) {


        // Adding child data
        //listDataHeader.add(getResources().getString(R.id.str));
        listDataHeader.add(getString(R.string.str_about_school_welcome));
        listDataHeader.add(getString(R.string.str_about_school_infrastructure));
        listDataHeader.add(getString(R.string.str_about_school_activity));
        listDataHeader.add(getString(R.string.str_about_school_curricular_activity));
        listDataHeader.add(getString(R.string.str_about_school_facilities));
        listDataHeader.add(getString(R.string.str_about_school_vision));
        listDataHeader.add(getString(R.string.str_about_school_mission));
        listDataHeader.add(getString(R.string.str_about_school_develop_child));


        // Adding child data
        List<String> welcome = new ArrayList<String>();
        welcome.add(getString(R.string.str_about_school_welcome_message));

        List<String> infrastructure = new ArrayList<String>();
        infrastructure.add(getString(R.string.str_about_school_infrastructure_message));

        List<String> activity = new ArrayList<String>();
        activity.add(getString(R.string.str_about_school_activity_message));

        List<String> curricular = new ArrayList<String>();
        curricular.add(getString(R.string.str_about_school_curricular_activity_message));

        List<String> facilities = new ArrayList<String>();
        facilities.add(getString(R.string.str_about_school_facilities_message));

        List<String> vision = new ArrayList<String>();
        vision.add(getString(R.string.str_about_school_vision_message));

        List<String> mission = new ArrayList<String>();
        mission.add(getString(R.string.str_about_school_mission_message));

        List<String> develop_child = new ArrayList<String>();
        develop_child.add(getString(R.string.str_about_school_develop_child_message));



        // Header, Child data
        listDataChild.put(listDataHeader.get(0), welcome);
        listDataChild.put(listDataHeader.get(1), infrastructure);
        listDataChild.put(listDataHeader.get(2), activity);
        listDataChild.put(listDataHeader.get(3), curricular);
        listDataChild.put(listDataHeader.get(4), facilities);
        listDataChild.put(listDataHeader.get(5), vision);
        listDataChild.put(listDataHeader.get(6), mission);
        listDataChild.put(listDataHeader.get(7), develop_child);


    }



}//InformationFragment
