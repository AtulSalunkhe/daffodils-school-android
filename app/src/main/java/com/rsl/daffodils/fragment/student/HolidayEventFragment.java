package com.rsl.daffodils.fragment.student;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidListener;
import com.rsl.daffodils.R;
import com.rsl.daffodils.activity.EventDetailsActivity;
import com.rsl.daffodils.model.Constants;
import com.rsl.daffodils.model.Event;
import com.rsl.daffodils.utils.ConnectionDetector;
import com.rsl.daffodils.utils.StringUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;


public class HolidayEventFragment extends Fragment {


    View view;
    private CaldroidFragment caldroidFragment;
    ListArrayAdapter listArrayAdapter;
    ListView listView;
    ArrayList<Event> eventItems;
    int currentMonth, currentYear;
    ConnectionDetector internet;

    LinearLayout linear_date,linear_event;

    SharedPreferences sp;
    SharedPreferences.Editor editor;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_holiday_event, container, false);
        sp = getActivity().getSharedPreferences("daffodils", 0);
        editor = sp.edit();
        editor.apply();
        Constants.activity_calender = true;
        linear_date=(LinearLayout)view.findViewById(R.id.linear_date);
        linear_event=(LinearLayout)view.findViewById(R.id.linear_event);
        internet = new ConnectionDetector(getActivity());
        eventItems = new ArrayList<>();
        ArrayList<Event> items = new ArrayList<Event>();
        listArrayAdapter = new ListArrayAdapter(getActivity(), R.layout.list_view_event_item, items);
        listView = (ListView) view.findViewById(R.id.listView1);
        listView.setAdapter(listArrayAdapter);
        caldroidFragment = new CaldroidFragment();
        // If Activity is created after rotation
        if (savedInstanceState != null) {
            caldroidFragment.restoreStatesFromKey(savedInstanceState,
                    "CALDROID_SAVED_STATE");
        }

        // If activity is created from fresh
        else {
            Bundle args = new Bundle();
            Calendar cal = Calendar.getInstance();
            args.putInt(CaldroidFragment.MONTH, cal.get(Calendar.MONTH) + 1);
            args.putInt(CaldroidFragment.YEAR, cal.get(Calendar.YEAR));
            args.putBoolean(CaldroidFragment.ENABLE_SWIPE, true);
            args.putBoolean(CaldroidFragment.SIX_WEEKS_IN_CALENDAR, true);
            caldroidFragment.setArguments(args);
        }

        //setCustomResourceForDates();
        // Attach to the activity
        FragmentTransaction t = getFragmentManager().beginTransaction();
        t.replace(R.id.calendar, caldroidFragment);
        t.commit();

        // Setup listener
        final CaldroidListener listener = new CaldroidListener() {

            @Override
            public void onSelectDate(Date date, View view) {
                Calendar cal = Calendar.getInstance();
                cal.setTime(date);

                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);
                int year = cal.get(Calendar.YEAR);
                Log.e("month/year", month + "/" + year);
                Log.e("date", "==========" + date.toString());
                showEventsForDate(date);
            }

            @Override
            public void onChangeMonth(int month, int year) {
                currentMonth = month;
                currentYear = year;
//                loadLocalData(currentMonth, currentYear);
                if (internet.isConnectingToInternet()) {
                    Log.e("class id", "" + sp.getString("class_id", ""));
                    loadEventsData(sp.getString("class_id", ""));
                }else {
                    internet.showAlertDialog(getActivity(), false);
                }
                if (listArrayAdapter != null) {
                    listArrayAdapter.clear();
                    listArrayAdapter.notifyDataSetChanged();
                }
            }
        };
        // Setup Caldroid
        caldroidFragment.setCaldroidListener(listener);

        caldroidFragment.setBackgroundResourceForDate(R.drawable.date_current_date, Calendar.getInstance(Locale.getDefault()).getTime());
//        if (sp.getString("role","").equals("student")) {
//            updatecount("EVENT");
//        } else if (sp.getString("role","").equals("teacher")) {
//            updateTeacherCount("EVENT");
//        }else {
//            Log.e("Other,","");
//        }



        return view;
    }//onCreateView


    private void loadLocalData(int month, int year ) {
        List<Event> eventList = getEventsForMonth(eventItems, year, month);

        Log.d("EventsFragments", ""+eventList.size());

        Calendar currentCalender = Calendar.getInstance(Locale.getDefault());

        // eventItems = new ArrayList<>();
        for (Event event : eventList) {
            String dateTime = event.getDate();
            String[] dateTimes = dateTime.split(" ");
            Log.e("dateTime", "=====" + dateTimes[0]);//07-09-2017

            String[] separated = dateTimes[0].split("-");
            currentCalender.set(Integer.parseInt(separated[0]), Integer.parseInt(separated[1])-1, Integer.parseInt(separated[2]));

            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
            Calendar calendar = Calendar.getInstance();
            Date today = calendar.getTime();

            String todayAsString = dateFormat.format(today);
            Log.e("dateTimes[0]", dateTimes[0]);
            Log.e("setBackgroundResource","before");
            Log.e("todayAsString", todayAsString);
            Log.e("setBackgroundResource","after");
            if (!(dateTimes[0].equals(todayAsString))) {
                caldroidFragment.setBackgroundResourceForDate(R.drawable.holiday_background, currentCalender.getTime());
                Log.e("setBackgroundResource","true");
            } else {
                caldroidFragment.setBackgroundResourceForDate(R.drawable.date_current_date, currentCalender.getTime());
                Log.e("setBackgroundResource","true");

            }
            // -- FOR LIST VIEW --
            //String[] dateSplit = event.date.split(" 00:00:00");
//            Event item = new Event(event.getEvent_id(), event.getTitle(), event.getDescription(), event.getDate(), event.getEvent_images());

            //eventItems.add(event);
            //listArrayAdapter.add(item);
        }
        caldroidFragment.refreshView();
    }

    private List<Event> getEventsForMonth(List<Event> models, int year, int month) {
        final List<Event> filteredModelList = new ArrayList<>();
        for (Event model : models) {
            final int m = model.getMonth();
            final int y = model.getYear();
            if (m == month && y == year) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }

    private void showEventsForDate(Date date)  {
        try {
            listArrayAdapter.clear();
            Integer i;
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
            SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

            for (i = 0; i < eventItems.size(); i++) {
                Event item = eventItems.get(i);
                String eventDate = item.getDate().split(" ")[0];
                //dateFormat1.parse(eventDate);
                Log.e(dateFormat.format(date), eventDate);
                if (!dateFormat.format(date).equalsIgnoreCase(dateFormat.format(dateFormat1.parse(eventDate)))) {
                    Log.e("setTitle", "===" + item.title);

                } else {
                    listArrayAdapter.add(item);
                    Log.e("setTitle", "else");
                }
            }

            Log.e("listArrayAdapter  size", "="+listArrayAdapter.getCount());
            if (listArrayAdapter.getCount() > 0) {
                Log.e("listArrayAdapter", "if");
                linear_date.setVisibility(View.GONE);
                linear_event.setVisibility(View.GONE);
            } else {
                Log.e("listArrayAdapter", "else");
                linear_date.setVisibility(View.VISIBLE);
                linear_event.setVisibility(View.VISIBLE);
            }

            listArrayAdapter.notifyDataSetChanged();
            //listArrayAdapter.notifyDataSetChanged();
        }catch (ParseException e){
            e.printStackTrace();
        }
    }

    public class ListArrayAdapter extends ArrayAdapter<Event> {

        Context mContext;
        int layoutResourceId;
        //EventItem data[] = null;
        ArrayList<Event> data = null;

        public ListArrayAdapter(Context mContext, int layoutResourceId, ArrayList<Event> data) {

            super(mContext, layoutResourceId, data);

            this.layoutResourceId = layoutResourceId;
            this.mContext = mContext;
            this.data = data;
        }


        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
        /*
         * The convertView argument is essentially a "ScrapView" as described is Lucas post
         * http://lucasr.org/2012/04/05/performance-tips-for-androids-listview/
         * It will have a non-null value when ListView is asking you recycle the row layout.
         * So, when convertView is not null, you should simply update its contents instead of inflating a new row layout.
         */
            View row = convertView;
            LayoutInflater inflater = LayoutInflater.from(getActivity());
            if (row == null) {
                row = inflater.inflate(R.layout.row_event_data, null);
            }
            final Event item = data.get(position);

            TextView txt_item = (TextView) row.findViewById(R.id.txt_item);
            TextView txt_data = (TextView) row.findViewById(R.id.txt_data);
            TextView txt_date = (TextView) row.findViewById(R.id.txt_date);
            ImageView image = (ImageView) row.findViewById(R.id.bg_img);

            String path=sp.getString("new_event","");

            Glide.with(getActivity()).load(path+item.getEvent_image()).dontAnimate().placeholder(R.drawable.app_logo).into(image);
            txt_item.setText(StringUtils.trimTrailingWhitespace(Html.fromHtml(item.getTitle())));
            txt_data.setText(StringUtils.trimTrailingWhitespace(Html.fromHtml(item.getDescription())));
            txt_date.setText(StringUtils.trimTrailingWhitespace(Html.fromHtml(item.getDate())));


            row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(getActivity(), EventDetailsActivity.class);
                    i.putExtra("tag", "HOLIDAY DETAILS");
                    i.putExtra("description", item.getDescription());
                    i.putExtra("title", item.getTitle());
                    i.putExtra("img_url", item.getEvent_image());
                    i.putExtra("event_date", item.getDate());
                    startActivity(i);
                }
            });
            return row;
        }
    }


    private void loadEventsData(final String classid) {

        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.setCancelable(false);
        dialog.show();
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        String requestURL = sp.getString("new_link","") +"get_holidays/"+classid;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                Log.e("response", "code=" + response);
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response != null) {
                    if (response.equals("204")) {
                        // text_no_data.setVisibility(View.VISIBLE);
                       //  text_no_data.setText("No Records Available");
                       // alertDialog1("No Records Available");
                        Log.e("Error message", "No Records Available");

                    } else {

                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                Toast.makeText(getActivity(), "Poor Internet Coonection", Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                Log.e("param", "carcompanies=" + params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                    VolleyError error = new VolleyError(new String(volleyError.networkResponse.data));
                    volleyError = error;
                    dialog.dismiss();
                }
//                Toast.makeText(getActivity(), "Poor Internet Coonection", Toast.LENGTH_SHORT).show();
                Log.e("parseNetworkError", "==" + volleyError);
                Log.e("parseNetworkError", "string==" + volleyError.toString());
                Log.e("parseNetworkError", "msg==" + volleyError.getMessage());
                return volleyError;
            }

            @Override
            protected Response<String> parseNetworkResponse(final NetworkResponse response) {
                String responseString = "";
                if (response != null) {
                    responseString = String.valueOf(response.statusCode);
                    // can get more details such as response.headers
                }
                Map<String, String> responseHeaders = response.headers;
                if (response.statusCode == 200) {
                    Response<String> result = Response.success(responseHeaders.get("Content-Type"), HttpHeaderParser.parseCacheHeaders(response));
                    Log.e("Response_hoilidays", "==" + parseToString(response));
                    eventItems.clear();
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            try {
                                JSONArray JArray = new JSONArray(parseToString(response));


                                if (JArray.length() > 0) {
                                    Log.e("JArray","JArray");
                                    Integer i;
                                    for (i = 0; i < JArray.length(); i++) {
                                        Log.e("for","JArray");
                                        JSONObject obj = JArray.getJSONObject(i);
                                        String dateStr = obj.getString("event_date");//15-03-2015 15:03:00"
                                        String[] dateTime = dateStr.split(" ");

                                        String[] separated = dateTime[0].split("-");
                                        Event event = new Event();
                                        event.setEvent_id(obj.getString("event_id"));
                                        event.setDate(obj.getString("event_date"));
//                                                event.setTime(obj.getString("time"));
                                        event.setDescription(obj.getString("event_text"));
                                        event.setTitle(obj.getString("event_title"));
                                        event.setEvent_image(obj.getString("event_image"));
                                        event.setDay(Integer.parseInt(separated[2]));
                                        event.setMonth(Integer.parseInt(separated[1]));
                                        event.setYear(Integer.parseInt(separated[0]));
                                        eventItems.add(event);
                                        Log.e("eventItems",""+eventItems.size());

                                    }
                                    loadLocalData(currentMonth, currentYear);
                                    showEventsForDate(new Date());
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });

                    return result;
                } else {

                    Log.e("Error message", "Login Failed");
                }
                Log.e("responseString", "" + responseString);
                return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
            }

            public String parseToString(NetworkResponse response) {
                String parsed;
                try {
                    parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                } catch (UnsupportedEncodingException e) {
                    parsed = new String(response.data);
                }
                return parsed;
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }//loadEventsData

    private void alertDialog1(String error) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                getActivity());
        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setIcon(R.drawable.daffodils_logo);
        alertDialog.setMessage(error);
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton(getString(R.string.dlg_ok),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }


}//HolidayEventFragment
