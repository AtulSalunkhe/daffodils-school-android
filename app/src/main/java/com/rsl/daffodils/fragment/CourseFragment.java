package com.rsl.daffodils.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.rsl.daffodils.R;
import com.rsl.daffodils.adapter.InformationExpandableListAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class CourseFragment extends Fragment {

    View view;
    ExpandableListView expListView;
    InformationExpandableListAdapter listAdapter;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;
    private int lastExpandedPosition = -1;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_course, container, false);

        expListView=(ExpandableListView)view.findViewById(R.id.listView1);

        enableExpandableList();

        return view;
    }//onCreateView


    private void enableExpandableList() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();

        prepareListData(listDataHeader, listDataChild);
        listAdapter = new InformationExpandableListAdapter(getActivity(), listDataHeader, listDataChild);
        // setting list adapter
        expListView.setAdapter(listAdapter);

        expListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {

                return false;
            }
        });
        // Listview Group expanded listener
        expListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {

            }
        });

        // Listview Group collasped listener
        expListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {


            }
        });

        // Listview on child click listener
        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                // TODO Auto-generated method stub

                return false;
            }

        });
    }

    private void prepareListData(List<String> listDataHeader, Map<String,
            List<String>> listDataChild) {


        // Adding child data
        listDataHeader.add(getString(R.string.str_daycare));
        listDataHeader.add(getString(R.string.str_course_playgroup));
        listDataHeader.add(getString(R.string.str_course_nursery));
        listDataHeader.add(getString(R.string.str_course_lkg));
        listDataHeader.add(getString(R.string.str_course_ukg));
        listDataHeader.add(getString(R.string.str_1st));
        listDataHeader.add(getString(R.string.str_2sd));
        listDataHeader.add(getString(R.string.str_3th));
        listDataHeader.add(getString(R.string.str_4th));
        listDataHeader.add(getString(R.string.str_abacus));
        listDataHeader.add(getString(R.string.str_activity_club));



        // Adding child data

        List<String> daycare = new ArrayList<String>();
        daycare.add(getString(R.string.str_course_daycare_message));

        List<String> playgroup = new ArrayList<String>();
        playgroup.add(getString(R.string.str_course_playgroup_message));

        List<String> nursery = new ArrayList<String>();
        nursery.add(getString(R.string.str_course_nursery_message));

        List<String> lkg = new ArrayList<String>();
        lkg.add(getString(R.string.str_course_lkg_message));

        List<String> ukg = new ArrayList<String>();
        ukg.add(getString(R.string.str_course_ukg_message));

        List<String> first = new ArrayList<String>();
        first.add(getString(R.string.str_course_1st_message));

        List<String> second = new ArrayList<String>();
        second.add(getString(R.string.str_course_2sd_message));

        List<String> thired = new ArrayList<String>();
        thired.add(getString(R.string.str_course_3th_message));

        List<String> four = new ArrayList<String>();
        four.add(getString(R.string.str_course_4th_message));

        List<String> abacus = new ArrayList<String>();
        abacus.add(getString(R.string.str_course_abacus_message));

        List<String> activity_club = new ArrayList<String>();
        activity_club.add(getString(R.string.str_course_activity_message));

        // Header, Child data
        listDataChild.put(listDataHeader.get(0), daycare);
        listDataChild.put(listDataHeader.get(1), playgroup);
        listDataChild.put(listDataHeader.get(2), nursery);
        listDataChild.put(listDataHeader.get(3), lkg);
        listDataChild.put(listDataHeader.get(4), ukg);
        listDataChild.put(listDataHeader.get(5), first);
        listDataChild.put(listDataHeader.get(6), second);
        listDataChild.put(listDataHeader.get(7), thired);
        listDataChild.put(listDataHeader.get(8), four);
        listDataChild.put(listDataHeader.get(9), abacus);
        listDataChild.put(listDataHeader.get(10), activity_club);


    }

}//CourseFragment
