package com.rsl.daffodils.utils;// http://pastebin.com/KMcwGrS8

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.GridView;

public class ScrollableGridView extends GridView {
    boolean expanded = true;
    Context context;

    public ScrollableGridView(Context context)
    {
        super(context);
    }

    public ScrollableGridView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public ScrollableGridView(Context context, AttributeSet attrs,
                              int defStyle)
    {
        super(context, attrs, defStyle);
    }

    public boolean isExpanded()
    {
        return expanded;
    }


    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        // HACK! TAKE THAT ANDROID!
        if (isExpanded()) {
            // Calculate entire height by providing a very large height hint.
            // But do not use the highest 2 bits of this integer; those are
            // reserved for the MeasureSpec mode.
            int expandSpec = MeasureSpec.makeMeasureSpec(
                    Integer.MAX_VALUE >> 2, MeasureSpec.AT_MOST);

            super.onMeasure(widthMeasureSpec, expandSpec);

            ViewGroup.LayoutParams params = getLayoutParams();
            params.height = getMeasuredHeight();

        } else {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }
//        int heightSpec;
//
//        if (getLayoutParams().height == LayoutParams.MATCH_PARENT) {
//            // The great Android "hackatlon", the love, the magic.
//            // The two leftmost bits in the height measure spec have
//            // a special meaning, hence we can't use them to describe height.
//            heightSpec = MeasureSpec.makeMeasureSpec(
//                    Integer.MAX_VALUE >> 2, MeasureSpec.AT_MOST);
//        }
//        else {
//            // Any other height should be respected as is.
//            heightSpec = heightMeasureSpec;
//        }
//
//        super.onMeasure(widthMeasureSpec, heightSpec);
    }


    public void setExpanded(boolean expanded)
    {
        this.expanded = expanded;
    }
}