package com.rsl.daffodils.activity.student;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidListener;
import com.rsl.daffodils.R;

import com.rsl.daffodils.model.Event;
import com.rsl.daffodils.utils.ConnectionDetector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class AttendenceActivity extends AppCompatActivity {

    private CaldroidFragment caldroidFragment;
    int currentMonth, currentYear;
    ConnectionDetector internet;
    LinearLayout linear_date, linear_event;
    ArrayList<Event> eventItems;
    SharedPreferences sp;
    SharedPreferences.Editor editor;
    String current_year;
    ArrayList<String> list_data;
    TextView text_no_data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.attendance);
        text_no_data = (TextView) findViewById(R.id.text_no_data);
        linear_date = (LinearLayout) findViewById(R.id.linear_date);
        linear_event = (LinearLayout) findViewById(R.id.linear_event);
        initView();
        sp = getSharedPreferences("daffodils", 0);
        editor = sp.edit();
        editor.apply();
        eventItems = new ArrayList<>();
        list_data = new ArrayList<>();
        internet = new ConnectionDetector(AttendenceActivity.this);
        caldroidFragment = new CaldroidFragment();
        // If Activity is created after rotation
        if (savedInstanceState != null) {
            caldroidFragment.restoreStatesFromKey(savedInstanceState,
                    "CALDROID_SAVED_STATE");
            //  caldroidFragment.setStyle(R.style.CaldroidDefaultDark);
        }

        // If activity is created from fresh
        else {
            Bundle args = new Bundle();
            Calendar cal = Calendar.getInstance();
            args.putInt(CaldroidFragment.MONTH, cal.get(Calendar.MONTH) + 1);
            args.putInt(CaldroidFragment.YEAR, cal.get(Calendar.YEAR));
            args.putBoolean(CaldroidFragment.ENABLE_SWIPE, true);
            args.putBoolean(CaldroidFragment.SIX_WEEKS_IN_CALENDAR, true);
            //   args.putInt(CaldroidFragment.THEME_RESOURCE, com.caldroid.R.style.CaldroidDefaultDark);
            caldroidFragment.setArguments(args);
        }

        //setCustomResourceForDates();
        // Attach to the activity
        FragmentTransaction t = getSupportFragmentManager().beginTransaction();
        t.replace(R.id.calendar1, caldroidFragment);
        t.commit();

        // Setup listener
        final CaldroidListener listener = new CaldroidListener() {

            @Override
            public void onSelectDate(Date date, View view) {
                Calendar cal = Calendar.getInstance();
                cal.setTime(date);

                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);
                int year = cal.get(Calendar.YEAR);
                Log.e("month/year", month + "/" + year);
                Log.e("date", "==========" + date.toString());
                //  showEventsForDate(date);
            }

            @Override
            public void onChangeMonth(int month, int year) {
                currentMonth = month;
                currentYear = year;
                loadLocalData(currentMonth, currentYear);
                current_year = String.valueOf(currentYear);
                Log.e("current_year", "======" + current_year);
                if (internet.isConnectingToInternet())
                    loadEventsData();
                else {
                    internet.showAlertDialog(AttendenceActivity.this, false);
                }

            }
        };
        // Setup Caldroid
        caldroidFragment.setCaldroidListener(listener);
        caldroidFragment.setBackgroundResourceForDate(R.drawable.date_current_date, Calendar.getInstance(Locale.getDefault()).getTime());


    }//onCreate


    private void initView() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.str_stud_attendence));
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


    private void loadLocalData(int month, int year) {


        List<Event> eventList = getEventsForMonth(eventItems, year, month);
        countWeekendDays(year, month);
        month--;
        Log.e("loadLocalData", month + "/" + year);
        Log.d("EventsFragments", "" + eventList.size());

        Calendar currentCalender = Calendar.getInstance(Locale.getDefault());
        Calendar currentCalender1 = Calendar.getInstance(Locale.getDefault());


        DateFormat timeformat = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.YEAR, year);

        cal.set(Calendar.HOUR_OF_DAY, 0); // ! clear would not reset the hour of day !
        cal.clear(Calendar.MINUTE);
        cal.clear(Calendar.SECOND);
        cal.clear(Calendar.MILLISECOND);
        cal.set(Calendar.DAY_OF_MONTH, 1);

        Calendar mycal = Calendar.getInstance();
        // Get the number of days in that month
        mycal.set(Calendar.MONTH, month);
        mycal.set(Calendar.YEAR, year);
        int daysInMonth = mycal.getActualMaximum(Calendar.DAY_OF_MONTH); // 28
        mycal.set(Calendar.DAY_OF_MONTH, daysInMonth);
        Log.e("daysInMonth", "=" + daysInMonth);

        String first_date = timeformat.format(cal.getTime());
        String last_date = timeformat.format(mycal.getTime());

        Log.e("first_date", "=" + timeformat.format(cal.getTime()));
        Log.e("last_date", "=" + timeformat.format(mycal.getTime()));

        List<Date> dates = getDates(first_date, last_date);

        for (Date calenderdate : dates) {
            for (Event event : eventList) {
                String dateTime = event.getDate();
                String[] dateTimes = dateTime.split(" ");
                Log.e("dateTime", "=====" + dateTimes[0]);//07-09-2017


                String[] separated = dateTimes[0].split("-");
                currentCalender.set(Integer.parseInt(separated[0]), Integer.parseInt(separated[1]) - 1, Integer.parseInt(separated[2]));

                //DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
                DateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                Calendar calendar = Calendar.getInstance();
                Date today = calendar.getTime();

                String todayAsString = dateFormat1.format(today);
                String[] dateTimes1 = todayAsString.split(" ");
                String[] separated1 = dateTimes1[0].split("-");

                currentCalender1.set(Integer.parseInt(separated1[0]), Integer.parseInt(separated1[1]) - 1, Integer.parseInt(separated1[2]));


                String formattedcalenderdate = dateFormat1.format(calenderdate);
                Log.e("absent date", dateTimes[0]);
                Log.e("calenderdate", formattedcalenderdate);
                Log.e("todayAsString", todayAsString);


                if (list_data.contains(formattedcalenderdate)) {
                    if ((formattedcalenderdate.equals(todayAsString)))
                        caldroidFragment.setBackgroundResourceForDate(R.drawable.currenrt_date_circle, calenderdate);
                    else
                        caldroidFragment.setBackgroundResourceForDate(R.drawable.sunday_background, calenderdate);
                } else {
                    if ((formattedcalenderdate.equals(todayAsString))) {
                        if (dateTimes[0].equals(todayAsString)) {

                            if (event.getPresent().equals("false")) {
                                caldroidFragment.setBackgroundResourceForDate(R.drawable.date_highlight_holiday, calenderdate);
                            } else
                                caldroidFragment.setBackgroundResourceForDate(R.drawable.default_date, calenderdate);
                            break;
                        } else {
                            caldroidFragment.setBackgroundResourceForDate(R.drawable.currenrt_date_circle, calenderdate);
                            Log.e("setBackgroundResource", "truerr");
                        }
                    } else {

                        if (formattedcalenderdate.equals(dateTimes[0])) {
                            Log.e("getPresent", "=====" + event.getPresent());//07-09-2017
//                            caldroidFragment.setBackgroundResourceForDate(R.drawable.date_highlight_holiday, calenderdate);
                            if (event.getPresent().equals("false")) {
                                caldroidFragment.setBackgroundResourceForDate(R.drawable.date_highlight_holiday, calenderdate);
                            } else
                                caldroidFragment.setBackgroundResourceForDate(R.drawable.default_date, calenderdate);
//                           break;
                            Log.e("setBackgroundResource", "true");
                            break;

                        } else {

                            // if (calenderdate.before(today))
                            //  caldroidFragment.setBackgroundResourceForDate(R.drawable.default_date, calenderdate);
                        }

                    }
                }

            }
        }
        //
        caldroidFragment.refreshView();
    }


    public int countWeekendDays(int year, int month) {


        Calendar calendar = Calendar.getInstance();
        // Note that month is 0-based in calendar, bizarrely.
        calendar.set(year, month - 1, 1);
        int daysInMonth = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        list_data.clear();
        int count = 0;
        for (int day = 1; day <= daysInMonth; day++) {
            calendar.set(year, month - 1, day);
            int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
            Log.e("week", "" + dayOfWeek);
            if (dayOfWeek == Calendar.SUNDAY || dayOfWeek == Calendar.SATURDAY) {

                calendar.getTime();
                Log.e("time", "" + calendar.getTime());
                DateFormat timeformat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                String date = timeformat.format(calendar.getTime());
                Log.e("dateformat", "====" + date);
                list_data.add(date);

                count++;
                // Or do whatever you need to with the result.
                //caldroidFragment.setBackgroundResourceForDate(R.drawable.holiday_background,);
            }
        }
        Log.e("arrylist", "***" + list_data);
        return count;
    }


    private List<Event> getEventsForMonth(List<Event> models, int year, int month) {
        final List<Event> filteredModelList = new ArrayList<>();
        for (Event model : models) {
            final int m = model.getMonth();
            final int y = model.getYear();
            if (m == month && y == year) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }

    private static List<Date> getDates(String dateString1, String dateString2) {
        ArrayList<Date> dates = new ArrayList<Date>();

        DateFormat df1 = new SimpleDateFormat("dd-MM-yyyy", Locale.US);

        Date date1 = null;
        Date date2 = null;

        try {
            date1 = df1.parse(dateString1);
            date2 = df1.parse(dateString2);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);


        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);

        while (!cal1.after(cal2)) {
            dates.add(cal1.getTime());
            cal1.add(Calendar.DATE, 1);
        }
        return dates;
    }

    private void showEventsForDate(Date date) {
        try {
            //  listArrayAdapter.clear();
            Integer i;
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
            SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

            for (i = 0; i < eventItems.size(); i++) {
                Event item = eventItems.get(i);
                String eventDate = item.getDate().split(" ")[0];
                //dateFormat1.parse(eventDate);
                Log.e(dateFormat.format(date), eventDate);
                if (!dateFormat.format(date).equalsIgnoreCase(dateFormat.format(dateFormat1.parse(eventDate)))) {
                    Log.e("setTitle", "===" + item.title);

                } else {
                    // listArrayAdapter.add(item);
                    Log.e("setTitle", "else");
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }


    private void loadEventsData() {
        final ProgressDialog dialog = new ProgressDialog(AttendenceActivity.this);
        dialog.setCancelable(false);
        dialog.setMessage(getResources().getString(R.string.str_requesting));
        dialog.show();
        RequestQueue queue = Volley.newRequestQueue(AttendenceActivity.this);
        String requestURL = sp.getString("new_link", "") + "get_student_attendance";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", "code=" + response);
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response != null) {
                    if (response.equals("204")) {
                        // text_no_data.setVisibility(View.VISIBLE);
                        // text_no_data.setText("No Records Available");
                        alertDialog1("No Records Available");
                        Log.e("Error message", "No Records Available");

                    } else {

                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                //  Toast.makeText(getActivity(), "Poor Internet Coonection", Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("student_id", sp.getString("student_id", ""));
                params.put("year", current_year);


                Log.e("param", "attendence=" + params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                    VolleyError error = new VolleyError(new String(volleyError.networkResponse.data));
                    volleyError = error;
                    dialog.dismiss();
                }
//                Toast.makeText(AttendenceActivity.this, "Poor Internet Coonection", Toast.LENGTH_SHORT).show();
                Log.e("parseNetworkError", "==" + volleyError);
                Log.e("parseNetworkError", "string==" + volleyError.toString());
                Log.e("parseNetworkError", "msg==" + volleyError.getMessage());
                return volleyError;
            }

            @Override
            protected Response<String> parseNetworkResponse(final NetworkResponse response) {
                String responseString = "";
                if (response != null) {
                    responseString = String.valueOf(response.statusCode);
                    // can get more details such as response.headers
                }
                Map<String, String> responseHeaders = response.headers;
                if (response.statusCode == 200) {
                    Response<String> result = Response.success(responseHeaders.get("Content-Type"), HttpHeaderParser.parseCacheHeaders(response));
                    Log.e("Response_attendence", "==" + parseToString(response));
                    eventItems.clear();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            try {
                                JSONArray JArray = new JSONArray(parseToString(response));


                                if (JArray.length() > 0) {
                                    for (int i = 0; i < JArray.length(); i++) {

                                        JSONObject obj = JArray.getJSONObject(i);
                                        String dateStr = obj.getString("attendance_date");//15-03-2015 15:03:00"

                                        String[] dateTime = dateStr.split(" ");

                                        String[] separated = dateTime[0].split("-");
                                        Event event = new Event();

                                        event.setDate(obj.getString("attendance_date"));
//                                                event.setTime(obj.getString("time"));
                                        event.setDay(Integer.parseInt(separated[2]));
                                        event.setMonth(Integer.parseInt(separated[1]));
                                        event.setYear(Integer.parseInt(separated[0]));
                                        event.setPresent(obj.getString("present"));
                                        eventItems.add(event);
                                        Log.e("eventItems", "" + eventItems.size());

                                    }
                                    loadLocalData(currentMonth, currentYear);
                                    showEventsForDate(new Date());
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });

                    return result;
                } else {

                    Log.e("Error message", "Login Failed");
                }
                Log.e("responseString", "" + responseString);
                return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
            }

            public String parseToString(NetworkResponse response) {
                String parsed;
                try {
                    parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                } catch (UnsupportedEncodingException e) {
                    parsed = new String(response.data);
                }
                return parsed;
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 4, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }//loadEventsData

    private void alertDialog1(String error) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                AttendenceActivity.this);
        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setIcon(R.drawable.daffodils_logo);
        alertDialog.setMessage(error);
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton(getString(R.string.dlg_ok),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent in = new Intent(AttendenceActivity.this, StudentsHomeActivity.class);
        startActivity(in);
        finish();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
