package com.rsl.daffodils.activity.admin;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.rsl.daffodils.R;

public class FeedbackDeatilsActivity extends AppCompatActivity {

    String feedback,email,teacher_name,student_name,subject;
    TextView text_feedback,text_email,text_subject,text_teacher_name,text_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback_deatils);
        initView();
        feedback= getIntent().getStringExtra("feedback");
        email = getIntent().getStringExtra("email");
        teacher_name= getIntent().getStringExtra("teacher_name");
        student_name = getIntent().getStringExtra("name");
        subject= getIntent().getStringExtra("subject");


        text_feedback.setText(feedback);
        text_email.setText(email);
        text_teacher_name.setText(teacher_name);
        text_name.setText(student_name);
        text_subject.setText(subject);




    }//onCreate



    private void initView() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.str_feedback_details);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        text_feedback=(TextView)findViewById(R.id.text_feedback);
        text_email=(TextView)findViewById(R.id.text_email);
        text_subject=(TextView)findViewById(R.id.text_subject);
        text_teacher_name=(TextView)findViewById(R.id.text_teacher);
        text_name=(TextView)findViewById(R.id.text_name);

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}//FeedbackDeatilsActivity
