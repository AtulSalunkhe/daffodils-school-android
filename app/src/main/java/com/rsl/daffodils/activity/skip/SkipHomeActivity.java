package com.rsl.daffodils.activity.skip;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.rsl.daffodils.R;
import com.rsl.daffodils.activity.HomeActivity;
import com.rsl.daffodils.utils.ConnectionDetector;
import com.rsl.daffodils.utils.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SkipHomeActivity extends AppCompatActivity {


    static final int send_data = 1;
    BottomNavigationView navigation;

    SharedPreferences sp;
    SharedPreferences.Editor editor;
    SessionManager session;

    TextView announcement_text;

    ConnectionDetector internet;
    LinearLayout linear_about, linear_experts, linear_topper, linear_announcement, linear_fourm;

    private SliderLayout mDemoSlider;
    TextView txt_message;
    ImageView img_slider;
    TextSliderView textSliderView;
    ArrayList<HashMap<String, String>> list_data;


    int[] imageId = {R.drawable.slide1, R.drawable.slide2, R.drawable.slide3, R.drawable.slide4, R.drawable.slide5,R.drawable.slide6};


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    return true;
                case R.id.navigation_contacts:
                    navigation.getMenu().getItem(1).setChecked(false);
                    Intent in = new Intent(SkipHomeActivity.this, StudentContactUsActivity.class);
                    startActivity(in);
                    finish();
                    return true;
                case R.id.navigation_share:

                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    //add a subject
                    sendIntent.putExtra(Intent.EXTRA_SUBJECT,
                            getResources().getString(R.string.app_name));
                    sendIntent.putExtra(Intent.EXTRA_TEXT,getString(R.string.link));
                    sendIntent.setType("text/plain");
                    startActivityForResult(sendIntent, send_data);

                    return false;
            }
            return false;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_skip_home);
        sp = getSharedPreferences("daffodils", 0);
        editor = sp.edit();
        editor.apply();
        list_data=new ArrayList<HashMap<String, String>>();
        session = new SessionManager(SkipHomeActivity.this);
        internet = new ConnectionDetector(SkipHomeActivity.this);
        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        announcement_text=(TextView)findViewById(R.id.announcement_text);
        if(sp.getString("role","").equals("teacher")){
            navigation.setVisibility(View.GONE);
            announcement_text.setText(getResources().getString(R.string.str_announcement));
        }
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigation.getMenu().getItem(0).setChecked(true);
        img_slider = (ImageView) findViewById(R.id.img_slider);
        mDemoSlider = (SliderLayout) findViewById(R.id.view_pager);
        txt_message = (TextView) findViewById(R.id.txt_message);

        textSliderView = new TextSliderView(SkipHomeActivity.this);

        if (internet.isConnectingToInternet()) {
            display_images();
        } else {
            internet.showAlertDialog(SkipHomeActivity.this, true);
        }
//        if (imageId.length > 1 && imageId != null) {
//            for (int j = 0; j < imageId.length; j++) {
//                textSliderView = new TextSliderView(SkipHomeActivity.this);
//
//                // initialize a SliderLayout
//                textSliderView.image(imageId[j]).setScaleType(BaseSliderView.ScaleType.Fit);
//                mDemoSlider.addSlider(textSliderView);
//            }
//        } else {
//            textSliderView = new TextSliderView(SkipHomeActivity.this);
//            textSliderView.image(R.drawable.app_logo).setScaleType(BaseSliderView.ScaleType.Fit);
//            mDemoSlider.addSlider(textSliderView);
//        }


        linear_about = (LinearLayout) findViewById(R.id.linear_about);
        linear_experts = (LinearLayout) findViewById(R.id.linear_experts);
        linear_topper = (LinearLayout) findViewById(R.id.linear_topper);
        linear_announcement = (LinearLayout) findViewById(R.id.linear_announcement);
        linear_fourm = (LinearLayout) findViewById(R.id.linear_fourm);
        // linear_contact=(LinearLayout)findViewById(R.id.linear_contact);



        linear_about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("click","clicking");
                Intent intent = new Intent(SkipHomeActivity.this, HomeActivity.class);
                startActivity(intent);
            }
        });

        linear_experts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SkipHomeActivity.this, FeaturesActivity.class);
                startActivity(intent);
            }
        });


        linear_announcement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SkipHomeActivity.this, AdvertisementActivity.class);
                startActivity(intent);
            }
        });

        linear_fourm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SkipHomeActivity.this, DisplayTeacherListActivity.class);
                startActivity(intent);
            }
        });

        linear_topper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                  Intent intent=new Intent(SkipHomeActivity.this, ToppersActivity.class);
                 startActivity(intent);
            }
        });


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ContextCompat.getColor(SkipHomeActivity.this, R.color.colorPrimaryDark));
        }
    }//on create



    private void display_images() {

        final ProgressDialog dialog = new ProgressDialog(SkipHomeActivity.this);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.str_requesting));
        dialog.show();
        RequestQueue queue = Volley.newRequestQueue(SkipHomeActivity.this);
        String requestURL = sp.getString("new_link","") + "get_home_imgaes";
        Log.e("requestURL", "images" + requestURL);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", "code=" + response);
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response != null) {
                    if (response.equals("204")) {
                        // text_msg.setVisibility(View.VISIBLE);
                        //  text_msg.setText("No Records Available");
                        alertDialog1("No image available");
                        Log.e("Error message", "No Records Available");
                    } else {
//                        text_no_data.setVisibility(View.GONE);
//                        adapter = new CarCompanyAdapter(car_company, CarComapnyActivity.this);
//                        recyclerView_company.setAdapter(adapter);
//                        adapter.notifyDataSetChanged();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                //Toast.makeText(getApplicationContext(), "Poor Internet Coonection", Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                // params.put("id",id);
                Log.e("param", "" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                    VolleyError error = new VolleyError(new String(volleyError.networkResponse.data));
                    volleyError = error;
                    dialog.dismiss();
                }
                // Toast.makeText(getApplicationContext(),"Poor Internet Coonection",Toast.LENGTH_SHORT).show();
                Log.e("parseNetworkError", "==" + volleyError);
                Log.e("parseNetworkError", "string==" + volleyError.toString());
                Log.e("parseNetworkError", "msg==" + volleyError.getMessage());
                return volleyError;
            }

            @Override
            protected Response<String> parseNetworkResponse(final NetworkResponse response) {
                String responseString = "";
                if (response != null) {
                    responseString = String.valueOf(response.statusCode);
                    // can get more details such as response.headers
                }
                Map<String, String> responseHeaders = response.headers;
                if (response.statusCode == 200) {
                    Response<String> result = Response.success(responseHeaders.get("Content-Type"), HttpHeaderParser.parseCacheHeaders(response));
                    Log.e("Response_image", "" + parseToString(response));

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                JSONArray JArray = new JSONArray(parseToString(response));

                                for (int i = 0; i < JArray.length(); i++) {
                                    JSONObject jobj_Data = JArray.getJSONObject(i);

                                    HashMap<String, String> offers = new HashMap<String, String>();
                                    offers.put("img_path",jobj_Data.getString("img_path"));
                                    list_data.add(offers);

                                }//for
                                for (int i = 0; i <list_data.size(); i++) {
                                    TextSliderView textSliderView = new TextSliderView(SkipHomeActivity.this);
                                    // initialize a SliderLayout
                                    textSliderView
                                            .image(list_data.get(i).get("img_path"))
                                            .setScaleType(BaseSliderView.ScaleType.Fit);
                                    mDemoSlider.addSlider(textSliderView);
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });

                    return result;
                } else {


                    Log.e("Error message", "Login Failed");
                }
                Log.e("responseString", "" + responseString);
                return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
            }

            public String parseToString(NetworkResponse response) {
                String parsed;
                try {
                    parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                } catch (UnsupportedEncodingException e) {
                    parsed = new String(response.data);
                }
                return parsed;
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 4, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }//display_image


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
//        ContactUsActivity.activity.finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void alertDialog1(String error) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                SkipHomeActivity.this);
        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setIcon(R.drawable.app_logo_main);
        alertDialog.setMessage(error);
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton(getString(R.string.dlg_ok),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }


}
