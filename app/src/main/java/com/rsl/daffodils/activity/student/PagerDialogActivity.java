package com.rsl.daffodils.activity.student;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import com.rsl.daffodils.R;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.security.auth.login.LoginException;

public class PagerDialogActivity extends Activity {
    ImagePagerAdapter imagePagerAdapter;

   public static ProgressBar progressBar;
    ArrayList<HashMap<String, String>> list_data = null;
    ImageButton download, Share, closedialog1;
    Bitmap anImage;
    public static ImageView imagemain;
    private int REQUEST_READ_SERVICE = 11;
    private int REQUEST_WRITE_SERVICE = 22;
    File file;

    SharedPreferences sp;
    SharedPreferences.Editor editor;
    String dirPath, fileName;
    String imagepath = "";
    String imagetodown = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //imagemain.getIntent().getStringExtra("pre_wedding_images");

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //this.requestWindowFeature(Window.FEATURE_NO_TITLE);

//Remove notification bar
        //this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_pager_dialog);
        sp = getSharedPreferences("daffodils", 0);
        editor = sp.edit();
        int pos = Integer.parseInt(getIntent().getExtras().getString("position"));
        Log.e("posPagerDialogActivity",""+pos);
        list_data = GalleryPhotoView.list_data;
        imagePagerAdapter = new ImagePagerAdapter(this);
        imagePagerAdapter.notifyDataSetChanged();

        ViewPager pager = (ViewPager) findViewById(R.id.viewpager);

        pager.setAdapter(imagePagerAdapter);

        pager.setCurrentItem(pos);
        // getphotos();
        //view.show();

        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                Log.e("onPageScrolled ", "" + position);
                imagetodown = list_data.get(position).get("image");

            }

            @Override
            public void onPageSelected(int position) {
                Log.e("onPageSelected ", "" + position);
//
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }


        });
        download = (ImageButton) findViewById(R.id.download);
        Share = (ImageButton) findViewById(R.id.Share);
        closedialog1 = (ImageButton) findViewById(R.id.closedialog);

        Share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isReadStorageAllowed()) {
                    //If permission is already having then showing the toast
                    //Toast.makeText(PagerDialogActivity.this, "You already have the permission", Toast.LENGTH_LONG).show();
                    //Existing the method with return
                    new GetImageInPhone2(sp.getString("new_gallerylink","")+ imagetodown).execute();
                    return;
                }

                //If the app has not the permission then asking for the permission
                requestStoragePermission();


            }
        });



            download.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (isReadStorageAllowed()) {
                        //If permission is already having then showing the toast
                        //Toast.makeText(PagerDialogActivity.this, "You already have the permission", Toast.LENGTH_LONG).show();
                        //Existing the method with return
                        Log.e("imagepath ", "=======" + imagetodown);
                        Log.e("imagepathsp ", "******" + sp.getString("new_gallerylink",""));
                        new GetImageInPhone(sp.getString("new_gallerylink","")+imagetodown).execute();
                        return;
                    }

                    //If the app has not the permission then asking for the permission
                    requestStoragePermission();


                }

            });

        closedialog1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("", "This is close dialog");
                finish();
            }
        });


    }


    public class ImagePagerAdapter extends PagerAdapter {

        private Context mContext;
        LayoutInflater vinflater;


        public ImagePagerAdapter(Context context) {
            mContext = context;
            vinflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

//        public void add(HashMap<String, String> hash) {
//
//            list_data.add(hash);
//            notifyDataSetChanged();
//        }

        @Override
        public int getCount() {

            return list_data.size();
            // Log.e("size",""+String.valueOf(list_data.size()));
        }

        @Override
        public Object instantiateItem(ViewGroup container, final int position) {


            View viewItem = (View) vinflater.inflate(R.layout.pagerrowimage, container, false);
            imagemain = (ImageView) viewItem.findViewById(R.id.member_picture2);


            int listsize = list_data.size();
            Log.e("The size is", String.valueOf(listsize));
            Log.e("The data is", list_data.toString());
            Log.e("position", String.valueOf(position));

            imagepath = sp.getString("new_gallerylink","")+ list_data.get(position).get("image");
            Log.e("imagepathtttttt","((()))))))"+imagepath);

            final String str_imagepath = sp.getString("new_gallerylink","")+list_data.get(position).get("image");

            Log.e("image", "" + str_imagepath);

            //Glide.with(getApplicationContext()).load(str_imagepath).asBitmap().placeholder(R.drawable.placeholder).dontAnimate().into(imagemain);


//            if(null!=imagemain.getDrawable())
//            {
                download.setClickable(false);
                //imageview have image
                progressBar = (ProgressBar) findViewById(R.id.progressBar);
                progressBar.setVisibility(View.VISIBLE);
                Glide.with(PagerDialogActivity.this)

                        .load(str_imagepath)
                        .dontAnimate()
                        .listener(new RequestListener<String, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                progressBar.setVisibility(View.GONE);
                                download.setClickable(true );
                                return false;
                            }
                        })
                        .into(imagemain);
//            else{
//                //imageview have no image
//                download.setClickable(false);
//                Share.setClickable(false);
//            }

            ;
            container.addView(viewItem);


            return viewItem;
        }


        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((LinearLayout) object);
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }


        private void SaveImage(Bitmap finalBitmap) {

            String root = Environment.getExternalStorageDirectory().toString();
            File myDir = new File(root + "/saved_images");
            myDir.mkdirs();
            Random generator = new Random();
            int n = 10000;
            n = generator.nextInt(n);
            String fname = "Image-" + n + ".jpg";
            File file = new File(myDir, fname);
            if (file.exists()) file.delete();
            try {
                FileOutputStream out = new FileOutputStream(file);
                finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
                out.flush();
                out.close();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }

    class GetImageInPhone extends AsyncTask<String, Void, String> {
        String temp_image_path = "";
        String result = "";
        final ProgressDialog dialog = new ProgressDialog(PagerDialogActivity.this);
        Bitmap bmap = imagemain.getDrawingCache();
        public GetImageInPhone(String profile_image) {
            temp_image_path = profile_image;
            Log.e("path","&&&&&"+temp_image_path);
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            dialog.setCancelable(false);
            dialog.setMessage("Downloading...");
            //dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();
            imagemain.buildDrawingCache();


        }

        @Override
        protected String doInBackground(String... params) {
            try {
                if (isReadStorageAllowed()) {
                    URL url = new URL(temp_image_path);
                    URLConnection ucon = url.openConnection();
                    InputStream is = ucon.getInputStream();

                    if (is != null) {

                        OutputStream fOut = null;
                        String filename = temp_image_path.substring(temp_image_path.lastIndexOf("/") + 1);
                        String PATH = Environment.getExternalStorageDirectory() + "/" + getString(R.string.app_name) + "/";
                        File folder = new File(PATH);
                        Log.e("folder====path", String.valueOf(folder));
                        //,\"GE_\"+ System.currentTimeMillis() +\".jpg"

                        if (!folder.exists()) {
                            folder.mkdir();//If there is no folder it will be created.
                            Log.e("folder====", "created");
                        }
                        File f = new File(PATH + filename);
                        f.createNewFile();
                        Log.e("ImageCreated", "created");


//                        fOut = new FileOutputStream(folder);
//                        bmap.compress(Bitmap.CompressFormat.JPEG, 100, fOut);

                        // File file = new File(fileName);
                        long startTime = System.currentTimeMillis();
                        BufferedInputStream bis = new BufferedInputStream(is);
                        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
                        //We create an array of bytes
                        byte[] data = new byte[50];
                        int current = 0;
                        while ((current = bis.read(data, 0, data.length)) != -1) {
                            buffer.write(data, 0, current);
                        }



                        FileOutputStream fos = new FileOutputStream(f);
                        fos.write(buffer.toByteArray());
                        fos.close();
//                        fOut.flush();
//                        fOut.close();

                        ContentValues values = new ContentValues();
                        values.put(MediaStore.Images.Media.TITLE, filename);
                        values.put(MediaStore.Images.Media.MIME_TYPE,"image/jpeg");
                        values.put("_data", f.getAbsolutePath());


                        ContentResolver cr = getContentResolver();
                        Log.e("ContentResolver", String.valueOf(cr));
                        cr.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

                        result="sucess";


                    }

                    requestStoragePermission();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }



            return result;
        }

        protected void onPostExecute(String result) {
            if (!result.equals("") && result != null) {
                try {


                    Toast.makeText(PagerDialogActivity.this, "Image Saved To Photos", Toast.LENGTH_LONG).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            dialog.dismiss();
        }
    }

    private boolean isReadStorageAllowed() {
        //Getting the permission status
        int result1 = ContextCompat.checkSelfPermission(PagerDialogActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE);
        int result2 = ContextCompat.checkSelfPermission(PagerDialogActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        //If permission is granted returning true
        if (result1 == PackageManager.PERMISSION_GRANTED && result2 == PackageManager.PERMISSION_GRANTED)
            return true;

        //If permission is not granted returning false
        return false;
    }

    //Requesting permission
    private void requestStoragePermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(PagerDialogActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) &&
                ActivityCompat.shouldShowRequestPermissionRationale(PagerDialogActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }

        //And finally ask for the permission
        ActivityCompat.requestPermissions(PagerDialogActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_READ_SERVICE);
        ActivityCompat.requestPermissions(PagerDialogActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_SERVICE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        //Checking the request code of our request
        if (requestCode == REQUEST_WRITE_SERVICE && requestCode == REQUEST_READ_SERVICE) {

            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//
//                //Displaying a toast
                Toast.makeText(this, "Permission granted now you can read the storage", Toast.LENGTH_LONG).show();
//                Intent callIntent = new Intent(Intent.ACTION_CALL);
//                callIntent.setData(Uri.parse("tel:"+heventcontactno.getText().toString()));
//                startActivity(callIntent);
            } else {
                //Displaying another toast if permission is not granted
                Toast.makeText(this, "Oops you just denied the permission", Toast.LENGTH_LONG).show();
            }
        }
    }






//    class GetImageInPhone2 extends AsyncTask<String, Void, String> {
//        String temp_image_path = "";
//        String result = "";
//        String filename = "";
//        final ProgressDialog dialog = new ProgressDialog(PagerDialogActivity.this);
//
//        public GetImageInPhone2(String profile_image) {
//            temp_image_path = profile_image;
//            Log.e("share","######"+temp_image_path);
//        }
//
//        @Override
//        protected void onPreExecute() {
//            // TODO Auto-generated method stub
//            dialog.setCancelable(false);
//            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//            dialog.show();
//
//        }
//
//        @Override
//        protected String doInBackground(String... params) {
//            try {
//                if (isReadStorageAllowed()) {
//                    URL url = new URL(temp_image_path);
//                    URLConnection ucon = url.openConnection();
//                    InputStream is = ucon.getInputStream();
//                    if (is != null) {
//                        filename = temp_image_path.substring(temp_image_path.lastIndexOf("/") + 1);
//                        String PATH = Environment.getExternalStorageDirectory() + "/" + "daffodils" + "/";
//                        Log.e("Downloadsource====", PATH);
//                        File folder = new File(PATH);
//
//                        if (!folder.exists()) {
//                            folder.mkdir();//If there is no folder it will be created.
//                            Log.e("folder====", "created");
//                        }
//                        File f = new File(PATH + filename);
//                        f.createNewFile();
//                        Log.e("ImageCreated", "created");
//
//
//                        // File file = new File(fileName);
//                        long startTime = System.currentTimeMillis();
//                        BufferedInputStream bis = new BufferedInputStream(is);
//                        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
//                        //We create an array of bytes
//                        byte[] data = new byte[50];
//                        int current = 0;
//                        while ((current = bis.read(data, 0, data.length)) != -1) {
//                            buffer.write(data, 0, current);
//                            Log.e("Buffer", String.valueOf(buffer));
//                        }
//
//                        FileOutputStream fos = new FileOutputStream(f);
//                        fos.write(buffer.toByteArray());
//                        Log.e("BufferFOS", String.valueOf(fos));
//                        fos.close();
//                        result = "success";
//
//                        ContentValues values= new ContentValues();
//
//                    }
//
//                    requestStoragePermission();
//                }
//            } catch (FileNotFoundException e1) {
//                e1.printStackTrace();
//            } catch (MalformedURLException e1) {
//                e1.printStackTrace();
//            } catch (IOException e1) {
//                e1.printStackTrace();
//            }
//            return result;
//        }
//
//        protected void onPostExecute(String result) {
//            if (!result.equals("") && result != null) {
//                try {
//
//
//                    Log.e("result", result);
//                    Intent shareIntent = new Intent();
//                    shareIntent.setAction(Intent.ACTION_SEND);
//
//                    Uri uri = Uri.parse(Environment.getExternalStorageDirectory() + "/" + getString(R.string.app_name) + "/" + filename);
//                    shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
//                    Log.e("This from shareintent", String.valueOf(uri));
//
//                    shareIntent.setType("image/jpeg");
//
//                    shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//                    startActivity(Intent.createChooser(shareIntent, "Share "));
////                    Log.e("result", result);
////                    Intent shareIntent = new Intent();
////                    shareIntent.setAction(Intent.ACTION_SEND);
//                   // Log.e("filename","^^^^^"+filename);
//                   // Uri uri = Uri.parse(Environment.getExternalStorageDirectory() + "/" + getString(R.string.app_name) + "/" + filename);
////                    shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
////                    Log.e("This from shareintent", String.valueOf(uri));
////
////                    shareIntent.setType("image/jpeg");
////
////                    shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
////                    startActivity(Intent.createChooser(shareIntent, "Share "));
//
////                    String fileName = "image-3116.jpg";//Name of an image
////                    String externalStorageDirectory = Environment.getExternalStorageDirectory().toString();
////                    String myDir = externalStorageDirectory + "/saved_images/"; // the file will be in saved_images
////                    Uri uri = Uri.parse("file:///" + myDir + fileName);
//
//                   // Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
//                   // shareIntent.setType("text/html");
//                 //   shareIntent.setType("image/jpeg");
//                   // shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Test Mail");
//                  //  shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Launcher");
//                  //  shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
//                 //   Log.e("uri","%%%"+uri);
//                  //  shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//                  //  startActivity(Intent.createChooser(shareIntent, "Share Deal"));
//
////                    startActivity(Intent.createChooser(shareIntent, "Share " + Constants.getPre_merchant_name()));
//                    //startActivity(Intent.createChooser(shareIntent, getResources().getText(R.string.send_to)));
//
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//            dialog.dismiss();
//        }
//    }
         private static final String TEMP_PHOTO = ".jpg";

    class GetImageInPhone2 extends AsyncTask<String, Void, String> {
        String temp_image_path = "";
        String result = "";
        String filename = "";
        final ProgressDialog dialog = new ProgressDialog(PagerDialogActivity.this);


        public GetImageInPhone2(String profile_image) {
            temp_image_path = profile_image;
            Log.e("share","######"+temp_image_path);
        }

        @Override
        protected void onPreExecute() {
// TODO Auto-generated method stub
            dialog.setCancelable(false);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();

        }

        @Override
        protected String doInBackground(String... params) {
            try {
                URL url = new URL(temp_image_path);
                URLConnection ucon = url.openConnection();
                InputStream is = ucon.getInputStream();
                if (is != null) {

                    filename = temp_image_path.substring(temp_image_path.lastIndexOf("/") + 1);

// File file = new File(Environment.getExternalStorageDirectory() + File.separator + "img.jpg");
                    String PATH = Environment.getExternalStorageDirectory() + "/daffodils/" + getPackageName() + "/";
// String PATH = Environment.getExternalStorageDirectory() + "/Android/data/" + object.getPackageName() + "img.jpg";
                    File folder = new File(PATH);
                    if (!folder.exists()) {
                        folder.mkdir();//If there is no folder it will be created.
                        Log.e("folder====", "created");
                    }
                    File f = new File(PATH +filename );
                    f.createNewFile();


// File file = new File(fileName);
                    long startTime = System.currentTimeMillis();
                    BufferedInputStream bis = new BufferedInputStream(is);
                    ByteArrayOutputStream buffer = new ByteArrayOutputStream();
//We create an array of bytes
                    byte[] data = new byte[50];
                    int current = 0;
                    while ((current = bis.read(data, 0, data.length)) != -1) {
                        buffer.write(data, 0, current);
                    }

                    FileOutputStream fos = new FileOutputStream(f);
                    fos.write(buffer.toByteArray());
                    fos.close();
                    result = "success";
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;
        }

        protected void onPostExecute(String result) {

            if (!result.equals("") && result != null) {
                try {
                    Log.e("result", result);
                    Intent shareIntent = new Intent();
                    shareIntent.setAction(Intent.ACTION_SEND);
                 //   String myText = getResources().getString(R.string.str_tastee) + " " + str_menu_item_name + " " + object.getResources().getString(R.string.str_enjoying_at);
                 //   shareIntent.putExtra(Intent.EXTRA_TEXT, myText +"\n"+ " " +"http://www.foodorder-app.com/");
                  //  shareIntent.setType("text/plain");
//add a subject

                    shareIntent.putExtra(Intent.EXTRA_SUBJECT,
                            "Daffodils");
                    Uri uri = Uri.parse("file:///" + Environment.getExternalStorageDirectory() + "/daffodils/" + getPackageName() + "/" +filename);
                    shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
                    shareIntent.setType("image/*");
                    shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    startActivity(Intent.createChooser(shareIntent, getResources().getString(R.string.str_share) ));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            dialog.dismiss();
        }
    }


}
