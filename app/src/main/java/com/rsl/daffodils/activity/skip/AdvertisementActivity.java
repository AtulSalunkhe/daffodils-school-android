package com.rsl.daffodils.activity.skip;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.rsl.daffodils.R;
import com.rsl.daffodils.activity.student.StudentsHomeActivity;
import com.rsl.daffodils.activity.teacher.TeachersHomeActivity;
import com.rsl.daffodils.adapter.AdvertisementAdapter;
import com.rsl.daffodils.adapter.NewsAdapter;
import com.rsl.daffodils.model.Constants;
import com.rsl.daffodils.utils.ConnectionDetector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AdvertisementActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener{


    RecyclerView rl_advertise_list;
    private ConnectionDetector internet;
    private NewsAdapter adapter;
    ArrayList<HashMap<String, String>> list_data;
    private SwipeRefreshLayout swipeRefreshLayout;
    TextView txt_message;
    SharedPreferences sp;
    SharedPreferences.Editor editor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_advertisement);
        sp =getSharedPreferences("daffodils", 0);
        editor = sp.edit();
        editor.apply();
      //  Constants.activity_news = true;
        swipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary, R.color.colorAccent, R.color.event_holiday_color, R.color.event_color);
        initView();
        initrecycleView();
        internet = new ConnectionDetector(AdvertisementActivity.this);
        if (internet.isConnectingToInternet()) {
            if (sp.getString("role","").equals("teacher")){
                getannouncement();
            }else {
               getadvertisement();
            }
            Log.e("oncreateview", "" + sp.getString("student_id", ""));
        } else {
            internet.showAlertDialog(AdvertisementActivity.this, false);
            txt_message.setText(getString(R.string.str_internet_title));
            txt_message.setVisibility(View.VISIBLE);
            rl_advertise_list.setVisibility(View.GONE);
        }



    }//onCreate

    @Override
    public void onRefresh() {
        if (internet.isConnectingToInternet()) {
            if (sp.getString("role","").equals("teacher")){
                getannouncement();
            }else {
               getadvertisement();
            }
        } else {
            swipeRefreshLayout.setRefreshing(false);
            internet.showAlertDialog(AdvertisementActivity.this, false);
        }
    }

    public void initrecycleView() {
        final LinearLayoutManager layoutManager = new LinearLayoutManager(AdvertisementActivity.this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rl_advertise_list.setLayoutManager(layoutManager);
        final Activity object = AdvertisementActivity.this;
        list_data = new ArrayList<>();
        new Thread(new Runnable() {
            @Override
            public void run() {
                adapter = new NewsAdapter(list_data, object);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        rl_advertise_list.setAdapter(adapter);
                    }
                });
            }
        }).start();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            if (extras.containsKey("from_fcm")) {
                if (getIntent().getStringExtra("from_fcm").equals("true")) {
                    if (sp.getString("role", "").equals("student")) {
                        Intent in = new Intent(AdvertisementActivity.this, StudentsHomeActivity.class);
                        startActivity(in);
                        finish();
                    } else if (sp.getString("role", "").equals("teacher")) {
                        Intent in = new Intent(AdvertisementActivity.this, TeachersHomeActivity.class);
                        startActivity(in);
                        finish();
                    }
                }
            }
        } else finish();
    }

    private void initView() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (sp.getString("role","").equals("teacher")){
            getSupportActionBar().setTitle(R.string.str_cap_announcement);
        }else{
            getSupportActionBar().setTitle(R.string.str_advertisement_cap);
        }
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        rl_advertise_list = (RecyclerView) findViewById(R.id.rl_advertise_list);
        txt_message = (TextView) findViewById(R.id.txt_message);
    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
        Constants.activity_advertisement = false;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        if (internet.isConnectingToInternet()) {
            if (sp.getString("role","").equals("teacher")){
                getannouncement();
            }else {
                getadvertisement();
            }
        } else {
            internet.showAlertDialog(AdvertisementActivity.this, false);
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void getannouncement() {
        swipeRefreshLayout.setRefreshing(false);
        final ProgressDialog dialog = new ProgressDialog(AdvertisementActivity.this);
        dialog.setMessage(getString(R.string.str_requesting));
        dialog.setCancelable(false);
        dialog.show();
        RequestQueue queue = Volley.newRequestQueue(AdvertisementActivity.this);
        String requestURL = sp.getString("new_link","") + "get_announcement/"+"0" ;
        Log.e("requestURL", "URL" + requestURL);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", "code=" + response);
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response != null) {
                    if (response.equals("204")) {
                        // txt_message.setVisibility(View.VISIBLE);
                        // txt_message.setText("No Records Available");
                        alertDialog1("No Records Available");
                        Log.e("Error message", "No Records Available");
                    } else {
//                        text_no_data.setVisibility(View.GONE);
//                        adapter = new CarCompanyAdapter(car_company, CarComapnyActivity.this);
//                        recyclerView_company.setAdapter(adapter);
//                        adapter.notifyDataSetChanged();
                    }
                }else {
                    txt_message.setText("Server not responding");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
              //  Toast.makeText(AdvertisementActivity.this, "Poor Internet Coonection", Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                 //params.put("id","0");
                Log.e("param", "announcement=" + params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                    VolleyError error = new VolleyError(new String(volleyError.networkResponse.data));
                    volleyError = error;
                    dialog.dismiss();
                }
                //  Toast.makeText(getActivity(),"Poor Internet Coonection",Toast.LENGTH_SHORT).show();
                Log.e("parseNetworkError", "==" + volleyError);
                Log.e("parseNetworkError", "string==" + volleyError.toString());
                Log.e("parseNetworkError", "msg==" + volleyError.getMessage());
                return volleyError;
            }

            @Override
            protected Response<String> parseNetworkResponse(final NetworkResponse response) {
                String responseString = "";
                if (response != null) {
                    responseString = String.valueOf(response.statusCode);
                    // can get more details such as response.headers
                }
                Map<String, String> responseHeaders = response.headers;
                if (response.statusCode == 200) {
                    Response<String> result = Response.success(responseHeaders.get("Content-Type"), HttpHeaderParser.parseCacheHeaders(response));
                    Log.e("Response", "==" + parseToString(response));
                    rl_advertise_list.setVisibility(View.VISIBLE);
                    txt_message.setVisibility(View.GONE);
                    list_data.clear();
                    dialog.dismiss();

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            try {
                                JSONArray JArray = new JSONArray(parseToString(response));

                                for (int i = 0; i < JArray.length(); i++) {
                                    JSONObject jobj_Data = JArray.getJSONObject(i);

                                    HashMap<String, String> grp_data = new HashMap<>();
                                    grp_data.put("notification_id", jobj_Data.getString("notification_id"));
                                    grp_data.put("class_id", jobj_Data.getString("class_id"));
                                    grp_data.put("student_id", jobj_Data.getString("student_id"));
                                    grp_data.put("notification_title", jobj_Data.getString("notification_title"));
                                    grp_data.put("notification_data", jobj_Data.getString("notification_data"));
                                    grp_data.put("notification_img", jobj_Data.getString("notification_img"));
                                    grp_data.put("notification_date", jobj_Data.getString("notification_date"));
                                    grp_data.put("is_active", jobj_Data.getString("is_active"));
                                    grp_data.put("type", jobj_Data.getString("type"));
                                    list_data.add(grp_data);
                                }
                                adapter.notifyDataSetChanged();


                            }//for
                            catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });

                    return result;
                } else {

                    rl_advertise_list.setVisibility(View.GONE);
//                   //txt_message.setVisibility(View.VISIBLE);
//                    //txt_message.setText(jObj.getString("msg"));
//                    dialog.dismiss();
                    Log.e("Error message", "Login Failed");
                }
                Log.e("responseString", "" + responseString);
                return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
            }

            public String parseToString(NetworkResponse response) {
                String parsed;
                try {
                    parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                } catch (UnsupportedEncodingException e) {
                    parsed = new String(response.data);
                }
                return parsed;
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }//getannouncement


    private void getadvertisement() {
        swipeRefreshLayout.setRefreshing(false);
        final ProgressDialog dialog = new ProgressDialog(AdvertisementActivity.this);
        dialog.setMessage(getString(R.string.str_requesting));
        dialog.setCancelable(false);
        dialog.show();
        RequestQueue queue = Volley.newRequestQueue(AdvertisementActivity.this);
        String requestURL = sp.getString("new_link","") + "get_advertisement" ;
        Log.e("requestURL", "advertisement" + requestURL);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", "code=" + response);
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response != null) {
                    if (response.equals("204")) {
                         txt_message.setVisibility(View.VISIBLE);
                         txt_message.setText("No Records Available");
                       // alertDialog1("No Records Available");
                        Log.e("Error message", "No Records Available");
                    } else {
//                        text_no_data.setVisibility(View.GONE);
//                        adapter = new CarCompanyAdapter(car_company, CarComapnyActivity.this);
//                        recyclerView_company.setAdapter(adapter);
//                        adapter.notifyDataSetChanged();
                    }
                }else {
                    txt_message.setText("Server not responding");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
              //  Toast.makeText(AdvertisementActivity.this, "Poor Internet Coonection", Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                //params.put("id","0");
                Log.e("param", "announcement=" + params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                    VolleyError error = new VolleyError(new String(volleyError.networkResponse.data));
                    volleyError = error;
                    dialog.dismiss();
                }
                //  Toast.makeText(getActivity(),"Poor Internet Coonection",Toast.LENGTH_SHORT).show();
                Log.e("parseNetworkError", "==" + volleyError);
                Log.e("parseNetworkError", "string==" + volleyError.toString());
                Log.e("parseNetworkError", "msg==" + volleyError.getMessage());
                return volleyError;
            }

            @Override
            protected Response<String> parseNetworkResponse(final NetworkResponse response) {
                String responseString = "";
                if (response != null) {
                    responseString = String.valueOf(response.statusCode);
                    // can get more details such as response.headers
                }
                Map<String, String> responseHeaders = response.headers;
                if (response.statusCode == 200) {
                    Response<String> result = Response.success(responseHeaders.get("Content-Type"), HttpHeaderParser.parseCacheHeaders(response));
                    Log.e("Responseadvertisement", "Response==" + parseToString(response));
                    rl_advertise_list.setVisibility(View.VISIBLE);
                    txt_message.setVisibility(View.GONE);
                    list_data.clear();
                    dialog.dismiss();

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            try {
                                JSONArray JArray = new JSONArray(parseToString(response));

                                for (int i = 0; i < JArray.length(); i++) {
                                    JSONObject jobj_Data = JArray.getJSONObject(i);

                                    HashMap<String, String> grp_data = new HashMap<>();
                                    grp_data.put("notification_id", jobj_Data.getString("advertise_id"));
                                   // grp_data.put("class_id", jobj_Data.getString("class_id"));
                                  //  grp_data.put("student_id", jobj_Data.getString("student_id"));
                                    grp_data.put("notification_title", jobj_Data.getString("advertise_title"));
                                    grp_data.put("notification_data", jobj_Data.getString("advertise_message"));
                                    grp_data.put("notification_img", jobj_Data.getString("advertise_img"));
                                    grp_data.put("notification_date", jobj_Data.getString("advertise_date"));
                                   // grp_data.put("is_active", jobj_Data.getString("is_active"));
                                  //  grp_data.put("type", jobj_Data.getString("type"));
                                    list_data.add(grp_data);
                                }
                                adapter.notifyDataSetChanged();


                            }//for
                            catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }
                    });

                    return result;
                } else {

                  //  rl_advertise_list.setVisibility(View.GONE);
//                   //txt_message.setVisibility(View.VISIBLE);
//                    //txt_message.setText(jObj.getString("msg"));
//                    dialog.dismiss();
                    Log.e("Error message", "Login Failed");
                }
                Log.e("responseString", "" + responseString);
                return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
            }

            public String parseToString(NetworkResponse response) {
                String parsed;
                try {
                    parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                } catch (UnsupportedEncodingException e) {
                    parsed = new String(response.data);
                }
                return parsed;
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }//getadvertisement




    private void alertDialog1(String error) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                AdvertisementActivity.this);
        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setIcon(R.drawable.daffodils_logo);
        alertDialog.setMessage(error);
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton(getString(R.string.dlg_ok),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }


}
