package com.rsl.daffodils.activity.teacher;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.rsl.daffodils.R;
import com.rsl.daffodils.activity.admin.EnquiryActivity;
import com.rsl.daffodils.activity.admin.FeedbackListActivity;
import com.rsl.daffodils.activity.admin.StudentAttendence;
import com.rsl.daffodils.activity.admin.TeacherAttendence;
import com.rsl.daffodils.activity.skip.SkipHomeActivity;
import com.rsl.daffodils.utils.ConnectionDetector;
import com.rsl.daffodils.utils.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.rsl.daffodils.model.Constants.teacherHomeActivity;

public class TeachersHomeActivity extends AppCompatActivity {
    static final int send_data = 1;


    public static int[] MenuImages = {R.drawable.ic_news, R.drawable.ic_students, R.drawable.ic_students,
            R.drawable.ic_events, R.drawable.ic_forums, R.drawable.ic_students,R.drawable.contact_us,R.drawable.facebook};
    public static int[] MenuLabels = {R.string.str_class_news, R.string.students, R.string.teacher_corner, R.string.current_event, R.string.teacher_profile, R.string.feedback,R.string.str_contact_us,R.string.facebook};
    SharedPreferences sp;
    SharedPreferences.Editor editor;
    SessionManager session;
    TextView txt_logout;
    ConnectionDetector internet;
    ImageView img_slider;
    TextSliderView textSliderView;
    private SliderLayout mDemoSlider;
    ArrayList<HashMap<String, String>> list_data;
    int[] imageId = {R.drawable.slide1, R.drawable.slide2, R.drawable.slide3, R.drawable.slide4, R.drawable.slide5};
    LinearLayout linear_student,linear_teacher,linear_feedback,linear_enquiey,linear_about_school,linear_share;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher_home);
        sp = getSharedPreferences("daffodils", 0);
        editor = sp.edit();
        editor.apply();
        session = new SessionManager(TeachersHomeActivity.this);
        internet = new ConnectionDetector(TeachersHomeActivity.this);
        txt_logout = (TextView) findViewById(R.id.txt_logout);
        txt_logout.setOnClickListener(new onclicklistener());
        list_data=new ArrayList<HashMap<String, String>>();
        img_slider = (ImageView) findViewById(R.id.img_slider);
        mDemoSlider = (SliderLayout)findViewById(R.id.view_pager);


        textSliderView = new TextSliderView(TeachersHomeActivity.this);



        linear_student = (LinearLayout) findViewById(R.id.linear_student);
        linear_teacher = (LinearLayout) findViewById(R.id.linear_teacher);
        linear_feedback = (LinearLayout) findViewById(R.id.linear_feedback);
        linear_enquiey = (LinearLayout) findViewById(R.id.linear_enquiey);
        linear_about_school = (LinearLayout) findViewById(R.id.linear_about_school);
        linear_share = (LinearLayout) findViewById(R.id.linear_share);

        linear_student.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(TeachersHomeActivity.this, StudentAttendence.class);
                startActivity(intent);
            }
        });

        if(sp.getString("Admin_type","").equals("Admin")) {
            linear_teacher.setVisibility(View.VISIBLE);
            linear_teacher.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(TeachersHomeActivity.this, TeacherAttendence.class);
                    startActivity(intent);
                }
            });
        }else {
            linear_teacher.setVisibility(View.GONE);
        }

        linear_feedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(TeachersHomeActivity.this, FeedbackListActivity.class);
                startActivity(intent);
            }
        });


        linear_enquiey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(TeachersHomeActivity.this, EnquiryActivity.class);
                startActivity(intent);
            }
        });


        linear_about_school.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(TeachersHomeActivity.this, SkipHomeActivity.class);
                startActivity(intent);
            }
        });

        linear_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String myText = getResources().getString(R.string.app_text);
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                //add a subject
                sendIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,
                        getResources().getString(R.string.app_name));
                sendIntent.putExtra(Intent.EXTRA_TEXT, myText+ " " + getResources().getString(R.string.link));
                sendIntent.setType("text/plain");
                startActivityForResult(sendIntent, send_data);
            }
        });


        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(networkStateReceiver, filter);

    }//onCreate


    BroadcastReceiver networkStateReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            boolean noConnectivity = intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, false);
            if (!noConnectivity) {
                onConnectionFound();
            } else {
                onConnectionLost();
            }
        }
    };

    public void onConnectionFound() {
        display_images();
    }

    public void onConnectionLost() {
        //Toast.makeText(getActivity(), "Connection lost", Toast.LENGTH_LONG).show();
    }

    private class onclicklistener implements View.OnClickListener {


        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.txt_logout: {
                    if (!internet.isConnectingToInternet()) {
                        internet.showAlertDialog(TeachersHomeActivity.this, false);
                    } else {
                        final AlertDialog.Builder builder = new AlertDialog.Builder(TeachersHomeActivity.this);
                        builder.setTitle(getString(R.string.app_name));
                        builder.setIcon(R.drawable.app_logo_main);
                        builder.setMessage(R.string.sure_logout);
                        builder.setCancelable(false);
                        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                getOut();
                                dialogInterface.dismiss();
                            }
                        });
                        builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });

                        AlertDialog alert = builder.create();
                        alert.show();
                    }
                }
                break;

                default:
                    break;
            }
        }
    }
    private boolean doublebackpressed = false;

    @Override
    public void onBackPressed() {

        if (doublebackpressed) {
            super.onBackPressed();
            finish();
        }
        Toast t = Toast.makeText(TeachersHomeActivity.this, getString(R.string.str_double_backPress_message), Toast.LENGTH_SHORT);
        t.setGravity(Gravity.CENTER, 0, 0);
        t.show();
        doublebackpressed = true;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doublebackpressed = false;
            }
        }, 2000);


    }

    @Override
    protected void onResume() {
        super.onResume();
        teacherHomeActivity=true;
        //getcount();
    }

    @Override
    protected void onPause() {
        super.onPause();
        teacherHomeActivity=false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        teacherHomeActivity=false;
        unregisterReceiver(networkStateReceiver);
    }
    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.e("intent_teacher_home",""+intent);
        setIntent(intent);
      //  getcount();
    }

    private void getOut() {

        final ProgressDialog dialog = new ProgressDialog(TeachersHomeActivity.this);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.str_requesting));
        dialog.show();
        RequestQueue queue = Volley.newRequestQueue(TeachersHomeActivity.this);
        String requestURL = sp.getString("new_link","") + "logout";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", "code=" + response);
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response != null) {
                    if (response.equals("204")) {
                        // text_msg.setVisibility(View.VISIBLE);
                        //  text_msg.setText("No Records Available");
                     //   alertDialog1("No Records Available");
                        Log.e("Error message", "No Records Available");
                    } else {
//                        text_no_data.setVisibility(View.GONE);
//                        adapter = new CarCompanyAdapter(car_company, CarComapnyActivity.this);
//                        recyclerView_company.setAdapter(adapter);
//                        adapter.notifyDataSetChanged();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
               // Toast.makeText(getApplicationContext(), "Poor Internet Coonection", Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("account_id", sp.getString("account_id", ""));
                params.put("entry_code", sp.getString("entry_code", ""));

                Log.e("param", "logout" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                    VolleyError error = new VolleyError(new String(volleyError.networkResponse.data));
                    volleyError = error;
                    dialog.dismiss();
                }
                // Toast.makeText(getApplicationContext(),"Poor Internet Coonection",Toast.LENGTH_SHORT).show();
                Log.e("parseNetworkError", "==" + volleyError);
                Log.e("parseNetworkError", "string==" + volleyError.toString());
                Log.e("parseNetworkError", "msg==" + volleyError.getMessage());
                return volleyError;
            }

            @Override
            protected Response<String> parseNetworkResponse(final NetworkResponse response) {
                String responseString = "";
                if (response != null) {
                    responseString = String.valueOf(response.statusCode);
                    // can get more details such as response.headers
                }
                Map<String, String> responseHeaders = response.headers;
                if (response.statusCode == 200) {
                    Response<String> result = Response.success(responseHeaders.get("Content-Type"), HttpHeaderParser.parseCacheHeaders(response));
                    Log.e("logout", "" + parseToString(response));

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
//                            sp.edit().clear().apply();
                            // Creating Session
                            session.logoutUser();
                            Toast.makeText(TeachersHomeActivity.this, "" + "Logout", Toast.LENGTH_SHORT).show();

                        }
                    });

                    return result;
                } else {


                    // text_msg.setText(jObj.getString("msg"));
                    Log.e("Error message", "Login Failed");
                }
                Log.e("responseString", "" + responseString);
                return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
            }

            public String parseToString(NetworkResponse response) {
                String parsed;
                try {
                    parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                } catch (UnsupportedEncodingException e) {
                    parsed = new String(response.data);
                }
                return parsed;
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }//getOut



    private void display_images() {

        final ProgressDialog dialog = new ProgressDialog(TeachersHomeActivity.this);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.str_requesting));
        dialog.show();
        RequestQueue queue = Volley.newRequestQueue(TeachersHomeActivity.this);
        String requestURL = sp.getString("new_link","") + "get_home_imgaes";
        Log.e("requestURL", "images" + requestURL);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", "code=" + response);
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response != null) {
                    if (response.equals("204")) {
                        // text_msg.setVisibility(View.VISIBLE);
                        //  text_msg.setText("No Records Available");
                        alertDialog1("No image available");
                        Log.e("Error message", "No Records Available");
                    } else {
//                        text_no_data.setVisibility(View.GONE);
//                        adapter = new CarCompanyAdapter(car_company, CarComapnyActivity.this);
//                        recyclerView_company.setAdapter(adapter);
//                        adapter.notifyDataSetChanged();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                //Toast.makeText(getApplicationContext(), "Poor Internet Coonection", Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                // params.put("id",id);
                Log.e("param", "" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                    VolleyError error = new VolleyError(new String(volleyError.networkResponse.data));
                    volleyError = error;
                    dialog.dismiss();
                }
                // Toast.makeText(getApplicationContext(),"Poor Internet Coonection",Toast.LENGTH_SHORT).show();
                Log.e("parseNetworkError", "==" + volleyError);
                Log.e("parseNetworkError", "string==" + volleyError.toString());
                Log.e("parseNetworkError", "msg==" + volleyError.getMessage());
                return volleyError;
            }

            @Override
            protected Response<String> parseNetworkResponse(final NetworkResponse response) {
                String responseString = "";
                if (response != null) {
                    responseString = String.valueOf(response.statusCode);
                    // can get more details such as response.headers
                }
                Map<String, String> responseHeaders = response.headers;
                if (response.statusCode == 200) {
                    Response<String> result = Response.success(responseHeaders.get("Content-Type"), HttpHeaderParser.parseCacheHeaders(response));
                    Log.e("Response_image", "" + parseToString(response));

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                JSONArray JArray = new JSONArray(parseToString(response));

                                for (int i = 0; i < JArray.length(); i++) {
                                    JSONObject jobj_Data = JArray.getJSONObject(i);

                                    HashMap<String, String> offers = new HashMap<String, String>();
                                    offers.put("img_path",jobj_Data.getString("img_path"));
                                    list_data.add(offers);

                                }//for
                                for (int i = 0; i <list_data.size(); i++) {
                                    TextSliderView textSliderView = new TextSliderView(TeachersHomeActivity.this);
                                    // initialize a SliderLayout
                                    textSliderView
                                            .image(list_data.get(i).get("img_path"))
                                            .setScaleType(BaseSliderView.ScaleType.Fit);
                                    mDemoSlider.addSlider(textSliderView);
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });

                    return result;
                } else {


                    Log.e("Error message", "Login Failed");
                }
                Log.e("responseString", "" + responseString);
                return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
            }

            public String parseToString(NetworkResponse response) {
                String parsed;
                try {
                    parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                } catch (UnsupportedEncodingException e) {
                    parsed = new String(response.data);
                }
                return parsed;
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 4, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }//display_image

    private void alertDialog1(String error) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                TeachersHomeActivity.this);
        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setIcon(R.drawable.app_logo_main);
        alertDialog.setMessage(error);
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton(getString(R.string.dlg_ok),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }



}
