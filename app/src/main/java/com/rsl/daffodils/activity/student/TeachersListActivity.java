package com.rsl.daffodils.activity.student;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.rsl.daffodils.R;
import com.rsl.daffodils.model.User;
import com.rsl.daffodils.utils.ConnectionDetector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class TeachersListActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    RecyclerView rl_teachers_list;
    private ConnectionDetector internet;
    SharedPreferences sp;
    private ArrayList<User> users;
    private SwipeRefreshLayout swipeRefreshLayout;
    TextView txt_message;

    private String total_list_count;
    int check_count;
    int count = 1;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teachers_list);
        sp = getSharedPreferences("GoresClasses", MODE_PRIVATE);
        internet = new ConnectionDetector(TeachersListActivity.this);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary, R.color.colorAccent, R.color.event_holiday_color, R.color.event_color);
        initView();
        initrecycleView();
        if (internet.isConnectingToInternet()) {
          //  getContactList(count);
        } else {
            internet.showAlertDialog(TeachersListActivity.this, false);
            txt_message.setText(getString(R.string.str_internet_title));
            txt_message.setVisibility(View.VISIBLE);
            rl_teachers_list.setVisibility(View.GONE);
        }






    }

    @Override
    public void onRefresh() {
        if (internet.isConnectingToInternet()) {
            getContactList(count);
        } else {
            swipeRefreshLayout.setRefreshing(false);
            internet.showAlertDialog(TeachersListActivity.this, false);
        }
    }

    public void initrecycleView() {
        final LinearLayoutManager layoutManager = new LinearLayoutManager(TeachersListActivity.this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rl_teachers_list.setLayoutManager(layoutManager);
        final Activity object = TeachersListActivity.this;
        users = new ArrayList<User>();
        new Thread(new Runnable() {
            @Override
            public void run() {
             //   adapter = new TeachersListAdapter(users, object);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                      //  rl_teachers_list.setAdapter(adapter);
                    }
                });
            }
        }).start();
    }

    private void initView() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.expert_info));
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        rl_teachers_list = (RecyclerView) findViewById(R.id.rl_teachers_list);
        txt_message = (TextView) findViewById(R.id.txt_message);

    }

    public void getContactList(final int enter_count) {
        swipeRefreshLayout.setRefreshing(false);
        final ProgressDialog dialog = new ProgressDialog(TeachersListActivity.this);
        dialog.setMessage("Loading please wait");
        dialog.setCancelable(false);
        dialog.show();
        final RequestQueue queue = Volley.newRequestQueue(this);
        String requestURL = sp.getString("new_link","") + "get_teachers_info.php";
        Log.e("requestURL","=="+requestURL);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("API", ":: Response :: " + response);
                rl_teachers_list.setVisibility(View.VISIBLE);
                try {
                    JSONObject jobj = new JSONObject(response);
                    if (jobj.length() > 0) {
                        if (jobj.getString("result").equals("success")) {
                            dialog.dismiss();
                            rl_teachers_list.setVisibility(View.VISIBLE);
                            txt_message.setVisibility(View.GONE);
                            users.clear();
                            Log.e("web_totl_count", "" + jobj.getString("total_count"));
                            total_list_count = jobj.getString("total_count");
                            check_count = Integer.parseInt(jobj.getString("total_count"));

                            JSONArray jarray = jobj.getJSONArray("Teacher_Details");
                            if (jarray.length() > 0) {
                                for (int i = 0; i < jarray.length(); i++) {
                                    JSONObject user = jarray.getJSONObject(i);
                                    User u = new User();
                                    u.setAccount_id(user.getString("account_id"));
                                    u.setName(user.getString("teacher_name"));
                                    u.setSubject(user.getString("teacher_subject"));
                                    u.setMobile(user.getString("teacher_mobile"));
                                    u.setImg_url(user.getString("teacher_image"));
                                    u.setQualification(user.getString("qualification"));
                                    u.setExpeiance(user.getString("experience"));
                                    users.add(u);
                                }
                                //adapter.notifyDataSetChanged();
                            }
                        } else if (jobj.getString("result").equals("failed")) {
                            rl_teachers_list.setVisibility(View.GONE);
                            txt_message.setVisibility(View.VISIBLE);
                            txt_message.setText(jobj.getString("msg"));
                            dialog.dismiss();
                        }
                    } else {
                        Toast.makeText(TeachersListActivity.this, "No users found!", Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    dialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                Log.e("Error", "" + error.getMessage());
                Toast.makeText(TeachersListActivity.this, getString(R.string.internet_lost), Toast.LENGTH_SHORT).show();
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("page_number",String.valueOf(enter_count));
                Log.e("get_all_forum", "param=" + params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        queue.add(stringRequest);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
