package com.rsl.daffodils.activity.student;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.rsl.daffodils.R;
import com.rsl.daffodils.activity.AllStudentListActivity;
import com.rsl.daffodils.activity.teacher.TeachersHomeActivity;
import com.rsl.daffodils.adapter.DownloadAdapter;
import com.rsl.daffodils.adapter.ReferencesAdapter;
import com.rsl.daffodils.model.Constants;
import com.rsl.daffodils.utils.ConnectionDetector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DownloadsActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    RecyclerView rl_download_list;
    private ConnectionDetector internet;
    private DownloadAdapter adapter;
    SharedPreferences sp;
    SharedPreferences.Editor editor;
    ArrayList<HashMap<String, String>> list_data;
    private SwipeRefreshLayout swipeRefreshLayout;
    TextView txt_message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_downloads);
        Constants.activity_downlosds = true;
        sp = getSharedPreferences("daffodils", 0);
        editor = sp.edit();
        editor.apply();
        internet = new ConnectionDetector(DownloadsActivity.this);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary, R.color.colorAccent, R.color.event_holiday_color, R.color.event_color);
        initView();
        initrecycleView();
        internet = new ConnectionDetector(DownloadsActivity.this);


        Log.e("Intent", "on create" + getIntent().getExtras());


        if (internet.isConnectingToInternet()) {
            Bundle extras = getIntent().getExtras();
            Log.e("fcm oncreate", "type : " + extras.getString("type"));
            Log.e("fcm oncreate", "out : " + extras.getString("from_fcm"));
            if (extras != null) {
                Log.e("fcm oncreate", "in");
                if (extras.containsKey("type")) {
                    editor.putString("student_id", getIntent().getExtras().getString("student_id")).apply();
                    Log.e("id",""+getIntent().getExtras().getString("student_id"));
                    editor.putString("account_id",getIntent().getExtras().getString("account_id")).apply();
                    Log.e("ac id",""+getIntent().getExtras().getString("account_id"));
                    editor.putString("student_image", getIntent().getExtras().getString("student_image")).apply();
                    Log.e("image",""+getIntent().getExtras().getString("student_image"));
                    editor.putString("parent_mobile", getIntent().getExtras().getString("parent_mobile")).apply();
                    Log.e("parent_mobile",""+getIntent().getExtras().getString("parent_mobile"));
                    editor.putString("student_name1",getIntent().getExtras().getString("student_name1")).apply();
                    Log.e("student_name",""+getIntent().getExtras().getString("student_name1"));
                    editor.putString("parent_email", getIntent().getExtras().getString("parent_email")).apply();
                    Log.e("parent_email",""+getIntent().getExtras().getString("parent_email"));
                    editor.putString("date_of_birth", getIntent().getExtras().getString("date_of_birth")).apply();
                    Log.e("dob",""+getIntent().getExtras().getString("date_of_birth"));
                    editor.putString("gender_id", getIntent().getExtras().getString("gender_id")).apply();
                    Log.e("gender_id",""+getIntent().getExtras().getString("gender_id"));
                    editor.putString("student_class1",getIntent().getExtras().getString("student_class1")).apply();
                    Log.e("class",""+getIntent().getExtras().getString("student_class1"));
                    editor.putString("class_id", getIntent().getExtras().getString("class_id")).apply();
                    Log.e("class_id",""+getIntent().getExtras().getString("class_id"));
                    editor.commit();
                }
            } else {
                Log.e("finish", "direct finish");
                finish();
            }

            Log.e("onNewIntent", "onNewIntent");
        } else {
            //  internet.showAlertDialog(CalenderActivity.this, false);
        }
        if (internet.isConnectingToInternet()) {
            getDownloads();
        } else {
            internet.showAlertDialog(DownloadsActivity.this, false);
            txt_message.setText(getString(R.string.str_internet_title));
            txt_message.setVisibility(View.VISIBLE);
            rl_download_list.setVisibility(View.GONE);
        }
        updatecount("STUDENT_CORNER");

    }//onCreate

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Constants.activity_downlosds = false;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
       /* Log.e("Intent", "" + getIntent().getExtras());
        if (internet.isConnectingToInternet()) {
            getDownloads();
            Log.e("onNewIntent", "onNewIntent");
        } else {
            internet.showAlertDialog(DownloadsActivity.this, false);
        }*/
    }

    @Override
    public void onBackPressed() {
       super.onBackPressed();
        Bundle extras = getIntent().getExtras();
        Log.e("fcm", "out : "+extras.getString("from_fcm"));
        if (extras != null) {
            Log.e("fcm", "in");
            if (extras.containsKey("from_fcm")) {
                if (getIntent().getStringExtra("from_fcm").equals("true")) {
                    if (sp.getString("role", "").equals("student")) {
                        Log.e("role","+++++");
                        if(sp.getBoolean("select_student",false)){
                            Log.e("select_student","+++++");
                            Intent in = new Intent(DownloadsActivity.this, StudentsHomeActivity.class);
                            startActivity(in);
                            finish();
                        }else {
                            Intent in = new Intent(DownloadsActivity.this, AllStudentListActivity.class);
                            startActivity(in);
                            finish();
                        }

                    }
                }
            }else {
                Intent in = new Intent(DownloadsActivity.this, StudentsHomeActivity.class);
                startActivity(in);
                finish();
            }
        } else {
            Log.e("finish", "direct finish");
            finish();
        }
    }

    public void initrecycleView() {
        final LinearLayoutManager layoutManager = new LinearLayoutManager(DownloadsActivity.this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rl_download_list.setLayoutManager(layoutManager);
        final Activity object = DownloadsActivity.this;
        list_data = new ArrayList<>();
        new Thread(new Runnable() {
            @Override
            public void run() {
                adapter = new DownloadAdapter(list_data, object);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        rl_download_list.setAdapter(adapter);
                    }
                });
            }
        }).start();
    }

    @Override
    public void onRefresh() {
        if (internet.isConnectingToInternet()) {
            getDownloads();
        } else {
            swipeRefreshLayout.setRefreshing(false);
            internet.showAlertDialog(DownloadsActivity.this, false);
        }
    }

    private void updatecount(final String type) {

        final ProgressDialog dialog = new ProgressDialog(DownloadsActivity.this);
        dialog.setMessage(getString(R.string.str_requesting));
        dialog.setCancelable(false);
        dialog.show();
        RequestQueue queue = Volley.newRequestQueue(DownloadsActivity.this);
        String requestURL = sp.getString("new_link","") + "update_student_notification_count";
        Log.e("requestURL", "URL" + requestURL);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", "code=" + response);
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response != null) {
                    if (response.equals("204")) {

                        // alertDialog1("No Records Available");
                        Log.e("Error message", "No Records Available");
                    } else {
//                        text_no_data.setVisibility(View.GONE);
//                        adapter = new CarCompanyAdapter(car_company, CarComapnyActivity.this);
//                        recyclerView_company.setAdapter(adapter);
//                        adapter.notifyDataSetChanged();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                //  Toast.makeText(getActivity(), "Poor Internet Coonection", Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
//                type=ANNOUNCEMENT&student_id=73
                params.put("student_id", sp.getString("student_id", ""));
                params.put("type", type);


                Log.e("param", "=" + params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                    VolleyError error = new VolleyError(new String(volleyError.networkResponse.data));
                    volleyError = error;
                    dialog.dismiss();
                }
                //  Toast.makeText(getActivity(),"Poor Internet Coonection",Toast.LENGTH_SHORT).show();
                Log.e("parseNetworkError", "==" + volleyError);
                Log.e("parseNetworkError", "string==" + volleyError.toString());
                Log.e("parseNetworkError", "msg==" + volleyError.getMessage());
                return volleyError;
            }

            @Override
            protected Response<String> parseNetworkResponse(final NetworkResponse response) {
                String responseString = "";
                if (response != null) {
                    responseString = String.valueOf(response.statusCode);
                    // can get more details such as response.headers
                }
                Map<String, String> responseHeaders = response.headers;
                if (response.statusCode == 200) {
                    Response<String> result = Response.success(responseHeaders.get("Content-Type"), HttpHeaderParser.parseCacheHeaders(response));
                    Log.e("Response", "= count =" + parseToString(response));

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

//                            try {
//
//
//                                }
//                            }//for
//                            catch (JSONException e) {
//                                e.printStackTrace();
//                            }

                        }
                    });

                    return result;
                } else {


                    Log.e("Error message", "Login Failed");
                }
                Log.e("responseString", "" + responseString);
                return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
            }

            public String parseToString(NetworkResponse response) {
                String parsed;
                try {
                    parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                } catch (UnsupportedEncodingException e) {
                    parsed = new String(response.data);
                }
                return parsed;
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }//updatecount


    private void getDownloads() {
        swipeRefreshLayout.setRefreshing(false);
        final ProgressDialog dialog = new ProgressDialog(DownloadsActivity.this);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.str_requesting));
        dialog.show();
        RequestQueue queue = Volley.newRequestQueue(DownloadsActivity.this);
        String requestURL = sp.getString("new_link","") + "student_corner";
        Log.e("requestURL", "" + requestURL);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", "code=" + response);
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response != null) {
                    if (response.equals("204")) {
                        txt_message.setVisibility(View.VISIBLE);
                        txt_message.setText("No Records Available");
                        alertDialog1("No Records Available");
                        Log.e("Error message", "No Records Available");
                    } else {
//                        text_no_data.setVisibility(View.GONE);
//                        adapter = new CarCompanyAdapter(car_company, CarComapnyActivity.this);
//                        recyclerView_company.setAdapter(adapter);
//                        adapter.notifyDataSetChanged();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            //    Toast.makeText(getApplicationContext(), "Poor Internet Coonection", Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
//                params.put("class_id",sp.getString("class_id",""));
//                params.put("account_id",sp.getString("account_id",""));

                //params.put("class_id","1");
                params.put("class_id", sp.getString("class_id", ""));
                params.put("account_id", sp.getString("account_id", ""));
                //  params.put("account_id","74");


                Log.e("param", "" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                    VolleyError error = new VolleyError(new String(volleyError.networkResponse.data));
                    volleyError = error;
                    dialog.dismiss();
                }
                // Toast.makeText(getApplicationContext(),"Poor Internet Coonection",Toast.LENGTH_SHORT).show();
                Log.e("parseNetworkError", "==" + volleyError);
                Log.e("parseNetworkError", "string==" + volleyError.toString());
                Log.e("parseNetworkError", "msg==" + volleyError.getMessage());
                return volleyError;
            }

            @Override
            protected Response<String> parseNetworkResponse(final NetworkResponse response) {
                String responseString = "";
                if (response != null) {
                    responseString = String.valueOf(response.statusCode);
                    // can get more details such as response.headers
                }
                Map<String, String> responseHeaders = response.headers;
                if (response.statusCode == 200) {
                    Response<String> result = Response.success(responseHeaders.get("Content-Type"), HttpHeaderParser.parseCacheHeaders(response));
                    Log.e("Response_downloade", "" + parseToString(response));
                    rl_download_list.setVisibility(View.VISIBLE);
                    txt_message.setVisibility(View.GONE);
                    list_data.clear();
                    dialog.dismiss();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                JSONArray JArray = new JSONArray(parseToString(response));

                                for (int i = 0; i < JArray.length(); i++) {
                                    JSONObject jobj_Data = JArray.getJSONObject(i);

                                    HashMap<String, String> offers = new HashMap<String, String>();

                                    offers.put("download_id", jobj_Data.getString("download_id"));
                                    offers.put("download_name", jobj_Data.getString("download_name"));
                                    offers.put("download_path", jobj_Data.getString("download_path"));
                                    offers.put("teacher_name", jobj_Data.getString("teacher_name"));
                                    list_data.add(offers);

                                }//for
                                adapter.notifyDataSetChanged();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });

                    return result;
                } else {

                  /*  rl_download_list.setVisibility(View.GONE);
                    txt_message.setVisibility(View.VISIBLE);
                    txt_message.setText("No Data Available");*/
                    dialog.dismiss();
                    Log.e("Error message", "Login Failed");
                }
                Log.e("responseString", "" + responseString);
                return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
            }

            public String parseToString(NetworkResponse response) {
                String parsed;
                try {
                    parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                } catch (UnsupportedEncodingException e) {
                    parsed = new String(response.data);
                }
                return parsed;
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }//getDownloads

//    private void getDownloads() {
//        swipeRefreshLayout.setRefreshing(false);
//        final ProgressDialog dialog = new ProgressDialog(DownloadsActivity.this);
//        dialog.setMessage("Loading please wait");
//        dialog.setCancelable(true);
//        dialog.show();
//        final RequestQueue queue = Volley.newRequestQueue(DownloadsActivity.this);
//        String requestURL = getString(R.string.web_path) + "get_download_data.php";
//        Log.e("url","=="+requestURL);
//        StringRequest stringRequest = new StringRequest(Request.Method.POST,
//                requestURL.replaceAll(" ", "%20"),
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String s) {
//                        // Display the first 500 characters of the response
//                        Log.e("get_reference_book", ":: Response :: " + s);
//                        try {
//                            JSONObject jObj = new JSONObject(s);
//                            if (jObj.length() > 0) {
//                                if (jObj.getString("result").equals("success")) {
//                                    rl_download_list.setVisibility(View.VISIBLE);
//                                    txt_message.setVisibility(View.GONE);
//
//                                    list_data.clear();
//                                    dialog.dismiss();
//                                    if (jObj.getJSONArray("data").length() > 0) {
//                                        for (int i = 0; i < jObj.getJSONArray("data").length(); i++) {
//                                            JSONObject jobj_Data = jObj.getJSONArray("data").getJSONObject(i);
//                                            HashMap<String, String> grp_data = new HashMap<>();
//                                            grp_data.put("download_id", jobj_Data.getString("download_id"));
//                                            grp_data.put("download_name", jobj_Data.getString("download_name"));
//                                            grp_data.put("download_path", jobj_Data.getString("download_path"));
//                                            grp_data.put("teacher_name", jobj_Data.getString("teacher_name"));
//                                            list_data.add(grp_data);
//                                        }
//                                        adapter.notifyDataSetChanged();
//                                    }
//                                } else if (jObj.getString("result").equals("failed")) {
//                                    rl_download_list.setVisibility(View.GONE);
//                                    txt_message.setVisibility(View.VISIBLE);
//                                    txt_message.setText(jObj.getString("msg"));
//                                    dialog.dismiss();
//                                }
//
//                            }
//
//                        } catch (JSONException e) {
//                            // TODO Auto-generated catch block
//                            if (dialog.isShowing())
//                                dialog.dismiss();
//                            e.printStackTrace();
//
//                        }
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError volleyError) {
//
//                if (dialog.isShowing()) {
//                    dialog.dismiss();
//                }
//                Log.e("Error", "" + volleyError.getMessage());
//            }
//        }){
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                HashMap<String, String> params = new HashMap<String, String>();
//                params.put("class", sp.getString("class", ""));//sp.getString("account_id", "")
//                Log.e("params", "==" + params);
//
//                return params;
//            }
//
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                Map<String, String> params = new HashMap<String, String>();
//                params.put("Content-Type", "application/x-www-form-urlencoded");
//                return params;
//            }
//        };
//        queue.add(stringRequest);
//    }//get_following_details


    private void alertDialog1(String error) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                DownloadsActivity.this);
        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setIcon(R.drawable.daffodils_logo);
        alertDialog.setMessage(error);
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton(getString(R.string.dlg_ok),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }


    private void initView() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.student_corner));
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        rl_download_list = (RecyclerView) findViewById(R.id.rl_download_list);
        txt_message = (TextView) findViewById(R.id.txt_message);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case ReferencesAdapter.REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    Toast.makeText(DownloadsActivity.this, getString(R.string.storage_permission), Toast.LENGTH_SHORT)
                            .show();
                }
            }
            break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


}
