package com.rsl.daffodils.activity.skip;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.rsl.daffodils.R;
import com.rsl.daffodils.activity.student.StudentsHomeActivity;
import com.rsl.daffodils.activity.teacher.TeachersHomeActivity;
import com.rsl.daffodils.utils.ConnectionDetector;
import com.rsl.daffodils.utils.SessionManager;

public class StudentContactUsActivity extends AppCompatActivity implements OnMapReadyCallback {
    BottomNavigationView navigation;
    ConnectionDetector internet;
    static final int send_data = 1;
    SharedPreferences sp;
    SharedPreferences.Editor editor;
    SessionManager session;
    public static Activity activity;
    private GoogleMap map;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stud_contact_us);
        sp = getSharedPreferences("daffodils", 0);
        editor = sp.edit();
        editor.apply();
        initView();
        SupportMapFragment mapFrag = (SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.map1);
        mapFrag.getMapAsync(this);
        session = new SessionManager(getApplicationContext());
        internet = new ConnectionDetector(StudentContactUsActivity.this);
        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigation.getMenu().getItem(1).setChecked(true);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.getUiSettings().setMyLocationButtonEnabled(false);
        try {
            MapsInitializer.initialize(StudentContactUsActivity.this);
            initilizeMap();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void initilizeMap() {
        Double latitude = 18.638114;
        Double longitude =73.7651048;
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 15.0f);
        map.animateCamera(cameraUpdate);
        MarkerOptions marker = new MarkerOptions()
                .position(new LatLng(latitude, longitude)).title(getResources().getString(R.string.map_title));
        map.addMarker(marker);

        // Updates the location and zoom of the MapView

    }//initilizeMap




    private void initView() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.str_contact_us_cap));
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    Intent in;
                    if (sp.getString("role", "").equals("student")) {
                        in = new Intent(StudentContactUsActivity.this, StudentsHomeActivity.class);
                    } else if (sp.getString("role", "").equals("teacher")) {
                        in = new Intent(StudentContactUsActivity.this, TeachersHomeActivity.class);
                    } else
                        in = new Intent(StudentContactUsActivity.this, SkipHomeActivity.class);
                    startActivity(in);
                    finish();
                    return true;
                case R.id.navigation_contacts:
                    return true;
                case R.id.navigation_share:
//                    if (internet.isConnectingToInternet()) {
                    String myText = getResources().getString(R.string.app_text);
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.app_name));
                    sendIntent.putExtra(Intent.EXTRA_TEXT, myText+ " " + getResources().getString(R.string.link));
                    sendIntent.setType("text/plain");
                    startActivityForResult(sendIntent, send_data);
//                    } else {
//                        internet.showAlertDialog(ContactUsActivity.this, false);
//                    }
                    return false;
            }
            return false;
        }
    };

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent in;
        if (sp.getString("role", "").equals("student")) {
            in = new Intent(StudentContactUsActivity.this, StudentsHomeActivity.class);
        } else if (sp.getString("role", "").equals("teacher")) {
            in = new Intent(StudentContactUsActivity.this, TeachersHomeActivity.class);
        } else
            in = new Intent(StudentContactUsActivity.this, SkipHomeActivity.class);
        startActivity(in);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
