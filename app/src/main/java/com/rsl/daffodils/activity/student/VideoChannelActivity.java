package com.rsl.daffodils.activity.student;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.rsl.daffodils.R;
import com.rsl.daffodils.activity.AddStudentActivity;
import com.rsl.daffodils.activity.AllStudentListActivity;
import com.rsl.daffodils.utils.ConnectionDetector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class VideoChannelActivity extends AppCompatActivity {

    private ListView listview;
    ConnectionDetector internet;
    TextView text_msg;
    SharedPreferences sp;
    SharedPreferences.Editor editor;

    ArrayList<HashMap<String, String>> list_data = null;
    Context context;
    Myadapter myadapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_channel);
        sp = getSharedPreferences("daffodils", 0);
        editor = sp.edit();
        editor.apply();
        initView();
        internet = new ConnectionDetector(VideoChannelActivity.this);
        text_msg = (TextView) findViewById(R.id.text_msg);

        list_data = new ArrayList<HashMap<String, String>>();
        listview = (ListView) findViewById(R.id.list_all_Student);
        myadapter = new Myadapter(VideoChannelActivity.this);
        listview.setAdapter(myadapter);

        if (internet.isConnectingToInternet()) {
            display_video();
        } else {
            internet.showAlertDialog(VideoChannelActivity.this, false);
            text_msg.setText(getString(R.string.str_internet_title));
            text_msg.setVisibility(View.VISIBLE);
            listview.setVisibility(View.GONE);
        }

        if (!internet.isConnectingToInternet()) {
            internet.showAlertDialog(VideoChannelActivity.this, false);
        } else {

            display_video();
        }

    }//onCreate


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent in = new Intent(VideoChannelActivity.this, StudentsHomeActivity.class);
        startActivity(in);
        finish();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void initView() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.str_stud_videochannel);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }//initView


    private void display_video() {

        final ProgressDialog dialog = new ProgressDialog(VideoChannelActivity.this);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.str_requesting));
        dialog.show();
        RequestQueue queue = Volley.newRequestQueue(VideoChannelActivity.this);
        String requestURL = sp.getString("new_link","") + "videochannels";
        Log.e("requestURL", "" + requestURL);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", "code=" + response);
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response != null) {
                    if (response.equals("204")) {
                        text_msg.setVisibility(View.VISIBLE);
                        text_msg.setText("No video available");
                      //  alertDialog1("No video Available");
                        Log.e("Error message", "No Records Available");
                    } else {
//                        text_no_data.setVisibility(View.GONE);
//                        adapter = new CarCompanyAdapter(car_company, CarComapnyActivity.this);
//                        recyclerView_company.setAdapter(adapter);
//                        adapter.notifyDataSetChanged();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
               // Toast.makeText(getApplicationContext(), "Poor Internet Coonection", Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                // params.put("id",id);
                Log.e("param", "" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                    VolleyError error = new VolleyError(new String(volleyError.networkResponse.data));
                    volleyError = error;
                    dialog.dismiss();
                }
                // Toast.makeText(getApplicationContext(),"Poor Internet Coonection",Toast.LENGTH_SHORT).show();
                Log.e("parseNetworkError", "==" + volleyError);
                Log.e("parseNetworkError", "string==" + volleyError.toString());
                Log.e("parseNetworkError", "msg==" + volleyError.getMessage());
                return volleyError;
            }

            @Override
            protected Response<String> parseNetworkResponse(final NetworkResponse response) {
                String responseString = "";
                if (response != null) {
                    responseString = String.valueOf(response.statusCode);
                    // can get more details such as response.headers
                }
                Map<String, String> responseHeaders = response.headers;
                if (response.statusCode == 200) {
                    Response<String> result = Response.success(responseHeaders.get("Content-Type"), HttpHeaderParser.parseCacheHeaders(response));
                    Log.e("Respons_videochannels", "==" + parseToString(response));
                    list_data.clear();
                    text_msg.setVisibility(View.GONE);
                    listview.setVisibility(View.VISIBLE);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                JSONArray JArray = new JSONArray(parseToString(response));

                                for (int i = 0; i < JArray.length(); i++) {
                                    JSONObject jobj_Data = JArray.getJSONObject(i);

                                    HashMap<String, String> offers = new HashMap<String, String>();

                                    offers.put("name", jobj_Data.getString("name"));
                                    offers.put("link", jobj_Data.getString("link"));

                                    myadapter.add(offers);

                                }//for

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });

                    return result;
                } else {
//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//
//                    text_msg.setVisibility(View.VISIBLE);
//                    listview.setVisibility(View.GONE);
//                    // text_msg.setText(jObj.getString("msg"));
//                    Log.e("Error message", "Login Failed");
//
//                        }
//                    });
                }
                Log.e("responseString", "" + responseString);
                return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
            }

            public String parseToString(NetworkResponse response) {
                String parsed;
                try {
                    parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                } catch (UnsupportedEncodingException e) {
                    parsed = new String(response.data);
                }
                return parsed;
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }//display_video






    public class Myadapter extends BaseAdapter {


        private LayoutInflater inflater = null;
        Context context;

        public Myadapter(Context context) {

            this.context = context;
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        public void add(HashMap<String, String> hash) {

            list_data.add(hash);
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return list_data.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {


           // LayoutInflater inflater = getLayoutInflater();
            convertView = inflater.inflate(R.layout.row_videos_list, parent, false);


            TextView txt_videos_name = (TextView) convertView.findViewById(R.id.txt_videos_name);

            txt_videos_name.setText(list_data.get(position).get("name"));
            txt_videos_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i=new Intent(VideoChannelActivity.this, YoutubeActivity.class);
                    Log.e("video_name","="+list_data.get(position).get("name"));
                    Log.e("video_link","="+list_data.get(position).get("link"));
                    i.putExtra("video_name",list_data.get(position).get("name"));
                    i.putExtra("video_link",list_data.get(position).get("link"));
                    startActivity(i);
                }
            });

            return convertView;
        }
    }//Myadapter

    private void alertDialog1(String error) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                VideoChannelActivity.this);
        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setIcon(R.drawable.daffodils_logo);
        alertDialog.setMessage(error);
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton(getString(R.string.dlg_ok),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }


}//VideoChannelActivity
