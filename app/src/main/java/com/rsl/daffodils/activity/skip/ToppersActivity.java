package com.rsl.daffodils.activity.skip;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.rsl.daffodils.R;
import com.rsl.daffodils.activity.AllStudentListActivity;
import com.rsl.daffodils.adapter.MenuItemExpandListAdapter;
import com.rsl.daffodils.adapter.MyListAdapter;
import com.rsl.daffodils.model.MenuItemGroup;
import com.rsl.daffodils.model.MenuItemGroupName;
import com.rsl.daffodils.utils.ConnectionDetector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ToppersActivity extends AppCompatActivity {

    private RecyclerView rv_header_list;
    private RecyclerView.LayoutManager mLayoutManager;
    private MyListAdapter mAdapter;
    ArrayList<MenuItemGroupName> group;
    private ConnectionDetector internet;

    SharedPreferences sp;
    SharedPreferences.Editor editor;

    //expandable
    public static ExpandableListView expand_topper_list;
    public static TextView text_msg;
    public static ArrayList<MenuItemGroup> group1;
    public static MenuItemExpandListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_toppers);
        sp = getSharedPreferences("daffodils", 0);
        editor = sp.edit();
        editor.apply();
        initView();
        rv_header_list = (RecyclerView) findViewById(R.id.recycler_view);
        rv_header_list.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);//new LinearLayoutManager(this);
        rv_header_list.setLayoutManager(mLayoutManager);
        group = new ArrayList<MenuItemGroupName>();
        group1 = new ArrayList<MenuItemGroup>();
        mAdapter = new MyListAdapter(group, ToppersActivity.this);
        rv_header_list.setAdapter(mAdapter);

        expand_topper_list = (ExpandableListView) findViewById(R.id.topper_list);


        expand_topper_list.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            public boolean onGroupClick(ExpandableListView arg0, View itemView, int itemPosition, long itemId) {
                expand_topper_list.expandGroup(itemPosition);
                return true;
            }
        });

        internet = new ConnectionDetector(ToppersActivity.this);
        if (internet.isConnectingToInternet()){
            getAllMenu();
            // getMainData();
        } else {
            internet.showAlertDialog(ToppersActivity.this, false);
        }
    }//onCreate

    private void initView() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.topper_list));
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        text_msg = (TextView) findViewById(R.id.text_msg);

    }

    private void getAllMenu() {

        final ProgressDialog dialog = new ProgressDialog(ToppersActivity.this);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.str_requesting));
        dialog.show();
        RequestQueue queue = Volley.newRequestQueue(ToppersActivity.this);
        String requestURL = sp.getString("new_link","") + "yearlist";
        Log.e("requestURL", "" + requestURL);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", "code=" + response);
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response != null) {
                    if (response.equals("204")) {
                        text_msg.setVisibility(View.VISIBLE);
                          text_msg.setText("No Records Available");
                        Log.e("Error message", "No Records Available");
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }


            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                // params.put("id",id);
                Log.e("param", "" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                    VolleyError error = new VolleyError(new String(volleyError.networkResponse.data));
                    volleyError = error;
                    dialog.dismiss();
                }

                Log.e("parseNetworkError", "==" + volleyError);
                Log.e("parseNetworkError", "string==" + volleyError.toString());
                Log.e("parseNetworkError", "msg==" + volleyError.getMessage());
                return volleyError;
            }

            @Override
            protected Response<String> parseNetworkResponse(final NetworkResponse response) {
                String responseString = "";
                if (response != null) {
                    responseString = String.valueOf(response.statusCode);
                    // can get more details such as response.headers
                }
                Map<String, String> responseHeaders = response.headers;
                if (response.statusCode == 200) {
                    Response<String> result = Response.success(responseHeaders.get("Content-Type"), HttpHeaderParser.parseCacheHeaders(response));
                    Log.e("Response_yearlist", "" + parseToString(response));


                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {

                                JSONArray JArray = new JSONArray(parseToString(response));

                                for (int i = 0; i < JArray.length(); i++) {
                                    JSONObject jobj_Data = JArray.getJSONObject(i);

                                    MenuItemGroupName grp_data = new MenuItemGroupName();
                                        grp_data.setMenu_item_group_id(jobj_Data.getString("year_id"));
                                        grp_data.setMenu_item_group_name(jobj_Data.getString("year"));

                                        group.add(grp_data);
                                        Log.e("getMenu_items", "==" + group.get(i).getMenu_item_group_name());
                                        mAdapter.notifyDataSetChanged();


                                }//for

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });

                    return result;
                } else {


                    // text_msg.setText(jObj.getString("msg"));
                    Log.e("Error message", "Login Failed");
                }
                Log.e("responseString", "" + responseString);
                return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
            }

            public String parseToString(NetworkResponse response) {
                String parsed;
                try {
                    parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                } catch (UnsupportedEncodingException e) {
                    parsed = new String(response.data);
                }
                return parsed;
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }//getallmenu


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }


}
