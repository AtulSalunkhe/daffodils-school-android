package com.rsl.daffodils.activity.admin;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.rsl.daffodils.R;
import com.rsl.daffodils.activity.AddStudentActivity;
import com.rsl.daffodils.model.Constants;
import com.rsl.daffodils.utils.ConnectionDetector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class FeedbackListActivity extends AppCompatActivity {


    private ListView listview;
    ConnectionDetector internet;
    TextView text_msg;
    ArrayList<HashMap<String, String>> list_data = null;
    Context context;
    Myadapter myadapter;
    SharedPreferences sp;
    SharedPreferences.Editor editor;
    Button btn_loadmore;
    View footerView;
    RelativeLayout footer_loadmore;
    private String total_list_count;
    int count = 1;
    int check_count;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback_list);
        Constants.teacherFeedBackActivity = true;
        sp = getSharedPreferences("daffodils", 0);
        editor = sp.edit();
        initView();
        internet = new ConnectionDetector(FeedbackListActivity.this);
        list_data = new ArrayList<HashMap<String, String>>();
        listview = (ListView) findViewById(R.id.list_all_Student);
        myadapter = new Myadapter();
        listview.setAdapter(myadapter);
        footerView = FeedbackListActivity.this.getLayoutInflater().inflate(R.layout.row_lode_more, listview, false);
        footer_loadmore = (RelativeLayout) footerView.findViewById(R.id.footer_loadmore);
        btn_loadmore = (Button) footerView.findViewById(R.id.btn_loadmore);


//        if (internet.isConnectingToInternet()) {
//            display_feedback(count);
//        } else {
//            internet.showAlertDialog(FeedbackListActivity.this, false);
//            text_msg.setText(getString(R.string.str_internet_title));
//            text_msg.setVisibility(View.VISIBLE);
//        }


        btn_loadmore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!internet.isConnectingToInternet()) {
                    internet.showAlertDialog(FeedbackListActivity.this, false);
                } else {
                    Log.e("load_more", "" + Integer.parseInt(total_list_count)
                            + list_data.size());
                    Log.e("list size", "" + list_data.size());
                    if (Integer.parseInt(total_list_count) == list_data.size()) {
                        btn_loadmore.setVisibility(View.GONE);
                        Log.e("If", "loadmore if");
                    } else {
                        count = count + 1;
                        Log.e("else", "loadmore call web_service");
                        display_feedback(count);
                    }

                }

            }//onClick
        });

    }//onCreate

    @Override
    public void onResume() {
        super.onResume();
        if (!internet.isConnectingToInternet()) {
            internet.showAlertDialog(FeedbackListActivity.this, false);
        } else {
            display_feedback(count);
        }
    }//onResume


    private void initView() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.str_feedback_list);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        text_msg = (TextView) findViewById(R.id.text_msg);

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }




    private void display_feedback(final int enter_count) {

        final ProgressDialog dialog = new ProgressDialog(FeedbackListActivity.this);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.str_requesting));
        dialog.show();
        RequestQueue queue = Volley.newRequestQueue(FeedbackListActivity.this);
        String requestURL = sp.getString("new_link","") + "get_all_feedback/" + String.valueOf(enter_count)+"/"+sp.getString("id","");
        Log.e("requestURL", "feedback" + requestURL);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", "code=" + response);
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response != null) {
                    if (response.equals("204")) {
                        alertDialog("No feedback available");
                        listview.setVisibility(View.GONE);
                        text_msg.setVisibility(View.VISIBLE);
                        text_msg.setText("No Record Found");
                        Log.e("Error message", "No Records Available");
                    } else {

                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                //Toast.makeText(getApplicationContext(), "Poor Internet Coonection", Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                // params.put("id",id);

                Log.e("param", "" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                    VolleyError error = new VolleyError(new String(volleyError.networkResponse.data));
                    volleyError = error;
                    dialog.dismiss();
                }
                // Toast.makeText(getApplicationContext(),"Poor Internet Coonection",Toast.LENGTH_SHORT).show();
                Log.e("parseNetworkError", "==" + volleyError);
                Log.e("parseNetworkError", "string==" + volleyError.toString());
                Log.e("parseNetworkError", "msg==" + volleyError.getMessage());
                return volleyError;
            }

            @Override
            protected Response<String> parseNetworkResponse(final NetworkResponse response) {
                String responseString = "";
                if (response != null) {
                    responseString = String.valueOf(response.statusCode);
                    // can get more details such as response.headers
                }
                Map<String, String> responseHeaders = response.headers;
                if (response.statusCode == 200) {
                    Response<String> result = Response.success(responseHeaders.get("Content-Type"), HttpHeaderParser.parseCacheHeaders(response));
                    Log.e("Res_get_all_feedback", "" + parseToString(response));
                    list_data.clear();
                    listview.setVisibility(View.VISIBLE);
                    text_msg.setVisibility(View.GONE);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                JSONArray JArray = new JSONArray(parseToString(response));

                                for (int i = 0; i < JArray.length(); i++) {
                                    JSONObject jobj_Data = JArray.getJSONObject(i);

                                    HashMap<String, String> offers = new HashMap<String, String>();


                                    offers.put("feedback", jobj_Data.getString("feedback"));
                                    offers.put("email", jobj_Data.getString("email"));
                                    offers.put("name", jobj_Data.getString("name"));
                                    offers.put("teacher_name", jobj_Data.getString("teacher_name"));
                                    offers.put("subject", jobj_Data.getString("subject"));

                                    myadapter.add(offers);

                                }//for

                                if (check_count <= 20) {
                                    btn_loadmore.setVisibility(View.GONE);
                                } else if (Integer.parseInt(total_list_count) == list_data.size()) {
                                    btn_loadmore.setVisibility(View.GONE);

                                } else {
                                    btn_loadmore.setVisibility(View.VISIBLE);
                                    listview.addFooterView(footerView);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                    });
                    return result;
                } else {

                    Log.e("Error message", "Login Failed");
                }
                Log.e("responseString", "" + responseString);
                return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
            }

            public String parseToString(NetworkResponse response) {
                String parsed;
                try {
                    parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                } catch (UnsupportedEncodingException e) {
                    parsed = new String(response.data);
                }
                return parsed;
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 4, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }//add feedback


    private void alertDialog(String errer) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                FeedbackListActivity.this);
        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setMessage(errer);
        alertDialog.setCancelable(false);
        alertDialog.setIcon(R.drawable.daffodils_logo);
        alertDialog.setPositiveButton(getString(R.string.dlg_ok),
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                    }
                });

        alertDialog.show();
    }//alertDialog

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Constants.teacherFeedBackActivity = false;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);

    }
    public class Myadapter extends BaseAdapter {


        public void add(HashMap<String, String> hash) {

            list_data.add(hash);
            Log.e("data", "" + hash);
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return list_data.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {


            LayoutInflater inflater = getLayoutInflater();
            View list = inflater.inflate(R.layout.feedbacklist_row, parent, false);


            TextView name = (TextView) list.findViewById(R.id.text_name);
            TextView teacher_name = (TextView) list.findViewById(R.id.text_teacher);
            TextView subject = (TextView) list.findViewById(R.id.text_subject);


            name.setText(list_data.get(position).get("name"));
            teacher_name.setText(list_data.get(position).get("teacher_name"));
            subject.setText(list_data.get(position).get("subject"));


            list.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(FeedbackListActivity.this, FeedbackDeatilsActivity.class);
                    intent.putExtra("feedback", list_data.get(position).get("feedback"));
                    intent.putExtra("email", list_data.get(position).get("email"));
                    intent.putExtra("name", list_data.get(position).get("name"));
                    intent.putExtra("teacher_name", list_data.get(position).get("teacher_name"));
                    intent.putExtra("subject", list_data.get(position).get("subject"));

                    startActivity(intent);
                }
            });


            return list;
        }
    }//Myadapter


    private void alertDialog1(String errer) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                FeedbackListActivity.this);
        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setMessage(errer);
        alertDialog.setCancelable(false);
        alertDialog.setIcon(R.drawable.daffodils_logo);
        alertDialog.setPositiveButton(getString(R.string.dlg_ok),
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                    }
                });

        alertDialog.show();
    }//alertDialog1

}//FeedbackListActivity
