package com.rsl.daffodils.activity.student;

import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.rsl.daffodils.R;
import com.rsl.daffodils.activity.AllStudentListActivity;
import com.rsl.daffodils.adapter.ClenderpagerAdapter;
import com.rsl.daffodils.adapter.NotificationpagerAdapter;
import com.rsl.daffodils.fragment.student.HolidayFragment;
import com.rsl.daffodils.model.Constants;
import com.rsl.daffodils.utils.ConnectionDetector;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class CalenderActivity extends AppCompatActivity {

    TabLayout tabLayout;
    SharedPreferences sp;
    SharedPreferences.Editor editor;
    private ConnectionDetector internet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calender);
        sp = getSharedPreferences("daffodils", 0);
        editor = sp.edit();
        editor.apply();
        initView();
        internet = new ConnectionDetector(CalenderActivity.this);
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("Events"));
        tabLayout.addTab(tabLayout.newTab().setText("Holidays"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);


        final ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        final ClenderpagerAdapter adapter = new ClenderpagerAdapter
                (CalenderActivity.this.getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        viewPager.setCurrentItem(0);
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        getupdatecount("EVENT");

        if (internet.isConnectingToInternet()) {
            Bundle extras = getIntent().getExtras();
            Log.e("fcm oncreate", "type : " + extras.getString("type"));
            Log.e("fcm oncreate", "out : " + extras.getString("from_fcm"));
            if (extras != null) {
                Log.e("fcm oncreate", "in");
                if (extras.containsKey("type")) {

                    editor.putString("student_id", getIntent().getExtras().getString("student_id")).apply();
                    Log.e("id",""+getIntent().getExtras().getString("student_id"));
                    editor.putString("account_id",getIntent().getExtras().getString("account_id")).apply();
                    Log.e("ac id",""+getIntent().getExtras().getString("account_id"));
                    editor.putString("student_image", getIntent().getExtras().getString("student_image")).apply();
                    Log.e("image",""+getIntent().getExtras().getString("student_image"));
                    editor.putString("parent_mobile", getIntent().getExtras().getString("parent_mobile")).apply();
                    Log.e("parent_mobile",""+getIntent().getExtras().getString("parent_mobile"));
                    editor.putString("student_name1",getIntent().getExtras().getString("student_name1")).apply();
                    Log.e("student_name",""+getIntent().getExtras().getString("student_name1"));
                    editor.putString("parent_email", getIntent().getExtras().getString("parent_email")).apply();
                    Log.e("parent_email",""+getIntent().getExtras().getString("parent_email"));
                    editor.putString("date_of_birth", getIntent().getExtras().getString("date_of_birth")).apply();
                    Log.e("dob",""+getIntent().getExtras().getString("date_of_birth"));
                    editor.putString("gender_id", getIntent().getExtras().getString("gender_id")).apply();
                    Log.e("gender_id",""+getIntent().getExtras().getString("gender_id"));
                    editor.putString("student_class1",getIntent().getExtras().getString("student_class1")).apply();
                    Log.e("class",""+getIntent().getExtras().getString("student_class1"));
                    editor.putString("class_id", getIntent().getExtras().getString("class_id")).apply();
                    Log.e("class_id",""+getIntent().getExtras().getString("class_id"));
                    editor.commit();

                    if (getIntent().getStringExtra("type").equals("event")) {
                        new Handler().postDelayed(
                                new Runnable() {
                                    @Override
                                    public void run() {
                                        tabLayout.getTabAt(0).select();

                                    }
                                }, 100);

                    } else {
                        new Handler().postDelayed(
                                new Runnable() {
                                    @Override
                                    public void run() {
                                        tabLayout.getTabAt(1).select();

                                    }
                                }, 100);
                    }
                }
            } else {
                Log.e("finish", "direct finish");
                finish();
            }

            Log.e("onNewIntent", "onNewIntent");
        } else {
          //  internet.showAlertDialog(CalenderActivity.this, false);
        }

    }//onCreate




    @Override
    protected void onPause() {
        super.onPause();
        Constants.activity_calender = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Constants.activity_calender = false;
    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        Log.e("Intent", "xczxc===" + intent.getStringExtra("type"));
        if (internet.isConnectingToInternet()) {
            if (intent.getStringExtra("type").equals("event")) {

                new Handler().postDelayed(
                        new Runnable() {
                            @Override
                            public void run() {
                                tabLayout.getTabAt(0).select();

                            }
                        }, 100);

            } else {
                new Handler().postDelayed(
                        new Runnable() {
                            @Override
                            public void run() {
                                tabLayout.getTabAt(1).select();

                            }
                        }, 100);
            }


            Log.e("onNewIntent", "onNewIntent");
        } else {
           // internet.showAlertDialog(CalenderActivity.this, false);
        }
    }

    private void initView() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.str_stud_calender));
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }//initView


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Bundle extras = getIntent().getExtras();
        Log.e("fcm", "out : " + extras.getString("from_fcm"));
        if (extras != null) {
            Log.e("fcm", "in");
            if (extras.containsKey("from_fcm")) {
                if (getIntent().getStringExtra("from_fcm").equals("true")) {
                    if (sp.getString("role", "").equals("student")) {
                        Log.e("role","+++++");
                        if(sp.getBoolean("select_student",false)){
                            Log.e("select_student","+++++");
                            Intent in = new Intent(CalenderActivity.this, StudentsHomeActivity.class);
                            startActivity(in);
                            finish();
                        }else {
                            Intent in = new Intent(CalenderActivity.this, AllStudentListActivity.class);
                            startActivity(in);
                            finish();
                        }

                    }
                }
            }else {
                Intent in = new Intent(CalenderActivity.this, StudentsHomeActivity.class);
                startActivity(in);
                finish();
            }
        } else {
            Log.e("finish", "direct finish");
            finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }//onOptionsItemSelected


    private void getupdatecount(final String type) {
        final ProgressDialog dialog = new ProgressDialog(CalenderActivity.this);
        dialog.setMessage(getString(R.string.str_requesting));
        dialog.setCancelable(false);
        dialog.show();
        RequestQueue queue = Volley.newRequestQueue(CalenderActivity.this);
        String requestURL = sp.getString("new_link","") + "update_student_notification_count";
        Log.e("requestURL", "update_student" + requestURL);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", "code=" + response);
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response != null) {
                    if (response.equals("204")) {
                        // txt_message.setVisibility(View.VISIBLE);
                        // txt_message.setText("No Records Available");
                        // alertDialog1("No Records Available");
                        Log.e("Error message", "No Records Available");
                    } else {
//                        text_no_data.setVisibility(View.GONE);
//                        adapter = new CarCompanyAdapter(car_company, CarComapnyActivity.this);
//                        recyclerView_company.setAdapter(adapter);
//                        adapter.notifyDataSetChanged();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                //  Toast.makeText(getActivity(), "Poor Internet Coonection", Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
//                type=ANNOUNCEMENT&student_id=73
                params.put("student_id", sp.getString("student_id", ""));
                params.put("type", type);


                Log.e("param", "=" + params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                    VolleyError error = new VolleyError(new String(volleyError.networkResponse.data));
                    volleyError = error;
                    dialog.dismiss();
                }
                //  Toast.makeText(getActivity(),"Poor Internet Coonection",Toast.LENGTH_SHORT).show();
                Log.e("parseNetworkError", "==" + volleyError);
                Log.e("parseNetworkError", "string==" + volleyError.toString());
                Log.e("parseNetworkError", "msg==" + volleyError.getMessage());
                return volleyError;
            }

            @Override
            protected Response<String> parseNetworkResponse(final NetworkResponse response) {
                String responseString = "";
                if (response != null) {
                    responseString = String.valueOf(response.statusCode);
                    // can get more details such as response.headers
                }
                Map<String, String> responseHeaders = response.headers;
                if (response.statusCode == 200) {
                    Response<String> result = Response.success(responseHeaders.get("Content-Type"), HttpHeaderParser.parseCacheHeaders(response));
                    Log.e("Response", "= count =" + parseToString(response));

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

//                            try {
//
//
//                                }
//                            }//for
//                            catch (JSONException e) {
//                                e.printStackTrace();
//                            }

                        }
                    });

                    return result;
                } else {


                    Log.e("Error message", "Login Failed");
                }
                Log.e("responseString", "" + responseString);
                return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
            }

            public String parseToString(NetworkResponse response) {
                String parsed;
                try {
                    parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                } catch (UnsupportedEncodingException e) {
                    parsed = new String(response.data);
                }
                return parsed;
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }//getupdatecount


}//CalenderActivity
