package com.rsl.daffodils.activity.student;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.rsl.daffodils.R;
import com.rsl.daffodils.activity.AllStudentListActivity;
import com.rsl.daffodils.activity.FeedbackActivity;
import com.rsl.daffodils.adapter.ImageAdapter;
import com.rsl.daffodils.model.Gallery;
import com.rsl.daffodils.utils.ConnectionDetector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class GalleryActivity extends AppCompatActivity {

    GridView gridview;
    ConnectionDetector internet;
    ImageAdapter imageAdapter;
    SharedPreferences sp;
    SharedPreferences.Editor editor;
    public static ArrayList<Gallery> list_data = null;
    public static ArrayList<HashMap<String, String>> list_gallery = null;
    TextView txt_message;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);
        internet = new ConnectionDetector(GalleryActivity.this);
        sp = getSharedPreferences("daffodils", 0);
        editor = sp.edit();
        initView();

        gridview = (GridView) findViewById(R.id.gridview);
        list_data = new ArrayList<>();
        list_gallery = new ArrayList<HashMap<String, String>>();

        imageAdapter = new ImageAdapter();


//        if (internet.isConnectingToInternet()) {
//            display_items();
//        } else {
//            internet.showAlertDialog(GalleryActivity.this, true);
//        }


    }//onCreate


    private void initView() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.gallery));
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        txt_message = (TextView) findViewById(R.id.txt_message);
    }//initView

    @Override
    protected void onResume() {
        super.onResume();
        if (internet.isConnectingToInternet()) {
            display_items();
        } else {
            internet.showAlertDialog(GalleryActivity.this, false);
            txt_message.setText(getString(R.string.str_internet_title));
            txt_message.setVisibility(View.VISIBLE);
        }

    }



    private void display_items() {

        final ProgressDialog dialog = new ProgressDialog(GalleryActivity.this);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.str_requesting));
        dialog.show();
        RequestQueue queue = Volley.newRequestQueue(GalleryActivity.this);
        String requestURL = sp.getString("new_link","") + "get_gallery/" + "image";
        Log.e("requestURL", "" + requestURL);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", "code=" + response);
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response != null) {
                    if (response.equals("204")) {
                        // text_msg.setVisibility(View.VISIBLE);
                        //  text_msg.setText("No Records Available");
                        alertDialog1("No records available");
                        Log.e("Error message", "No Records Available");
                    } else {
//                        text_no_data.setVisibility(View.GONE);
//                        adapter = new CarCompanyAdapter(car_company, CarComapnyActivity.this);
//                        recyclerView_company.setAdapter(adapter);
//                        adapter.notifyDataSetChanged();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            //    Toast.makeText(getApplicationContext(), "Poor Internet Coonection", Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
//                params.put("type", "image");
                Log.e("param", "" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                    VolleyError error = new VolleyError(new String(volleyError.networkResponse.data));
                    volleyError = error;
                    dialog.dismiss();
                }
                // Toast.makeText(getApplicationContext(),"Poor Internet Coonection",Toast.LENGTH_SHORT).show();
                Log.e("parseNetworkError", "==" + volleyError);
                Log.e("parseNetworkError", "string==" + volleyError.toString());
                Log.e("parseNetworkError", "msg==" + volleyError.getMessage());
                return volleyError;
            }

            @Override
            protected Response<String> parseNetworkResponse(final NetworkResponse response) {
                String responseString = "";
                if (response != null) {
                    responseString = String.valueOf(response.statusCode);
                    // can get more details such as response.headers
                }
                Map<String, String> responseHeaders = response.headers;
                if (response.statusCode == 200) {
                    Response<String> result = Response.success(responseHeaders.get("Content-Type"), HttpHeaderParser.parseCacheHeaders(response));
                    Log.e("get_gallery", "" + parseToString(response));
                    list_data.clear();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                JSONArray JArray = new JSONArray(parseToString(response));

                                for (int i = 0; i < JArray.length(); i++) {

                                    JSONObject jobj_Data = JArray.getJSONObject(i);
                                    Gallery gallery = new Gallery();

                                    gallery.setTitle(jobj_Data.getString("image_title"));
                                    gallery.setDescription(jobj_Data.getString("description"));
                                    gallery.setDate(jobj_Data.getString("date"));

                                    JSONArray jsonArray = jobj_Data.getJSONArray("image");
                                    ArrayList<HashMap<String, String>> gallerydetails = new ArrayList<>();
                                    if (jsonArray.length() > 0) {
                                        for (int j = 0; j < jsonArray.length(); j++) {
                                            JSONObject job = jsonArray.getJSONObject(j);
                                            HashMap<String, String> data = new HashMap<>();
                                            data.put("image", job.getString("image"));
                                            gallerydetails.add(data);

                                        }

                                    }

                                    gallery.setGallery_images(gallerydetails);
                                    list_data.add(gallery);
                                    gridview.setAdapter(imageAdapter);
                                    //  imageAdapter.add(gallery);


                                }//for

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });

                    return result;
                } else {


                    // text_msg.setText(jObj.getString("msg"));
                    Log.e("Error message", "Login Failed");
                }
                Log.e("responseString", "" + responseString);
                return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
            }

            public String parseToString(NetworkResponse response) {
                String parsed;
                try {
                    parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                } catch (UnsupportedEncodingException e) {
                    parsed = new String(response.data);
                }
                return parsed;
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }//display_items


    private void alertDialog1(String error) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                GalleryActivity.this);
        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setIcon(R.drawable.daffodils_logo);
        alertDialog.setMessage(error);
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton(getString(R.string.dlg_ok),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent in = new Intent(GalleryActivity.this, StudentsHomeActivity.class);
        startActivity(in);
        finish();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }


    public class ImageAdapter extends BaseAdapter {


        public void add(Gallery hash) {

            list_data.add(hash);
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return list_data.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {


            LayoutInflater inflater = getLayoutInflater();
            View list = inflater.inflate(R.layout.row_grid_data, parent, false);
            ImageView imagemain = (ImageView) list.findViewById(R.id.img_background);
            TextView text = (TextView) list.findViewById(R.id.group_name);

            list_gallery = list_data.get(position).getGallery_images();
            Log.e("list_gallery--", "" + list_gallery.toString());
            Log.e("list_gallerysize", "" + list_gallery.size());
            String imagepath;
            Log.e("list_gallerysize", "" + sp.getString("new_gallerylink",""));


            if (list_data.get(position).getGallery_images() != null) {
                imagepath =  sp.getString("new_gallerylink","")+list_data.get(position).getGallery_images().get(0).get("image");
                Glide.with(GalleryActivity.this).load(imagepath).placeholder(R.drawable.daffodils_logo).into(imagemain);
            } else
                Glide.with(GalleryActivity.this).load(R.drawable.daffodils_logo).placeholder(R.drawable.daffodils_logo);

            text.setText(list_data.get(position).getTitle());

            list.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(GalleryActivity.this, GalleryPhotoView.class);
                    list_gallery = list_data.get(position).getGallery_images();
                    intent.putExtra("name", list_data.get(position).getTitle());
                    intent.putExtra("position", String.valueOf(position));
                    Log.e("position", "" + position);

                    startActivity(intent);

                }
            });


            //   Log.e("image",""+imagepath);


            return list;
        }
    }//ImageAdapter

}//GalleryActivity
