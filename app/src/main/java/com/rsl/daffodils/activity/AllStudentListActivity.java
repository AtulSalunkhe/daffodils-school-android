package com.rsl.daffodils.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.rsl.daffodils.LoginActivity;
import com.rsl.daffodils.R;
import com.rsl.daffodils.activity.skip.SkipHomeActivity;
import com.rsl.daffodils.activity.student.StudentsHomeActivity;
import com.rsl.daffodils.utils.ConnectionDetector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.rsl.daffodils.R.id.text_no_data;

public class AllStudentListActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener{


//    implements SwipeRefreshLayout.OnRefreshListener

    private ListView listview;
    ConnectionDetector internet;
  //  TextView text_msg;
    SharedPreferences sp;
    SharedPreferences.Editor editor;
    Button btn_add_student;
    private SwipeRefreshLayout swipeRefreshLayout;


    ArrayList<HashMap<String, String>> list_data = null;
    Context context;
    Myadapter myadapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_student_list);
        initView();
        sp = getSharedPreferences("daffodils", 0);
        editor = sp.edit();
        editor.apply();
        swipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary, R.color.colorAccent, R.color.event_holiday_color, R.color.event_color);
        internet = new ConnectionDetector(AllStudentListActivity.this);
        //text_msg = (TextView) findViewById(R.id.text_msg);
        btn_add_student = (Button) findViewById(R.id.btn_add_student);
        btn_add_student.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AllStudentListActivity.this, AddStudentActivity.class);
                startActivity(intent);

            }
        });
        list_data = new ArrayList<HashMap<String, String>>();
        listview = (ListView) findViewById(R.id.list_all_Student);
        listview.setDividerHeight(0);
        myadapter = new Myadapter();
        listview.setAdapter(myadapter);

    }//onCreate



    @Override
    public void onRefresh() {
        if (internet.isConnectingToInternet()) {
            display_student(sp.getString("stud_id", ""));
            Log.e("onrefresh", "" + sp.getString("stud_id", ""));
        } else {
            swipeRefreshLayout.setRefreshing(false);
            internet.showAlertDialog(AllStudentListActivity.this, false);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void initView() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.str_cap_student);
    }//initView


    @Override
    public void onResume() {
        super.onResume();
          if (!internet.isConnectingToInternet()) {
            internet.showAlertDialog(AllStudentListActivity.this, false);
        } else {
              Log.e("resume","on resume");
            display_student(sp.getString("stud_id", ""));
            // Log.e("id","--"+sp.getString("id",""));
            Log.e("id", "--" + sp.getString("stud_id", ""));
        }
    }//onResume





    private void display_student(final String id) {
        swipeRefreshLayout.setRefreshing(false);
        final ProgressDialog dialog = new ProgressDialog(AllStudentListActivity.this);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.str_requesting));
        dialog.show();
        RequestQueue queue = Volley.newRequestQueue(AllStudentListActivity.this);
        String requestURL = sp.getString("new_link","") + "get_parentwise_studentlist/" + id;
        Log.e("requestURL", "" + requestURL);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", "code=" + response);
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response != null) {
                    if (response.equals("204")) {
                        alertDialog1("No Records Available");
                        Log.e("Error message", "No Records Available");
                    } else {

                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                //Toast.makeText(getApplicationContext(), "Poor Internet Coonection", Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                // params.put("id",id);
                Log.e("param", "" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                    VolleyError error = new VolleyError(new String(volleyError.networkResponse.data));
                    volleyError = error;
                    dialog.dismiss();
                }
                // Toast.makeText(getApplicationContext(),"Poor Internet Coonection",Toast.LENGTH_SHORT).show();
                Log.e("parseNetworkError", "==" + volleyError);
                Log.e("parseNetworkError", "string==" + volleyError.toString());
                Log.e("parseNetworkError", "msg==" + volleyError.getMessage());
                return volleyError;
            }

            @Override
            protected Response<String> parseNetworkResponse(final NetworkResponse response) {
                String responseString = "";
                if (response != null) {
                    responseString = String.valueOf(response.statusCode);
                    // can get more details such as response.headers
                }
                Map<String, String> responseHeaders = response.headers;
                if (response.statusCode == 200) {
                    Response<String> result = Response.success(responseHeaders.get("Content-Type"), HttpHeaderParser.parseCacheHeaders(response));
                    Log.e("Response_add_stude", "" + parseToString(response));
                    list_data.clear();
                 //   text_msg.setVisibility(View.GONE);
                    listview.setVisibility(View.VISIBLE);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                JSONArray JArray = new JSONArray(parseToString(response));

                                for (int i = 0; i < JArray.length(); i++) {
                                    JSONObject jobj_Data = JArray.getJSONObject(i);

                                    HashMap<String, String> offers = new HashMap<String, String>();

                                    offers.put("student_image", jobj_Data.getString("student_image"));
                                    offers.put("student_id", jobj_Data.getString("student_id"));
                                    offers.put("parent_id", jobj_Data.getString("parent_id"));
                                    offers.put("student_name", jobj_Data.getString("student_name"));
                                    offers.put("date_of_birth", jobj_Data.getString("date_of_birth"));
                                    offers.put("gender_id", jobj_Data.getString("gender_id"));
                                    offers.put("class_id", jobj_Data.getString("class_id"));
                                    offers.put("isread", jobj_Data.getString("isread"));
                                    offers.put("account_id", jobj_Data.getString("account_id"));
                                    offers.put("parent_name", jobj_Data.getString("parent_name"));
                                    offers.put("parent_email", jobj_Data.getString("parent_email"));
                                    offers.put("parent_mobile", jobj_Data.getString("parent_mobile"));
                                    offers.put("student_class", jobj_Data.getString("student_class"));
                                    sp.edit().putString("class_id", jobj_Data.getString("class_id")).apply();

                                  //  sp.edit().putString("student_class1", jobj_Data.getString("class_id")).apply();
                                    Log.e("class_id","allstudentlist"+jobj_Data.getString("class_id"));
                                    Log.e("student_id","allstudentlist"+jobj_Data.getString("student_id"));
                                    Log.e("account_id","allstudentlist"+jobj_Data.getString("account_id"));
                                    Log.e("student_image","allstudentlist"+jobj_Data.getString("student_image"));
                                    Log.e("parent_mobile","allstudentlist"+jobj_Data.getString("parent_mobile"));
                                    Log.e("student_class","allstudentlist"+jobj_Data.getString("student_class"));


                                    myadapter.add(offers);
                                   // Log.e("","");

                                }//for

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });

                    return result;
                } else {

//                    text_msg.setVisibility(View.VISIBLE);
                //    listview.setVisibility(View.GONE);
                    // text_msg.setText(jObj.getString("msg"));
                    Log.e("Error message", "Login Failed");
                }
                Log.e("responseString", "" + responseString);
                return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
            }

            public String parseToString(NetworkResponse response) {
                String parsed;
                try {
                    parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                } catch (UnsupportedEncodingException e) {
                    parsed = new String(response.data);
                }
                return parsed;
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 4, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }//display_student


    public class Myadapter extends BaseAdapter {


        public void add(HashMap<String, String> hash) {

            list_data.add(hash);
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return list_data.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {


            LayoutInflater inflater = getLayoutInflater();
            View list = inflater.inflate(R.layout.all_student_list_row, parent, false);


            TextView student_name = (TextView) list.findViewById(R.id.text_student_name);
            TextView student_class = (TextView) list.findViewById(R.id.text_student_class);

            student_name.setText(list_data.get(position).get("student_name"));
            student_class.setText(list_data.get(position).get("student_class"));

            list.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(AllStudentListActivity.this, StudentsHomeActivity.class);
                    sp.edit().putBoolean("select_student",true).apply();
                    sp.edit().putString("student_id", list_data.get(position).get("student_id")).apply();
                  //  sp.edit().putString("class_id", list_data.get(position).get("class_id")).apply();
                    sp.edit().putString("account_id", list_data.get(position).get("account_id")).apply();
                    sp.edit().putString("student_image", list_data.get(position).get("student_image")).apply();
                    sp.edit().putString("parent_mobile", list_data.get(position).get("parent_mobile")).apply();
                    sp.edit().putString("student_name1",list_data.get(position).get("student_name")).apply();
                    sp.edit().putString("parent_email", list_data.get(position).get("parent_email")).apply();
                    sp.edit().putString("date_of_birth", list_data.get(position).get("date_of_birth")).apply();
                    sp.edit().putString("gender_id", list_data.get(position).get("gender_id")).apply();
                    sp.edit().putString("student_class1", list_data.get(position).get("student_class")).apply();
                    sp.edit().putString("class_id", list_data.get(position).get("class_id")).apply();
//                    intent.putExtra("student_class1",list_data.get(position).get("student_class"));
                    Log.e("print",""+list_data.get(position).get("student_class"));
                    startActivity(intent);
                    finish();
                }
            });

            return list;
        }
    }//Myadapter

    private void alertDialog1(String error) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                AllStudentListActivity.this);
        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setIcon(R.drawable.daffodils_logo);
        alertDialog.setMessage(error);
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton(getString(R.string.dlg_ok),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }


}//AllStudentListActivity
