package com.rsl.daffodils.activity.student;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.camera.CropImageIntentBuilder;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.signature.StringSignature;
import com.rsl.daffodils.BuildConfig;
import com.rsl.daffodils.R;
import com.rsl.daffodils.activity.FeedbackActivity;
import com.rsl.daffodils.utils.ConnectionDetector;
import com.rsl.daffodils.utils.Utility;
import com.soundcloud.android.crop.Crop;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import de.hdodenhof.circleimageview.CircleImageView;

public class StudentPortalActivity extends AppCompatActivity {

    CircleImageView img_profile;
    EditText edit_mobNo;
    TextView edt_dateofbirth, edit_class;
    TextView txt_full_name, txt_gender, txt_emailid;
    Button btn_update;

    SharedPreferences sp;
    SharedPreferences.Editor editor;
    ConnectionDetector internet;

    String encoded_image = "";
    String filePath = "";
    String update_type = "";
    private static final String TEMP_PHOTO_FILE = "temporary_holder.jpg";
    boolean edit_flag = false;

    private static final int CROP_FROM_CAMERA = 2;
    private static final int PICK_FROM_FILE = 3;
    private static final int CROP_FROM_FILE = 4;
    private int REQUEST_CAMERA_LOWER = 5;

    String mCurrentPhotoPath;
    Uri photoURI;
    private int REQUEST_CAMERA = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_portal);

        sp = getSharedPreferences("daffodils", 0);
        editor = sp.edit();
        editor.apply();
        internet = new ConnectionDetector(StudentPortalActivity.this);
        filePath = Environment.getExternalStorageDirectory() + "/Android/data/" + getPackageName() + "/" + TEMP_PHOTO_FILE;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );

        initialize();

        txt_full_name.setText(sp.getString("student_name1", ""));
        Log.e("name", "" + sp.getString("student_name1", ""));
        txt_emailid.setText(sp.getString("parent_email", ""));
        edt_dateofbirth.setText(sp.getString("date_of_birth", ""));
        edit_mobNo.setText(sp.getString("parent_mobile", ""));
        int pos = edit_mobNo.getText().length();
        edit_mobNo.setSelection(pos);

        //  edit_class.setText(sp.getString("student_class1", ""));
        String data = getIntent().getExtras().getString("student_class1");
        edit_class.setText(sp.getString("student_class1", ""));
        //Log.e("student_class1",""+getIntent().getExtras().getString("student_class1"));
        Log.e("student_class1", "sp=" + sp.getString("student_class1", ""));

        if (sp.getString("gender_id", "").equals("1")) {
            txt_gender.setText("Male");
        } else {
            txt_gender.setText("Female");
        }

        edit_mobNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edit_flag = true;
            }
        });

//        String path1=sp.getString("student_image","");
//        Log.e("image","======"+path1);
        //  Glide.with(StudentPortalActivity.this).load(path1).signature(new StringSignature(UUID.randomUUID().toString())).dontAnimate().into(img_profile);
        // Glide.with(StudentPortalActivity.this).load(path1).dontAnimate().placeholder(R.drawable.ic_profile).into(img_profile);

        /*if (sp.getString("student_image","").equals("")){
            img_profile.setImageResource(R.drawable.ic_profile);
        }else{*/

        String path1=sp.getString("student_image","");
        //String path1 = sp.getString("student_image", "");
        Log.e("image", "======" + path1);
        Log.e("image_path", "******" + sp.getString("new_profile",""));
        Glide.with(StudentPortalActivity.this).load(sp.getString("new_profile","")+path1).signature(new StringSignature(UUID.randomUUID().toString())).dontAnimate().into(img_profile);
        //}

        //img_profile.setImageResource(R.drawable.ic_profile);
        img_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage();
            }
        });

        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String number = edit_mobNo.getText().toString().trim();

                if (edit_mobNo.getText().toString().equals("")) {
                    edit_mobNo.setError(getResources().getString(R.string.please_enter_mob_number));
                    edit_mobNo.requestFocus();
                } else if (edit_mobNo.getText().toString().trim().equals("00000000") || edit_mobNo.getText().toString().trim().equals("000000000") || edit_mobNo.getText().toString().trim().equals("0000000000") || edit_mobNo.getText().toString().trim().equals("0000000000000") || edit_mobNo.getText().toString().trim().equals("00000000000000")) {
                    edit_mobNo.setError(getResources().getString(R.string.str_valid_moblie));
                    edit_mobNo.requestFocus();
                } else if (edit_mobNo.length() < 10) {
                    edit_mobNo.setError(getResources().getString(R.string.str_minten_mob));
                    edit_mobNo.requestFocus();
                } else {
                    Log.e("edited number", "===" + number);
                    updatestudent(number, update_type);
                }
            }
        });
    }

    private void initialize() {
        txt_full_name = (TextView) findViewById(R.id.txt_full_name);
        txt_gender = (TextView) findViewById(R.id.txt_gender);
        txt_emailid = (TextView) findViewById(R.id.txt_emailid);
        edt_dateofbirth = (TextView) findViewById(R.id.edt_dateofbirth);
        edit_mobNo = (EditText) findViewById(R.id.edit_mobNo);
        edit_class = (TextView) findViewById(R.id.edit_class);
        img_profile = (CircleImageView) findViewById(R.id.img_profile);
        btn_update = (Button) findViewById(R.id.btn_update);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.student_profile_cap));
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
        Log.e("edit_flag", "flag" + edit_flag);
        if (edit_flag) {
            Log.e("edit_flag", "if" + edit_flag);
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                    StudentPortalActivity.this);
            alertDialog.setTitle(getResources().getString(R.string.confirm));
            alertDialog.setMessage(getResources().getString(R.string.sure_save));
            alertDialog.setCancelable(false);
            alertDialog.setIcon(R.drawable.daffodils_logo);
            alertDialog.setPositiveButton(getResources().getString(R.string.yes),
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub
                            Log.e("mobile_number", "" + edit_mobNo.getText().toString().trim().length());
                            if (edit_mobNo.getText().toString().length() != 0) {
                                if (edit_mobNo.getText().toString().trim().equals("00000000") || edit_mobNo.getText().toString().trim().equals("000000000") || edit_mobNo.getText().toString().trim().equals("0000000000")) {
                                    edit_mobNo.setError(getResources().getString(R.string.str_valid_moblie));
                                    edit_mobNo.requestFocus();
                                } else {
                                    if (edit_mobNo.getText().toString().trim().length() >= 10) {
                                        updatestudent(edit_mobNo.getText().toString(), update_type);
                                    } else {
                                        edit_mobNo.setError(getResources().getString(R.string.str_minten_mob));
                                        edit_mobNo.requestFocus();
                                    }
                                }
                            } else {
                                edit_mobNo.setError(getResources().getString(R.string.please_enter_mob_number));
                                edit_mobNo.requestFocus();

                            }
                        }
                    });
            alertDialog.setNegativeButton(getResources().getString(R.string.no),
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog,
                                            int which) {
                            // TODO Auto-generated method stub
                            finish();
                        }
                    });
            alertDialog.show();


        } else {
            Log.e("edit_flag", "else" + edit_flag);
            finish();
        }
    }//onBackPressed

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }//onOptionsItemSelected

    private String userChoosenTask = "";

    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library"};

        AlertDialog.Builder builder = new AlertDialog.Builder(StudentPortalActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                //  boolean result = Utility.checkPermission(AddEvent.this);
                if (items[item].equals("Take Photo")) {
                    // cameraIntent();
                    boolean result = insertDummyContactWrapper();
                    userChoosenTask = "Take Photo";
                    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        try {
                            dispatchTakePictureIntent();
                        } catch (IOException e) {
                        }
                    } else {
                        Log.e("version", "else" + android.os.Build.VERSION.SDK_INT);
                        cameraIntent();
                    }

                } else if (items[item].equals("Choose from Library")) {
                    galleryIntent();
                    userChoosenTask = "Choose from Library";
//                    if (result)
//                        galleryIntent();
                } /*else if (items[item].equals("Cancel")) {
                    *//*if (!encoded_image.equals("")) {
                        File f = new File(filePath);
                        if (f.exists()) {
                            f.delete();
                            encoded_image = "";
                            update_type="image";
                            edit_flag=true;
                            img_profile.setImageResource(R.drawable.ic_profile);
                        }
                        dialog.dismiss();
                    }else {
                        Log.e("image","Image not available");
                    }*//*
                }*/
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }


    // runtime permissions block
    final private int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;

    @TargetApi(Build.VERSION_CODES.M)
    private Boolean insertDummyContactWrapper() {
        List<String> permissionsNeeded = new ArrayList<String>();
        final List<String> permissionsList = new ArrayList<String>();
        if (!addPermission(permissionsList, Manifest.permission.CAMERA))
            permissionsNeeded.add("Camera");

        if (!addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE))
            permissionsNeeded.add("Storage");

        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {
                // Need Rationale
                // String message = "You need to grant access to " + permissionsNeeded.get(0);
                for (int i = 1; i < permissionsNeeded.size(); i++)
                    //message = message + ", " + permissionsNeeded.get(i);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                                REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                    }
                return false;
            }
            requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                    REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }//runtime permission chcek

    @TargetApi(Build.VERSION_CODES.M)
    private boolean addPermission(List<String> permissionsList, String permission) {
        if (checkPermission(permission)) {
            permissionsList.add(permission);
            // Check for Rationale Option
            if (!shouldShowRequestPermissionRationale(permission))
                return false;
        }
        return true;
    }

    private boolean checkPermission(String permission) {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(), permission);
        if (result != PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        return false;
    }

    private void cameraIntent() {

        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        File file = new File(Environment.getExternalStorageDirectory() + File.separator + "img.jpg");
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(filePath)));
        startActivityForResult(intent, REQUEST_CAMERA_LOWER);
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), PICK_FROM_FILE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS: {
                Map<String, Integer> perms = new HashMap<String, Integer>();
                // Initial
                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);

                // Fill with results
                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);
                // Check for ACCESS_FINE_LOCATION
                if (perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                } else {
                    // Permission Denied
//                    Toast.makeText(SignInActivity.this, "Some Permission is Denied", Toast.LENGTH_SHORT)
//                            .show();
                }
            }

            case Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (userChoosenTask.equals("Take Photo"))
                        cameraIntent();
                    else if (userChoosenTask.equals("Choose from Library"))
                        galleryIntent();
                } else {
                    //code for deny
                }
                break;
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        File croppedImageFile = new File(getFilesDir(), "test.jpg");
        if (resultCode == RESULT_OK) {
            if (requestCode == PICK_FROM_FILE) {
                if (data != null) {
                    String url = data.getData().toString();

                    if (url.startsWith("content://com.google.android.apps.photos.content")) {
                        try {
                            InputStream is = null;
                            is = getContentResolver().openInputStream(Uri.parse(url));
                            Bitmap bitmap = BitmapFactory.decodeStream(is);
                            if (bitmap != null) {
                                onCaptureImageResult(bitmap);
                            } else {
                                img_profile.setImageResource(R.drawable.ic_profile);
                                encoded_image = "";
                            }

                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }

                    } else {
                        Uri croppedImage = Uri.fromFile(croppedImageFile);
                        CropImageIntentBuilder cropImage = new CropImageIntentBuilder(200, 200, croppedImage);
                        cropImage.setSourceImage(data.getData());

                        startActivityForResult(cropImage.getIntent(StudentPortalActivity.this), CROP_FROM_FILE);
                    }
                }
            } else if (requestCode == CROP_FROM_FILE) {
                Log.e("Path===============", "" + croppedImageFile.getAbsolutePath());

                Bitmap bitmap = BitmapFactory.decodeFile(croppedImageFile.getAbsolutePath());
                //img_profile.setImageBitmap(selectedImage);

                if (bitmap != null) {
                    onCaptureImageResult(bitmap);
                } else {
                    img_profile.setImageResource(R.drawable.ic_profile);
                    encoded_image = "";
                }
            } else if (requestCode == REQUEST_CAMERA_LOWER) {
                File file = new File(filePath);
                performCrop(Uri.fromFile(file));
            } else if (requestCode == CROP_FROM_CAMERA) {

                Bitmap bitmap = BitmapFactory.decodeFile(filePath);
                if (bitmap != null) {
                    onCaptureImageResult(bitmap);
                } else {
                    img_profile.setImageResource(R.drawable.ic_profile);
                    encoded_image = "";
                }
            } else if (requestCode == REQUEST_CAMERA) {
                beginCrop(photoURI);
            } else if (requestCode == Crop.REQUEST_CROP) {
                handleCrop(resultCode, data);
            }
        }
    }

    private void beginCrop(Uri source) {
        Uri destination = Uri.fromFile(new File(getCacheDir(), "cropped"));
        Crop.of(source, destination).asSquare().start(this);
    }

    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == RESULT_OK) {
            img_profile.setImageURI(Crop.getOutput(result));

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), Crop.getOutput(result));
                encoded_image = encodeTobase64(bitmap);
                Log.e("imge_cropping", "" + encoded_image);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(this, Crop.getError(result).getMessage() + "handleCrop", Toast.LENGTH_SHORT).show();
        }
    }

    private void onCaptureImageResult(Bitmap thumbnail) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        try {
            String PATH = Environment.getExternalStorageDirectory() + "/Android/data/" + getPackageName() + "/";
            File folder = new File(PATH);
            if (!folder.exists()) {
                folder.mkdir();//If there is no folder it will be created.
                Log.e("folder====", "created");
            }
            File destination = new File(PATH + TEMP_PHOTO_FILE);

            destination.createNewFile();

            FileOutputStream fo;

            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();

            if (thumbnail != null) {
                img_profile.setImageBitmap(thumbnail);
                encoded_image = encodeTobase64(thumbnail);
                update_type = "image";

                edit_flag = true;
            } else {
                img_profile.setImageResource(R.drawable.ic_profile);
                encoded_image = "";
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        File file = new File(Environment.getExternalStorageDirectory() + File.separator + "img.jpg");
        if (file.exists())
            file.delete();

    }

    public static String encodeTobase64(Bitmap image) {
        Bitmap immagex = image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immagex.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);
        Log.e("LOOK", imageEncoded);
        return imageEncoded;
    }


    private void performCrop(Uri picUri) {
        try {
            //call the standard crop action intent (the user device may not support it)
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            //indicate image type and Uri
            cropIntent.setDataAndType(picUri, "image/*");
            //set crop properties
            cropIntent.putExtra("crop", "true");
            //indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            //indicate output X and Y
            cropIntent.putExtra("outputX", 200);
            cropIntent.putExtra("outputY", 200);
            File file = new File(filePath);
            cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(file.toString())));
            //retrieve data on return
            cropIntent.putExtra("return-data", true);
            //start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, CROP_FROM_CAMERA);
        } catch (ActivityNotFoundException anfe) {
            //display an error message
            String errorMessage = "Whoops - your device doesn't support the crop action!";
            Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }

    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DCIM), "Camera");
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        return image;
    }

    private void dispatchTakePictureIntent() throws IOException {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();

                Log.e("photoFile", "" + createImageFile());
            } catch (IOException ex) {
                Log.e("ex_IOException", "" + ex);
                // Error occurred while creating the File
                return;
            }


            // Continue only if the File was successfully created
            if (photoFile != null) {

                photoURI = FileProvider.getUriForFile(StudentPortalActivity.this,
                        BuildConfig.APPLICATION_ID + ".provider",
                        createImageFile());
                //    photoURI= Uri.fromFile(new File(photoFile.toString()));
                Log.e("if++", "" + photoURI);

                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_CAMERA);
            }
        }
    }//dispatchTakePictureIntent


    private void updatestudent(final String num, final String enter_update_type) {

        final ProgressDialog dialog = new ProgressDialog(StudentPortalActivity.this);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.str_requesting));
        dialog.show();
        RequestQueue queue = Volley.newRequestQueue(StudentPortalActivity.this);
        String requestURL = sp.getString("new_link", "") + "update_student";
        Log.e("requestURL", "" + requestURL);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", "code=" + response);
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response != null) {
                    if (response.equals("304")) {
                        // text_msg.setVisibility(View.VISIBLE);
                        //  text_msg.setText("No Records Available");
                        alertDialog1("Information Not Updated");
                        Log.e("Error message", "No Records Available");
                    } else {
//                        text_no_data.setVisibility(View.GONE);
//                        adapter = new CarCompanyAdapter(car_company, CarComapnyActivity.this);
//                        recyclerView_company.setAdapter(adapter);
//                        adapter.notifyDataSetChanged();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                //  Toast.makeText(getApplicationContext(), "Poor Internet Coonection", Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("account_id", sp.getString("account_id", ""));
                params.put("mobile", num);
                params.put("type", "image");
                params.put("student_id", sp.getString("student_id", ""));
                Log.e("update_studentparam", "" + params);
                params.put("profile_image", encoded_image);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                    VolleyError error = new VolleyError(new String(volleyError.networkResponse.data));
                    volleyError = error;
                    dialog.dismiss();
                }
                // Toast.makeText(getApplicationContext(),"Poor Internet Coonection",Toast.LENGTH_SHORT).show();
                Log.e("parseNetworkError", "==" + volleyError);
                Log.e("parseNetworkError", "string==" + volleyError.toString());
                Log.e("parseNetworkError", "msg==" + volleyError.getMessage());
                return volleyError;
            }

            @Override
            protected Response<String> parseNetworkResponse(final NetworkResponse response) {
                String responseString = "";
                if (response != null) {
                    responseString = String.valueOf(response.statusCode);
                    // can get more details such as response.headers
                }
                Map<String, String> responseHeaders = response.headers;
                if (response.statusCode == 200) {
                    Response<String> result = Response.success(responseHeaders.get("Content-Type"), HttpHeaderParser.parseCacheHeaders(response));
                    Log.e("Response_update", "" + parseToString(response));


                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            try {

                                JSONObject jObj = new JSONObject(parseToString(response));

                                String path = jObj.getString("image_path");
                                Log.e("imageinweb", "====" + path);
                                sp.edit().putString("student_image", path).apply();
                                // Glide.with(StudentPortalActivity.this).load(path).signature(new StringSignature(UUID.randomUUID().toString())).dontAnimate().into(img_profile);
                                //Glide.with(StudentPortalActivity.this).load(path).dontAnimate().into(img_profile);
                                edit_mobNo.setText(jObj.getString("mobile_number"));
                                String mobile = jObj.getString("mobile_number");
                                edit_mobNo.setSelection(mobile.length());

                                sp.edit().putString("parent_mobile", jObj.getString("mobile_number")).apply();

                                alertDialog(jObj.getString("msg"));


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });

                    return result;
                } else {


                    // text_msg.setText(jObj.getString("msg"));
                    Log.e("Error message", "Login Failed");
                }
                Log.e("responseString", "" + responseString);
                return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
            }

            public String parseToString(NetworkResponse response) {
                String parsed;
                try {
                    parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                } catch (UnsupportedEncodingException e) {
                    parsed = new String(response.data);
                }
                return parsed;
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }//updatestudent



    private void alertDialog1(String errer) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                StudentPortalActivity.this);
        alertDialog.setTitle(R.string.app_name);
        alertDialog.setMessage(errer);
        alertDialog.setCancelable(false);
        alertDialog.setIcon(R.drawable.daffodils_logo);
        alertDialog.setPositiveButton(R.string.dlg_ok,
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        finish();
                    }
                });

        alertDialog.show();
    }


    private void alertDialog(String errer) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                StudentPortalActivity.this);
        alertDialog.setTitle(R.string.app_name);
        alertDialog.setMessage(errer);
        alertDialog.setCancelable(false);
        alertDialog.setIcon(R.drawable.daffodils_logo);
        alertDialog.setPositiveButton(R.string.dlg_ok,
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        Intent intent = new Intent(StudentPortalActivity.this, StudentsHomeActivity.class);
                        startActivity(intent);
                        finish();
                    }
                });

        alertDialog.show();
    }

}
