package com.rsl.daffodils.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.rsl.daffodils.R;
import com.rsl.daffodils.activity.admin.FeedbackListActivity;
import com.rsl.daffodils.activity.student.GalleryActivity;
import com.rsl.daffodils.activity.student.NotificationActivity;
import com.rsl.daffodils.activity.student.StudentsHomeActivity;
import com.rsl.daffodils.model.Constants;
import com.rsl.daffodils.model.Teacher;
import com.rsl.daffodils.model.classes;
import com.rsl.daffodils.utils.ConnectionDetector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FeedbackActivity extends AppCompatActivity {
    EditText edt_feedbacktext, edt_subjtext;
    TextView edt_emailtext;
    SharedPreferences sp;
    SharedPreferences.Editor editor;
    ConnectionDetector internet;
    Spinner spinner_teacher;

    String selected_teacher;
    ArrayList<HashMap<String, String>> try_teacher_list = new ArrayList<HashMap<String, String>>();
    ArrayList<String> teacher_name = new ArrayList<>();
    ArrayList<Teacher> arr_teacher = new ArrayList<Teacher>();
    HashMap<String, String> try_teacher_data;
    String teacher_id;
    TextView txt_message;
    boolean country_flag=false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        sp = getSharedPreferences("daffodils", 0);
        editor = sp.edit();
        editor.apply();
        internet = new ConnectionDetector(FeedbackActivity.this);
        initView();
        edt_emailtext.setText(sp.getString("parent_email", ""));
        edt_feedbacktext.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                String str = s.toString();
                if (str.length() > 0 && str.startsWith(" ")) {
                    edt_feedbacktext.setText("");
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });


        edt_subjtext.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                String str = s.toString();
                if (str.length() > 0 && str.startsWith(" ")) {
                    edt_subjtext.setText("");
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });

        spinner_teacher.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()

                                                  {
                                                      @Override
                                                      public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                                        // if (position > 0) {
                                                              country_flag = true;
                                                              selected_teacher = try_teacher_list.get(position).get("teacher_name");
                                                              Log.e("selected_teacher","="+selected_teacher);
                                                              Log.e("teacher_id","="+try_teacher_list.get(position).get("teacher_id"));
                                                          teacher_id=try_teacher_list.get(position).get("teacher_id");
                                                             /* editor.putString("selected_country", selected_teacher).apply();
                                                              editor.apply();*/
                                                         /* }
                                                          if (position == 0) {
                                                              country_flag = false;

                                                          }*/
                                                      }

                                                      @Override
                                                      public void onNothingSelected(AdapterView<?> parent) {

                                                      }
                                                  }

        );


    }//onCreate


    private void initView() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.feedback_form));
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        edt_emailtext = (TextView) findViewById(R.id.edt_emailtext);
        edt_feedbacktext = (EditText) findViewById(R.id.edt_feedbacktext);
        edt_subjtext = (EditText) findViewById(R.id.edt_subjtext);
        spinner_teacher = (Spinner) findViewById(R.id.spinner_class);
        txt_message = (TextView) findViewById(R.id.txt_message);

        if (internet.isConnectingToInternet()) {
            getTeacherList();
        } else {
            internet.showAlertDialog(FeedbackActivity.this, false);
            txt_message.setText(getString(R.string.str_internet_title));
            txt_message.setVisibility(View.VISIBLE);
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.feedback_menu, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
        Intent in = new Intent(FeedbackActivity.this, StudentsHomeActivity.class);
        startActivity(in);
        finish();
        //finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_send: {
                if (internet.isConnectingToInternet()) {
                    if ((edt_subjtext.length() > 0)) {
                        if ((edt_feedbacktext.length() > 0)) {
                            sendFeedback(edt_feedbacktext.getText().toString().trim(), edt_subjtext.getText().toString().trim());
                        } else {
                            showToast(getString(R.string.str_toast_feedback));
                            edt_emailtext.requestFocus();
                        }
                    } else {
                        showToast(getString(R.string.str_toast_subject));
                        edt_subjtext.requestFocus();
                    }

                } else {
                    internet.showAlertDialog(FeedbackActivity.this, false);
                }
            }
            return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }//onOptionsItemSelected


    private void sendFeedback(final String feedbacktext, final String subjtext) {

        final ProgressDialog dialog = new ProgressDialog(FeedbackActivity.this);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.str_requesting));
        dialog.show();
        RequestQueue queue = Volley.newRequestQueue(FeedbackActivity.this);
        String requestURL = sp.getString("new_link","") + "add_feedback";
        Log.e("requestURL", "" + requestURL);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", "code=" + response);
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response != null) {
                    if (response.equals("204")) {
                        // text_msg.setVisibility(View.VISIBLE);
                        //  text_msg.setText("No Records Available");
                        alertDialog1("No Records Available");
                        Log.e("Error message", "No Records Available");
                    } else {
                        if (response.equals("201")) {
                            Handler mHandler;
                            mHandler = new Handler(Looper.getMainLooper()) {
                                @Override
                                public void handleMessage(Message message) {
                                    showToast("thank you for registration");
                                    finish();
                                }
                            };
                        }

                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
              //  Toast.makeText(getApplicationContext(), "Poor Internet Coonection", Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("account_id", sp.getString("account_id", ""));
                params.put("feedback", feedbacktext);
                params.put("teacher_account_id", teacher_id);
                params.put("subject", subjtext);
                params.put("device_type", "android");
                params.put("student_id",sp.getString("student_id", ""));


                Log.e("param", "" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                    VolleyError error = new VolleyError(new String(volleyError.networkResponse.data));
                    volleyError = error;
                    dialog.dismiss();
                }
                // Toast.makeText(getApplicationContext(),"Poor Internet Coonection",Toast.LENGTH_SHORT).show();
                Log.e("parseNetworkError", "==" + volleyError);
                Log.e("parseNetworkError", "string==" + volleyError.toString());
                Log.e("parseNetworkError", "msg==" + volleyError.getMessage());
                return volleyError;
            }

            @Override
            protected Response<String> parseNetworkResponse(final NetworkResponse response) {
                String responseString = "";
                if (response != null) {
                    responseString = String.valueOf(response.statusCode);
                    // can get more details such as response.headers
                }
                Map<String, String> responseHeaders = response.headers;
                if (response.statusCode == 200) {
                    Response<String> result = Response.success(responseHeaders.get("Content-Type"), HttpHeaderParser.parseCacheHeaders(response));
                    Log.e("Response_add_stude", "" + parseToString(response));




                    return result;
                } else {
                    if (response.statusCode == 201) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                showToast("Your Feedback Submitted Sucessfully");
                                finish();

                                }

                        });
                    }

                    // text_msg.setText(jObj.getString("msg"));
                    Log.e("Error message", "Login Failed");
                }
                Log.e("responseString", "" + responseString);
                return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
            }

            public String parseToString(NetworkResponse response) {
                String parsed;
                try {
                    parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                } catch (UnsupportedEncodingException e) {
                    parsed = new String(response.data);
                }
                return parsed;
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 4, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }//send_feedback




//    private void sendFeedback(final String feedbacktext, final String subjtext) {
//        final ProgressDialog dialog = new ProgressDialog(FeedbackActivity.this);
//        dialog.setCancelable(false);
//        dialog.setMessage(getString(R.string.str_requesting));
//        dialog.show();
//        final RequestQueue queue = Volley.newRequestQueue(FeedbackActivity.this);
//
//        String requestURL = getString(R.string.web_path) + "feedback.php";
//        Log.e("check web_path", requestURL);
//
//        StringRequest stringRequest = new StringRequest(Request.Method.POST, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
//            @SuppressLint("NewApi")
//            @Override
//            public void onResponse(String result) {
//                Log.e("response", result);
//                if (result != null) {
//                    Log.e("check web_path", result);
//                    if (!result.startsWith("null")) {
//                        try {
//                            JSONObject jObj = new JSONObject(result);
//                            if (jObj.length() > 0) {
//                                if (jObj.getString("result").equals("success")) {
//                                    showToast(jObj.getString("msg"));
//                                    finish();
//                                } else if (jObj.getString("result").equals("failed")) {
//                                    showToast(jObj.getString("msg"));
//                                }
//                                dialog.dismiss();
//                            }
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                            if (dialog.isShowing())
//                                dialog.dismiss();
//                            Toast.makeText(getApplicationContext(), getString(R.string.str_json_exception), Toast.LENGTH_SHORT).show();
//                        }
//                    }
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError volleyError) {
//                if (dialog.isShowing())
//                    dialog.dismiss();
//                Toast.makeText(getApplicationContext(), getString(R.string.str_json_exception), Toast.LENGTH_SHORT).show();
//            }
//
//        }) {
//            @Override
//            protected Map<String, String> getParams() {
//                HashMap<String, String> params = new HashMap<String, String>();
//                params.put("account_id", sp.getString("account_id", ""));
//                params.put("feedback", feedbacktext);
//                params.put("teacher_account_id", teacher_id);
//                params.put("subject", subjtext);
//                params.put("device_type", "android");
//                Log.e("params", "==" + params);
//                return params;
//            }
//
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                Map<String, String> params = new HashMap<String, String>();
//                params.put("Content-Type", "application/x-www-form-urlencoded");
//                return params;
//            }
//        };
//        queue.add(stringRequest);
//    }//feedbback

    private void showToast(String msg) {
        Toast toast = Toast.makeText(FeedbackActivity.this, "" + msg, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

//    private boolean isValidEmail(String email) {
//        if (Character.isDigit(email.charAt(0))) {
//            return false;
//        } else if (email.startsWith(".")) {
//            return false;
//        } else if (android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
//            return true;
//        } else {
//            return false;
//        }
//    }

    public boolean validateEmail(String email) {
        Pattern pattern;
        Matcher matcher;

        String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    private void getTeacherList() {

        final ProgressDialog dialog = new ProgressDialog(FeedbackActivity.this);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.str_requesting));
        dialog.show();
        RequestQueue queue = Volley.newRequestQueue(FeedbackActivity.this);
        String requestURL = sp.getString("new_link","") + "teacherlist";
        Log.e("requestURL", "" + requestURL);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", "code=" + response);
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response != null) {
                    if (response.equals("204")) {
                        // text_msg.setVisibility(View.VISIBLE);
                        //  text_msg.setText("No Records Available");
                        alertDialog1("No Records Available");
                        Log.e("Error message", "No Records Available");
                    } else {
//                        text_no_data.setVisibility(View.GONE);
//                        adapter = new CarCompanyAdapter(car_company, CarComapnyActivity.this);
//                        recyclerView_company.setAdapter(adapter);
//                        adapter.notifyDataSetChanged();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
             //   Toast.makeText(getApplicationContext(), "Poor Internet Coonection", Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                // params.put("id",id);
                Log.e("param", "" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                    VolleyError error = new VolleyError(new String(volleyError.networkResponse.data));
                    volleyError = error;
                    dialog.dismiss();
                }
                // Toast.makeText(getApplicationContext(),"Poor Internet Coonection",Toast.LENGTH_SHORT).show();
                Log.e("parseNetworkError", "==" + volleyError);
                Log.e("parseNetworkError", "string==" + volleyError.toString());
                Log.e("parseNetworkError", "msg==" + volleyError.getMessage());
                return volleyError;
            }

            @Override
            protected Response<String> parseNetworkResponse(final NetworkResponse response) {
                String responseString = "";
                if (response != null) {
                    responseString = String.valueOf(response.statusCode);
                    // can get more details such as response.headers
                }
                Map<String, String> responseHeaders = response.headers;
                if (response.statusCode == 200) {
                    Response<String> result = Response.success(responseHeaders.get("Content-Type"), HttpHeaderParser.parseCacheHeaders(response));
                    Log.e("Response_teacherlist", "==" + parseToString(response));
                    try_teacher_list.clear();

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                JSONArray JArray = new JSONArray(parseToString(response));
                                arr_teacher = new ArrayList<Teacher>();
                                for (int i = 0; i < JArray.length(); i++) {
                                    JSONObject jobj_Data = JArray.getJSONObject(i);

                                    Teacher con = new Teacher();
                                    con.setTeacher_id(jobj_Data.getString("teacher_id"));
                                    con.setTeacher_name(jobj_Data.getString("teacher_name"));
                                    teacher_name.add(jobj_Data.getString("teacher_name"));
                                   // country_name.add(b.getString("account_id"));
                                    arr_teacher.add(con);

                                    try_teacher_data = new HashMap<String, String>();
                                    try_teacher_data.put("teacher_id", jobj_Data.getString("teacher_id"));
                                    try_teacher_data.put("teacher_name", jobj_Data.getString("teacher_name"));
                                    try_teacher_list.add(try_teacher_data);

                                    ArrayAdapter<String> adapter =
                                            new ArrayAdapter<String>(FeedbackActivity.this, android.R.layout.simple_spinner_dropdown_item, teacher_name) {

                                                public View getView(int position, View convertView, ViewGroup parent) {
                                                    View v = super.getView(position, convertView, parent);

                                                    // ((TextView) v).setTextSize(16);
                                                    //   ((TextView) v).setTextColor(getResources().getColorStateList(R.color.white));

                                                    return v;
                                                }

                                                public View getDropDownView(int position, View convertView, ViewGroup parent) {
                                                    View v = super.getDropDownView(position, convertView, parent);

                                                   /* ((TextView) v).setTextColor(
                                                            getResources().getColorStateList(R.color.color_preloader_start)
                                                    );
*/
                                                    return v;
                                                }


                                            };
                                    spinner_teacher.setAdapter(adapter);

                                }//for

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });

                    return result;
                } else {


                    Log.e("Error message", "Login Failed");
                }
                Log.e("responseString", "" + responseString);
                return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
            }

            public String parseToString(NetworkResponse response) {
                String parsed;
                try {
                    parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                } catch (UnsupportedEncodingException e) {
                    parsed = new String(response.data);
                }
                return parsed;
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }//display_student


    private void alertDialog1(String errer) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                FeedbackActivity.this);
        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setMessage(errer);
        alertDialog.setCancelable(false);
        alertDialog.setIcon(R.drawable.app_logo);
        alertDialog.setPositiveButton(getString(R.string.dlg_ok),
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub

                    }
                });

        alertDialog.show();
    }//alertDialog1

}
