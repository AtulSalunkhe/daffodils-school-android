package com.rsl.daffodils.activity.admin;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.iid.FirebaseInstanceId;
import com.rsl.daffodils.LoginActivity;
import com.rsl.daffodils.R;
import com.rsl.daffodils.activity.RegistrationActivity;
import com.rsl.daffodils.activity.teacher.TeachersHomeActivity;
import com.rsl.daffodils.model.classes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class EnquiryActivity extends AppCompatActivity {

    Spinner spinner_aboutus, spinner_contact;
    Button btn_signup;
    EditText edt_dob, edt_date, edt_gender, parents_name, student_name, student_dob, edt_local_address, edt_mobile, edt_mobile1, edt_mobile2, edt_ladline, edt_ladline1, edt_ladline2, edt_email, edt_school_attend, edt_message;
    String select_date = "";
    String select_dob = "";
    private int myear, mmonth, mday;

    String p_name, s_name, e_localadd, e_mob_no, e_ladline_pho, e_ladline_pho1, e_ladline_pho2, e_mail, e_schoool_attend, e_message;

    private int myear1, mmonth1, mday1;
    static final int DATE_DIALOG_ID = 999;
    static final int DATE_DIALOG_ID1 = 99;
    private int mYear;
    private int mMonth;
    private int mDay;
    SharedPreferences sp;
    SharedPreferences.Editor editor;
    private RadioGroup radioGroup;
    int gender_code = 1;
    String gender = "";

    //for spinner to select class
    Spinner spinner_class;
    String selected_class, class_id;
    ArrayList<HashMap<String, String>> try_class_list = new ArrayList<HashMap<String, String>>();
    ArrayList<String> class_name = new ArrayList<>();
    ArrayList<classes> arr_class = new ArrayList<classes>();
    HashMap<String, String> try_class_data;
    boolean classes_flag = false;

    String todayAsString;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enquiry);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        sp = getSharedPreferences("daffodils", 0);
        editor = sp.edit();
        initView();
        final Calendar c = Calendar.getInstance();
        DateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        Date today = c.getTime();
        todayAsString = dateFormat1.format(today);
        edt_date.setText(todayAsString);
        Log.e("current_date", "==" + todayAsString);

        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);


        Log.e("Current ", "" + mYear + "" + mMonth + "" + mDay);

        edt_dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  input_gender.setErrorEnabled(false);
                showDialog(DATE_DIALOG_ID);
            }
        });

        edt_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  input_gender.setErrorEnabled(false);
                //showDialog(DATE_DIALOG_ID1);
                edt_date.setText(todayAsString);
            }
        });


        radioGroup = (RadioGroup) findViewById(R.id.myRadioGroup);


        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override

            public void onCheckedChanged(RadioGroup group, int checkedId) {

                // find which radio button is selected

                if (checkedId == R.id.radio_male) {

                    gender_code = 1;
                    gender = "Male";
                    hideSoftKeyboard();
                    //EnquiryActivity.this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

                } else if (checkedId == R.id.radio_female) {

                    gender_code = 2;
                    gender = "Female";
                    hideSoftKeyboard();
                    // EnquiryActivity.this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                }

            }
        });


        spinner_class.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()

                                                {
                                                    @Override
                                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                                        // if (position > 0) {
                                                        classes_flag = true;
                                                        selected_class = try_class_list.get(position).get("class");
                                                        Log.e("selected_class", "=" + selected_class);
                                                        Log.e("account_id", "=" + try_class_list.get(position).get("class_id"));
                                                        class_id = try_class_list.get(position).get("class_id");
                                                        Log.e("class id", "----" + class_id);



                                                             /* editor.putString("selected_country", selected_teacher).apply();
                                                              editor.apply();*/
                                                         /* }
                                                          if (position == 0) {
                                                              country_flag = false;

                                                          }*/
                                                    }

                                                    @Override
                                                    public void onNothingSelected(AdapterView<?> parent) {

                                                    }
                                                }


        );

        edt_gender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Creating the instance of PopupMenu
                PopupMenu popup = new PopupMenu(EnquiryActivity.this, edt_gender);
                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(R.menu.poupup_menu, popup.getMenu());

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        gender = item.getTitle().toString();
                        edt_gender.setText(item.getTitle().toString());
                        //   Toast.makeText(getActivity(),"You Clicked : " + item.getTitle(),Toast.LENGTH_SHORT).show();
                        return true;
                    }
                });

                popup.show();//showing popup menu
            }
        });


        // Get reference of widgets from XML layout
        spinner_aboutus = (Spinner) findViewById(R.id.spinner_aboutus);

        // Initializing a String Array
        String[] plant = new String[]{
                "How do you come to know about us?",
                "Advertise",
                "Mouth to mouth",
                "Hoding",

        };

        final List<String> plantss = new ArrayList<>(Arrays.asList(plant));

        // Initializing an ArrayAdapter
        final ArrayAdapter<String> spinnerArrayAdapter1 = new ArrayAdapter<String>(EnquiryActivity.this, R.layout.spinner_item, plantss) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {

                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view1 = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view1;
                if (position == 0) {
                    // Set the hint text color gray
                    tv.setTextColor(Color.BLACK);
                } else {
                    tv.setTextColor(Color.GRAY);
                }
                return view1;
            }
        };
        spinnerArrayAdapter1.setDropDownViewResource(R.layout.spinner_item);
        spinner_aboutus.setAdapter(spinnerArrayAdapter1);


        // Get reference of widgets from XML layout
        spinner_contact = (Spinner) findViewById(R.id.spinner_contact);

        // Initializing a String Array
        String[] plants = new String[]{
                "Select Contact Method",
                "Email",
                "Phone",
                "SMS",

        };

        final List<String> plantsList = new ArrayList<>(Arrays.asList(plants));

        // Initializing an ArrayAdapter
        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(EnquiryActivity.this, R.layout.spinner_item, plantsList) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {

                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view2 = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view2;
                if (position == 0) {
                    // Set the hint text color gray
                    tv.setTextColor(Color.BLACK);

                } else {
                    tv.setTextColor(Color.GRAY);
                }
                return view2;
            }
        };
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        spinner_contact.setAdapter(spinnerArrayAdapter);


        btn_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                select_date = edt_date.getText().toString().trim();
                p_name = parents_name.getText().toString().trim();
                s_name = student_name.getText().toString().trim();
                select_dob = edt_dob.getText().toString().trim();
                e_localadd = edt_local_address.getText().toString().trim();
                e_mob_no = edt_mobile.getText().toString().trim();
                //    e_mob_no1 = edt_mobile1.getText().toString().trim();
                //    e_mob_no2 = edt_mobile2.getText().toString().trim();
                e_ladline_pho = edt_ladline.getText().toString().trim();
                //    e_ladline_pho1 = edt_ladline1.getText().toString().trim();
                //     e_ladline_pho2 = edt_ladline2.getText().toString().trim();
                e_mail = edt_email.getText().toString().trim();
                e_schoool_attend = edt_school_attend.getText().toString().trim();
                e_message = edt_message.getText().toString().trim();


                if (Is_Valid_Parent_Name(parents_name)) {
                    if (Is_Valid_student_Name(student_name)) {
                        if (!select_dob.equals("")) {
                            if (!gender.equals("")) {
                                if (e_localadd.equals("")) {
                                    alertDialog1(getResources().getString(R.string.str_local_address));
                                } else if (e_mob_no.equals("")) {
                                    alertDialog1(getResources().getString(R.string.please_enter_mob_number));
                                } else if (e_mob_no.equals("000000000") || e_mob_no.equals("0000000000")) {
                                    alertDialog1(getResources().getString(R.string.str_valid_moblie));
                                } else if (e_ladline_pho.equals("")) {
                                    alertDialog1(getResources().getString(R.string.str_ladline_no));
                                } else if (e_ladline_pho.equals("000000000") || e_ladline_pho.equals("0000000000")) {
                                    alertDialog1(getResources().getString(R.string.str_valid_ladline_no));
                                } else if (e_mail.equals("")) {
                                    alertDialog1(getResources().getString(R.string.str_enter_email_id));
                                    // edt_parent_email.setError(getResources().getString(R.string.str_enter_email_id));
                                    // edt_email.requestFocus();
                                } else if (isValidEmail(e_mail)) {
                                    alertDialog1(getResources().getString(R.string.str_please_enter_validemail));
                                } else if (e_schoool_attend.equals("")) {
                                    alertDialog1(getResources().getString(R.string.str_last_school));
                                } else if (e_message.equals("")) {
                                    alertDialog1(getResources().getString(R.string.str_message));
                                } else if (spinner_aboutus.getSelectedItemPosition() > 0) {
                                    if (spinner_contact.getSelectedItemPosition() > 0) {
                                        Enquiry();

                                    } else {
                                        alertDialog1(getResources().getString(R.string.str_contact_method));

                                    }


                                } else {
                                    alertDialog1(getResources().getString(R.string.str_about_us));

                                }


                            } else {
                                //Toast.makeText(getApplicationContext(), getString(R.string.select_gender), Toast.LENGTH_SHORT).show();
                                alertDialog1(getResources().getString(R.string.select_gender));
                                //    edt_gender.setError(getResources().getString(R.string.select_gender));
                                //edt_gender.requestFocus();
                            }
                        } else {
                            //Toast.makeText(getApplicationContext(), getResources().getString(R.string.select_dob), Toast.LENGTH_SHORT).show();
                            alertDialog1(getResources().getString(R.string.select_dob));
                            // edt_dob.setError(getResources().getString(R.string.select_dob));
                            // edt_date.requestFocus();
                        }

                    }

                }


            }
        });


    }//onCreate

    private void hideSoftKeyboard() {
        if (getCurrentFocus() != null && getCurrentFocus() instanceof EditText) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(radioGroup.getWindowToken(), 0);
        }
    }

    private boolean isValidEmail(String email) {
        if (email.startsWith(".")) {
            return true;
        } else if (android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            return false;
        } else {
            return true;
        }
    }

    private void initView() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.str_enquiry_form);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        edt_dob = (EditText) findViewById(R.id.edt_dob);//select dob
        edt_date = (EditText) findViewById(R.id.edt_date);//select date
        parents_name = (EditText) findViewById(R.id.parents_name);
        student_name = (EditText) findViewById(R.id.student_name);
        student_dob = (EditText) findViewById(R.id.edt_dob);
        edt_local_address = (EditText) findViewById(R.id.edt_local_address);
        edt_mobile = (EditText) findViewById(R.id.edt_mobile);
        edt_gender = (EditText) findViewById(R.id.edt_gender);//select gender
        edt_ladline = (EditText) findViewById(R.id.edt_ladline);
        edt_email = (EditText) findViewById(R.id.edt_email);
        edt_school_attend = (EditText) findViewById(R.id.edt_school_attend);
        edt_message = (EditText) findViewById(R.id.edt_message);
        btn_signup = (Button) findViewById(R.id.btn_signup);
        spinner_class = (Spinner) findViewById(R.id.spinner_class);//spinner for class list
        getClassList();

    }//initView


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }//onBackPressed


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }//onOptionsItemSelected


    public void setCurrentDateOnView() {
        final Calendar c = Calendar.getInstance();
        myear = c.get(Calendar.YEAR);
        mmonth = c.get(Calendar.MONTH);
        mday = c.get(Calendar.DAY_OF_MONTH);
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_DIALOG_ID:
                // set date picker as current date
                setCurrentDateOnView();
                DatePickerDialog _date = new DatePickerDialog(this,
                        datePickerListener, myear, mmonth, mday) {
                    @Override
                    public void onDateChanged(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {

                        if (year > mYear) {
                            view.updateDate(mYear, mMonth, mDay);
                            Toast.makeText(getApplicationContext(), getString(R.string.str_date_erro), Toast.LENGTH_SHORT).show();
                        }

                        if (monthOfYear > mMonth && year == mYear) {
                            view.updateDate(mYear, mMonth, mDay);
                            Toast.makeText(getApplicationContext(), getString(R.string.str_date_erro), Toast.LENGTH_SHORT).show();
                        }

                        if (dayOfMonth > mDay && year == mYear && monthOfYear == mMonth) {
                            view.updateDate(mYear, mMonth, mDay);
                            Toast.makeText(getApplicationContext(), getString(R.string.str_date_erro), Toast.LENGTH_SHORT).show();
                        }

                    }
                };
                Log.e("_date", "" + _date);
                return _date;

            case DATE_DIALOG_ID1:
                // set date picker as current date
                setCurrentDateOnView();
                DatePickerDialog _date1 = new DatePickerDialog(this,
                        datePickerListener1, myear, mmonth, mday) {
                    @Override
                    public void onDateChanged(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {

                        if (year > mYear) {
                            view.updateDate(mYear, mMonth, mDay);
                            Toast.makeText(getApplicationContext(), getString(R.string.str_date_erro), Toast.LENGTH_SHORT).show();
                        }

                        if (monthOfYear > mMonth && year == mYear) {
                            view.updateDate(mYear, mMonth, mDay);
                            Toast.makeText(getApplicationContext(), getString(R.string.str_date_erro), Toast.LENGTH_SHORT).show();
                        }

                        if (dayOfMonth > mDay && year == mYear && monthOfYear == mMonth) {
                            view.updateDate(mYear, mMonth, mDay);
                            Toast.makeText(getApplicationContext(), getString(R.string.str_date_erro), Toast.LENGTH_SHORT).show();
                        }

                    }
                };
                Log.e("_date", "" + _date1);
                return _date1;

        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            myear1 = selectedYear;
            mmonth1 = selectedMonth;
            mday1 = selectedDay;
            // set selected date into Textview
            edt_dob.setText(new StringBuilder().append(myear1)
                    .append("-").append(mmonth1 + 1).append("-").append(mday1));

        }
    };

    private DatePickerDialog.OnDateSetListener datePickerListener1 = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            myear1 = selectedYear;
            mmonth1 = selectedMonth;
            mday1 = selectedDay;
            // set selected date into Textview
            edt_date.setText(new StringBuilder().append(myear1)
                    .append("-").append(mmonth1 + 1).append("-").append(mday1));

        }
    };

    private void Enquiry() {
        final ProgressDialog dialog = new ProgressDialog(EnquiryActivity.this);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.str_requesting));
        dialog.show();
        if (gender.equals("Male")) {
            gender_code = 1;
        } else if (gender.equals("Female")) {
            gender_code = 2;
        } else {
            gender_code = 3;
        }
        RequestQueue queue = Volley.newRequestQueue(EnquiryActivity.this);
        String requestURL = sp.getString("new_link", "") + "add_enquiry";
        Log.e("requestURL", "API=" + requestURL);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", "code=" + response);
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response != null) {

                    //alertDialog("Registration Successful");
                    // Toast.makeText(getApplicationContext(),"Registration Successful",Toast.LENGTH_SHORT).show();
                    Log.e(" message", " Records Available");

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                //  Toast.makeText(getApplicationContext(), "Poor Internet Coonection", Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("date", edt_date.getText().toString());
                params.put("parent_name", parents_name.getText().toString());
                params.put("student_name", student_name.getText().toString());
                params.put("dob", edt_dob.getText().toString().trim());
                params.put("gender_id", String.valueOf(gender_code));
                params.put("class_id", class_id);
                params.put("about_us", spinner_aboutus.getItemAtPosition(spinner_aboutus.getSelectedItemPosition()).toString());
                params.put("address", edt_local_address.getText().toString());
                params.put("mobile", edt_mobile.getText().toString());
                params.put("phone", edt_ladline.getText().toString());
                params.put("email", edt_email.getText().toString());
                params.put("contact_method", spinner_contact.getItemAtPosition(spinner_contact.getSelectedItemPosition()).toString());
                params.put("school", edt_school_attend.getText().toString());
                params.put("comment", edt_message.getText().toString());

                Log.e("params", "" + params);


                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                    VolleyError error = new VolleyError(new String(volleyError.networkResponse.data));
                    volleyError = error;
                    dialog.dismiss();
                }
                //   Toast.makeText(getApplicationContext(),"Poor Internet Coonection",Toast.LENGTH_SHORT).show();
                Log.e("parseNetworkError", "==" + volleyError);
                Log.e("parseNetworkError", "string==" + volleyError.toString());
                Log.e("parseNetworkError", "msg==" + volleyError.getMessage());
                return volleyError;
            }

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                String responseString = "";
                if (response != null) {
                    responseString = String.valueOf(response.statusCode);
                    // can get more details such as response.headers
                    if (response.statusCode == 201) {

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                alertDialog(getResources().getString(R.string.str_enquiry_form_msg));
                            }
                        });
                    }


                }

                Log.e("responseStrinsaasag", "" + responseString);
                Map<String, String> responseHeaders = response.headers;
                if (response.statusCode == 201) {
                    Response<String> result = Response.success(responseHeaders.get("Content-Type"), HttpHeaderParser.parseCacheHeaders(response));
                    Log.e("Response_registration", "response=" + parseToString(response));

                    return result;
                } else {
//                    if (response.statusCode == 201) {
//
//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                alertDialog1(getResources().getString(R.string.str_attedence_submited));
//                            }
//                        });
//                    }


                    Log.e("Error message", "Login Failed");
                }
                return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
            }

            public String parseToString(NetworkResponse response) {
                String parsed;
                try {
                    parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                } catch (UnsupportedEncodingException e) {
                    parsed = new String(response.data);
                }
                return parsed;
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 4, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }//registerUser


    //    spinner list display
    private void getClassList() {

        final ProgressDialog dialog = new ProgressDialog(EnquiryActivity.this);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.str_requesting));
        dialog.show();
        RequestQueue queue = Volley.newRequestQueue(EnquiryActivity.this);
        String requestURL = sp.getString("new_link", "") + "classlist";
        Log.e("requestURL", "" + requestURL);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", "code=" + response);
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response != null) {
                    if (response.equals("204")) {
                        // text_msg.setVisibility(View.VISIBLE);
                        //  text_msg.setText("No Records Available");
                        alertDialog1("No Records Available");
                        Log.e("Error message", "No Records Available");
                    } else {
//                        text_no_data.setVisibility(View.GONE);
//                        adapter = new CarCompanyAdapter(car_company, CarComapnyActivity.this);
//                        recyclerView_company.setAdapter(adapter);
//                        adapter.notifyDataSetChanged();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                //  Toast.makeText(getApplicationContext(), "Poor Internet Coonection", Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                // params.put("id",id);
                Log.e("param", "" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                    VolleyError error = new VolleyError(new String(volleyError.networkResponse.data));
                    volleyError = error;
                    dialog.dismiss();
                }
                // Toast.makeText(getApplicationContext(),"Poor Internet Coonection",Toast.LENGTH_SHORT).show();
                Log.e("parseNetworkError", "==" + volleyError);
                Log.e("parseNetworkError", "string==" + volleyError.toString());
                Log.e("parseNetworkError", "msg==" + volleyError.getMessage());
                return volleyError;
            }

            @Override
            protected Response<String> parseNetworkResponse(final NetworkResponse response) {
                String responseString = "";
                if (response != null) {
                    responseString = String.valueOf(response.statusCode);
                    // can get more details such as response.headers
                }
                Map<String, String> responseHeaders = response.headers;
                if (response.statusCode == 200) {
                    Response<String> result = Response.success(responseHeaders.get("Content-Type"), HttpHeaderParser.parseCacheHeaders(response));
                    Log.e("Response_class list", "==" + parseToString(response));
                    try_class_list.clear();

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                JSONArray JArray = new JSONArray(parseToString(response));
                                arr_class = new ArrayList<classes>();
                                for (int i = 0; i < JArray.length(); i++) {
                                    JSONObject jobj_Data = JArray.getJSONObject(i);

                                    classes con = new classes();
                                    con.setClasses_id(jobj_Data.getString("class_id"));
                                    con.setClasses_name(jobj_Data.getString("class"));
                                    class_name.add(jobj_Data.getString("class"));
                                    // country_name.add(b.getString("account_id"));
                                    arr_class.add(con);

                                    try_class_data = new HashMap<String, String>();
                                    try_class_data.put("class_id", jobj_Data.getString("class_id"));
                                    try_class_data.put("class", jobj_Data.getString("class"));
                                    try_class_list.add(try_class_data);

                                    ArrayAdapter<String> adapter =
                                            new ArrayAdapter<String>(EnquiryActivity.this, android.R.layout.simple_spinner_dropdown_item, class_name) {

                                                public View getView(int position, View convertView, ViewGroup parent) {
                                                    View v = super.getView(position, convertView, parent);

//                                                    ((TextView) v).setTextSize(16);
//                                                    ((TextView) v).setTextColor(getResources().getColorStateList(R.color.white));

                                                    return v;
                                                }

                                                public View getDropDownView(int position, View convertView, ViewGroup parent) {
                                                    View v = super.getDropDownView(position, convertView, parent);

                                                   /* ((TextView) v).setTextColor(
                                                            getResources().getColorStateList(R.color.color_preloader_start)
                                                    );
*/
                                                    return v;
                                                }


                                            };
                                    spinner_class.setAdapter(adapter);

                                }//for

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });

                    return result;
                } else {


                    Log.e("Error message", "Login Failed");
                }
                Log.e("responseString", "" + responseString);
                return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
            }

            public String parseToString(NetworkResponse response) {
                String parsed;
                try {
                    parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                } catch (UnsupportedEncodingException e) {
                    parsed = new String(response.data);
                }
                return parsed;
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }//display_class


    private void alertDialog1(String errer) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                EnquiryActivity.this);
        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setMessage(errer);
        alertDialog.setCancelable(false);
        alertDialog.setIcon(R.drawable.daffodils_logo);
        alertDialog.setPositiveButton(getString(R.string.dlg_ok),
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                    }
                });

        alertDialog.create().show();
    }//alertDialog1


    private void alertDialog(String errer) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                EnquiryActivity.this);
        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setMessage(errer);
        alertDialog.setCancelable(false);
        alertDialog.setIcon(R.drawable.daffodils_logo);
        alertDialog.setPositiveButton(getString(R.string.dlg_ok),
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        Intent intent = new Intent(EnquiryActivity.this, TeachersHomeActivity.class);
                        startActivity(intent);
                    }
                });

        alertDialog.show();
    }

    public Boolean Is_Valid_Parent_Name(EditText edt)
            throws NumberFormatException {
        if (edt.getText().toString().trim().length() <= 0) {
            alertDialog1(getString(R.string.enter_parent_name));
            //  edt.setError(getString(R.string.enter_parent_name));
            // edt.requestFocus();
            p_name = null;
        } else if (edt.getText().toString().trim().length() <= 1) {
            alertDialog1(getString(R.string.enter_more_chars_lname));
            //edt.setError(getString(R.string.enter_more_chars_lname));
            //  edt.requestFocus();
            p_name = null;
        } else if (!edt.getText().toString().trim().matches("[a-zA-Z ]+")) {
            // edt.setError("Accept Alphabets Only.");
            alertDialog1(getString(R.string.name_alpha_only));
            // edt.setError(getString(R.string.lname_alpha_only));
            // edt.requestFocus();
            p_name = null;
        } else {
            //  input_last_name.setErrorEnabled(false);
            p_name = edt.getText().toString().trim();
            return true;
        }
        return false;
    }

    public Boolean Is_Valid_student_Name(EditText edt)
            throws NumberFormatException {
        if (edt.getText().toString().trim().length() <= 0) {
            alertDialog1(getString(R.string.enter_student_name));
            //  edt.setError(getString(R.string.enter_parent_name));
            //edt.requestFocus();
            s_name = null;
        } else if (edt.getText().toString().trim().length() <= 1) {
            alertDialog1(getString(R.string.enter_more_chars_lname));
            //edt.setError(getString(R.string.enter_more_chars_lname));
            // edt.requestFocus();
            s_name = null;
        } else if (!edt.getText().toString().trim().matches("[a-zA-Z ]+")) {
            // edt.setError("Accept Alphabets Only.");
            alertDialog1(getString(R.string.sname_alpha_only));
            // edt.setError(getString(R.string.lname_alpha_only));
            // edt.requestFocus();
            s_name = null;
        } else {
            //  input_last_name.setErrorEnabled(false);
            s_name = edt.getText().toString().trim();
            return true;
        }
        return false;
    }


}//EnquiryActivity
