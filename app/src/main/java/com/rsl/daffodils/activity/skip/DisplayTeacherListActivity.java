package com.rsl.daffodils.activity.skip;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.rsl.daffodils.R;
import com.rsl.daffodils.activity.AllStudentListActivity;
import com.rsl.daffodils.utils.ConnectionDetector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class DisplayTeacherListActivity extends AppCompatActivity {

    ListView list_teacher;
    MyCustomAdapter adapter;
    ArrayList<HashMap<String, String>> teacher_list_data = null;
    private ConnectionDetector internet;
    private LayoutInflater inflater;
    Toolbar mToolbar;
    SharedPreferences sp;
    SharedPreferences.Editor editor;
    TextView text_no_data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sp = getSharedPreferences("daffodils", 0);
        editor = sp.edit();

        setContentView(R.layout.activity_display_teacher_list);
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        internet = new ConnectionDetector(DisplayTeacherListActivity.this);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle(getString(R.string.expert_info));
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        teacher_list_data = new ArrayList<>();
        list_teacher = (ListView) findViewById(R.id.list_teacher);
        text_no_data = (TextView) findViewById(R.id.text_no_data);
        adapter = new MyCustomAdapter();
        list_teacher.setDividerHeight(0);
        list_teacher.setAdapter(adapter);
        if (internet.isConnectingToInternet()) {
            getTeacherList();
        } else {
            internet.showAlertDialog(DisplayTeacherListActivity.this, false);
        }

    }//onCreate

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public class MyCustomAdapter extends BaseAdapter {

        void add(HashMap<String, String> data) {
            teacher_list_data.add(data);
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {

            return teacher_list_data.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            View row = view;
            if (row == null) {
                row = inflater.inflate(R.layout.row_display_teacher_list, null);
            }
            ImageView img_teacher;
            TextView txt_teacher_name, txt_teacher_qualification, txt_teacher_subject, txt_teacher_experience;
            txt_teacher_name = (TextView) row.findViewById(R.id.txt_teacher_name);
            txt_teacher_qualification = (TextView) row.findViewById(R.id.txt_teacher_qualification);
            txt_teacher_subject = (TextView) row.findViewById(R.id.txt_teacher_subject);
            txt_teacher_experience = (TextView) row.findViewById(R.id.txt_teacher_experience);
            img_teacher = (ImageView) row.findViewById(R.id.img_teacher);

            Log.e("teacher_image","===="+sp.getString("new_profile",""));

            String img_url = sp.getString("new_profile","")+teacher_list_data.get(i).get("teacher_image");
            txt_teacher_name.setText(teacher_list_data.get(i).get("teacher_name"));
            txt_teacher_qualification.setText(teacher_list_data.get(i).get("qualification"));
            String sourceString = "<b>" + getString(R.string.expert) + "</b> " + teacher_list_data.get(i).get("teacher_subject");
            txt_teacher_subject.setText(Html.fromHtml(sourceString));
            String sourceString1 = "<b>" + getString(R.string.experience) + "</b> " + teacher_list_data.get(i).get("experience");
            txt_teacher_experience.setText(Html.fromHtml(sourceString1));

            Glide.with(getApplicationContext()).load(img_url).placeholder(R.drawable.daffodils_logo).into(img_teacher);


            return row;
        }
    }//MyCustomAdapter

//    public void getTeacherList() {
//
//        final ProgressDialog dialog = new ProgressDialog(DisplayTeacherListActivity.this);
//        dialog.setMessage("Loading please wait");
//        dialog.setCancelable(false);
//        dialog.show();
//        final RequestQueue queue = Volley.newRequestQueue(this);
//        String requestURL = getString(R.string.web_path) + "get_teachers_info.php";
//        StringRequest stringRequest = new StringRequest(Request.Method.POST, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.e("get_teachers_info", ":: Response :: " + response);
//
//                if (dialog.isShowing()) {
//                    dialog.dismiss();
//                }
//                try {
//                    JSONObject jobj = new JSONObject(response);
//                    teacher_list_data.clear();
//                    if (jobj.length() > 0) {
//                        if (jobj.getString("result").equals("success")) {
//                            text_no_data.setVisibility(View.GONE);
//                            dialog.dismiss();
//                            JSONArray jarray = jobj.getJSONArray("Teacher_Details");
//                            if (jarray.length() > 0) {
//                                for (int i = 0; i < jarray.length(); i++) {
//                                    HashMap<String, String> teacher_data = new HashMap<>();
//                                    JSONObject teacher = jarray.getJSONObject(i);
//                                    teacher_data.put("teacher_id", teacher.getString("teacher_id"));
//                                    teacher_data.put("teacher_image", teacher.getString("teacher_image"));
//                                    teacher_data.put("teacher_name", teacher.getString("teacher_name"));
//                                    teacher_data.put("teacher_subject", teacher.getString("teacher_subject"));
//                                    teacher_data.put("qualification", teacher.getString("qualification"));
//                                    teacher_data.put("experience", teacher.getString("experience"));
//                                    adapter.add(teacher_data);
//
//                                }
//
//                                adapter.notifyDataSetChanged();
//                            }
//                        } else if (jobj.getString("result").equals("failed")) {
//                            text_no_data.setVisibility(View.VISIBLE);
//                            text_no_data.setText(jobj.getString("msg"));
//                            dialog.dismiss();
//                        }
//                    } else {
//
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                    dialog.dismiss();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                dialog.dismiss();
//                Log.e("Error", "" + error.getMessage());
//                Toast.makeText(DisplayTeacherListActivity.this, getString(R.string.internet_lost), Toast.LENGTH_SHORT).show();
//            }
//        }) {
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                Map<String, String> params = new HashMap<String, String>();
//                params.put("page_number", "1");
//                Log.e("get_teachers_info", "param==" + params);
//                return params;
//            }
//
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                Map<String, String> params = new HashMap<String, String>();
//                params.put("Content-Type", "application/x-www-form-urlencoded");
//                return params;
//            }
//        };
//        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        queue.add(stringRequest);
//    }


    private void getTeacherList() {

        final ProgressDialog dialog = new ProgressDialog(DisplayTeacherListActivity.this);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.str_requesting));
        dialog.show();
        RequestQueue queue = Volley.newRequestQueue(DisplayTeacherListActivity.this);
        String requestURL = sp.getString("new_link","") + "teacherlist";
        Log.e("requestURL", "" + requestURL);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", "code=" + response);
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response != null) {
                    if (response.equals("204")) {
                        // text_msg.setVisibility(View.VISIBLE);
                        //  text_msg.setText("No Records Available");
                        alertDialog1("No Records Available");
                        Log.e("Error message", "No Records Available");
                    } else {
//                        text_no_data.setVisibility(View.GONE);
//                        adapter = new CarCompanyAdapter(car_company, CarComapnyActivity.this);
//                        recyclerView_company.setAdapter(adapter);
//                        adapter.notifyDataSetChanged();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
              //  Toast.makeText(getApplicationContext(), "Poor Internet Coonection", Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                // params.put("id",id);
                Log.e("param", "" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                    VolleyError error = new VolleyError(new String(volleyError.networkResponse.data));
                    volleyError = error;
                    dialog.dismiss();
                }
                // Toast.makeText(getApplicationContext(),"Poor Internet Coonection",Toast.LENGTH_SHORT).show();
                Log.e("parseNetworkError", "==" + volleyError);
                Log.e("parseNetworkError", "string==" + volleyError.toString());
                Log.e("parseNetworkError", "msg==" + volleyError.getMessage());
                return volleyError;
            }

            @Override
            protected Response<String> parseNetworkResponse(final NetworkResponse response) {
                String responseString = "";
                if (response != null) {
                    responseString = String.valueOf(response.statusCode);
                    // can get more details such as response.headers
                }
                Map<String, String> responseHeaders = response.headers;
                if (response.statusCode == 200) {
                    Response<String> result = Response.success(responseHeaders.get("Content-Type"), HttpHeaderParser.parseCacheHeaders(response));
                    Log.e("Response_teacherlist", "" + parseToString(response));


                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                JSONArray JArray = new JSONArray(parseToString(response));

                                for (int i = 0; i < JArray.length(); i++) {
                                    JSONObject jobj_Data = JArray.getJSONObject(i);
                                    HashMap<String, String> teacher_data = new HashMap<>();
                                    teacher_data.put("teacher_id", jobj_Data.getString("teacher_id"));
                                    teacher_data.put("teacher_name", jobj_Data.getString("teacher_name"));
                                    teacher_data.put("teacher_email", jobj_Data.getString("teacher_email"));
                                    teacher_data.put("teacher_mobile", jobj_Data.getString("teacher_mobile"));
                                    teacher_data.put("teacher_subject", jobj_Data.getString("teacher_subject"));
                                    teacher_data.put("qualification", jobj_Data.getString("qualification"));
                                    teacher_data.put("experience", jobj_Data.getString("experience"));
                                    teacher_data.put("teacher_image", jobj_Data.getString("teacher_image"));
                                    adapter.add(teacher_data);

                                }

                                adapter.notifyDataSetChanged();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });

                    return result;
                } else {


                    // text_msg.setText(jObj.getString("msg"));
                    Log.e("Error message", "Login Failed");
                }
                Log.e("responseString", "" + responseString);
                return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
            }

            public String parseToString(NetworkResponse response) {
                String parsed;
                try {
                    parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                } catch (UnsupportedEncodingException e) {
                    parsed = new String(response.data);
                }
                return parsed;
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }//display_student


    private void alertDialog1(String error) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                DisplayTeacherListActivity.this);
        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setIcon(R.drawable.app_logo);
        alertDialog.setMessage(error);
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton(getString(R.string.dlg_ok),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }


}
