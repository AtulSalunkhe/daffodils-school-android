package com.rsl.daffodils.activity.student;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.rsl.daffodils.R;
import com.rsl.daffodils.activity.AllStudentListActivity;
import com.rsl.daffodils.activity.skip.StudentContactUsActivity;
import com.rsl.daffodils.adapter.StudentHomeAdapter;
import com.rsl.daffodils.utils.BadgeView;
import com.rsl.daffodils.utils.ConnectionDetector;
import com.rsl.daffodils.utils.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.rsl.daffodils.model.Constants.studentHomeActivity;

public class StudentsHomeActivity extends AppCompatActivity {
    GridView gridview;
    static final int send_data = 1;
    BottomNavigationView navigation;
    public static int[] MenuImages = {R.drawable.ic_news, R.drawable.ic_events, R.drawable.ic_calendar_clock_white_48dp,
            R.drawable.ic_gallery_new, R.drawable.ic_feedback, R.drawable.ic_timetable_white_48dp, R.drawable.ic_video_white_48dp, R.drawable.ic_students, R.drawable.ic_onenote_white_48dp, R.drawable.facebook, R.drawable.ic_teacher_chat,R.drawable.result};
    public static int[] MenuLabels = {R.string.str_stud_notification, R.string.str_stud_calender, R.string.str_stud_attendence, R.string.str_stud_gallery, R.string.str_stud_feedback, R.string.str_stud_timetable, R.string.str_stud_videochannel, R.string.str_stud_profile, R.string.str_stud_notes, R.string.str_stud_facebook, R.string.str_stud_switch_student,R.string.str_result};
    public static int[] colors = {R.color.green_color, R.color.pink_color, R.color.yellow_color, R.color.red_color, R.color.lightblue_color, R.color.black, R.color.event_color, R.color.event_holiday_color, R.color.event_exam_color, R.color.news_row, R.color.primary_faint};

    SharedPreferences sp;
    SharedPreferences.Editor editor;
    SessionManager session;
    TextView txt_logout;
    ConnectionDetector internet;
    StudentHomeAdapter adapter;
    ImageView img_slider;
    TextSliderView textSliderView;
    ArrayList<HashMap<String, String>> list_data;
    private SliderLayout mDemoSlider;
    int[] imageId = {R.drawable.slide1, R.drawable.slide2, R.drawable.slide3, R.drawable.slide4, R.drawable.slide5, R.drawable.slide6};



    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    return true;
                case R.id.navigation_contacts:
                    navigation.getMenu().getItem(1).setChecked(false);
                    Intent in = new Intent(StudentsHomeActivity.this, StudentContactUsActivity.class);
                    startActivity(in);
                    finish();
                    return true;
                case R.id.navigation_share:
//                    if (internet.isConnectingToInternet()) {

                    String myText = getResources().getString(R.string.app_text);
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    //add a subject
                    sendIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,
                            getResources().getString(R.string.app_name));
                    sendIntent.putExtra(Intent.EXTRA_TEXT, myText + " " + getResources().getString(R.string.link));
                    sendIntent.setType("text/plain");
                    startActivityForResult(sendIntent, send_data);
//                    } else {
//                        internet.showAlertDialog(StudentsHomeActivity.this, false);
//                    }
                    return false;
            }
            return false;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_home);
        sp = getSharedPreferences("daffodils", 0);
        editor = sp.edit();
        editor.apply();
        session = new SessionManager(StudentsHomeActivity.this);
        internet = new ConnectionDetector(StudentsHomeActivity.this);
        list_data=new ArrayList<HashMap<String, String>>();
        txt_logout = (TextView) findViewById(R.id.txt_logout);
        img_slider = (ImageView) findViewById(R.id.img_slider);
        mDemoSlider = (SliderLayout) findViewById(R.id.view_pager);
        txt_logout.setOnClickListener(new onclicklistener());
        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigation.getMenu().getItem(0).setChecked(true);
        gridview = (GridView) findViewById(R.id.grid_home);
        adapter = new StudentHomeAdapter(StudentsHomeActivity.this, MenuImages, MenuLabels, colors);
        gridview.setAdapter(adapter);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ContextCompat.getColor(StudentsHomeActivity.this, R.color.colorPrimaryDark));
        }
        textSliderView = new TextSliderView(StudentsHomeActivity.this);


        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(networkStateReceiver, filter);


    }//oncreate



    BroadcastReceiver networkStateReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            boolean noConnectivity = intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, false);
            if (!noConnectivity) {
                onConnectionFound();
            } else {
                onConnectionLost();
            }
        }
    };

    public void onConnectionFound() {
        display_images();
    }

    public void onConnectionLost() {
        //Toast.makeText(getActivity(), "Connection lost", Toast.LENGTH_LONG).show();
    }



    private class onclicklistener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.txt_logout: {
                    if (internet.isConnectingToInternet()) {
                        final AlertDialog.Builder builder = new AlertDialog.Builder(StudentsHomeActivity.this);
                        builder.setTitle(getString(R.string.app_name));
                        builder.setIcon(R.drawable.daffodils_logo);
                        builder.setMessage(R.string.sure_logout);
                        builder.setCancelable(false);
                        builder.setPositiveButton(R.string.dlg_ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                getOut();
                                dialogInterface.dismiss();
                            }
                        });
                        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });

                        AlertDialog alert = builder.create();
                        alert.show();
                    } else {
                        internet.showAlertDialog(StudentsHomeActivity.this, false);
                    }
                }
                break;
                default:
                    break;
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        studentHomeActivity = true;
        getcount(sp.getString("student_id", ""));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        studentHomeActivity = false;
        unregisterReceiver(networkStateReceiver);
    }

    @Override
    protected void onPause() {
        super.onPause();
        studentHomeActivity = false;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.e("intent_on_home", "" + intent);
        setIntent(intent);
        getcount(sp.getString("student_id", ""));
    }


    private void getOut() {

        final ProgressDialog dialog = new ProgressDialog(StudentsHomeActivity.this);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.str_requesting));
        dialog.show();
        RequestQueue queue = Volley.newRequestQueue(StudentsHomeActivity.this);
        String requestURL = sp.getString("new_link","")+ "logout";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", "code=" + response);
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response != null) {
                    if (response.equals("204")) {
                        // text_msg.setVisibility(View.VISIBLE);
                        //  text_msg.setText("No Records Available");
                        //alertDialog1("No Records Available");
                        Log.e("Error message", "No Records Available");
                    } else {
//                        text_no_data.setVisibility(View.GONE);
//                        adapter = new CarCompanyAdapter(car_company, CarComapnyActivity.this);
//                        recyclerView_company.setAdapter(adapter);
//                        adapter.notifyDataSetChanged();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                //Toast.makeText(getApplicationContext(), "Poor Internet Coonection", Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("account_id", sp.getString("account_id", ""));
                params.put("entry_code", sp.getString("entry_code", ""));

                Log.e("param", "logout" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                    VolleyError error = new VolleyError(new String(volleyError.networkResponse.data));
                    volleyError = error;
                    dialog.dismiss();
                }
                // Toast.makeText(getApplicationContext(),"Poor Internet Coonection",Toast.LENGTH_SHORT).show();
                Log.e("parseNetworkError", "==" + volleyError);
                Log.e("parseNetworkError", "string==" + volleyError.toString());
                Log.e("parseNetworkError", "msg==" + volleyError.getMessage());
                return volleyError;
            }

            @Override
            protected Response<String> parseNetworkResponse(final NetworkResponse response) {
                String responseString = "";
                if (response != null) {
                    responseString = String.valueOf(response.statusCode);
                    // can get more details such as response.headers
                }
                Map<String, String> responseHeaders = response.headers;
                if (response.statusCode == 200) {
                    Response<String> result = Response.success(responseHeaders.get("Content-Type"), HttpHeaderParser.parseCacheHeaders(response));
                    Log.e("logout", "" + parseToString(response));

                    runOnUiThread(new Runnable() {
                        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                        @Override
                        public void run() {
//                            String link=sp.getString("new_link","");
//                            sp.edit().clear().apply();
//                            sp.edit().putString("new_link",link).apply();
                            // Creating Session
                            session.logoutUser();
                            finishAffinity();
                            Toast.makeText(StudentsHomeActivity.this, "" + "Logout", Toast.LENGTH_SHORT).show();

                        }
                    });

                    return result;
                } else {


                    // text_msg.setText(jObj.getString("msg"));
                    Log.e("Error message", "Login Failed");
                }
                Log.e("responseString", "" + responseString);
                return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
            }

            public String parseToString(NetworkResponse response) {
                String parsed;
                try {
                    parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                } catch (UnsupportedEncodingException e) {
                    parsed = new String(response.data);
                }
                return parsed;
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 4, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }//getOut




    private void display_images() {
        final ProgressDialog dialog = new ProgressDialog(StudentsHomeActivity.this);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.str_requesting));
        dialog.show();
        RequestQueue queue = Volley.newRequestQueue(StudentsHomeActivity.this);
        String requestURL = sp.getString("new_link","")+ "get_home_imgaes";
        Log.e("requestURL", "images" + requestURL);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", "code=" + response);
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response != null) {
                    if (response.equals("204")) {
                        // text_msg.setVisibility(View.VISIBLE);
                        //  text_msg.setText("No Records Available");
                        alertDialog1("No image available");
                        Log.e("Error message", "No Records Available");
                    } else {
//                        text_no_data.setVisibility(View.GONE);
//                        adapter = new CarCompanyAdapter(car_company, CarComapnyActivity.this);
//                        recyclerView_company.setAdapter(adapter);
//                        adapter.notifyDataSetChanged();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                //Toast.makeText(getApplicationContext(), "Poor Internet Coonection", Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                // params.put("id",id);
                Log.e("param", "" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                    VolleyError error = new VolleyError(new String(volleyError.networkResponse.data));
                    volleyError = error;
                    dialog.dismiss();
                }
                // Toast.makeText(getApplicationContext(),"Poor Internet Coonection",Toast.LENGTH_SHORT).show();
                Log.e("parseNetworkError", "==" + volleyError);
                Log.e("parseNetworkError", "string==" + volleyError.toString());
                Log.e("parseNetworkError", "msg==" + volleyError.getMessage());
                return volleyError;
            }

            @Override
            protected Response<String> parseNetworkResponse(final NetworkResponse response) {
                String responseString = "";
                if (response != null) {
                    responseString = String.valueOf(response.statusCode);
                    // can get more details such as response.headers
                }
                Map<String, String> responseHeaders = response.headers;
                if (response.statusCode == 200) {
                    Response<String> result = Response.success(responseHeaders.get("Content-Type"), HttpHeaderParser.parseCacheHeaders(response));
                    Log.e("Response_image", "" + parseToString(response));

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                JSONArray JArray = new JSONArray(parseToString(response));

                                for (int i = 0; i < JArray.length(); i++) {
                                    JSONObject jobj_Data = JArray.getJSONObject(i);

                                    HashMap<String, String> offers = new HashMap<String, String>();
                                    offers.put("img_path",jobj_Data.getString("img_path"));
                                    list_data.add(offers);

                                }//for
                                for (int i = 0; i <list_data.size(); i++) {
                                    TextSliderView textSliderView = new TextSliderView(StudentsHomeActivity.this);
                                    // initialize a SliderLayout
                                    textSliderView
                                            .image(list_data.get(i).get("img_path"))
                                            .setScaleType(BaseSliderView.ScaleType.Fit);
                                    mDemoSlider.addSlider(textSliderView);
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });

                    return result;
                } else {


                    Log.e("Error message", "Login Failed");
                }
                Log.e("responseString", "" + responseString);
                return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
            }

            public String parseToString(NetworkResponse response) {
                String parsed;
                try {
                    parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                } catch (UnsupportedEncodingException e) {
                    parsed = new String(response.data);
                }
                return parsed;
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 6, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }//display_image




    private void alertDialog1(String error) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                StudentsHomeActivity.this);
        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setIcon(R.drawable.daffodils_logo);
        alertDialog.setMessage(error);
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton(getString(R.string.dlg_ok),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }


    private boolean doublebackpressed = false;

    @Override
    public void onBackPressed() {

        if (doublebackpressed) {
            super.onBackPressed();
            finish();
        }
        Toast t = Toast.makeText(StudentsHomeActivity.this, getString(R.string.str_double_backPress_message), Toast.LENGTH_SHORT);
        t.setGravity(Gravity.CENTER, 0, 0);
        t.show();
        doublebackpressed = true;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doublebackpressed = false;
            }
        }, 2000);

    }//onBackPressed


    private void getcount(final String id) {

        final ProgressDialog dialog = new ProgressDialog(StudentsHomeActivity.this);
        dialog.setMessage(getString(R.string.str_requesting));
        dialog.setCancelable(false);
        dialog.show();
        RequestQueue queue = Volley.newRequestQueue(StudentsHomeActivity.this);
        String requestURL = sp.getString("new_link","") + "get_student_notification_count/" + id;
        Log.e("requestURL", "URL" + requestURL);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", "code=" + response);
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response != null) {
                    if (response.equals("204")) {
                        // txt_message.setVisibility(View.VISIBLE);
                        // txt_message.setText("No Records Available");
                       // alertDialog1("No Records Available");
                        Log.e("Error message", "No Records Available");
                    } else {
//                        text_no_data.setVisibility(View.GONE);
//                        adapter = new CarCompanyAdapter(car_company, CarComapnyActivity.this);
//                        recyclerView_company.setAdapter(adapter);
//                        adapter.notifyDataSetChanged();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                //Toast.makeText(StudentsHomeActivity.this, "Poor Internet Coonection", Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                Log.e("param", "=" + params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                    VolleyError error = new VolleyError(new String(volleyError.networkResponse.data));
                    volleyError = error;
                    dialog.dismiss();
                }
                //  Toast.makeText(getActivity(),"Poor Internet Coonection",Toast.LENGTH_SHORT).show();
                Log.e("parseNetworkError", "==" + volleyError);
                Log.e("parseNetworkError", "string==" + volleyError.toString());
                Log.e("parseNetworkError", "msg==" + volleyError.getMessage());
                return volleyError;
            }

            @Override
            protected Response<String> parseNetworkResponse(final NetworkResponse response) {
                String responseString = "";
                if (response != null) {
                    responseString = String.valueOf(response.statusCode);
                    // can get more details such as response.headers
                }
                Map<String, String> responseHeaders = response.headers;
                if (response.statusCode == 200) {
                    Response<String> result = Response.success(responseHeaders.get("Content-Type"), HttpHeaderParser.parseCacheHeaders(response));
                    Log.e("Response", "= count =" + parseToString(response));

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            try {
                                JSONArray JArray = new JSONArray(parseToString(response));

                                for (int i = 0; i < JArray.length(); i++) {
                                    JSONObject jObj = JArray.getJSONObject(i);

                                    Log.e("announcement", "" + jObj.getString("Announcement_count"));
                                    Log.e("message", "" + jObj.getString("Message_count"));
                                    Log.e("homework", "" + jObj.getString("Homework_count"));
                                    Log.e("Notification_count", "" + jObj.getString("Notification_count"));
                                    Log.e("Event_count", "" + jObj.getString("Event_count"));
                                    Log.e("Student_corner_count", "" + jObj.getString("Student_corner_count"));


                                    StudentHomeAdapter.count[0] = jObj.getString("Notification_count");
                                    StudentHomeAdapter.count[1] = jObj.getString("Event_count");
                                    StudentHomeAdapter.count[8] = jObj.getString("Student_corner_count");
//                                    StudentHomeAdapter.count[3] = jObj.getString("event_count");
//                                    StudentHomeAdapter.count[5] = jObj.getString("reference_count");
//                                    StudentHomeAdapter.count[6] = jObj.getString("forum_count");
                                    adapter.notifyDataSetChanged();

                                }
                            }//for
                            catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });

                    return result;
                } else {


                    Log.e("Error message", "Login Failed");
                }
                Log.e("responseString", "" + responseString);
                return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
            }

            public String parseToString(NetworkResponse response) {
                String parsed;
                try {
                    parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                } catch (UnsupportedEncodingException e) {
                    parsed = new String(response.data);
                }
                return parsed;
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }//getcount


    private void getupdatecount() {

        final ProgressDialog dialog = new ProgressDialog(StudentsHomeActivity.this);
        dialog.setMessage(getString(R.string.str_requesting));
        dialog.setCancelable(false);
        dialog.show();
        RequestQueue queue = Volley.newRequestQueue(StudentsHomeActivity.this);
        String requestURL = sp.getString("new_link","")+ "update_student_notification_count";
        Log.e("requestURL", "URL" + requestURL);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", "code=" + response);
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response != null) {
                    if (response.equals("204")) {
                        // txt_message.setVisibility(View.VISIBLE);
                        // txt_message.setText("No Records Available");
                        alertDialog1("No Records Available");
                        Log.e("Error message", "No Records Available");
                    } else {
//                        text_no_data.setVisibility(View.GONE);
//                        adapter = new CarCompanyAdapter(car_company, CarComapnyActivity.this);
//                        recyclerView_company.setAdapter(adapter);
//                        adapter.notifyDataSetChanged();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                Toast.makeText(StudentsHomeActivity.this, "Poor Internet Coonection", Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
//                type=ANNOUNCEMENT&student_id=73
                params.put("password", sp.getString("student_id", ""));
                params.put("password", sp.getString("type", ""));


                Log.e("param", "=" + params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                    VolleyError error = new VolleyError(new String(volleyError.networkResponse.data));
                    volleyError = error;
                    dialog.dismiss();
                }
                //  Toast.makeText(getActivity(),"Poor Internet Coonection",Toast.LENGTH_SHORT).show();
                Log.e("parseNetworkError", "==" + volleyError);
                Log.e("parseNetworkError", "string==" + volleyError.toString());
                Log.e("parseNetworkError", "msg==" + volleyError.getMessage());
                return volleyError;
            }

            @Override
            protected Response<String> parseNetworkResponse(final NetworkResponse response) {
                String responseString = "";
                if (response != null) {
                    responseString = String.valueOf(response.statusCode);
                    // can get more details such as response.headers
                }
                Map<String, String> responseHeaders = response.headers;
                if (response.statusCode == 200) {
                    Response<String> result = Response.success(responseHeaders.get("Content-Type"), HttpHeaderParser.parseCacheHeaders(response));
                    Log.e("Response", "= count =" + parseToString(response));

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

//                            try {
//
//
//                                }
//                            }//for
//                            catch (JSONException e) {
//                                e.printStackTrace();
//                            }

                        }
                    });

                    return result;
                } else {


                    Log.e("Error message", "Login Failed");
                }
                Log.e("responseString", "" + responseString);
                return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
            }

            public String parseToString(NetworkResponse response) {
                String parsed;
                try {
                    parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                } catch (UnsupportedEncodingException e) {
                    parsed = new String(response.data);
                }
                return parsed;
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }//getcount

    private void alertDialog(String message) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(StudentsHomeActivity.this);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setIcon(R.drawable.daffodils_logo);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();

    }

    private void alertDialogInactive(String errer) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                StudentsHomeActivity.this);
        alertDialog.setTitle(R.string.app_name);
        alertDialog.setMessage(errer);
        alertDialog.setCancelable(false);
        alertDialog.setIcon(R.drawable.daffodils_logo);
        alertDialog.setPositiveButton(getString(R.string.dlg_ok),
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        getOut();
                    }
                });

        alertDialog.show();
    }


}
