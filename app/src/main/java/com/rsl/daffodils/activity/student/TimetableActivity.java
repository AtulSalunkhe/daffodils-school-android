package com.rsl.daffodils.activity.student;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.rsl.daffodils.R;
import com.rsl.daffodils.utils.ConnectionDetector;

import org.json.JSONException;

import java.util.HashMap;
import java.util.Map;

public class TimetableActivity extends AppCompatActivity {

    WebView webView;
    String url;
    SharedPreferences sp;
    SharedPreferences.Editor editor;
    ConnectionDetector internet;
    TextView txt_message;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timetable);


        sp = getSharedPreferences("daffodils", 0);
        editor = sp.edit();
        editor.apply();
        internet = new ConnectionDetector(TimetableActivity.this);
        initView();
        webView = (WebView) findViewById(R.id.webview);

        if (internet.isConnectingToInternet()) {
            webView.setWebViewClient(new MyWebViewClient());
            url = "http://mywedding-app.com/Daffodils/ws/timetable.php?class_id="+sp.getString("class_id","");
            Log.e("url","=="+url);
            webView.getSettings().setJavaScriptEnabled(true);
            webView.loadUrl(url);
        } else {
            internet.showAlertDialog(TimetableActivity.this, false);
            txt_message.setText(getString(R.string.str_internet_title));
            txt_message.setVisibility(View.VISIBLE);
            webView.setVisibility(View.GONE);
        }




      //  webView.getSettings().setUseWideViewPort(true);
     //   webView.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);
       //+ webView.getSettings().setBuiltInZoomControls(true);

        //This will zoom out the WebView
//        webView.getSettings().setUseWideViewPort(true);
//        webView.getSettings().setLoadWithOverviewMode(true);
//        webView.setInitialScale(30);
       // webView.setInitialScale(5);


    }//onCreate



    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (url.startsWith("http:") || url.startsWith("https:")) {
                return false;
            }
            // Otherwise allow the OS to handle it
            else if (url.startsWith("tel:")) {
                Intent tel = new Intent(Intent.ACTION_DIAL, Uri.parse(url));
                startActivity(tel);
                return true;
            } else if (url.startsWith("mailto:")) {
                String body = "Enter your Question, Enquiry or Feedback below:\n\n";
                Intent mail = new Intent(Intent.ACTION_SEND);
                mail.setType("application/octet-stream");
                mail.putExtra(Intent.EXTRA_EMAIL, new String[]{"rsltechinfo@gmail.com"});
                mail.putExtra(Intent.EXTRA_SUBJECT, "Enquiry");
                mail.putExtra(Intent.EXTRA_TEXT, body);
                startActivity(mail);
                return true;
            }
            return true;
        }
    }//MyWebViewClient


    private void initView() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.str_stud_timetable));
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        txt_message = (TextView) findViewById(R.id.txt_message);
    }//initView


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent in = new Intent(TimetableActivity.this, StudentsHomeActivity.class);
        startActivity(in);
        finish();
    }//onBackPressed


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }//onOptionsItemSelected







}//TimetableActivity
