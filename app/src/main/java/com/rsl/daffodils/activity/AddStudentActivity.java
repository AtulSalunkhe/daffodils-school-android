package com.rsl.daffodils.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.camera.CropImageIntentBuilder;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import com.rsl.daffodils.BuildConfig;

import com.rsl.daffodils.R;

import com.rsl.daffodils.model.classes;
import com.rsl.daffodils.utils.ConnectionDetector;
import com.rsl.daffodils.utils.SessionManager;
import com.rsl.daffodils.utils.Utility;
import com.soundcloud.android.crop.Crop;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnShowRationale;
import permissions.dispatcher.PermissionRequest;

public class AddStudentActivity extends AppCompatActivity {

    EditText edt_student_name ,edt_gender, edt_dob;
    String gender = "";
    String select_date = "";
    int gender_code = 1;
    private int myear, mmonth, mday;

    private int myear1, mmonth1, mday1;
    static final int DATE_DIALOG_ID = 999;
    private int mYear;
    private int mMonth;
    private int mDay;
    ImageView imageView_profile;
    private String userChoosenTask;
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private int REQUEST_CAMERA_LOWER = 2;
    private static final int CAMERA_CROP = 12;
    private static final int REQUEST_CROP_PICTURE = 786;
    private static final String TEMP_PHOTO_FILE = "temporary_holder.jpg";
    String filePath = "";
    String encoded_image = "";
    ImageView img_back_press;
    String mCurrentPhotoPath;
    private RadioGroup radioGroup;
    Uri photoURI;
    RelativeLayout relative_class;
    Button btn_signup;
    String spinner_text,student_name;

    //for spinner to select class
    Spinner spinner_class;
    String selected_class,class_id;
    ArrayList<HashMap<String, String>> try_class_list = new ArrayList<HashMap<String, String>>();
    ArrayList<String> class_name = new ArrayList<>();
    ArrayList<classes> arr_class = new ArrayList<classes>();
    HashMap<String, String> try_class_data;
    boolean classes_flag=false;

    SharedPreferences sp;
    SharedPreferences.Editor editor;
    SessionManager session;
    ConnectionDetector internet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_student);
        sp = getSharedPreferences("daffodils", 0);
        editor = sp.edit();
        initView();
      //  editor.apply();
        internet = new ConnectionDetector(AddStudentActivity.this);
        session = new SessionManager(getApplicationContext());
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        filePath = Environment.getExternalStorageDirectory() + "/Android/data/" + getPackageName() + "/" + TEMP_PHOTO_FILE;
        String PATH = Environment.getExternalStorageDirectory() + "/Android/data/" + getPackageName() + "/";
        File folder = new File(PATH);
        if (!folder.exists()) {
            folder.mkdir();//If there is no folder it will be created.
            Log.e("folder====", "created");
        }
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        Log.e("Current ", "" + mYear + "" + mMonth + "" + mDay);
        edt_dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  input_gender.setErrorEnabled(false);
                showDialog(DATE_DIALOG_ID);
            }
        });

        radioGroup = (RadioGroup) findViewById(R.id.myRadioGroup);


        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override

            public void onCheckedChanged(RadioGroup group, int checkedId) {

                // find which radio button is selected

                if (checkedId == R.id.radio_male) {

                    gender_code = 1;
                    gender = "Male";

                } else if (checkedId == R.id.radio_female) {

                    gender_code = 2;
                    gender = "Female";

                }

            }
        });

        relative_class=(RelativeLayout)findViewById(R.id.relative_class);


        spinner_class.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()

                                                {
                                                    @Override
                                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                                        // if (position > 0) {
                                                        classes_flag = true;
                                                        selected_class = try_class_list.get(position).get("class");
                                                        Log.e("selected_class","="+selected_class);
                                                        Log.e("account_id","="+try_class_list.get(position).get("class_id"));
                                                        class_id=try_class_list.get(position).get("class_id");
                                                        Log.e("class id","----"+class_id);

                                                             /* editor.putString("selected_country", selected_teacher).apply();
                                                              editor.apply();*/
                                                         /* }
                                                          if (position == 0) {
                                                              country_flag = false;

                                                          }*/
                                                    }

                                                    @Override
                                                    public void onNothingSelected(AdapterView<?> parent) {

                                                    }
                                                }

        );





        imageView_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // startCamera();
                selectImage();
            }
        });
        edt_gender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Creating the instance of PopupMenu
                PopupMenu popup = new PopupMenu(AddStudentActivity.this, edt_gender);
                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(R.menu.poupup_menu, popup.getMenu());

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        gender = item.getTitle().toString();
                        edt_gender.setText(item.getTitle().toString());
                        //   Toast.makeText(getActivity(),"You Clicked : " + item.getTitle(),Toast.LENGTH_SHORT).show();
                        return true;
                    }
                });

                popup.show();//showing popup menu
            }
        });
        btn_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //registeruser();
                if (!internet.isConnectingToInternet()) {
                    internet.showAlertDialog(AddStudentActivity.this, false);
                } else {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);


                    student_name = edt_student_name.getText().toString().trim();
                    spinner_text = spinner_class.getSelectedItem().toString();
                    select_date = edt_dob.getText().toString().trim();

                    if (!encoded_image.equals("")) {
                        if (Is_Valid_Student_Name(edt_student_name)) {
                            if (!gender.equals("")) {
                                //input_gender.setErrorEnabled(false);
                                if (!select_date.equals("")) {
                                    //input_dob.setErrorEnabled(false);
                                    Log.e("if", "date");

                                    addstudent();

                                } else {
                                    Log.e("else", "date");
                                    //Toast.makeText(getApplicationContext(), getResources().getString(R.string.select_dob), Toast.LENGTH_SHORT).show();
                                    alertDialog(getResources().getString(R.string.select_dob));
                                    // edt_dob.setError(getResources().getString(R.string.select_dob));
                                    edt_dob.requestFocus();
                                }
                            } else {
                                //  Toast.makeText(getApplicationContext(), getString(R.string.select_gender), Toast.LENGTH_SHORT).show();
                                alertDialog(getResources().getString(R.string.select_gender));
                                //    edt_gender.setError(getResources().getString(R.string.select_gender));
                                edt_gender.requestFocus();
                            }

                        }
                    } else {
                        //  Toast.makeText(getApplicationContext(), getString(R.string.select_gender), Toast.LENGTH_SHORT).show();
                        alertDialog(getResources().getString(R.string.select_image));
                        //    edt_gender.setError(getResources().getString(R.string.select_gender));
                        // edt_gender.requestFocus();
                    }
                }
            }
        });


    }//onCreate


    private void addstudent() {
        final ProgressDialog dialog = new ProgressDialog(AddStudentActivity.this);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.str_requesting));
        dialog.show();
        RequestQueue queue = Volley.newRequestQueue(AddStudentActivity.this);
        String requestURL = sp.getString("new_link","") + "add_student" ;
        Log.e("requestURL", "" + requestURL);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", "code=" + response);
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response != null) {
                    if (response.equals("204")) {
                        // text_msg.setVisibility(View.VISIBLE);
                        //  text_msg.setText("No Records Available");
                        alertDialog("No Records Available");
                        Log.e("Error message", "No Records Available");
                    } else {
//                        text_no_data.setVisibility(View.GONE);
//                        adapter = new CarCompanyAdapter(car_company, CarComapnyActivity.this);
//                        recyclerView_company.setAdapter(adapter);
//                        adapter.notifyDataSetChanged();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                //Toast.makeText(getApplicationContext(), "Poor Internet Coonection", Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                // params.put("id",id);
                params.put("student_name", edt_student_name.getText().toString());
                params.put("class_id", class_id);
                params.put("gender_id", String.valueOf(gender_code));
                params.put("dob", edt_dob.getText().toString().trim());
                params.put("profile_image", encoded_image);
                params.put("account_id",sp.getString("account_id","") );
                Log.e("param", "" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                    VolleyError error = new VolleyError(new String(volleyError.networkResponse.data));
                    volleyError = error;
                    dialog.dismiss();
                }
                // Toast.makeText(getApplicationContext(),"Poor Internet Coonection",Toast.LENGTH_SHORT).show();
                Log.e("parseNetworkError", "==" + volleyError);
                Log.e("parseNetworkError", "string==" + volleyError.toString());
                Log.e("parseNetworkError", "msg==" + volleyError.getMessage());
                return volleyError;
            }

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                String responseString = "";
                if (response != null) {
                    responseString = String.valueOf(response.statusCode);
                    // can get more details such as response.headers
                }
                Map<String, String> responseHeaders = response.headers;
                if (response.statusCode == 200) {
                    Response<String> result = Response.success(responseHeaders.get("Content-Type"), HttpHeaderParser.parseCacheHeaders(response));
                    Log.e("Response_add_stude", "" + parseToString(response));




                    return result;
                } else {
                    if (response.statusCode == 201) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                alertDialog1("New admission request accepted successfully, please wait for the confirmation");

                            }

                        });
                    }
                    Log.e("Error message", "Login Failed");
                }
                Log.e("responseString", "" + responseString);
                return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
            }

            public String parseToString(NetworkResponse response) {
                String parsed;
                try {
                    parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                } catch (UnsupportedEncodingException e) {
                    parsed = new String(response.data);
                }
                return parsed;
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 4, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }//getCarInsuranceCompanies

    private void alertDialog(String errer) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                AddStudentActivity.this);
        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setMessage(errer);
        alertDialog.setCancelable(false);
        alertDialog.setIcon(R.drawable.daffodils_logo);
        alertDialog.setPositiveButton(getString(R.string.dlg_ok),
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                    }
                });

        alertDialog.show();
    }//alertDialog

    public Boolean Is_Valid_Student_Name(EditText edt)
            throws NumberFormatException {
        if (edt.getText().toString().trim().length() <= 0) {
            alertDialog(getString(R.string.enter_student_name));
            //  edt.setError(getString(R.string.enter_parent_name));
            edt.requestFocus();
            student_name = null;
        } else if (edt.getText().toString().trim().length() <= 1) {
            alertDialog(getString(R.string.enter_more_chars_lname));
            //edt.setError(getString(R.string.enter_more_chars_lname));
            edt.requestFocus();
            student_name = null;
        } else if (!edt.getText().toString().trim().matches("[a-zA-Z ]+")) {
            // edt.setError("Accept Alphabets Only.");
            alertDialog(getString(R.string.sname_alpha_only));
            // edt.setError(getString(R.string.lname_alpha_only));
            edt.requestFocus();
            student_name = null;
        } else {
            //  input_last_name.setErrorEnabled(false);
            student_name = edt.getText().toString().trim();
            return true;
        }
        return false;
    }//Is_Valid_Student_Name

    private void initView() {
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle(R.string.str_add_student);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        edt_student_name = (EditText) findViewById(R.id.edt_student_name);//student name
        spinner_class = (Spinner) findViewById(R.id.spinner_class);//spinner for class list
        edt_gender = (EditText) findViewById(R.id.edt_gender);//select gender
        edt_dob = (EditText) findViewById(R.id.edt_dob);//select date of birth
        btn_signup = (Button) findViewById(R.id.btn_signup);//registration button
        imageView_profile = (ImageView) findViewById(R.id.user_profile_photo);// user photo imageview

        getClassList();
    }


    private void alertDialog1(String errer) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                AddStudentActivity.this);
        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setMessage(errer);
        alertDialog.setCancelable(false);
        alertDialog.setIcon(R.drawable.daffodils_logo);
        alertDialog.setPositiveButton(getString(R.string.dlg_ok),
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                      //  Intent intent = new Intent(AddStudentActivity.this,AllStudentListActivity.class);
                       // startActivity(intent);
                        finish();

                    }
                });



        alertDialog.show();
    }//alertDialog1



    private boolean doublebackpressed = false;


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (doublebackpressed) {
            super.onBackPressed();
            finish();
        }
        Toast t = Toast.makeText(AddStudentActivity.this, getString(R.string.str_double_backPress_message), Toast.LENGTH_SHORT);
        t.setGravity(Gravity.CENTER, 0, 0);
        t.show();
        doublebackpressed = true;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doublebackpressed = false;
            }
        }, 2000);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(AddStudentActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = Utility.checkPermission(AddStudentActivity.this);

                if (items[item].equals("Take Photo")) {
                    userChoosenTask = "Take Photo";
                    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                        Log.e("version","IF"+android.os.Build.VERSION.SDK_INT );
                        try {

                            dispatchTakePictureIntent();
                        } catch (IOException e) {
                        }
                    }else {
                        Log.e("version","else"+android.os.Build.VERSION.SDK_INT );
                        //                    if (result)
                        cameraIntent();
                    }



                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Library";
                    if (result)
                        galleryIntent();

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), REQUEST_CROP_PICTURE);
    }

    private void cameraIntent() {

//        try {
//            dispatchTakePictureIntent();
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        File file = new File(Environment.getExternalStorageDirectory() + File.separator + "img.jpg");
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(file.toString())));
        startActivityForResult(intent, REQUEST_CAMERA_LOWER);
    }

    public static String encodeTobase64(Bitmap image) {
        Bitmap immagex = image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immagex.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);
        Log.e("LOOK", imageEncoded);
        return imageEncoded;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        File croppedImageFile = new File(AddStudentActivity.this.getFilesDir(), "test.jpg");
        Log.e("onActivityResult",""+requestCode);
        Log.e("resultCode",""+resultCode);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CROP_PICTURE) {
                if (data != null) {
                    String url = data.getData().toString();
                    InputStream is = null;
                    if (url.startsWith("content://com.google.android.apps.photos.content")) {
                        try {
                            is = getContentResolver().openInputStream(Uri.parse(url));
                            Bitmap bitmap = BitmapFactory.decodeStream(is);
                            onCaptureImageResult1(bitmap);
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Uri croppedImage = Uri.fromFile(croppedImageFile);
                        CropImageIntentBuilder cropImage = new CropImageIntentBuilder(200, 200, croppedImage);
                        cropImage.setSourceImage(data.getData());
                        sp.edit().putBoolean("crop", true).apply();
                        startActivityForResult(cropImage.getIntent(AddStudentActivity.this), 11);
                    }
                }
            } else if (requestCode == 11) {
                Log.e("Path===============", "" + croppedImageFile.getAbsolutePath());
                sp.edit().remove("crop").apply();
                sp.edit().apply();
                Bitmap selectedImage = BitmapFactory.decodeFile(croppedImageFile.getAbsolutePath());
                //img_profile.setImageBitmap(selectedImage);
                sp.edit().remove("crop").apply();
                sp.edit().apply();
                if (selectedImage != null)
                    onCaptureImageResult1(selectedImage);
                else
                    Toast.makeText(this, "Invalid image", Toast.LENGTH_SHORT).show();
            } else if (requestCode == REQUEST_CAMERA) {
                //   Crop.pickImage(this);
                //beginCrop(data.getData());
                beginCrop(photoURI);
            } else if(requestCode==REQUEST_CAMERA_LOWER){
                File file = new File(Environment.getExternalStorageDirectory() + File.separator + "img.jpg");
                performCrop(Uri.fromFile(file));
            }else if (requestCode == CAMERA_CROP) {
                Log.e("On_camera_crop","enter");
                if (data != null) {
                    Bundle extras = data.getExtras();
                    Log.e("On_camera_crop",""+extras);
                    //get the cropped bitmap from extras
                    // Bitmap selectedImage = extras.getParcelable("data");
                    Bitmap selectedImage = BitmapFactory.decodeFile(filePath);
                    if (selectedImage != null)
                        onCaptureImageResult(selectedImage);
                    else
                        Toast.makeText(this, "Invalid image", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "Operation cancelled", Toast.LENGTH_SHORT).show();
                    File file = new File(Environment.getExternalStorageDirectory() + File.separator + "img.jpg");
                    if (file.exists())
                        file.delete();
                }

            }  else if (requestCode == Crop.REQUEST_CROP) {
                handleCrop(resultCode, data);
            }

        }
    }

    private void beginCrop(Uri source) {
        Uri destination = Uri.fromFile(new File(getCacheDir(), "cropped"));
        Crop.of(source, destination).asSquare().start(this);
    }

    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == RESULT_OK) {
            imageView_profile.setImageURI(Crop.getOutput(result));

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), Crop.getOutput(result));
                encoded_image = encodeTobase64(bitmap);
                Log.e("imge_cropping",""+encoded_image);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(this, Crop.getError(result).getMessage()+"handleCrop", Toast.LENGTH_SHORT).show();
        }
    }

    private void onCaptureImageResult1(Bitmap thumbnail) {
//        Bitmap thumbnail = (Bitmap) data.getExtras().getParcelable("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        //thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        try {
            String PATH = Environment.getExternalStorageDirectory() + "/Android/data/" + getPackageName() + "/";
            File folder = new File(PATH);
            if (!folder.exists()) {
                folder.mkdir();//If there is no folder it will be created.
                Log.e("folder====", "created");
            }
            File destination = new File(PATH + TEMP_PHOTO_FILE);

            destination.createNewFile();
            FileOutputStream fo;

            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();

            if (thumbnail != null) {
                encoded_image = encodeTobase64(thumbnail);
                imageView_profile.setImageBitmap(thumbnail);
            } else {
                encoded_image = "";
                imageView_profile.setImageResource(R.drawable.ic_profile_100);
            }


        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    private void onCaptureImageResult(Bitmap thumbnail) {
        // Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        try {
            String PATH = Environment.getExternalStorageDirectory() + "/Android/data/" + getPackageName() + "/";
            File folder = new File(PATH);
            if (!folder.exists()) {
                folder.mkdir();//If there is no folder it will be created.
                Log.e("folder====", "created");
            }
            File destination = new File(PATH + TEMP_PHOTO_FILE);

            destination.createNewFile();

            FileOutputStream fo;

            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        File file = new File(Environment.getExternalStorageDirectory() + File.separator + "img.jpg");
        if (file.exists())
            file.delete();

        if (thumbnail != null) {
            boolean remove = false;
            encoded_image = encodeTobase64(thumbnail);
            // update_image(encodeTobase64(thumbnail), thumbnail, remove);
            imageView_profile.setImageBitmap(thumbnail);
        } else {
            encoded_image = "";
            Toast.makeText(AddStudentActivity.this, getString(R.string.str_invalid_image), Toast.LENGTH_SHORT).show();
        }

    }

    private void performCrop(Uri picUri) {
        try {
            //call the standard crop action intent (the user device may not support it)
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            //indicate image type and Uri
            cropIntent.setDataAndType(picUri, "image/*");
            //set crop properties
            cropIntent.putExtra("crop", "true");
            //indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            //indicate output X and Y
            cropIntent.putExtra("outputX", 200);
            cropIntent.putExtra("outputY", 200);
            File file = new File(filePath);
            Log.e("performCrop",""+picUri);
            cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(file.toString())));
            //retrieve data on return
            cropIntent.putExtra("return-data", true);
            //start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, CAMERA_CROP);
        } catch (ActivityNotFoundException anfe) {
            //display an error message
            String errorMessage = "Whoops - your device doesn't support the crop action!";
            Toast toast = Toast.makeText(this, errorMessage+" performCrop", Toast.LENGTH_SHORT);
            toast.show();
        }

    }


    ////////////
    // Camera //
    ////////////

    @NeedsPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    void startCamera() {
        try {
            dispatchTakePictureIntent();
        } catch (IOException e) {
        }
    }

    @OnShowRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    void showRationaleForCamera(final PermissionRequest request) {
        new AlertDialog.Builder(this)
                .setMessage("Access to External Storage is required")
                .setPositiveButton("Allow", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        request.proceed();
                    }
                })
                .setNegativeButton("Deny", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        request.cancel();
                    }
                })
                .show();
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // MainActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);

        //RegistrationActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DCIM), "Camera");
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        return image;
    }


    private void dispatchTakePictureIntent() throws IOException {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();

                Log.e("photoFile",""+createImageFile());
            } catch (IOException ex) {
                Log.e("ex_IOException",""+ex);
                // Error occurred while creating the File
                return;
            }


            // Continue only if the File was successfully created
            if (photoFile != null) {

                photoURI   = FileProvider.getUriForFile(AddStudentActivity.this,
                        BuildConfig.APPLICATION_ID + ".provider",
                        createImageFile());
                //    photoURI= Uri.fromFile(new File(photoFile.toString()));
                Log.e("if++",""+photoURI);

                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_CAMERA);
            }
        }

    }//dispatchTakePictureIntent




    public void setCurrentDateOnView() {
        final Calendar c = Calendar.getInstance();
        myear = c.get(Calendar.YEAR);
        mmonth = c.get(Calendar.MONTH);
        mday = c.get(Calendar.DAY_OF_MONTH);
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_DIALOG_ID:
                // set date picker as current date
                setCurrentDateOnView();
                DatePickerDialog _date = new DatePickerDialog(this,
                        datePickerListener, myear, mmonth, mday) {
                    @Override
                    public void onDateChanged(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {

                        if (year > mYear) {
                            view.updateDate(mYear, mMonth, mDay);
                            Toast.makeText(getApplicationContext(), getString(R.string.str_date_erro), Toast.LENGTH_SHORT).show();
                        }

                        if (monthOfYear > mMonth && year == mYear) {
                            view.updateDate(mYear, mMonth, mDay);
                            Toast.makeText(getApplicationContext(), getString(R.string.str_date_erro), Toast.LENGTH_SHORT).show();
                        }

                        if (dayOfMonth > mDay && year == mYear && monthOfYear == mMonth) {
                            view.updateDate(mYear, mMonth, mDay);
                            Toast.makeText(getApplicationContext(), getString(R.string.str_date_erro), Toast.LENGTH_SHORT).show();
                        }

                    }
                };
                Log.e("_date", "" + _date);
                return _date;
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            myear1 = selectedYear;
            mmonth1 = selectedMonth;
            mday1 = selectedDay;
            // set selected date into Textview
            edt_dob.setText(new StringBuilder().append(myear1)
                    .append("-").append(mmonth1 + 1).append("-").append(mday1));

        }
    };


    private void getClassList() {
        final ProgressDialog dialog = new ProgressDialog(AddStudentActivity.this);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.str_requesting));
        dialog.show();
        RequestQueue queue = Volley.newRequestQueue(AddStudentActivity.this);
        String requestURL = sp.getString("new_link","") + "classlist";
        Log.e("requestURL", "" + requestURL);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", "code=" + response);
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response != null) {
                    if (response.equals("204")) {
                        // text_msg.setVisibility(View.VISIBLE);
                        //  text_msg.setText("No Records Available");
                        alertDialog("No records available");
                        Log.e("Error message", "No Records Available");
                    } else {
//                        text_no_data.setVisibility(View.GONE);
//                        adapter = new CarCompanyAdapter(car_company, CarComapnyActivity.this);
//                        recyclerView_company.setAdapter(adapter);
//                        adapter.notifyDataSetChanged();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                //  Toast.makeText(getApplicationContext(), "Poor Internet Coonection", Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                // params.put("id",id);
                Log.e("param", "" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                    VolleyError error = new VolleyError(new String(volleyError.networkResponse.data));
                    volleyError = error;
                    dialog.dismiss();
                }
                // Toast.makeText(getApplicationContext(),"Poor Internet Coonection",Toast.LENGTH_SHORT).show();
                Log.e("parseNetworkError", "==" + volleyError);
                Log.e("parseNetworkError", "string==" + volleyError.toString());
                Log.e("parseNetworkError", "msg==" + volleyError.getMessage());
                return volleyError;
            }

            @Override
            protected Response<String> parseNetworkResponse(final NetworkResponse response) {
                String responseString = "";
                if (response != null) {
                    responseString = String.valueOf(response.statusCode);
                    // can get more details such as response.headers
                }
                Map<String, String> responseHeaders = response.headers;
                if (response.statusCode == 200) {
                    Response<String> result = Response.success(responseHeaders.get("Content-Type"), HttpHeaderParser.parseCacheHeaders(response));
                    Log.e("Response_class list", "==" + parseToString(response));
                    try_class_list.clear();

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                JSONArray JArray = new JSONArray(parseToString(response));
                                arr_class = new ArrayList<classes>();
                                for (int i = 0; i < JArray.length(); i++) {
                                    JSONObject jobj_Data = JArray.getJSONObject(i);

                                    classes con = new classes();
                                    con.setClasses_id(jobj_Data.getString("class_id"));
                                    con.setClasses_name(jobj_Data.getString("class"));
                                    class_name.add(jobj_Data.getString("class"));
                                    // country_name.add(b.getString("account_id"));
                                    arr_class.add(con);

                                    try_class_data = new HashMap<String, String>();
                                    try_class_data.put("class_id", jobj_Data.getString("class_id"));
                                    try_class_data.put("class", jobj_Data.getString("class"));
                                    try_class_list.add(try_class_data);

                                    ArrayAdapter<String> adapter =
                                            new ArrayAdapter<String>(AddStudentActivity.this, android.R.layout.simple_spinner_dropdown_item, class_name) {

                                                public View getView(int position, View convertView, ViewGroup parent) {
                                                    View v = super.getView(position, convertView, parent);

                                                    ((TextView) v).setTextSize(16);
                                                    ((TextView) v).setTextColor(getResources().getColorStateList(R.color.black));

                                                    return v;
                                                }

                                                public View getDropDownView(int position, View convertView, ViewGroup parent) {
                                                    View v = super.getDropDownView(position, convertView, parent);

                                                   /* ((TextView) v).setTextColor(
                                                            getResources().getColorStateList(R.color.color_preloader_start)
                                                    );
*/
                                                    return v;
                                                }


                                            };
                                    spinner_class.setAdapter(adapter);

                                }//for

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });

                    return result;
                } else {


                    Log.e("Error message", "Login Failed");
                }
                Log.e("responseString", "" + responseString);
                return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
            }

            public String parseToString(NetworkResponse response) {
                String parsed;
                try {
                    parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                } catch (UnsupportedEncodingException e) {
                    parsed = new String(response.data);
                }
                return parsed;
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }//display_student

    //    spinner list display
//    private void getClassList() {
//        final ProgressDialog dialog = new ProgressDialog(AddStudentActivity.this);
//        dialog.setCancelable(false);
//        dialog.setMessage(getString(R.string.str_requesting));
//        dialog.show();
//        RequestQueue queue = Volley.newRequestQueue(AddStudentActivity.this);
//        String requestURL = getString(R.string.web_path) + "get_all_classes.php";
//        StringRequest stringRequest = new StringRequest(Request.Method.POST, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
//            @Override
//            public void onResponse(final String s) {
//                Log.e("get_all_classes", s);
//                if (s != null) {
//                    try {
//                        JSONObject jobj = new JSONObject(s);
//                        final JSONObject jc = new JSONObject(s);
//                        if (jobj.getString("result").equals("success")) {
//                            if (dialog.isShowing()) {
//                                dialog.dismiss();
//                            }
//                            try_class_list.clear();
//                            spinner_class.setPrompt("Select Class");
//                            if (jobj.getJSONArray("student_classes").length() > 0) {
//
//                                arr_class = new ArrayList<classes>();
//                                for (int a = 0; a < jobj.getJSONArray("student_classes").length(); a++) {
//                                    final JSONObject b = jobj.getJSONArray("student_classes").getJSONObject(a);
//
//                                    classes con = new classes();
//                                    con.setClasses_id(b.getString("class_id"));
//                                    con.setClasses_name(b.getString("student_class"));
//                                    class_name.add(b.getString("student_class"));
//                                    // country_name.add(b.getString("account_id"));
//                                    arr_class.add(con);
//
//                                    try_class_data = new HashMap<String, String>();
//                                    try_class_data.put("class_id", b.getString("class_id"));
//                                    try_class_data.put("student_class", b.getString("student_class"));
//                                    try_class_list.add(try_class_data);
//
//                                    ArrayAdapter<String> adapter =
//                                            new ArrayAdapter<String>(AddStudentActivity.this, android.R.layout.simple_spinner_dropdown_item, class_name) {
//
//                                                public View getView(int position, View convertView, ViewGroup parent) {
//                                                    View v = super.getView(position, convertView, parent);
//
//                                                     ((TextView) v).setTextSize(16);
//                                                       ((TextView) v).setTextColor(getResources().getColorStateList(R.color.white));
//
//                                                    return v;
//                                                }
//
//                                                public View getDropDownView(int position, View convertView, ViewGroup parent) {
//                                                    View v = super.getDropDownView(position, convertView, parent);
//
//                                                   /* ((TextView) v).setTextColor(
//                                                            getResources().getColorStateList(R.color.color_preloader_start)
//                                                    );
//*/
//                                                    return v;
//                                                }
//
//
//                                            };
//                                    spinner_class.setAdapter(adapter);
//
//                                }
//                            }
//
//                            dialog.dismiss();
//                        } else if (jobj.getString("result").equals("failed")) {
//
//                            dialog.dismiss();
//                        }
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                        if (dialog.isShowing()) {
//                            dialog.dismiss();
//                        }
//                    }
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError volleyError) {
//                if (dialog.isShowing()) {
//                    dialog.dismiss();
//                }
//            }
//        }) {
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                return super.getParams();
//            }
//
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                return super.getHeaders();
//            }
//        };
//        queue.add(stringRequest);
//    }//getClassList





}//AddStudentActivity
