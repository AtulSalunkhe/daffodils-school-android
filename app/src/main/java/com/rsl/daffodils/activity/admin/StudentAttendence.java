package com.rsl.daffodils.activity.admin;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.rsl.daffodils.R;
import com.rsl.daffodils.activity.AddStudentActivity;
import com.rsl.daffodils.activity.AllStudentListActivity;
import com.rsl.daffodils.activity.RegistrationActivity;
import com.rsl.daffodils.activity.teacher.TeachersHomeActivity;
import com.rsl.daffodils.model.classes;
import com.rsl.daffodils.utils.ConnectionDetector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.jar.JarEntry;

public class StudentAttendence extends AppCompatActivity {

    EditText edt_dob;

    private int myear, mmonth, mday;

    private int myear1, mmonth1, mday1;
    static final int DATE_DIALOG_ID = 999;
    private int mYear;
    private int mMonth;
    private int mDay;

    HashMap<String, String> presnet_attendence = new HashMap<>();
    //for spinner to select class
    Spinner spinner_class;
    String selected_class, class_id;
    ArrayList<HashMap<String, String>> try_class_list = new ArrayList<HashMap<String, String>>();
    ArrayList<String> class_name = new ArrayList<>();
    ArrayList<classes> arr_class = new ArrayList<classes>();
    HashMap<String, String> try_class_data;
    boolean classes_flag = false;
    SharedPreferences sp;
    SharedPreferences.Editor editor;
    private ListView listview;
    ConnectionDetector internet;
    TextView text_msg;
    ArrayList<HashMap<String, String>> list_data = null;
    Context context;
    Myadapter myadapter;
    String selected_date = "";

    Button btn_submit;

    public static JSONObject parent_jObject;
    public static JSONObject data;
    public static JSONObject student;
    SimpleDateFormat readFormat = new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat writeFormat = new SimpleDateFormat("dd MMM yyyy");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_attendence);
        sp = getSharedPreferences("daffodils", 0);
        editor = sp.edit();
        initView();
        parent_jObject = new JSONObject();
        data = new JSONObject();
        student = new JSONObject();


        internet = new ConnectionDetector(StudentAttendence.this);
        list_data = new ArrayList<HashMap<String, String>>();
        listview = (ListView) findViewById(R.id.list_all_Student);
        myadapter = new Myadapter();
        listview.setAdapter(myadapter);

        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        Log.e("Current ", "" + mYear + "" + mMonth + "" + mDay);
        edt_dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  input_gender.setErrorEnabled(false);
                showDialog(DATE_DIALOG_ID);
            }
        });

//        edt_dob.setText(new StringBuilder().append(mYear)
//                .append("-").append(mMonth+1).append("-").append(mDay));
        selected_date = new StringBuilder().append(mYear)
                .append("-").append(mMonth + 1).append("-").append(mDay).toString();

        Log.e("parse_date_s", "" + selected_date);

        try {
            Date date1 = readFormat.parse(selected_date);
            Log.e("Changed date", "" + writeFormat.format(date1));

            edt_dob.setText(writeFormat.format(date1));

        } catch (ParseException e) {
            e.printStackTrace();
        }



        spinner_class.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()

                                                {
                                                    @Override
                                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                                        // if (position > 0) {
                                                        classes_flag = true;
                                                        selected_class = try_class_list.get(position).get("class");
                                                        Log.e("selected_class", "=" + selected_class);
                                                        Log.e("account_id", "=" + try_class_list.get(position).get("class_id"));
                                                        class_id = try_class_list.get(position).get("class_id");
                                                        Log.e("class id", "----" + class_id);

                                                        if (!internet.isConnectingToInternet()) {
                                                            internet.showAlertDialog(StudentAttendence.this, false);
                                                        } else {
                                                            display_student();
                                                        }


                                                    }

                                                    @Override
                                                    public void onNothingSelected(AdapterView<?> parent) {

                                                    }
                                                }


        );

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final JSONArray job_main = new JSONArray();
                if (presnet_attendence.size() > 0) {
                    JSONObject job = null;

                    Iterator myVeryOwnIterator = presnet_attendence.keySet().iterator();
                    while (myVeryOwnIterator.hasNext()) {
                        String key = (String) myVeryOwnIterator.next();
                        String value = (String) presnet_attendence.get(key);
                        try {
                            job = new JSONObject(value);
                            job_main.put(job);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }


                }
                if (!selected_date.equals("")) {
                    //input_dob.setErrorEnabled(false);
                    Log.e("if", "date");

                    try {
                        data.put("class_id", class_id);
                        data.put("date", selected_date);
                        data.put("student_list", job_main);
                        parent_jObject.put("data", data);
                        Log.e("Submit_Attendence_DATA ", "=====" + parent_jObject.toString());
                        studentattendence();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                } else {
                    Log.e("else", "date");
                    //Toast.makeText(getApplicationContext(), getResources().getString(R.string.select_dob), Toast.LENGTH_SHORT).show();
                    alertDialog(getResources().getString(R.string.select_dob_studdent_attendance));
                    // edt_dob.setError(getResources().getString(R.string.select_dob));
                    edt_dob.requestFocus();
                }
            }
        });

    }//onCreate


    private void studentattendence() {

        final ProgressDialog dialog = new ProgressDialog(StudentAttendence.this);
        dialog.setMessage(getResources().getString(R.string.str_requesting));
        dialog.setCancelable(false);
        dialog.show();
        RequestQueue queue = Volley.newRequestQueue(StudentAttendence.this);
        String requestURL = sp.getString("new_link","") + "student_attendance";
        StringRequest stringRequest = new StringRequest(Request.Method.PUT, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", "code=" + response);
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response != null) {
                    if (response.equals("204")) {
                        text_msg.setVisibility(View.VISIBLE);
                        alertDialog1("No Records Available");
                        Log.e("Error message", "No Records Available");
                    } else {

                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                //  Toast.makeText(getApplicationContext(), "Poor Internet Coonection", Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return parent_jObject == null ? null : parent_jObject.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", parent_jObject.toString(), "utf-8");
                    return null;
                }
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("data", "" + parent_jObject);

                Log.e("param", "student=" + params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                    VolleyError error = new VolleyError(new String(volleyError.networkResponse.data));
                    volleyError = error;
                    dialog.dismiss();
                }


                //Toast.makeText(getApplicationContext(),"Poor Internet Coonection",Toast.LENGTH_SHORT).show();
                Log.e("parseNetworkError", "==" + volleyError);
                Log.e("parseNetworkError", "string==" + volleyError.toString());
                Log.e("parseNetworkError", "msg==" + volleyError.getMessage());
                return volleyError;
            }

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                String responseString = "";
                if (response != null) {
                    responseString = String.valueOf(response.statusCode);
                    // can get more details such as response.headers
                }
                Map<String, String> responseHeaders = response.headers;
                if (response.statusCode == 200) {
                    Response<String> result = Response.success(responseHeaders.get("Content-Type"), HttpHeaderParser.parseCacheHeaders(response));
                    Log.e("Response_add_car", "" + parseToString(response));


                    return result;
                } else {
                    if (response.statusCode == 201) {

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                alertDialog1(getResources().getString(R.string.str_student_attendence_msg));
                            }
                        });
                    }


                    Log.e("Error message", "Login Failed");
                }
                Log.e("responseString", "" + responseString);
                return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
            }

            public String parseToString(NetworkResponse response) {
                String parsed;
                try {
                    parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                } catch (UnsupportedEncodingException e) {
                    parsed = new String(response.data);
                }
                return parsed;
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 4, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }//studentattendence


    private void alertDialog1(String errer) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                StudentAttendence.this);
        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setMessage(errer);
        alertDialog.setCancelable(false);
        alertDialog.setIcon(R.drawable.daffodils_logo);
        alertDialog.setPositiveButton(getString(R.string.dlg_ok),
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        Intent intent = new Intent(StudentAttendence.this, TeachersHomeActivity.class);
                        startActivity(intent);
                    }
                });

        alertDialog.create().show();

    }//alertDialog1


    private void alertDialog(String errer) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                StudentAttendence.this);
        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setMessage(errer);
        alertDialog.setCancelable(false);
        alertDialog.setIcon(R.drawable.daffodils_logo);
        alertDialog.setPositiveButton(getString(R.string.dlg_ok),
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                    }
                });

        alertDialog.show();
    }//alertDialog


    private void initView() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.str_student_attendence);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        edt_dob = (EditText) findViewById(R.id.edt_dob);//select date of attendance
        spinner_class = (Spinner) findViewById(R.id.spinner_class);//spinner for class list
        text_msg = (TextView) findViewById(R.id.text_msg);
        btn_submit = (Button) findViewById(R.id.btn_submit);
        getClassList();

    }


    @Override
    public void onResume() {
        super.onResume();
        if (!internet.isConnectingToInternet()) {
            internet.showAlertDialog(StudentAttendence.this, false);
        } else {

        }
    }//onResume


    private void display_student() {
        final ProgressDialog dialog = new ProgressDialog(StudentAttendence.this);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.str_requesting));
        dialog.show();
        RequestQueue queue = Volley.newRequestQueue(StudentAttendence.this);
        String requestURL = sp.getString("new_link", "") + "student_attendance_list/" + class_id + "/" + selected_date;
        Log.e("requestURL", "" + requestURL);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", "code=" + response);
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response != null) {
                    if (response.equals("204")) {
                        list_data.clear();
                       // alertDialog1("No records available");
                        Log.e("Error message", "No Records Available");
                    } else {

                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                //  Toast.makeText(getApplicationContext(), "Poor Internet Coonection", Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("data", "" + parent_jObject);

                Log.e("param", "" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                    VolleyError error = new VolleyError(new String(volleyError.networkResponse.data));
                    volleyError = error;
                    dialog.dismiss();
                }
                // Toast.makeText(getApplicationContext(),"Poor Internet Coonection",Toast.LENGTH_SHORT).show();
                Log.e("parseNetworkError", "==" + volleyError);
                Log.e("parseNetworkError", "string==" + volleyError.toString());
                Log.e("parseNetworkError", "msg==" + volleyError.getMessage());
                return volleyError;
            }

            @Override
            protected Response<String> parseNetworkResponse(final NetworkResponse response) {
                String responseString = "";
                if (response != null) {
                    responseString = String.valueOf(response.statusCode);
                    // can get more details such as response.headers
                }
                Map<String, String> responseHeaders = response.headers;
                if (response.statusCode == 200) {
                    Response<String> result = Response.success(responseHeaders.get("Content-Type"), HttpHeaderParser.parseCacheHeaders(response));
                    Log.e("Response_studentlist", "" + parseToString(response));

                    list_data.clear();

                    presnet_attendence.clear();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                JSONObject JObj = new JSONObject(parseToString(response));
                                Log.e("class", "" + JObj.getString("classes"));
                                if (JObj.getJSONArray("data").length() > 0) {
                                    for (int i = 0; i < JObj.getJSONArray("data").length(); i++) {
                                        JSONObject jobj_Data = JObj.getJSONArray("data").getJSONObject(i);
                                        HashMap<String, String> record = new HashMap<String, String>();
                                        record.put("student_id", jobj_Data.getString("student_id"));
                                        record.put("student_name", jobj_Data.getString("student_name"));
                                        record.put("present", jobj_Data.getString("present"));
                                        myadapter.add(record);
                                        JSONObject js = new JSONObject();
                                        try {
                                            js.put("student_id", jobj_Data.getString("student_id"));
                                            js.put("present", jobj_Data.getString("present"));

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                        presnet_attendence.put(String.valueOf(i), js.toString());


                                    }
                                }
                                myadapter.notifyDataSetChanged();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });

                    return result;
                } else {
                    Log.e("Error message", "Login Failed");
                }
                Log.e("responseString", "" + responseString);
                return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
            }

            public String parseToString(NetworkResponse response) {
                String parsed;
                try {
                    parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                } catch (UnsupportedEncodingException e) {
                    parsed = new String(response.data);
                }
                return parsed;
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }//display_student


    public class Myadapter extends BaseAdapter {


        public void add(HashMap<String, String> hash) {

            list_data.add(hash);
            notifyDataSetChanged();
        }

        public void clear() {

            list_data.clear();
            notifyDataSetChanged();
        }


        @Override
        public int getCount() {
            return list_data.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, final ViewGroup parent) {


            LayoutInflater inflater = getLayoutInflater();
            View list = convertView;
            if (list == null)
                list = inflater.inflate(R.layout.student_list_row, parent, false);


            TextView student_name = (TextView) list.findViewById(R.id.text_student_name);
            final CheckBox checkBox = (CheckBox) list.findViewById(R.id.check_box);
            checkBox.setTag(list_data.get(position).get("student_id"));
            student_name.setText(list_data.get(position).get("student_name").toString());

            if (list_data.get(position).get("present").equals("true")) {
                checkBox.setChecked(true);
            } else {
                checkBox.setChecked(false);
            }

            Log.e("on getView", "" + presnet_attendence);
            checkBox.setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                @Override
                public void onClick(View v) {
                    if (((CheckBox) v).isChecked()) {

                        try {
                            Log.e("Checked", "" + v.getTag().toString());
                            Log.e("Checked_possition", "" + position);
                            JSONObject js = new JSONObject();
                            js.put("student_id", v.getTag().toString());
                            js.put("present", "true");
                            presnet_attendence.put(String.valueOf(position), js.toString());
                            Log.e("presnet_checked", "" + presnet_attendence.toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        try {
                            Log.e("Unchecked", "" + v.getTag().toString());
                            Log.e("Unchecked_possition", "" + position);
                            JSONObject js = new JSONObject();
                            presnet_attendence.remove(String.valueOf(position));
                            js.put("student_id", v.getTag().toString());
                            js.put("present", "false");
                            presnet_attendence.put(String.valueOf(position), js.toString());
                            Log.e("presnet_unchekd", "" + presnet_attendence.toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }
            });


            return list;
        }
    }//Myadapter


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }//onBackPressed


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }//onOptionsItemSelected


    public void setCurrentDateOnView() {
        final Calendar c = Calendar.getInstance();
        myear = c.get(Calendar.YEAR);
        mmonth = c.get(Calendar.MONTH);
        mday = c.get(Calendar.DAY_OF_MONTH);

    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_DIALOG_ID:
                // set date picker as current date
                setCurrentDateOnView();
                DatePickerDialog _date = new DatePickerDialog(this,
                        datePickerListener, myear, mmonth, mday) {
                    @Override
                    public void onDateChanged(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {

                        if (year > mYear) {
                            view.updateDate(mYear, mMonth, mDay);
                            Toast.makeText(getApplicationContext(), getString(R.string.str_date_erro), Toast.LENGTH_SHORT).show();
                        }

                        if (monthOfYear > mMonth && year == mYear) {
                            view.updateDate(mYear, mMonth, mDay);
                            Toast.makeText(getApplicationContext(), getString(R.string.str_date_erro), Toast.LENGTH_SHORT).show();
                        }

                        if (dayOfMonth > mDay && year == mYear && monthOfYear == mMonth) {
                            view.updateDate(mYear, mMonth, mDay);
                            Toast.makeText(getApplicationContext(), getString(R.string.str_date_erro), Toast.LENGTH_SHORT).show();
                        }

                    }
                };
                Log.e("_date", "" + _date);
                return _date;
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            myear1 = selectedYear;
            mmonth1 = selectedMonth;
            mday1 = selectedDay;
            // set selected date into Textview
            selected_date = new StringBuilder().append(myear1)
                    .append("-").append(mmonth1 + 1).append("-").append(mday1).toString();


            try {
                Date date1 = readFormat.parse(selected_date);
                Log.e("Changed date", "" + writeFormat.format(date1));
                edt_dob.setText(writeFormat.format(date1));

            } catch (ParseException e) {
                e.printStackTrace();
            }

        }
    };


    //    spinner list display
    private void getClassList() {

        final ProgressDialog dialog = new ProgressDialog(StudentAttendence.this);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.str_requesting));
        dialog.show();
        RequestQueue queue = Volley.newRequestQueue(StudentAttendence.this);
        String requestURL = sp.getString("new_link", "") + "classlist";
        Log.e("requestURL", "" + requestURL);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", "code=" + response);
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response != null) {
                    if (response.equals("204")) {
                        // text_msg.setVisibility(View.VISIBLE);
                        //  text_msg.setText("No Records Available");
                        alertDialog("No records available");
                        Log.e("Error message", "No Records Available");
                    } else {
//                        text_no_data.setVisibility(View.GONE);
//                        adapter = new CarCompanyAdapter(car_company, CarComapnyActivity.this);
//                        recyclerView_company.setAdapter(adapter);
//                        adapter.notifyDataSetChanged();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                //  Toast.makeText(getApplicationContext(), "Poor Internet Coonection", Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                // params.put("id",id);
                Log.e("param", "" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                    VolleyError error = new VolleyError(new String(volleyError.networkResponse.data));
                    volleyError = error;
                    dialog.dismiss();
                }
                // Toast.makeText(getApplicationContext(),"Poor Internet Coonection",Toast.LENGTH_SHORT).show();
                Log.e("parseNetworkError", "==" + volleyError);
                Log.e("parseNetworkError", "string==" + volleyError.toString());
                Log.e("parseNetworkError", "msg==" + volleyError.getMessage());
                return volleyError;
            }

            @Override
            protected Response<String> parseNetworkResponse(final NetworkResponse response) {
                String responseString = "";
                if (response != null) {
                    responseString = String.valueOf(response.statusCode);
                    // can get more details such as response.headers
                }
                Map<String, String> responseHeaders = response.headers;
                if (response.statusCode == 200) {
                    Response<String> result = Response.success(responseHeaders.get("Content-Type"), HttpHeaderParser.parseCacheHeaders(response));
                    Log.e("Response_class list", "==" + parseToString(response));
                    try_class_list.clear();

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                JSONArray JArray = new JSONArray(parseToString(response));
                                arr_class = new ArrayList<classes>();
                                for (int i = 0; i < JArray.length(); i++) {
                                    JSONObject jobj_Data = JArray.getJSONObject(i);

                                    classes con = new classes();
                                    con.setClasses_id(jobj_Data.getString("class_id"));
                                    con.setClasses_name(jobj_Data.getString("class"));
                                    class_name.add(jobj_Data.getString("class"));
                                    // country_name.add(b.getString("account_id"));
                                    arr_class.add(con);

                                    try_class_data = new HashMap<String, String>();
                                    try_class_data.put("class_id", jobj_Data.getString("class_id"));
                                    try_class_data.put("class", jobj_Data.getString("class"));
                                    try_class_list.add(try_class_data);

                                    ArrayAdapter<String> adapter =
                                            new ArrayAdapter<String>(StudentAttendence.this, android.R.layout.simple_spinner_dropdown_item, class_name) {

                                                public View getView(int position, View convertView, ViewGroup parent) {
                                                    View v = super.getView(position, convertView, parent);

//                                                    ((TextView) v).setTextSize(16);
//                                                    ((TextView) v).setTextColor(getResources().getColorStateList(R.color.white));

                                                    return v;
                                                }

                                                public View getDropDownView(int position, View convertView, ViewGroup parent) {
                                                    View v = super.getDropDownView(position, convertView, parent);

                                                   /* ((TextView) v).setTextColor(
                                                            getResources().getColorStateList(R.color.color_preloader_start)
                                                    );
*/
                                                    return v;
                                                }


                                            };
                                    spinner_class.setAdapter(adapter);

                                }//for

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });

                    return result;
                } else {


                    Log.e("Error message", "Login Failed");
                }
                Log.e("responseString", "" + responseString);
                return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
            }

            public String parseToString(NetworkResponse response) {
                String parsed;
                try {
                    parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                } catch (UnsupportedEncodingException e) {
                    parsed = new String(response.data);
                }
                return parsed;
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }//display_student


}//StudentAttendence
