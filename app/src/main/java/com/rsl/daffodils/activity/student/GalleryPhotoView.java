package com.rsl.daffodils.activity.student;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.rsl.daffodils.R;
import com.rsl.daffodils.model.Gallery;
import com.rsl.daffodils.utils.ConnectionDetector;

import java.util.ArrayList;
import java.util.HashMap;

public class GalleryPhotoView extends AppCompatActivity {

    GridView gridview;
    ConnectionDetector internet;
    ImageAdapter imageAdapter;
    String name;
    public static ArrayList<HashMap<String,String>> list_data;
    SharedPreferences sp;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery_photo_view);
        internet = new ConnectionDetector(GalleryPhotoView.this);
        sp = getSharedPreferences("daffodils", 0);
        editor = sp.edit();
        gridview = (GridView) findViewById(R.id.grid_layout_menu);
        list_data = new ArrayList<>();
        int pos = Integer.parseInt(getIntent().getExtras().getString("position"));
        name = getIntent().getStringExtra("name");
        list_data= GalleryActivity.list_data.get(pos).getGallery_images();
     //   list_data = (ArrayList<HashMap<String, String>>) getIntent().getSerializableExtra("arraylist");
        Log.e("list_data",""+list_data.toString());
        Log.e("list_datasize",""+list_data.size());
        initView();

        imageAdapter = new ImageAdapter(this);
        gridview.setAdapter(imageAdapter);
        imageAdapter.notifyDataSetChanged();


    }//onCreate


    private void initView() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(name);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }//initView


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }


    public class ImageAdapter extends BaseAdapter {

        private LayoutInflater mInflater;
        private int mItemHeight = 0;
        private int mNumColumns = 0;
        Context context;
        private RelativeLayout.LayoutParams mImageViewLayoutParams;

        public ImageAdapter(Context context) {
            this.context = context;

            mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            mImageViewLayoutParams = new RelativeLayout.LayoutParams(GridLayout.LayoutParams.MATCH_PARENT,
                    GridLayout.LayoutParams.MATCH_PARENT);
        }

        public void add(HashMap<String,String> data) {
            list_data.add(data);
            Log.e("add","add");
            notifyDataSetChanged();
        }

        public void remove(int pos) {
//        image_data.remove(pos);
            list_data.get(pos);
            notifyDataSetChanged();
        }

        public void clear() {
            list_data.clear();
        }

        public int getCount() {
            Log.e("getCount",""+list_data.size());
            return list_data.size();
        }

        // set numcols
        public void setNumColumns(int numColumns) {
            mNumColumns = numColumns;
        }

        public int getNumColumns() {
            return mNumColumns;
        }

        // set photo item height
        public void setItemHeight(int height) {
            if (height == mItemHeight) {
                return;
            }
            mItemHeight = height;
            mImageViewLayoutParams = new RelativeLayout.LayoutParams(GridLayout.LayoutParams.MATCH_PARENT, mItemHeight);
            notifyDataSetChanged();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View view, ViewGroup parent) {
            Log.e("getView","getView");
            if (view == null)
                view = mInflater.inflate(R.layout.row_grid_view, null);


            final String img_url = sp.getString("new_gallerylink","")+list_data.get(position).get("image");
            Log.e("img_url",""+img_url);

            final ImageView img_background = (ImageView) view.findViewById(R.id.img_background);
//            TextView txt_menu = (TextView) view.findViewById(R.id.group_name);
            img_background.setLayoutParams(mImageViewLayoutParams);


            if (img_background.getLayoutParams().height != mItemHeight) {
                img_background.setLayoutParams(mImageViewLayoutParams);
            }
            Glide.with(context.getApplicationContext()).load(img_url).placeholder(R.drawable.daffodils_logo).into(img_background);


            img_background.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent= new Intent(GalleryPhotoView.this,PagerDialogActivity.class);
                    intent.putExtra("position", String.valueOf(position));
                    Log.e("position","This is image"+position);
                    startActivity(intent);
                }
            });



//            img_background.setOnClickListener(new View.OnClickListener() {
//
//                @Override
//                public void onClick(View v) {
//                    // TODO Auto-generated method stub
//                    final Dialog view = new Dialog(GalleryPhotoView.this);
//                    view.getWindow().requestFeature(1);
//                    view.setContentView(R.layout.view_gallery_images);
//                    view.setCancelable(false);
//                    view.setCanceledOnTouchOutside(true);
//                    ImageView profilepictureview = (ImageView) view
//                            .findViewById(R.id.member_picture);
//                    try {
//                        //  profilepictureview.setImageResource(imagepath);
//                       Glide.with(context.getApplicationContext()).load(img_url).placeholder(R.drawable.daffodils_logo).into(profilepictureview);
//                        //Glide.with(context.getApplicationContext()).load(img_url).into(img_background);
//                    } catch (Exception exception) {
//                        exception.printStackTrace();
//                    }
//                    Button b = (Button) view.findViewById(R.id.button1);
//                    b.setOnClickListener(new View.OnClickListener() {
//
//                        @Override
//                        public void onClick(View v) {
//                            // TODO Auto-generated method stub
//                            view.dismiss();
//                        }
//                    });
//                    view.show();
//                }
//            });

            return view;
        }


    }

}//GalleryPhotoView
