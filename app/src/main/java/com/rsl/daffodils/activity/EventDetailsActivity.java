package com.rsl.daffodils.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.rsl.daffodils.R;

public class EventDetailsActivity extends AppCompatActivity {
    //    ViewPager view_pager;
    ImageView img_slider;
    TextView txt_title, txt_descr,txt_date;

    SharedPreferences sp;
    SharedPreferences.Editor editor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_details);
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView mToolTitle = (TextView) mToolbar.findViewById(R.id.tool_title);
        setSupportActionBar(mToolbar);
        sp = getSharedPreferences("daffodils", 0);
        editor = sp.edit();
        String img_url = sp.getString("new_event","")+getIntent().getStringExtra("img_url");
        String tag = getIntent().getStringExtra("tag");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(tag);
        txt_title = (TextView) findViewById(R.id.txt_title);
        txt_descr = (TextView) findViewById(R.id.txt_descr);
        txt_date = (TextView) findViewById(R.id.txt_date);
        img_slider = (ImageView) findViewById(R.id.img_slider);
        txt_title.setText(getIntent().getStringExtra("title"));
        txt_descr.setText(getIntent().getStringExtra("description"));

        if(getIntent().getStringExtra("event_date").equals("")){

        }else {
            txt_date.setText(getIntent().getStringExtra("event_date"));
        }

        Log.e("img_url","Event"+img_url);

        Glide.with(getApplicationContext()).load(img_url).placeholder(R.drawable.daffodils_logo).into(img_slider);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
