package com.rsl.daffodils.activity.skip;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.rsl.daffodils.R;
import com.rsl.daffodils.adapter.InformationExpandableListAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FeaturesActivity extends AppCompatActivity {
    private SliderLayout mDemoSlider;
    TextView txt_message;
    ImageView img_slider;

    int[] imageId = {R.drawable.slide1, R.drawable.slide2, R.drawable.slide3,R.drawable.slide4,R.drawable.slide5};
    TextSliderView textSliderView;

    ExpandableListView expListView;
    InformationExpandableListAdapter listAdapter;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;
    private int lastExpandedPosition = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_features);

        initView();
        textSliderView = new TextSliderView(FeaturesActivity.this);
        if (imageId.length > 1 && imageId != null) {
            for (int j = 0; j < imageId.length; j++) {
                textSliderView = new TextSliderView(FeaturesActivity.this);
                textSliderView.image(imageId[j]).setScaleType(BaseSliderView.ScaleType.Fit);
                mDemoSlider.addSlider(textSliderView);
            }
        } else {
            textSliderView = new TextSliderView(FeaturesActivity.this);
            textSliderView.image(R.drawable.app_logo).setScaleType(BaseSliderView.ScaleType.Fit);
            mDemoSlider.addSlider(textSliderView);
        }

        expListView=(ExpandableListView)findViewById(R.id.listView1);

        enableExpandableList();


    }//onCreate


    private void initView() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.str_features));
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        img_slider = (ImageView) findViewById(R.id.img_slider);

        mDemoSlider = (SliderLayout) findViewById(R.id.view_pager);
        txt_message = (TextView) findViewById(R.id.txt_message);

    }




    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void enableExpandableList() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();

        prepareListData(listDataHeader, listDataChild);
        listAdapter = new InformationExpandableListAdapter(FeaturesActivity.this, listDataHeader, listDataChild);
        // setting list adapter
        expListView.setAdapter(listAdapter);

        expListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {

                return false;
            }
        });
        // Listview Group expanded listener
        expListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {

            }
        });

        // Listview Group collasped listener
        expListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {


            }
        });

        // Listview on child click listener
        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                // TODO Auto-generated method stub

                return false;
            }

        });
    }

    private void prepareListData(List<String> listDataHeader, Map<String,
            List<String>> listDataChild) {


        // Adding child data
        listDataHeader.add(getString(R.string.str_features));
        listDataHeader.add(getString(R.string.str_rule));



        // Adding child data
        List<String> feature = new ArrayList<String>();
        feature.add(getString(R.string.str_features_message));

        List<String> rule = new ArrayList<String>();
        rule.add(getString(R.string.str_rule_message));



        // Header, Child data
        listDataChild.put(listDataHeader.get(0), feature);
        listDataChild.put(listDataHeader.get(1), rule);



    }

}//FeaturesActivity
