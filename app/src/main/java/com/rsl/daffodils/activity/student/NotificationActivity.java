package com.rsl.daffodils.activity.student;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.daimajia.slider.library.SliderLayout;
import com.rsl.daffodils.LoginActivity;
import com.rsl.daffodils.R;
import com.rsl.daffodils.activity.AllStudentListActivity;
import com.rsl.daffodils.activity.HomeActivity;
import com.rsl.daffodils.adapter.AboutpagerAdapter;
import com.rsl.daffodils.adapter.NotificationpagerAdapter;
import com.rsl.daffodils.utils.BadgeView;
import com.rsl.daffodils.utils.ConnectionDetector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class NotificationActivity extends AppCompatActivity {
    public static TabLayout tabLayout;
    TabLayout.Tab tab;
    TabLayout.Tab tab1;
    TabLayout.Tab tab2;
    SharedPreferences sp;
    SharedPreferences.Editor editor;
    private ConnectionDetector internet;
    public static TextView txtcount,txtcount1,txtcount2;
    public static ImageView notification_img,notification_img1,notification_img2;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        sp = getSharedPreferences("daffodils", 0);
        editor = sp.edit();

        initView();
        tabLayout = (TabLayout)findViewById(R.id.tab_layout);

        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.a_logo));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.h_logo));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.m_logo));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        internet = new ConnectionDetector(NotificationActivity.this);

        if (internet.isConnectingToInternet()) {
            getcount(sp.getString("student_id", ""));
            Log.e("onrefresh", "" + sp.getString("student_id", ""));
        } else {

        }

        final ViewPager viewPager = (ViewPager)findViewById(R.id.pager);
        final NotificationpagerAdapter adapter = new NotificationpagerAdapter
                (NotificationActivity.this.getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        viewPager.setCurrentItem(0);

        View view= LayoutInflater.from(NotificationActivity.this).inflate(R.layout.custom_tab,null);
        txtcount = (TextView) view.findViewById(R.id.txt_notificaton);
        notification_img = (ImageView) view.findViewById(R.id.img_custom);
        notification_img.setImageResource(R.drawable.a_logo);
        View view1= LayoutInflater.from(NotificationActivity.this).inflate(R.layout.custom_tab,null);
        txtcount1 = (TextView) view1.findViewById(R.id.txt_notificaton);
        notification_img1 = (ImageView) view1.findViewById(R.id.img_custom);
        notification_img1.setImageResource(R.drawable.h_logo);
        View view2= LayoutInflater.from(NotificationActivity.this).inflate(R.layout.custom_tab,null);
        txtcount2 = (TextView) view2.findViewById(R.id.txt_notificaton);
        notification_img2 = (ImageView) view2.findViewById(R.id.img_custom);
        notification_img2.setImageResource(R.drawable.m_logo);

        tab = tabLayout.getTabAt(0);
        tab1 = tabLayout.getTabAt(1);
        tab2 = tabLayout.getTabAt(2);



        tab.setCustomView(view);
        tab1.setCustomView(view1);
        tab2.setCustomView(view2);


        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                //tab.ge
                viewPager.setCurrentItem(tab.getPosition());
                if (tab.getPosition()==0){
                    getupdatecount("ANNOUNCEMENT");
                }else if(tab.getPosition()==1){
                    getupdatecount("HOMEWORK");
                }else {
                    getupdatecount("MESSAGE");
                }

                //tab.getCustomView().setVisibility(View.GONE);
                //tabLayout.getTabAt(tab.getPosition()).getCustomView().findViewById(R.id.txt_notificaton).setVisibility(View.GONE);

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

               // tabLayout.getTabAt(tab.getPosition()).getCustomView().findViewById(R.id.txt_notificaton).setVisibility(View.GONE);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        if(tab.getPosition()==0) {
            new Handler().postDelayed(
                    new Runnable() {
                        @Override
                        public void run() {
                            tabLayout.getTabAt(0).select();

                        }
                    }, 100);
            getupdatecount("ANNOUNCEMENT");
        }




        if (internet.isConnectingToInternet()) {
            Bundle extras = getIntent().getExtras();
            Log.e("fcm oncreate", "type : " + extras.getString("type"));
            Log.e("fcm oncreate", "out : " + extras.getString("from_fcm"));

            if (extras.containsKey("type")) {

                editor.putString("student_id", getIntent().getExtras().getString("student_id")).apply();
                Log.e("id",""+getIntent().getExtras().getString("student_id"));
                editor.putString("account_id",getIntent().getExtras().getString("account_id")).apply();
                Log.e("ac id",""+getIntent().getExtras().getString("account_id"));
                editor.putString("student_image", getIntent().getExtras().getString("student_image")).apply();
                Log.e("image",""+getIntent().getExtras().getString("student_image"));
                editor.putString("parent_mobile", getIntent().getExtras().getString("parent_mobile")).apply();
                Log.e("parent_mobile",""+getIntent().getExtras().getString("parent_mobile"));
                editor.putString("student_name1",getIntent().getExtras().getString("student_name1")).apply();
                Log.e("student_name",""+getIntent().getExtras().getString("student_name1"));
                editor.putString("parent_email", getIntent().getExtras().getString("parent_email")).apply();
                Log.e("parent_email",""+getIntent().getExtras().getString("parent_email"));
                editor.putString("date_of_birth", getIntent().getExtras().getString("date_of_birth")).apply();
                Log.e("dob",""+getIntent().getExtras().getString("date_of_birth"));
                editor.putString("gender_id", getIntent().getExtras().getString("gender_id")).apply();
                Log.e("gender_id",""+getIntent().getExtras().getString("gender_id"));
                editor.putString("student_class1",getIntent().getExtras().getString("student_class1")).apply();
                Log.e("class",""+getIntent().getExtras().getString("student_class1"));
                editor.putString("class_id", getIntent().getExtras().getString("class_id")).apply();
                Log.e("class_id",""+getIntent().getExtras().getString("class_id"));
                editor.commit();

                if (getIntent().getStringExtra("type").equals("announcement")) {

                    new Handler().postDelayed(
                            new Runnable() {
                                @Override
                                public void run() {
                                    tabLayout.getTabAt(0).select();

                                }
                            }, 100);

                } else if (getIntent().getStringExtra("type").equals("homework")) {
                    new Handler().postDelayed(
                            new Runnable() {
                                @Override
                                public void run() {
                                    tabLayout.getTabAt(1).select();

                                }
                            }, 100);
                } else {
                    new Handler().postDelayed(
                            new Runnable() {
                                @Override
                                public void run() {
                                    tabLayout.getTabAt(2).select();

                                }
                            }, 100);

                }
            }

            Log.e("onCreate", "onCreate");
        } else {
            //internet.showAlertDialog(NotificationActivity.this, false);
        }
    }//onCreate


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        Log.e("Intent", "xczxc===" + intent.getStringExtra("type"));
        if (internet.isConnectingToInternet()) {
            Log.e("student_id","---"+intent.getStringExtra("student_id"));
            if (intent.getStringExtra("type").equals("announcement")) {

                new Handler().postDelayed(
                        new Runnable() {
                            @Override
                            public void run() {
                                tabLayout.getTabAt(0).select();

                            }
                        }, 100);

            } else if (intent.getStringExtra("type").equals("homework")){
                new Handler().postDelayed(
                        new Runnable() {
                            @Override
                            public void run() {
                                tabLayout.getTabAt(1).select();

                            }
                        }, 100);
            }else{

                new Handler().postDelayed(
                        new Runnable() {
                            @Override
                            public void run() {
                                tabLayout.getTabAt(2).select();

                            }
                        }, 100);

            }


            Log.e("onNewIntent", "onNewIntent");
        } else {
           // internet.showAlertDialog(NotificationActivity.this, false);
        }
    }

    private void initView() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.str_stud_notification));
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }//initView


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Bundle extras = getIntent().getExtras();
        Log.e("fcm", "out : "+extras.getString("from_fcm"));
        if (extras != null) {
            Log.e("fcm", "in");

            if (extras.containsKey("from_fcm")) {
                if (getIntent().getStringExtra("from_fcm").equals("true")) {
                    if (sp.getString("role", "").equals("student")) {
                        Log.e("role","+++++");
                        if(sp.getBoolean("select_student",false)){
                            Log.e("select_student","+++++");
                            Intent in = new Intent(NotificationActivity.this, StudentsHomeActivity.class);
                            startActivity(in);
                            finish();
                        }else {
                            Intent in = new Intent(NotificationActivity.this, AllStudentListActivity.class);
                            startActivity(in);
                            finish();
                        }

                    }
                }
            }else {
                Intent in = new Intent(NotificationActivity.this, StudentsHomeActivity.class);
                startActivity(in);
                finish();
            }
        } else {
            Log.e("finish", "direct finish");
            //finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }


    private void getupdatecount(final String type) {
        final ProgressDialog dialog = new ProgressDialog(NotificationActivity.this);
        dialog.setMessage(getString(R.string.str_requesting));
        dialog.setCancelable(false);
        dialog.show();
        RequestQueue queue = Volley.newRequestQueue(NotificationActivity.this);
        String requestURL = sp.getString("new_link","") + "update_student_notification_count";
        Log.e("requestURL", "update_student" + requestURL);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", "code=" + response);
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response != null) {
                    if (response.equals("204")) {
                        // txt_message.setVisibility(View.VISIBLE);
                        // txt_message.setText("No Records Available");
                        // alertDialog1("No Records Available");
                        Log.e("Error message", "No Records Available");
                    } else {
//                        text_no_data.setVisibility(View.GONE);
//                        adapter = new CarCompanyAdapter(car_company, CarComapnyActivity.this);
//                        recyclerView_company.setAdapter(adapter);
//                        adapter.notifyDataSetChanged();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                //  Toast.makeText(getActivity(), "Poor Internet Coonection", Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
//                type=ANNOUNCEMENT&student_id=73
                params.put("student_id", sp.getString("student_id", ""));
                params.put("type",type);


                Log.e("param", "=" + params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                    VolleyError error = new VolleyError(new String(volleyError.networkResponse.data));
                    volleyError = error;
                    dialog.dismiss();
                }
                //  Toast.makeText(getActivity(),"Poor Internet Coonection",Toast.LENGTH_SHORT).show();
                Log.e("parseNetworkError", "==" + volleyError);
                Log.e("parseNetworkError", "string==" + volleyError.toString());
                Log.e("parseNetworkError", "msg==" + volleyError.getMessage());
                return volleyError;
            }

            @Override
            protected Response<String> parseNetworkResponse(final NetworkResponse response) {
                String responseString = "";
                if (response != null) {
                    responseString = String.valueOf(response.statusCode);
                    // can get more details such as response.headers
                }
                Map<String, String> responseHeaders = response.headers;
                if (response.statusCode == 200) {
                    Response<String> result = Response.success(responseHeaders.get("Content-Type"), HttpHeaderParser.parseCacheHeaders(response));
                    Log.e("Response", "= count =" + parseToString(response));

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            try {
                                JSONArray JArray = new JSONArray(parseToString(response));

                                for (int i = 0; i < JArray.length(); i++) {
                                    JSONObject jobj_Data = JArray.getJSONObject(i);


                                    if(type.equals("ANNOUNCEMENT")){
                                        txtcount.setText(jobj_Data.getString("count"));
                                        txtcount.setVisibility(View.GONE);
                                    }else if (type.equals("HOMEWORK")){
                                        txtcount1.setText(jobj_Data.getString("count"));
                                        txtcount1.setVisibility(View.GONE);
                                    }else {
                                        txtcount2.setText(jobj_Data.getString("count"));
                                        txtcount2.setVisibility(View.GONE);
                                    }




                                }//for

                                } catch (JSONException e1) {
                                e1.printStackTrace();
                            }


                        }
                    });

                    return result;
                } else {


                    Log.e("Error message", "Login Failed");
                }
                Log.e("responseString", "" + responseString);
                return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
            }

            public String parseToString(NetworkResponse response) {
                String parsed;
                try {
                    parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                } catch (UnsupportedEncodingException e) {
                    parsed = new String(response.data);
                }
                return parsed;
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }//getupdatecount

    private void getcount(final String id) {
        final ProgressDialog dialog = new ProgressDialog(NotificationActivity.this);
        dialog.setMessage(getString(R.string.str_requesting));
        dialog.setCancelable(false);
        dialog.show();
        RequestQueue queue = Volley.newRequestQueue(NotificationActivity.this);
        String requestURL = sp.getString("new_link","") + "get_student_notification_count/" + id;
        Log.e("requestURL", "URL" + requestURL);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("responsecount", "====" + response);
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response != null) {
                    if (response.equals("204")) {
                        // txt_message.setVisibility(View.VISIBLE);
                        // txt_message.setText("No Records Available");
                       // alertDialog1("No Records Available");
                        Log.e("Error message", "No Records Available");
                    } else {
//                        text_no_data.setVisibility(View.GONE);
//                        adapter = new CarCompanyAdapter(car_company, CarComapnyActivity.this);
//                        recyclerView_company.setAdapter(adapter);
//                        adapter.notifyDataSetChanged();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
               // Toast.makeText(NotificationActivity.this, "Poor Internet Coonection", Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                Log.e("param", "=" + params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                    VolleyError error = new VolleyError(new String(volleyError.networkResponse.data));
                    volleyError = error;
                    dialog.dismiss();
                }
                //  Toast.makeText(getActivity(),"Poor Internet Coonection",Toast.LENGTH_SHORT).show();
                Log.e("parseNetworkError", "==" + volleyError);
                Log.e("parseNetworkError", "string==" + volleyError.toString());
                Log.e("parseNetworkError", "msg==" + volleyError.getMessage());
                return volleyError;
            }

            @Override
            protected Response<String> parseNetworkResponse(final NetworkResponse response) {
                String responseString = "";
                if (response != null) {
                    responseString = String.valueOf(response.statusCode);
                    // can get more details such as response.headers
                }
                Map<String, String> responseHeaders = response.headers;
                if (response.statusCode == 200) {
                    Response<String> result = Response.success(responseHeaders.get("Content-Type"), HttpHeaderParser.parseCacheHeaders(response));
                    Log.e("Response", "= count =" + parseToString(response));

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            try {
                                JSONArray JArray = new JSONArray(parseToString(response));
                                for (int i = 0; i < JArray.length(); i++) {
                                    JSONObject jObj = JArray.getJSONObject(i);

                                    Log.e("announcement",""+jObj.getString("Announcement_count"));
                                    Log.e("message",""+jObj.getString("Message_count"));
                                    Log.e("homework",""+jObj.getString("Homework_count"));

                                    if (jObj.getString("Announcement_count").equals("") || jObj.getString("Announcement_count").equals("0")){
                                        txtcount.setVisibility(View.GONE);
                                        }
                                    else {
                                        txtcount.setVisibility(View.VISIBLE);
                                        txtcount.setText(jObj.getString("Announcement_count"));

//                                        BadgeView bageq=new BadgeView(NotificationActivity.this,view);
//                                        bageq.setId(R.id.txt_notificaton);
//                                        bageq.setText(jObj.getString("Announcement_count"));
//                                        bageq.show();
                                    }
                                    if (jObj.getString("Homework_count").equals("") || jObj.getString("Homework_count").equals("0")){
                                        txtcount1.setVisibility(View.GONE);
                                        }
                                    else {
                                        txtcount1.setVisibility(View.VISIBLE);
                                        txtcount1.setText(jObj.getString("Homework_count"));
//                                        BadgeView bageq1=new BadgeView(NotificationActivity.this,view1);
//                                        bageq1.setId(R.id.txt_notificaton);
//                                        bageq1.setText(jObj.getString("Message_count"));
//                                        bageq1.show();
                                    }
                                    if (jObj.getString("Message_count").equals("") || jObj.getString("Message_count").equals("0")){
                                        txtcount2.setVisibility(View.GONE);
                                        }
                                    else {
                                        txtcount2.setVisibility(View.VISIBLE);
                                        txtcount2.setText(jObj.getString("Message_count"));
//                                        BadgeView bageq2=new BadgeView(NotificationActivity.this,view2);
//                                        bageq2.setId(R.id.txt_notificaton);
//                                        bageq2.setText(jObj.getString("Message_count"));
//                                        bageq2.show();
                                    }
                                }
                            }//for
                            catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    return result;
                } else {
                    Log.e("Error message", "Login Failed");
                }
                Log.e("responseString", "" + responseString);
                return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
            }

            public String parseToString(NetworkResponse response) {
                String parsed;
                try {
                    parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                } catch (UnsupportedEncodingException e) {
                    parsed = new String(response.data);
                }
                return parsed;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }//getcount



    private void alertDialog1(String error) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                NotificationActivity.this);
        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setIcon(R.drawable.daffodils_logo);
        alertDialog.setMessage(error);
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton(getString(R.string.dlg_ok),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }



}//NotificationActivity
