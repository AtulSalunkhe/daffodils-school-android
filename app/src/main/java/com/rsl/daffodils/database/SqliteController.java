package com.rsl.daffodils.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.rsl.daffodils.model.User;

import java.util.ArrayList;
import java.util.HashMap;

public class SqliteController extends SQLiteOpenHelper {
    private static final String LOGCAT = null;

    public SqliteController(Context applicationcontext) {
        super(applicationcontext, "androidsqlite.db", null, 1);
        Log.d(LOGCAT, "Created");
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        String query, query1, query2;
        query = "CREATE TABLE AllFriends ( id INTEGER PRIMARY KEY,friend_id INTEGER, friend_name TEXT,friend_phno INTEGER,"
                + "friend_email TEXT,friend_path TEXT,server_path TEXT,friend_mood TEXT,friend_quickboxid INTEGER,timestamp DATETIME)";
        query1 = "CREATE TABLE CreateGroup ( group_Id INTEGER PRIMARY KEY, admin_id INTEGER, group_name TEXT,"
                + " group_image TEXT, str TEXT,timestamp DATETIME,server_group_id TEXT)";
        query2 = "CREATE TABLE ChatDetails  ( createchat_id INTEGER PRIMARY KEY, reciver_id INTEGER,msg TEXT,"
                + "timestamp DATETIME,type TEXT,local_img_path TEXT,msg_type TEXT,download_flag TEXT,chat_type TEXT,group_id TEXT)";


        database.execSQL(query);
        database.execSQL(query1);
        database.execSQL(query2);
        // database.execSQL(query3);

        Log.d(LOGCAT, "YOU TRAIN PROGRAM Created");
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, int version_old,
                          int current_version) {
        String query1, query2, query3;
        query1 = "ALTER TABLE ChatDetails ADD COLUMN local_img_path TEXT";
        query2 = "ALTER TABLE ChatDetails ADD COLUMN msg_type TEXT DEFAULT 'text'";
        query3 = "ALTER TABLE ChatDetails ADD COLUMN download_flag TEXT";
        query3 = "ALTER TABLE ChatDetails ADD COLUMN chat_type TEXT";
        query3 = "ALTER TABLE ChatDetails ADD COLUMN group_id TEXT";

        database.execSQL(query1);
        database.execSQL(query2);
        database.execSQL(query3);
        Log.e("alter", "chat detail table altered");
        Log.e("fields", "local_img_path,msg_type,download_flag");
        // onCreate(database);
    }

    public void insertAllFriends(HashMap<String, String> queryValues) {

        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("friend_id", queryValues.get("friend_id"));
        values.put("friend_name", queryValues.get("friend_name"));
        values.put("friend_phno", queryValues.get("friend_phno"));
        values.put("friend_email", queryValues.get("friend_email"));
        values.put("friend_path", queryValues.get("friend_path"));
        values.put("server_path", queryValues.get("server_path"));
        values.put("friend_mood", queryValues.get("friend_mood"));
        values.put("friend_quickboxid", queryValues.get("friend_quickboxid"));
        values.put("timestamp", queryValues.get("time"));
        database.insert("AllFriends", null, values);
        database.close();
    }

    public boolean checkMember(String friend_id) {
        SQLiteDatabase database = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM friend_details where friend_id='" + friend_id
                + "'";
        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            return true;
        }
        cursor.close();
        return false;
    }

    public int updateDetails(User u) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("name", u.getName());
        values.put("friend_id", u.getAccount_id());
        values.put("friend_phno", u.getMobile());
        Log.e("update", "record updated");
        return database.update("friend_details", values, "friend_id" + " = ?",
                new String[]{u.getAccount_id()});
    }

    public ArrayList<HashMap<String, String>> getAllFriends() {
        ArrayList<HashMap<String, String>> wordList;
        wordList = new ArrayList<HashMap<String, String>>();
        //ArrayList<User> rr = new ArrayList<User>();
        String selectQuery = "SELECT * FROM AllFriends";
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                HashMap<String, String> map = new HashMap<String, String>();
                map.put("friend_id", cursor.getString(1));
                map.put("friend_name", cursor.getString(2));
                map.put("friend_phno", cursor.getString(3));
                map.put("friend_email", cursor.getString(4));
                map.put("friend_path", cursor.getString(5));
                map.put("server_path", cursor.getString(6));
                map.put("friend_mood", cursor.getString(7));
                map.put("friend_quickboxid", cursor.getString(8));
                map.put("timestamp", cursor.getString(9));
                wordList.add(map);
                /*com.rsl.Databases.Model m = new com.rsl.Databases.Model(
                        cursor.getString(1), cursor.getString(2),
						cursor.getString(3), cursor.getString(4),
						cursor.getString(5), cursor.getString(6),
						cursor.getString(7), cursor.getString(8),
						cursor.getString(9));*/
                //rr.add(m);
            } while (cursor.moveToNext());
            cursor.close();
        }

        // return contact list
        return wordList;
    }

    public HashMap<String, String> getFriendByXmpp_username(String xmpp_username) {
        xmpp_username = xmpp_username.toLowerCase();
        String selectQuery = "SELECT * FROM AllFriends where friend_quickboxid='" + xmpp_username + "'";
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, null);
        Log.d("courser.size", "" + cursor.getCount());
        if (cursor.moveToFirst()) {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("friend_id", cursor.getString(1));
            map.put("friend_name", cursor.getString(2));
            map.put("friend_phno", cursor.getString(3));
            map.put("friend_email", cursor.getString(4));
            map.put("friend_path", cursor.getString(5));
            map.put("server_path", cursor.getString(6));
            map.put("friend_mood", cursor.getString(7));
            map.put("friend_quickboxid", cursor.getString(8));
            map.put("timestamp", cursor.getString(9));
            //wordList.add(map);
            return map;
        }
        cursor.close();

        // return contact list
        return null;
    }

    public long insertCreateGroup(HashMap<String, String> queryValues) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("admin_id", queryValues.get("admin_id"));
        values.put("group_name", queryValues.get("group_name"));
        values.put("group_image", queryValues.get("group_image"));
        values.put("str", queryValues.get("str"));
        values.put("timestamp", queryValues.get("timestamp"));
        values.put("server_group_id", queryValues.get("server_group_id"));
        long id = database.insert("CreateGroup", null, values);
        database.close();
        return id;
    }

    public ArrayList<HashMap<String, String>> getCreateGroup() {
        ArrayList<HashMap<String, String>> wordList;
        wordList = new ArrayList<HashMap<String, String>>();
        String selectQuery = "SELECT * FROM CreateGroup";
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, null);
        // String[] tokens;
        if (cursor.moveToFirst()) {
            do {
                HashMap<String, String> map = new HashMap<String, String>();
                map.put("group_Id", cursor.getString(0));
                map.put("admin_id", cursor.getString(1));
                map.put("group_name", cursor.getString(2));
                map.put("group_image", cursor.getString(3));
                map.put("str", cursor.getString(4));
                map.put("timestamp", cursor.getString(5));
                map.put("server_group_id", cursor.getString(6));
                wordList.add(map);
            } while (cursor.moveToNext());

        }
        // return contact list
        return wordList;
    }

    public ArrayList<HashMap<String, String>> getGroupMemberInfo(String id) {
        ArrayList<HashMap<String, String>> wordList;
        wordList = new ArrayList<HashMap<String, String>>();
        String selectQuery = "SELECT * FROM CreateGroup where group_Id='" + id
                + "'";
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, null);
        String[] tokens;
        if (cursor.moveToFirst()) {
            do {
                HashMap<String, String> map = new HashMap<String, String>();
                map.put("group_Id", cursor.getString(0));
                map.put("admin_id", cursor.getString(1));
                map.put("group_name", cursor.getString(2));
                map.put("group_image", cursor.getString(3));
                map.put("str", cursor.getString(4));
                map.put("timestamp", cursor.getString(5));
                map.put("server_group_id", cursor.getString(6));

                tokens = cursor.getString(4).split("/");
                // Log.e("str_array", "" + tokens[0]);
                for (int i = 0; i < tokens.length; i++) {
                    // Log.e("str_array", "" + tokens[i]);
                    String selectQuery1 = "SELECT * FROM AllFriends where friend_phno='"
                            + tokens[i] + "'";
                    Log.v("selectQuery1", selectQuery1);
                    SQLiteDatabase database1 = this.getWritableDatabase();
                    Cursor cursor1 = database1.rawQuery(selectQuery1, null);
                    if (cursor1.moveToFirst()) {
                        do {
                            HashMap<String, String> map1 = new HashMap<String, String>();
                            map1.put("group_member_mood", cursor1.getString(7));
                            map1.put("group_member_image", cursor1.getString(5));
                            map1.put("group_member_name", cursor1.getString(2));
                            map1.put("group_member_phno", cursor1.getString(3));
                            wordList.add(map1);
                        } while (cursor1.moveToNext());
                    }
                }
                wordList.add(map);
            } while (cursor.moveToNext());

        }
        // return contact list
        return wordList;
    }

/*	public ArrayList<Group> GetGroupMemberInfo(SharedPreferences sp, String id) {
		ArrayList<HashMap<String, String>> wordList;
		wordList = new ArrayList<HashMap<String, String>>();
		ArrayList<Group> rr = new ArrayList<Group>();
		String selectQuery = "SELECT * FROM CreateGroup where server_group_id='"
				+ id + "'";
		// Log.e("str_array", selectQuery);
		SQLiteDatabase database = this.getWritableDatabase();
		Cursor cursor = database.rawQuery(selectQuery, null);
		String[] tokens;
		if (cursor.moveToFirst()) {
			do {

				tokens = cursor.getString(4).split("/");
				// Log.e("str_array", "" + tokens[0]);
				for (int i = 0; i < tokens.length; i++) {
					// Log.e("str_array", "" + tokens[i]);
					String selectQuery1 = "SELECT * FROM AllFriends where friend_phno='"
							+ tokens[i] + "'";
					// Log.v("selectQuery1", selectQuery1);
					SQLiteDatabase database1 = this.getWritableDatabase();
					Cursor cursor1 = database1.rawQuery(selectQuery1, null);
					if (cursor1.moveToFirst()) {
						do {
							HashMap<String, String> map1 = new HashMap<String, String>();
							if (sp.getString("user_phno", "").equals(tokens[i])) {
								map1.put("group_member_mood",
										cursor1.getString(7));
								map1.put("group_member_name",
										cursor1.getString(2));
								map1.put("group_member_image",
										cursor1.getString(5));

							} else {
								Log.v("selectQuery1", selectQuery1);
								map1.put("group_member_mood",
										cursor1.getString(7));
								map1.put("group_member_image",
										cursor1.getString(5));
								map1.put("group_member_name",
										cursor1.getString(2));
								map1.put("group_member_phno",
										cursor1.getString(3));
							}
							wordList.add(map1);
						} while (cursor1.moveToNext());
					} else {
						HashMap<String, String> map1 = new HashMap<String, String>();
						if (sp.getString("user_phno", "").equals(tokens[i])) {
							map1.put("group_member_mood",
									sp.getString("user_mood", ""));
							map1.put("group_member_name",
									sp.getString("user_name", ""));
							map1.put("group_member_image",
									sp.getString("Local_profile_path", ""));
						} else {
							map1.put("group_member_mood", "");
							map1.put("group_member_image", "");
							map1.put("group_member_name", tokens[i]);
							map1.put("group_member_phno", tokens[i]);

						}
						wordList.add(map1);
					}
				}
				com.rsl.Databases.Group m = new com.rsl.Databases.Group(
						cursor.getString(0), cursor.getString(1),
						cursor.getString(2), cursor.getString(3),
						cursor.getString(4), cursor.getString(5),
						cursor.getString(6), wordList);
				rr.add(m);
			} while (cursor.moveToNext());
		}

		// return contact list
		return rr;
	}*/

    public long insertChat(HashMap<String, String> queryValues) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("reciver_id", queryValues.get("reciver_id"));
        values.put("local_img_path", queryValues.get("local_img_path"));
        values.put("msg", queryValues.get("msg"));
        values.put("timestamp", queryValues.get("timestamp"));
        values.put("msg_type", queryValues.get("msg_type"));
        values.put("type", queryValues.get("type"));
        values.put("download_flag", queryValues.get("download_flag"));
        values.put("chat_type", queryValues.get("chat_type"));
        values.put("group_id", queryValues.get("group_id"));
        long id = database.insert("ChatDetails", null, values);
        database.close();
        return id;
    }

    public ArrayList<HashMap<String, String>> getCreateChat() {
        ArrayList<HashMap<String, String>> wordList;
        wordList = new ArrayList<HashMap<String, String>>();
        String selectQuery = "SELECT * FROM ChatDetails";
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                HashMap<String, String> map = new HashMap<String, String>();
                map.put("createchat_id", cursor.getString(0));
                map.put("reciver_id", cursor.getString(1));
                map.put("msg", cursor.getString(2));
                map.put("timestamp", cursor.getString(3));
                map.put("type", cursor.getString(4));
                map.put("local_img_path", cursor.getString(5));
                map.put("msg_type", cursor.getString(6));
                wordList.add(map);
            } while (cursor.moveToNext());
        }
        // return contact list
        return wordList;
    }

    public ArrayList<HashMap<String, String>> getResendChatId() {
        ArrayList<HashMap<String, String>> wordList;
        wordList = new ArrayList<HashMap<String, String>>();
        String selectQuery = "SELECT reciver_id,msg,timestamp,local_img_path,msg_type,chat_type,group_id FROM ChatDetails GROUP BY reciver_id ORDER BY timestamp DESC";
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, null);
        Log.v("selectQuery1", "" + cursor);
        if (cursor.moveToFirst()) {
            do {
                Log.v("cursor.getString(5)", "" + cursor.getString(5));
                HashMap<String, String> map = new HashMap<String, String>();
                if (cursor.getString(5) != null) {


                    if (cursor.getString(5).equals("single")) {

                        map.put("chat_type", "single");
                        map.put("reciver_id", cursor.getString(0));
                        map.put("msg", cursor.getString(1));
                        map.put("timestamp", cursor.getString(2));
                        map.put("local_img_path", cursor.getString(3));


                        String selectQuery1 = "SELECT * FROM AllFriends where friend_phno='"
                                + cursor.getString(0) + "'";
                        Log.v("selectQuery1", selectQuery1);
                        SQLiteDatabase database1 = this.getWritableDatabase();
                        Cursor cursor1 = database1.rawQuery(selectQuery1, null);
                        if (cursor1.moveToFirst()) {
                            map.put("resend_chat_name", cursor1.getString(2));
                            map.put("resend_chat_image", cursor1.getString(5));
                            map.put("xmpp_username", cursor1.getString(8));
                            map.put("friend_email", cursor1.getString(4));
                            //map.put("resend_chat_image", cursor.getString(6));
                            Log.d(cursor1.getString(2) + "----", "------" + cursor1.getString(5));
                            Log.d("cursor records", "no records");

                        }
                        // wordList.add(map);
                    } else {

                        map.put("chat_type", "group");
                        map.put("reciver_id", cursor.getString(0));
                        map.put("msg", cursor.getString(1));
                        map.put("timestamp", cursor.getString(2));
                        map.put("local_img_path",
                                "/sdcard/.PointApp/" + cursor.getString(0) + ".png");
                        map.put("group_id", cursor.getString(6));
                        String selectQuery1 = "SELECT * FROM CreateGroup where server_group_id='"
                                + cursor.getString(0) + "'";
                        Log.v("selectQuery1", selectQuery1);
                        SQLiteDatabase database1 = this.getWritableDatabase();
                        Cursor cursor1 = database1.rawQuery(selectQuery1, null);
                        if (cursor1.moveToFirst()) {
                            map.put("resend_chat_name", cursor1.getString(2));
                            map.put("resend_chat_image", "/sdcard/.PointApp/"
                                    + cursor.getString(0) + ".png");
                        } else {
                            map.put("resend_chat_name", cursor.getString(0));
                            map.put("resend_chat_image", "/sdcard/.PointApp/"
                                    + cursor.getString(0) + ".png");
                            Log.d("cursor records", "no records");
                        }
                        // wordList.add(map);

                    }
                }
                wordList.add(map);
            } while (cursor.moveToNext());
        }

        // return contact list
        return wordList;
    }

    public ArrayList<HashMap<String, String>> getChatInfo(String id) {
        ArrayList<HashMap<String, String>> wordList;
        wordList = new ArrayList<HashMap<String, String>>();
        String selectQuery = "SELECT * FROM ChatDetails where reciver_id='"
                + id + "'";
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                HashMap<String, String> map = new HashMap<String, String>();
                map.put("chat_id", cursor.getString(cursor
                        .getColumnIndex("createchat_id")));
                map.put("msg", cursor.getString(2));
                map.put("timestamp", cursor.getString(3));
                map.put("type", cursor.getString(4));
                map.put("local_img_path", cursor.getString(5));
                map.put("msg_type", cursor.getString(6));
                map.put("download_flag", cursor.getString(7));
                map.put("chat_type", cursor.getString(8));
                wordList.add(map);
            } while (cursor.moveToNext());
        }
        return wordList;
        // }
    }

    public ArrayList<HashMap<String, String>> getgroupChatInfo(String id) {
        ArrayList<HashMap<String, String>> wordList;
        wordList = new ArrayList<HashMap<String, String>>();
        String selectQuery = "SELECT * FROM ChatDetails where reciver_id='"
                + id + "'";
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                HashMap<String, String> map = new HashMap<String, String>();
                map.put("chat_id", cursor.getString(cursor
                        .getColumnIndex("createchat_id")));
                map.put("msg", cursor.getString(2));
                map.put("timestamp", cursor.getString(3));
                map.put("type", cursor.getString(4));
                map.put("local_img_path", cursor.getString(5));
                map.put("msg_type", cursor.getString(6));
                map.put("download_flag", cursor.getString(7));
                map.put("chat_type", cursor.getString(8));
                map.put("reciver_id", cursor.getString(9));
                wordList.add(map);
            } while (cursor.moveToNext());
        }
        return wordList;
        // }
    }

    public void insertMessages(HashMap<String, String> queryValues,
                               HashMap<String, String> queryValues1,
                               HashMap<String, String> queryValues2) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("Message1", queryValues.get("Message"));
        values.put("Message2", queryValues1.get("Message"));
        values.put("Message3", queryValues2.get("Message"));
        database.insert("Messages", null, values);
        database.close();
    }

    public ArrayList<HashMap<String, String>> getAllMessage() {
        ArrayList<HashMap<String, String>> wordList;
        wordList = new ArrayList<HashMap<String, String>>();
        String selectQuery = "SELECT * FROM Messages";
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                HashMap<String, String> map = new HashMap<String, String>();
                map.put("Message", cursor.getString(0));
                HashMap<String, String> map1 = new HashMap<String, String>();
                map1.put("Message", cursor.getString(1));
                HashMap<String, String> map2 = new HashMap<String, String>();
                map2.put("Message", cursor.getString(2));
				/*
				 * map.put("Message", cursor.getString(1)); map.put("Message",
				 * cursor.getString(2));
				 */
                wordList.add(map);
                wordList.add(map1);
                wordList.add(map2);
            } while (cursor.moveToNext());
        }
        // return contact list
        return wordList;
    }

    public int updateAllFriends(HashMap<String, String> queryValues) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("friend_path", queryValues.get("friend_path"));

        return database.update("AllFriends", values, "friend_phno" + " = ?",
                new String[]{queryValues.get("friend_phno")});
        // String updateQuery =
        // "Update words set txtWord='"+word+"' where txtWord='"+ oldWord +"'";
        // Log.d(LOGCAT,updateQuery);
        // database.rawQuery(updateQuery, null);
        // return database.update("words", values, "txtWord = ?", new String[] {
        // word });
    }

    public int updateUsername(HashMap<String, String> queryValues) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("friend_name", queryValues.get("friend_name"));
        return database.update("AllFriends", values, "friend_phno" + " = ?",
                new String[]{queryValues.get("friend_phno")});
        // String updateQuery =
        // "Update words set txtWord='"+word+"' where txtWord='"+ oldWord +"'";
        // Log.d(LOGCAT,updateQuery);
        // database.rawQuery(updateQuery, null);
        // return database.update("words", values, "txtWord = ?", new String[] {
        // word });
    }

    public int updateGroupname(HashMap<String, String> queryValues) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("group_name", queryValues.get("group_name"));
        return database.update("CreateGroup", values, "server_group_id"
                + " = ?", new String[]{queryValues.get("server_group_id")});
        // String updateQuery =
        // "Update words set txtWord='"+word+"' where txtWord='"+ oldWord +"'";
        // Log.d(LOGCAT,updateQuery);
        // database.rawQuery(updateQuery, null);
        // return database.update("words", values, "txtWord = ?", new String[] {
        // word });

    }

    public int updateGroupimage(HashMap<String, String> queryValues) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("group_image", queryValues.get("group_image"));
        return database.update("CreateGroup", values, "server_group_id"
                + " = ?", new String[]{queryValues.get("server_group_id")});
        // String updateQuery =
        // "Update words set txtWord='"+word+"' where txtWord='"+ oldWord +"'";
        // Log.d(LOGCAT,updateQuery);
        // database.rawQuery(updateQuery, null);
        // return database.update("words", values, "txtWord = ?", new String[] {
        // word });

    }

    public int updateChatDetails(HashMap<String, String> queryValues) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("download_flag", queryValues.get("download_flag"));
        values.put("local_img_path", queryValues.get("local_img_path"));
        return database.update("ChatDetails", values, "createchat_id" + " = ?",
                new String[]{queryValues.get("createchat_id")});
        // String updateQuery =
        // "Update words set txtWord='"+word+"' where txtWord='"+ oldWord +"'";
        // Log.d(LOGCAT,updateQuery);
        // database.rawQuery(updateQuery, null);
        // return database.update("words", values, "txtWord = ?", new String[] {
        // word });
    }

    public void deleteStudent(String id) {
        Log.d(LOGCAT, "delete");
        SQLiteDatabase database = this.getWritableDatabase();
        String deleteQuery = "DELETE FROM Students where StudentId='" + id
                + "'";
        Log.d("query", deleteQuery);
        database.execSQL(deleteQuery);
    }

    public void deleteChatMsg(String id) {
        Log.d(LOGCAT, "delete msg");
        SQLiteDatabase database = this.getWritableDatabase();
        String deleteQuery = "DELETE FROM ChatDetails where createchat_id='"
                + id + "'";
        Log.d("query", deleteQuery);
        database.execSQL(deleteQuery);
    }

    public void deleteChatHistory(String id) {
        Log.d(LOGCAT, "delete chat history");
        SQLiteDatabase database = this.getWritableDatabase();
        String deleteQuery = "DELETE FROM ChatDetails where reciver_id='" + id
                + "'";
        Log.d("query", deleteQuery);
        database.execSQL(deleteQuery);
    }

    public void deleteAll() {
        Log.d(LOGCAT, "delete");
        SQLiteDatabase database = this.getWritableDatabase();
        String deleteQuery = "DELETE FROM AllFriends";
        Log.d("query", deleteQuery);
        database.execSQL(deleteQuery);
    }

    public void dropAllTable() {
        Log.d(LOGCAT, "delete");
        SQLiteDatabase database = this.getWritableDatabase();
        String dropAllFriends = "DELETE FROM AllFriends";
        String dropCreateGroup = "DELETE FROM CreateGroup";
        String dropChatDetails = "DELETE FROM ChatDetails";
        Log.d("dropAllFriends", dropAllFriends);
        Log.d("dropCreateGroup", dropCreateGroup);
        Log.d("dropChatDetails", dropChatDetails);
        database.execSQL(dropAllFriends);
        database.execSQL(dropCreateGroup);
        database.execSQL(dropChatDetails);
    }

	/*public Model getFriend(String id) {
		ArrayList<HashMap<String, String>> wordList;
		wordList = new ArrayList<HashMap<String, String>>();
		ArrayList<Model> rr = new ArrayList<Model>();
		String selectQuery = "SELECT * FROM AllFriends where friend_phno='"
				+ id + "'";
		SQLiteDatabase database = this.getWritableDatabase();
		Cursor cursor = database.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			// do {
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("friend_id", cursor.getString(1));
			map.put("friend_name", cursor.getString(2));
			map.put("friend_phno", cursor.getString(3));
			map.put("friend_email", cursor.getString(4));
			map.put("friend_path", cursor.getString(5));
			map.put("server_path", cursor.getString(6));
			map.put("friend_mood", cursor.getString(7));
			map.put("friend_quickboxid", cursor.getString(8));
			map.put("timestamp", cursor.getString(9));
			wordList.add(map);
			com.rsl.Databases.Model m = new com.rsl.Databases.Model(
					cursor.getString(1), cursor.getString(2),
					cursor.getString(3), cursor.getString(4),
					cursor.getString(5), cursor.getString(6),
					cursor.getString(7), cursor.getString(8),
					cursor.getString(9));
			return m;
			// rr.add(m);
			// } while (cursor.moveToNext());
		}

		// return contact list
		return null;
	}*/

    public boolean checkFriend(String id) {
        // HashMap<String, String> wordList = new HashMap<String, String>();
        SQLiteDatabase database = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM AllFriends where friend_phno='"
                + id + "'";
        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            return true;
        }
        return false;
    }

    public String getadmin(String id) {
        // HashMap<String, String> wordList = new HashMap<String, String>();
        SQLiteDatabase database = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM AllFriends where friend_phno='"
                + id + "'";
        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            return cursor.getString(2).toString();
        } else {
            return id;
        }
    }
}
