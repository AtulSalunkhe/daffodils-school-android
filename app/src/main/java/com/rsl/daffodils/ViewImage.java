package com.rsl.daffodils;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;



public class ViewImage extends Activity {
	// Declare Variable
	TextView text;
	ImageView imageview;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Get the view from view_image.xml
		setContentView(R.layout.view_image);
		Intent in = getIntent();
		String path = in.getStringExtra("path");
		ImageView i = (ImageView) findViewById(R.id.full_image_view);
		Bitmap new_image = BitmapFactory.decodeFile(path);
		i.setImageBitmap(new_image);

	}
}