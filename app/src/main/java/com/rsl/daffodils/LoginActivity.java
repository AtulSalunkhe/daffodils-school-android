package com.rsl.daffodils;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.iid.FirebaseInstanceId;
import com.rsl.daffodils.activity.AddStudentActivity;
import com.rsl.daffodils.activity.AllStudentListActivity;
import com.rsl.daffodils.activity.RegistrationActivity;
import com.rsl.daffodils.activity.skip.SkipHomeActivity;
import com.rsl.daffodils.activity.student.StudentsHomeActivity;
import com.rsl.daffodils.activity.teacher.TeachersHomeActivity;
import com.rsl.daffodils.utils.ConnectionDetector;
import com.rsl.daffodils.utils.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LoginActivity extends AppCompatActivity {
    Button btn_login, btn_about;
    TextInputLayout input_email, input_password;
    EditText edt_email, edt_password;
    SharedPreferences sp;
    SharedPreferences.Editor editor;
    SessionManager session;
    ConnectionDetector internet;
    //TextView txt_skip;

    TextView txt_singup;
    StringBuilder macadress;

    private TelephonyManager mTelephonyManager;
    private static final int PERMISSIONS_REQUEST_READ_PHONE_STATE = 999;

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        sp = getSharedPreferences("daffodils", 0);
        editor = sp.edit();
        editor.apply();
        internet = new ConnectionDetector(LoginActivity.this);
        initView();
        macid();
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // Do something for lollipop and above versions
            if (checkSelfPermission(android.Manifest.permission.READ_PHONE_STATE)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{android.Manifest.permission.READ_PHONE_STATE},
                        PERMISSIONS_REQUEST_READ_PHONE_STATE);
            } else {
                getDeviceImei();
            }
        } else {
            getDeviceImei();
            // do something for phones running an SDK before lollipop
        }

        btn_login.setOnClickListener(new ClickListener());
        txt_singup.setOnClickListener(new ClickListener());
        btn_about.setOnClickListener(new ClickListener());

        session = new SessionManager(getApplicationContext());
        check_login();
    }

    private class ClickListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_login: {
                    if (internet.isConnectingToInternet()) {
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                        if (edt_password.getText().toString().trim().length() <= 0) {
                            alertDialog1(getResources().getString(R.string.str_user));
                        } else {
                            //getlink();
                            Login(edt_password.getText().toString().trim());
                        }
                    } else {
                        internet.showAlertDialog(LoginActivity.this, true);
                    }
                }
                break;
                case R.id.txt_singup: {
                    Intent i = new Intent(LoginActivity.this, RegistrationActivity.class);
                    startActivity(i);
                    edt_password.setText("");
                }
                break;
                case R.id.btn_about: {
                    Intent i = new Intent(LoginActivity.this, SkipHomeActivity.class);
                    startActivity(i);
                    edt_password.setText("");
                }
                break;
                default:
                    break;
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    private boolean check_login() {
        Log.e("role", sp.getString("role", ""));
        if (session.isLoggedIn()) {
            if (sp.getString("role", "").equals("student")) {
                if(sp.getBoolean("select_student",false)){
                    Intent in = new Intent(LoginActivity.this, StudentsHomeActivity.class);
                    startActivity(in);
                    finish();
                    return true;
                }else {
                    Intent in = new Intent(LoginActivity.this, AllStudentListActivity.class);
                    startActivity(in);
                    finish();
                }

            } else if (sp.getString("role", "").equals("teacher")) {
                Intent in = new Intent(LoginActivity.this, TeachersHomeActivity.class);
                startActivity(in);
                finish();
                return true;
            }
            finishAffinity();
            return true;
        }
        return false;
    }//check is login


    public  void getlink() {
        final RequestQueue queue = Volley.newRequestQueue(LoginActivity.this);
        String requestURL = getString(R.string.splash_link) + "get_link.php";
        Log.e("check web_path", requestURL);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
            @Override
            public void onResponse(String result) {

                if (result != null) {
                    Log.e("check web_path", result);
                    if (!result.startsWith("null")) {
                        try {
                            JSONObject jObj = new JSONObject(result);
                            if (jObj.length() > 0) {
                                if (jObj.getString("result").equals("success")) {
                                    String new_link=jObj.getString("link");
                                    editor.putString("new_link",new_link).apply();
                                    Login(edt_password.getText().toString().trim());

                                }
                                if (jObj.getString("result").equals("failed")) {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                           // sp.edit().clear().apply();
                                            // Creating Session
                                          //  session.logoutUser();
                                            Toast.makeText(LoginActivity.this, "" + "error", Toast.LENGTH_SHORT).show();

                                        }
                                    });

                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("id", "4");
                Log.e("params", "" + params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        queue.add(stringRequest);
    }//ResendCode


    private void Login(final String password) {

        final ProgressDialog dialog1 = new ProgressDialog(LoginActivity.this);
        dialog1.setCancelable(false);
        dialog1.setMessage(getString(R.string.str_requesting));
        dialog1.show();
        RequestQueue queue = Volley.newRequestQueue(LoginActivity.this);
        String requestURL = sp.getString("new_link","") + "login";
        Log.e("requestURL", "" + requestURL);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", "code=" + response);
                if (dialog1.isShowing()) {
                    dialog1.dismiss();
                }
                if (response != null) {
                    if (response.equals("412")) {
                        // text_msg.setVisibility(View.VISIBLE);
                        //  text_msg.setText("No Records Available");
                       // alertDialog1("Please enter valid user code..!");
                        Log.e("Error message", "No Records Available");
                    } else if(response.equals("409")){
//                        text_no_data.setVisibility(View.GONE);
//                        adapter = new CarCompanyAdapter(car_company, CarComapnyActivity.this);
//                        recyclerView_company.setAdapter(adapter);
//                        adapter.notifyDataSetChanged();
                        Log.e("Check===","409");
                        alertDialog1("Already logged in..!");
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (dialog1.isShowing()) {
                    dialog1.dismiss();
                }
             //   Toast.makeText(getApplicationContext(), "Poor Internet Coonection", Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                String fcm_id = FirebaseInstanceId.getInstance().getToken();
                params.put("password", password);
                params.put("device_token", fcm_id);
                params.put("device_type", "android");
                params.put("imie_no", String.valueOf(mTelephonyManager.getDeviceId()));
                params.put("mac_addr", macadress.toString());
                Log.e("param", "" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                    VolleyError error = new VolleyError(new String(volleyError.networkResponse.data));
                    volleyError = error;

                    final VolleyError finalVolleyError = volleyError;
                    final String msg=finalVolleyError.getMessage();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                JSONObject jObj = new JSONObject(msg);
                                if(jObj.getString("result").equals("failed")){
                                    alertDialog1( jObj.getString("msg"));
                                    dialog1.dismiss();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                    //
                   // alertDialog1(volleyError.toString());
                    //
                }

//                alertDialog1("Please enter valid user code..!");
                // Toast.makeText(getApplicationContext(),"Poor Internet Coonection",Toast.LENGTH_SHORT).show();
                Log.e("parseNetworkError", "==" + volleyError);
                Log.e("parseNetworkError", "string==" + volleyError.toString());
                Log.e("parseNetworkError", "msg==" + volleyError.getMessage());
                return volleyError;
            }

            @Override
            protected Response<String> parseNetworkResponse(final NetworkResponse response) {
                String responseString = "";
                if (response != null) {
                    responseString = String.valueOf(response.statusCode);
                    // can get more details such as response.headers
                }
                Map<String, String> responseHeaders = response.headers;
                if (response.statusCode == 200) {
                    Response<String> result = Response.success(responseHeaders.get("Content-Type"), HttpHeaderParser.parseCacheHeaders(response));
                    Log.e("login", "" + parseToString(response));

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {

                                JSONObject jObj = new JSONObject(parseToString(response));
                                sp.edit().putString("entry_code", password).apply();
//                                alertDialog1(jObj.getString("msg"));
                                Intent start = null;
                                if (jObj.getString("role").equals("student")) {
                                    sp.edit().putString("stud_id", jObj.getString("id")).apply();
                                    sp.edit().putString("account_id", jObj.getString("account_id")).apply();
                                    sp.edit().putString("email", jObj.getString("email")).apply();
                                    sp.edit().putString("name", jObj.getString("name")).apply();
                                    sp.edit().putString("mobile", jObj.getString("mobile")).apply();
                                    sp.edit().putString("role", jObj.getString("role")).apply();

                                    start = new Intent(LoginActivity.this, AllStudentListActivity.class);
                                    session.createLoginSession(jObj.getString("name"), jObj.getString("account_id"), jObj.getString("email"), "", jObj.getString("role"));
                                } else if (jObj.getString("role").equals("teacher")) {
                                    sp.edit().putString("teacher_image", jObj.getString("teacher_image")).apply();
                                    sp.edit().putString("id", jObj.getString("id")).apply();
                                    sp.edit().putString("account_id", jObj.getString("account_id")).apply();
                                    sp.edit().putString("name", jObj.getString("name")).apply();
                                    sp.edit().putString("email", jObj.getString("email")).apply();
                                    sp.edit().putString("mobile", jObj.getString("mobile")).apply();
                                    sp.edit().putString("subject", jObj.getString("subject")).apply();
                                    sp.edit().putString("qualification", jObj.getString("qualification")).apply();
                                    sp.edit().putString("experience", jObj.getString("experience")).apply();
                                    sp.edit().putString("role", jObj.getString("role")).apply();
                                    sp.edit().putString("Admin_type", jObj.getString("type")).apply();

                                    start = new Intent(LoginActivity.this, TeachersHomeActivity.class);
                                    session.createLoginSession(jObj.getString("name"), jObj.getString("id"), jObj.getString("email"), "", jObj.getString("role"));
                                }
                                startActivity(start);
                                finish();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });

                    return result;
                } else {


                    // text_msg.setText(jObj.getString("msg"));
                    Log.e("Error message", "Login Failed");
                }
                Log.e("responseString", "" + responseString);
                return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
            }

            public String parseToString(NetworkResponse response) {
                String parsed;
                try {
                    parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                } catch (UnsupportedEncodingException e) {
                    parsed = new String(response.data);
                }
                return parsed;
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 4, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }//login




    public boolean validateEmail(String email) {
        Pattern pattern;
        Matcher matcher;

        String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    private void alertDialog(String message) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();

    }
//
//    @Override
//    protected void onResume() {
//        super.onResume();
//        edt_password.setText("");
//    }

    private void initView() {
        btn_login = (Button) findViewById(R.id.btn_login);
        btn_about = (Button) findViewById(R.id.btn_about);
        txt_singup = (TextView) findViewById(R.id.txt_singup);
        edt_password = (EditText) findViewById(R.id.edt_password);
        // txt_skip = (TextView) findViewById(R.id.txt_skip);
        // input_email = (TextInputLayout) findViewById(R.id.input_email);
        //  input_password = (TextInputLayout) findViewById(R.id.input_password);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );
    }


    public boolean isPermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(LoginActivity.this, Manifest.permission.READ_PHONE_STATE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("TAG", "Permission is granted");
                return true;
            } else {

                Log.v("TAG", "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, 2);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v("TAG", "Permission is granted");
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_READ_PHONE_STATE
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            getDeviceImei();
        }
    }

    public String macid() {
        try {
            // get all the interfaces
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
            //find network interface wlan0
            for (NetworkInterface networkInterface : all) {
                if (!networkInterface.getName().equalsIgnoreCase("wlan0")) continue;
                //get the hardware address (MAC) of the interface
                byte[] macBytes = networkInterface.getHardwareAddress();
                if (macBytes == null) {
                    return "";
                }


                macadress = new StringBuilder();
            	for (byte b : macBytes) {
                    //gets the last byte of b
					macadress.append(Integer.toHexString(b & 0xFF) + ":");
				}

				if (macadress.length() > 0) {
					macadress.deleteCharAt(macadress.length() - 1);
				}
				Log.e("MacId","----"+macadress.toString());
				return macadress.toString();
              /*  return macadress.append("54:27:58:a5:f4:0d").toString();*/
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }
    private void getDeviceImei() {

        mTelephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        String deviceid = mTelephonyManager.getDeviceId();
        Log.d("msg", "DeviceImei " + deviceid);
    }


    private void alertDialog1(String error) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                LoginActivity.this);
        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setIcon(R.drawable.daffodils_logo);
        alertDialog.setMessage(error);
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton(getString(R.string.dlg_ok),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }
}
