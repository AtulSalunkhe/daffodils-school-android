package com.rsl.daffodils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.rsl.daffodils.activity.student.StudentsHomeActivity;
import com.rsl.daffodils.utils.ConnectionDetector;
import com.rsl.daffodils.utils.SessionManager;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class SplashActivity extends Activity {

    ImageView img_app;
   RelativeLayout lay_zoom;
    TextView app_text;
    SharedPreferences sp;
    SharedPreferences.Editor editor;
    SessionManager session;
    private ConnectionDetector internet;
    String version_code="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        sp = getSharedPreferences("daffodils", 0);
        editor = sp.edit();
        session = new SessionManager(SplashActivity.this);

        internet = new ConnectionDetector(SplashActivity.this);
        getVersionInfo();
        if (internet.isConnectingToInternet()) {
            getlink();
            CheckVersion();
        } else {
            internet.showAlertDialog(SplashActivity.this, false);
        }



        img_app = (ImageView) findViewById(R.id.app_image);
        lay_zoom = (RelativeLayout) findViewById(R.id.lay_zoom);
        app_text = (TextView) findViewById(R.id.app_text);

        Animation animationrotate = AnimationUtils.loadAnimation(
                getApplicationContext(), R.anim.zoom);
        app_text.startAnimation(animationrotate);

     /*   Thread timerThread = new Thread(){
            public void run(){
                try{
                    sleep(4000);
                }catch(InterruptedException e){
                    e.printStackTrace();
                }finally{
                    Intent intent = new Intent(SplashActivity.this,LoginActivity.class);
                    startActivity(intent);
                }
            }
        };
        timerThread.start();*/


    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        finish();
    }


    public  void getlink() {
        final RequestQueue queue = Volley.newRequestQueue(SplashActivity.this);
        String requestURL = getString(R.string.splash_link) + "get_link.php";
        Log.e("check web_path", requestURL);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
            @Override
            public void onResponse(String result) {

                if (result != null) {
                    Log.e("check web_path", result);
                    if (!result.startsWith("null")) {
                        try {
                            JSONObject jObj = new JSONObject(result);
                            if (jObj.length() > 0) {
                                if (jObj.getString("result").equals("success")) {
                                    String new_link=jObj.getString("link");
                                    editor.putString("new_link",new_link).apply();
                                    String new_alllink=jObj.getString("upload_path");
                                    editor.putString("new_event",new_alllink).apply();
                                    String new_studentprofilel=jObj.getString("student_path");
                                    editor.putString("new_profile",new_studentprofilel).apply();
                                    String new_pdflink=jObj.getString("pdf_path");
                                    editor.putString("new_pdflink",new_pdflink).apply();
                                    String new_gallerylink=jObj.getString("gallary_image_path");
                                    editor.putString("new_gallerylink",new_gallerylink).apply();

                                }
                                if (jObj.getString("result").equals("failed")) {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            sp.edit().clear().apply();
                                            // Creating Session
                                            session.logoutUser();
                                            Intent intent=new Intent(SplashActivity.this,LoginActivity.class);
                                            startActivity(intent);
                                            Toast.makeText(SplashActivity.this, "" + "Logout", Toast.LENGTH_SHORT).show();

                                        }
                                    });

                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("id", "4");
                Log.e("params", "" + params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        queue.add(stringRequest);
    }//ResendCode

    private void getVersionInfo() {
        PackageInfo packageinfo = null;
        try {
            packageinfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        Log.e("version","====="+packageinfo.versionName.toString());
        version_code=packageinfo.versionName.toString();
        //alertDialog(packageinfo.versionName.toString());

    }

    private void CheckVersion() {
        final ProgressDialog dialog = new ProgressDialog(SplashActivity.this);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.str_loading));
        dialog.show();

        final RequestQueue queue = Volley.newRequestQueue(SplashActivity.this);

        String requestURL = getString(R.string.version_path) + "app_version.php";
        Log.e("check web_path", requestURL);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
            @SuppressLint("NewApi")
            @Override
            public void onResponse(String result) {
                Log.e("response", result);
                if (result != null) {
                    if (!result.startsWith("null")) {
                        try {
                            JSONObject jObj = new JSONObject(result);
                            if (jObj.length() > 0) {
                                if (jObj.getString("result").equals("success")) {
                                    if(jObj.getString("app_version").equals(version_code)){
                                        Thread timerThread = new Thread(){
                                            public void run(){
                                                try{
                                                    sleep(2000);
                                                }catch(InterruptedException e){
                                                    e.printStackTrace();
                                                }finally{
                                                    Intent intent = new Intent(SplashActivity.this,LoginActivity.class);
                                                    startActivity(intent);
                                                }
                                            }
                                        };
                                        timerThread.start();
                                    }else {
                                        alertDialog(getString(R.string.update_version_error));

                                    }

                                } else if (jObj.getString("result").equals(
                                        "failed")) {
                                    if (jObj.getString("msg") != null) {
                                        Log.e("Delete Ac check", "login");

                                    }
                                    // }

                                }
                                dialog.dismiss();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (dialog.isShowing())
                                dialog.dismiss();
                            Toast.makeText(getApplicationContext(), getString(R.string.str_json_exception), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                if (dialog.isShowing())
                    dialog.dismiss();
                Toast.makeText(getApplicationContext(), getString(R.string.str_json_exception), Toast.LENGTH_SHORT).show();
            }

        }) {
            @Override
            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();
                Log.e("params", "" + params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 4, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }
    private void alertDialog(String error) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                SplashActivity.this);
        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setIcon(R.drawable.daffodils_logo);
        alertDialog.setMessage(error);
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton(getString(R.string.str_upgrade),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                        try {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                        } catch (android.content.ActivityNotFoundException anfe) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                        }
                        dialog.dismiss();
                    }
                });
        alertDialog.setNegativeButton(getString(R.string.str_later), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(SplashActivity.this,LoginActivity.class);
                startActivity(intent);
                dialog.dismiss();
            }
        });
        alertDialog.show();
    }
    }//oncreate
