package com.rsl.daffodils.adapter;

/**
 * Created by Heaven on 01-10-2015.
 */

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.rsl.daffodils.R;
import com.rsl.daffodils.model.Message;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static com.rsl.daffodils.model.DateDifference.printDifference;

/**
 * Created by Heaven on 24-09-2015.
 */
public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.PlaceViewHolder> {
    private static List<Message> mPlaces;
    private static Activity object;

    public ChatAdapter(List<Message> places, Activity object) {
        this.mPlaces = places;
        this.object = object;
    }

    public void updateList(List<Message> data) {
        mPlaces = data;
        notifyDataSetChanged();
    }

    @Override
    public PlaceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_item, parent, false);
        PlaceViewHolder pvh = new PlaceViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(PlaceViewHolder holder, int position) {
        Log.d("position", "" + position);
        String type = mPlaces.get(position).getType();
        String message = mPlaces.get(position).getMessage();
        String date = mPlaces.get(position).getDate();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date curent_date = new Date();

        if (type.equals("send")) {
            //holder.reciver_user_message.setText(message);
            //holder.sender_layout.setVisibility(View.GONE);
            setAlignment(holder, false);
        } else {
          /* String name = mPlaces.get(position).getName();
           holder.sender_user_name.setText(name);
           holder.sender_user_message.setText(message);
           holder.reciver_user_message.setVisibility(View.GONE);*/
            setAlignment(holder, true);
        }
        holder.txtMessage.setText(message);
/*
        if(date.startsWith(dateFormat.format(curent_date))){
            String time = date.split(" ")[1];
            holder.txtInfo.setText(time.substring(0, time.lastIndexOf(":")));
        }else {
           // holder.txtInfo.setText(date);
            holder.txtInfo.setText(date.substring(0, date.lastIndexOf(":")));
        }*/
        //2015-10-25 16:55:54
        SimpleDateFormat sdateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss");
        Date d = null;
        try {
            d = sdateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Date c = new Date();
        holder.txtInfo.setText(printDifference(d, c));


        //String fb_id = mPlaces.get(position).getFb_id();
        // String user_info = mPlaces.get(position).getInfo();
        //String u_id = mPlaces.get(position).getProfile_id();

        //Glide.with(object).load(img_url).into(holder.user_img);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public View v;

        public ViewHolder(View v) {
            super(v);
            this.v = v;
        }
    }

    @Override
    public void onBindViewHolder(PlaceViewHolder holder, final int position, final List<Object> payloads) {
        super.onBindViewHolder(holder, position, payloads);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Context context = v.getContext();
                ClipboardManager clipmanager = (ClipboardManager) context.getSystemService(context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("data", mPlaces.get(position).getMessage());
                Toast.makeText(context.getApplicationContext(), "Copied to clipboard ", Toast.LENGTH_SHORT).show();
                clipmanager.setPrimaryClip(clip);
                return false;
            }
        });
    }


    public interface OnItemClickListener {
        public void onItemClicked(int position);
    }

    public interface OnItemLongClickListener {
        public boolean onItemLongClicked(int position);
    }



    @Override
    public int getItemCount() {

        return this.mPlaces.size();
    }

    public static class PlaceViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView txtMessage;
        public TextView txtInfo;
        public LinearLayout content;
        public LinearLayout contentWithBG;


        PlaceViewHolder(View v) {
            super(v);
            itemView.setOnClickListener(this);
            txtMessage = (TextView) v.findViewById(R.id.txtMessage);
            content = (LinearLayout) v.findViewById(R.id.content);
            contentWithBG = (LinearLayout) v.findViewById(R.id.contentWithBackground);
            txtInfo = (TextView) v.findViewById(R.id.txtInfo);
        }

        @Override
        public void onClick(View v) {
            Context context = itemView.getContext();
            /*Intent i =new Intent(context, UserDetails.class);
            i.putExtra("uid",mPlaces.get(getPosition()).getId());
            i.putExtra("fb_email",mPlaces.get(getPosition()).getEmail());
            context.startActivity(i);*/

            // object.finish();
        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void setAlignment(PlaceViewHolder holder, boolean isMe) {
        if (!isMe) {
            holder.contentWithBG.setBackgroundResource(R.drawable.send_msg);

            LinearLayout.LayoutParams layoutParams =
                    (LinearLayout.LayoutParams) holder.contentWithBG.getLayoutParams();
            layoutParams.gravity = Gravity.RIGHT | Gravity.CENTER_VERTICAL;
            layoutParams.leftMargin = 0;
            layoutParams.rightMargin = 0;
            holder.contentWithBG.setLayoutParams(layoutParams);

            RelativeLayout.LayoutParams lp =
                    (RelativeLayout.LayoutParams) holder.content.getLayoutParams();
            // lp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, 0);
            lp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            lp.addRule(RelativeLayout.ALIGN_PARENT_END);
            holder.content.setLayoutParams(lp);
            layoutParams = (LinearLayout.LayoutParams) holder.txtMessage.getLayoutParams();
            layoutParams.gravity = Gravity.LEFT;
            layoutParams.leftMargin = 5;
            layoutParams.rightMargin = 20;
            layoutParams.bottomMargin = 5;
            holder.txtMessage.setLayoutParams(layoutParams);

            layoutParams = (LinearLayout.LayoutParams) holder.txtInfo.getLayoutParams();
            layoutParams.gravity = Gravity.END;
            layoutParams.rightMargin = 5;
            layoutParams.bottomMargin = 5;
            holder.txtInfo.setLayoutParams(layoutParams);
        } else {
            holder.contentWithBG.setBackgroundResource(R.drawable.received_msg);

            LinearLayout.LayoutParams layoutParams =
                    (LinearLayout.LayoutParams) holder.contentWithBG.getLayoutParams();
            layoutParams.gravity = Gravity.START;
            layoutParams.rightMargin = 0;
            layoutParams.leftMargin = 0;
            holder.contentWithBG.setLayoutParams(layoutParams);

            RelativeLayout.LayoutParams lp =
                    (RelativeLayout.LayoutParams) holder.content.getLayoutParams();
            lp.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            lp.addRule(RelativeLayout.ALIGN_PARENT_START);
            //lp.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            holder.content.setLayoutParams(lp);
            layoutParams = (LinearLayout.LayoutParams) holder.txtMessage.getLayoutParams();
            layoutParams.gravity = Gravity.START;
            layoutParams.leftMargin = 22;
            layoutParams.rightMargin = 5;
            layoutParams.bottomMargin = 5;
            holder.txtMessage.setLayoutParams(layoutParams);

            layoutParams = (LinearLayout.LayoutParams) holder.txtInfo.getLayoutParams();
            layoutParams.gravity = Gravity.START;
            layoutParams.rightMargin = 0;
            holder.txtInfo.setLayoutParams(layoutParams);

        }
    }


}

