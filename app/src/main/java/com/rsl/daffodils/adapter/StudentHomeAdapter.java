package com.rsl.daffodils.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.rsl.daffodils.R;
import com.rsl.daffodils.activity.AllStudentListActivity;
import com.rsl.daffodils.activity.FeedbackActivity;
import com.rsl.daffodils.activity.student.AttendenceActivity;
import com.rsl.daffodils.activity.student.CalenderActivity;
import com.rsl.daffodils.activity.student.DownloadsActivity;
import com.rsl.daffodils.activity.student.GalleryActivity;
import com.rsl.daffodils.activity.student.NotificationActivity;
import com.rsl.daffodils.activity.student.ResultActivity;
import com.rsl.daffodils.activity.student.StudentPortalActivity;
import com.rsl.daffodils.activity.student.StudentsHomeActivity;
import com.rsl.daffodils.activity.student.TimetableActivity;
import com.rsl.daffodils.activity.student.VideoChannelActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by vinay on 2/21/2017.
 */

public class StudentHomeAdapter extends BaseAdapter {

    private int[] MenuImages;
    private int[] MenuLabels;
    private int[] colors;
    Context context;
    public static String[] count = {"0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0"};
    SharedPreferences sp;
    SharedPreferences.Editor editor;
    private LayoutInflater inflater = null;


    public StudentHomeAdapter(Context context, int[] MenuImages, int[] MenuLabels, int[] colors) {

        this.context = context;
        this.MenuImages = MenuImages;
        this.MenuLabels = MenuLabels;
        this.colors = colors;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        sp = context.getSharedPreferences("daffodils", 0);
        editor = sp.edit();
        editor.apply();
    }

    @Override
    public int getCount() {
        return MenuImages.length;
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public class Holder {
        ImageView img_item_image;
        //;TextView txt_item_label;
        TextView txt_item_label, count_text;
        // RelativeLayout album_item;

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        Holder holder = new Holder();
        convertView = inflater.inflate(R.layout.row_menu_home, parent, false);
        holder.img_item_image = (ImageView) convertView.findViewById(R.id.img_item_image);
        holder.txt_item_label = (TextView) convertView.findViewById(R.id.txt_item_label);
        holder.count_text = (TextView) convertView.findViewById(R.id.count_text);

//        holder.album_item=(RelativeLayout)convertView.findViewById(R.id.album_item);
//        holder.album_item.setBackgroundColor(colors[position]);

        holder.img_item_image.setImageResource(MenuImages[position]);
        holder.txt_item_label.setText(MenuLabels[position]);

        if (position == 0) {
            convertView.setBackgroundResource(R.color.green_color);
        } else if (position == 1) {
            convertView.setBackgroundResource(R.color.pink_color);
        } else if (position == 2) {
            convertView.setBackgroundResource(R.color.yellow_color);
        } else if (position == 3) {
            convertView.setBackgroundResource(R.color.red_color);
        } else if (position == 4) {
            convertView.setBackgroundResource(R.color.lightblue_color);
        } else if (position == 5) {
            convertView.setBackgroundResource(R.color.event_color);
        } else if (position == 6) {
            convertView.setBackgroundResource(R.color.black);
        } else if (position == 7) {
            convertView.setBackgroundResource(R.color.event_holiday_color);
        } else if (position == 8) {
            convertView.setBackgroundResource(R.color.event_exam_color);
        } else if (position == 9) {
            convertView.setBackgroundResource(R.color.dot_dark_screen1);
        } else if (position == 10) {
            convertView.setBackgroundResource(R.color.dot_dark_screen2);
        } else if (position == 11) {
            convertView.setBackgroundResource(R.color.dot_dark_screen4);
        }


        // convertView.setBackgroundColor(colors[position]);

        if (count[position].equals("") || count[position].equals("0")) {
            holder.count_text.setVisibility(View.GONE);
        } else {
            holder.count_text.setVisibility(View.VISIBLE);

        }
        holder.count_text.setText(count[position]);
        convertView.setTag(holder);

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = null;


                if (position == 0) {
                    intent = new Intent(context, NotificationActivity.class);
                } else if (position == 1) {
                    intent = new Intent(context, CalenderActivity.class);
                } else if (position == 2) {
                    intent = new Intent(context, AttendenceActivity.class);
                } else if (position == 3) {
                    intent = new Intent(context, GalleryActivity.class);
                } else if (position == 4) {
                    intent = new Intent(context, FeedbackActivity.class);
                } else if (position == 5) {
                    intent = new Intent(context, TimetableActivity.class);
                } else if (position == 6) {
                    intent = new Intent(context, VideoChannelActivity.class);
                } else if (position == 7) {
                    intent = new Intent(context, StudentPortalActivity.class);
                } else if (position == 8) {
                    intent = new Intent(context, DownloadsActivity.class);
                } else if (position == 9) {
                      link();
//                    String url = context.getResources().getString(R.string.fb_linkfb_link);
//                    intent = new Intent(Intent.ACTION_VIEW);
//                    intent.setData(Uri.parse(url));

                } else if (position == 10) {
                    intent = new Intent(context, AllStudentListActivity.class);
                } else if (position == 11) {
                    intent = new Intent(context, ResultActivity.class);
                }
                if (intent != null && position!= 9) {
                    intent.putExtra("from_activity", "Student");
                    context.startActivity(intent);
                }

            }

        });

        return convertView;
    }


    private void link() {
        final ProgressDialog dialog = new ProgressDialog(context);
        dialog.setCancelable(false);
        dialog.setMessage(context.getString(R.string.str_requesting));
        dialog.show();
        RequestQueue queue = Volley.newRequestQueue(context);
        String requestURL = sp.getString("new_link","") + "linklist";
        Log.e("requestURL", "" + requestURL);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", "code=" + response);
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response != null) {
                    if (response.equals("204")) {

                        Log.e("Error message", "No Records Available");
                    } else {

                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                //Toast.makeText(getApplicationContext(), "Poor Internet Coonection", Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                // params.put("id",id);
                Log.e("param", "" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                    VolleyError error = new VolleyError(new String(volleyError.networkResponse.data));
                    volleyError = error;
                    dialog.dismiss();
                }
                // Toast.makeText(getApplicationContext(),"Poor Internet Coonection",Toast.LENGTH_SHORT).show();
                Log.e("parseNetworkError", "==" + volleyError);
                Log.e("parseNetworkError", "string==" + volleyError.toString());
                Log.e("parseNetworkError", "msg==" + volleyError.getMessage());
                return volleyError;
            }

            @Override
            protected Response<String> parseNetworkResponse(final NetworkResponse response) {
                String responseString = "";
                if (response != null) {
                    responseString = String.valueOf(response.statusCode);
                    // can get more details such as response.headers
                }
                Map<String, String> responseHeaders = response.headers;
                if (response.statusCode == 200) {
                    Response<String> result = Response.success(responseHeaders.get("Content-Type"), HttpHeaderParser.parseCacheHeaders(response));
                    Log.e("Response_link", "" + parseToString(response));


                    ((StudentsHomeActivity) context).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                JSONArray JArray = new JSONArray(parseToString(response));

                               // for (int i = 0; i < JArray.length(); i++) {
                                    JSONObject jobj_Data = JArray.getJSONObject(0);
                                    Intent intent;
                                    String id = jobj_Data.getString("link_id");
                                    String url = jobj_Data.getString("link_name");
                                Log.e("url",""+url);
                                    intent = new Intent(Intent.ACTION_VIEW);
                                    intent.setData(Uri.parse(url));
                                   context.startActivity(intent);
                               // }//for

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });

                    return result;
                } else {

//                    text_msg.setVisibility(View.VISIBLE);
                    //    listview.setVisibility(View.GONE);
                    // text_msg.setText(jObj.getString("msg"));
                    Log.e("Error message", "Login Failed");
                }
                Log.e("responseString", "" + responseString);
                return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
            }

            public String parseToString(NetworkResponse response) {
                String parsed;
                try {
                    parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                } catch (UnsupportedEncodingException e) {
                    parsed = new String(response.data);
                }
                return parsed;
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 4, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }//link


}//baseadapter
