package com.rsl.daffodils.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.rsl.daffodils.fragment.CourseFragment;
import com.rsl.daffodils.fragment.InformationFragment;

/**
 * Created by Developmenttwelve on 6/28/2017.
 */

public class AboutpagerAdapter extends FragmentStatePagerAdapter {


    int mNumOfTabs;

    public AboutpagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                InformationFragment tab1 = new InformationFragment();
                return tab1;
            case 1:
                CourseFragment tab2 = new CourseFragment();
                return tab2;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
