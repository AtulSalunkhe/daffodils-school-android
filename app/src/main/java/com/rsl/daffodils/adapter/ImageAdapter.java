package com.rsl.daffodils.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.rsl.daffodils.R;


public class ImageAdapter extends BaseAdapter {


    private int[] MenuImages;



    private RelativeLayout.LayoutParams mImageViewLayoutParams;
    private int mItemHeight = 0;
    private int mNumColumns = 0;

       // Fragment fragment=null;

    Context context;

    private LayoutInflater inflater = null;


    public ImageAdapter(Context context,int[] MenuImages) {

        this.context = context;
        this.MenuImages = MenuImages;

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        Log.e("count",""+MenuImages.length);
        return MenuImages.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    // set numcols
    public void setNumColumns(int numColumns) {
        mNumColumns = numColumns;
    }

    public int getNumColumns() {
        return mNumColumns;
    }

    // set photo item height
    public void setItemHeight(int height) {
        if (height == mItemHeight) {
            return;
        }
        mItemHeight = height;
        mImageViewLayoutParams = new RelativeLayout.LayoutParams(GridLayout.LayoutParams.MATCH_PARENT, mItemHeight);
        notifyDataSetChanged();
    }

    public class Holder
    {
        TextView tv;
        ImageView img;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // return null;
//        TextView tv;
//        ImageView img;

        Holder holder=new Holder();
//        View rowView;
        if(convertView==null) {
//            LayoutInflater inflater = ((AppCompatActivity) context).getLayoutInflater();
            convertView=inflater.inflate(R.layout.activity_gridnew, parent,false);



            holder.img = (ImageView) convertView.findViewById(R.id.gridimage);
            holder.img.setImageResource(MenuImages[position]);
            convertView.setTag(holder);
        }else{
            holder = (Holder) convertView.getTag();

            holder.img = (ImageView) convertView.findViewById(R.id.gridimage);
            holder.img.setImageResource(MenuImages[position]);
            convertView.setTag(holder);
        }


        return convertView;
    }
}



