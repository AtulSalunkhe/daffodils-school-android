package com.rsl.daffodils.adapter;

/**
 * Created by vinay on 29-09-2015.
 */

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.rsl.daffodils.BuildConfig;
import com.rsl.daffodils.R;
import com.rsl.daffodils.utils.ConnectionDetector;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;


/**
 * Created by Heaven on 24-09-2015.
 */
public class ReferencesAdapter extends RecyclerView.Adapter<ReferencesAdapter.PlaceViewHolder> {
    private static ArrayList<HashMap<String, String>> mPlaces;
    private static Activity object;
    final public static int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;
    private static ConnectionDetector internet;
    static Uri vedioURI;
    public ReferencesAdapter(ArrayList<HashMap<String, String>> places, Activity object) {
        this.mPlaces = places;
        this.object = object;
        internet = new ConnectionDetector(object);

    }

    public void updateList(ArrayList<HashMap<String, String>> data) {
        mPlaces = data;
        notifyDataSetChanged();

    }

    @Override
    public PlaceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_references_list, parent, false);
        PlaceViewHolder pvh = new PlaceViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(PlaceViewHolder holder, int position) {
        String str_title = mPlaces.get(position).get("reference_name");

        holder.txt_title.setText(str_title);

    }

    @Override
    public int getItemCount() {

        return this.mPlaces.size();
    }

    public static class PlaceViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView txt_title;
        ImageView img_download;

        PlaceViewHolder(View itemView) {
            super(itemView);
            txt_title = (TextView) itemView.findViewById(R.id.txt_doc_title);
            img_download = (ImageView) itemView.findViewById(R.id.img_download);


            itemView.setOnClickListener(this);
            img_download.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (checkPermission()) {
                        if (internet.isConnectingToInternet()) {
                            String str_path = mPlaces.get(getAdapterPosition()).get("reference_path");
                            new DownloadFileFromURL().execute(str_path);
                        } else {
                            internet.showAlertDialog(object, false);
                        }
                    } else
                        ActivityCompat.requestPermissions(object, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                }
            });
        }

        @Override
        public void onClick(View v) {

        }
    }

    private static class DownloadFileFromURL extends AsyncTask<String, String, String> {
        String filename = "temp.pdf";
        final ProgressDialog dialog = new ProgressDialog(object);

        /**
         * Before starting background thread
         * Show Progress Bar Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.setCancelable(false);
            dialog.setMessage(object.getString(R.string.str_downloading));
            dialog.show();
        }

        /**
         * Downloading file in background thread
         */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            filename = f_url[0].substring(f_url[0].lastIndexOf("/") + 1);
            Log.e("filename", "==" + filename);
            try {
                URL url = new URL(f_url[0]);
                URLConnection conection = url.openConnection();
                conection.connect();
                // this will be useful so that you can show a tipical 0-100% progress bar
                int lenghtOfFile = conection.getContentLength();
                // download the file
                InputStream input = new BufferedInputStream(url.openStream(), 8192);

                String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/GoreClasses";
                File dir = new File(path);
                if (!dir.exists())
                    dir.mkdirs();
                File file = new File(dir, filename);
                vedioURI   = FileProvider.getUriForFile(object,
                        BuildConfig.APPLICATION_ID + ".provider", file);
                // Output stream
                OutputStream output = new FileOutputStream(file);

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                    // wrting data to file
                    output.write(data, 0, count);
                }
                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();
                return "success";
            } catch (Exception e) {
                if (dialog.isShowing()) dialog.dismiss();
                Log.e("Error: ", e.getMessage());
                return "fail";
            }
        }

        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
        }

        /**
         * After completing background task
         * Dismiss the progress dialog
         **/
        @Override
        protected void onPostExecute(String result) {
            if (result.equals("success")) {
                if (dialog.isShowing()) dialog.dismiss();
                viewPdf(filename);
            }
        }
    }

    // Method for opening a pdf file
    private static void viewPdf(String file) {
        File pdfFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/GoreClasses/" + file);
       // Uri path = Uri.fromFile(pdfFile);
        Log.e("download_path",""+pdfFile);
        Uri path =  FileProvider.getUriForFile(object, BuildConfig.APPLICATION_ID + ".provider", pdfFile);
        Log.e("download_url",""+path);
        Log.e("download_vedioURI",""+vedioURI);
        // Setting the intent for pdf reader
        Intent pdfIntent = new Intent(Intent.ACTION_VIEW);
        pdfIntent.setDataAndType(vedioURI, "application/pdf");
       // pdfIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        pdfIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        try {
            object.startActivity(pdfIntent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(object, "Can't read pdf file", Toast.LENGTH_SHORT).show();
        }
    }

    private static boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(object, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        return false;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

}

