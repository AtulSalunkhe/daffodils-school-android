package com.rsl.daffodils.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.rsl.daffodils.fragment.CourseFragment;
import com.rsl.daffodils.fragment.InformationFragment;
import com.rsl.daffodils.fragment.student.AnnouncementFragment;
import com.rsl.daffodils.fragment.student.HolidayFragment;
import com.rsl.daffodils.fragment.student.MessageFragment;
import com.rsl.daffodils.model.Message;

/**
 * Created by Developmenttwelve on 9/4/2017.
 */

public class NotificationpagerAdapter extends FragmentStatePagerAdapter {

    int mNumOfTabs;

    public NotificationpagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                AnnouncementFragment tab1 = new AnnouncementFragment();
                return tab1;
            case 1:
                HolidayFragment tab2 = new HolidayFragment();
                return tab2;
            case 2:
                MessageFragment tab3 = new MessageFragment();
                return tab3;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
