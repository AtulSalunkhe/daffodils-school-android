package com.rsl.daffodils.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.rsl.daffodils.R;
import com.rsl.daffodils.activity.EventDetailsActivity;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Developmenttwelve on 6/27/2017.
 */

public class AdvertisementAdapter extends RecyclerView.Adapter<AdvertisementAdapter.PlaceViewHolder> {
    private static ArrayList<HashMap<String, String>> mPlaces;
    private static Activity object;

    public AdvertisementAdapter(ArrayList<HashMap<String, String>> places, Activity object) {
        this.mPlaces = places;
        this.object = object;

    }

    public void updateList(ArrayList<HashMap<String, String>> data) {
        mPlaces = data;
        notifyDataSetChanged();

    }

    @Override
    public PlaceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_news_list, parent, false);
        PlaceViewHolder pvh = new PlaceViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(PlaceViewHolder holder, int position) {
        String str_title = mPlaces.get(position).get("advertise_title");
        String url = mPlaces.get(position).get("advertise_img");
        String str_data = mPlaces.get(position).get("advertise_data");
        holder.txt_title.setText(str_title);
        holder.txt_data.setText(str_data);
        Log.e("adv_","url="+url);
        Glide.with(object).load(url).dontAnimate().placeholder(R.drawable.app_logo).into(holder.bg_img);

    }

    @Override
    public int getItemCount() {

        return this.mPlaces.size();
    }

    public static class PlaceViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView txt_title, txt_date, txt_data;
        ImageView bg_img;

        PlaceViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            txt_title = (TextView) itemView.findViewById(R.id.txt_title);
            txt_date = (TextView) itemView.findViewById(R.id.txt_date);
            txt_data = (TextView) itemView.findViewById(R.id.txt_data);
            bg_img = (ImageView) itemView.findViewById(R.id.bg_img);

        }

        @Override
        public void onClick(View v) {
            Context context = itemView.getContext();
            int pos = getAdapterPosition();
            Intent i = new Intent(object, EventDetailsActivity.class);
            i.putExtra("tag", "Announcement Details");
            i.putExtra("title", mPlaces.get(pos).get("advertise_title"));
            i.putExtra("img_url", mPlaces.get(pos).get("advertise_img"));
            i.putExtra("description", mPlaces.get(pos).get("advertise_data"));
            i.putExtra("event_date", "");
            object.startActivity(i);
        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

}
