package com.rsl.daffodils.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import com.rsl.daffodils.fragment.student.EventFragment;
import com.rsl.daffodils.fragment.student.HolidayEventFragment;


/**
 * Created by Developmenttwelve on 9/8/2017.
 */

public class ClenderpagerAdapter  extends FragmentStatePagerAdapter {

    int mNumOfTabs;

    public ClenderpagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                EventFragment tab1 = new EventFragment();
                return tab1;
            case 1:
                HolidayEventFragment tab2 = new HolidayEventFragment();
                return tab2;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }


}
