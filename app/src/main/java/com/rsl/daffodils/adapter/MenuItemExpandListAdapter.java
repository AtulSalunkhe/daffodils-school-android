package com.rsl.daffodils.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.rsl.daffodils.R;
import com.rsl.daffodils.model.MenuItemGroup;

import java.util.ArrayList;
import java.util.HashMap;

@SuppressLint("ResourceAsColor")
public class MenuItemExpandListAdapter extends BaseExpandableListAdapter {

    private Context context;
    ArrayList<MenuItemGroup> group1;


    ArrayList<HashMap<String, String>> child;

    SharedPreferences sp;
    SharedPreferences.Editor editor;


    public MenuItemExpandListAdapter(Context context,
                                     ArrayList<MenuItemGroup> group1) {
        this.context = context;
        this.group1 = group1;
        sp = context.getSharedPreferences("daffodils", 0);
        editor = sp.edit();

    }

    @Override
    public HashMap<String, String> getChild(int groupPosition, int childPosition) {
        ArrayList<HashMap<String, String>> chList = group1.get(groupPosition)
                .getMenu_items();
        Log.v("Answer list==========", "" + chList);
        return chList.get(childPosition);
        // ArrayList<Child> chList = groups.get(groupPosition).getItems();
        // return chList.get(childPosition);
    }


    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final HashMap<String, String> child = getChild(groupPosition,
                childPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) context
                    .getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.row_topper, null);
        }

        TextView student_name = (TextView) convertView.findViewById(R.id.txt_title);
        Log.e("name","=="+child.get("name"));
        student_name.setText(child.get("name"));

        TextView student_mark = (TextView) convertView.findViewById(R.id.txt_data);
        TextView txt_company = (TextView) convertView.findViewById(R.id.txt_company);
        //String str_company = child.get("company_name");

        String str_placement  = "percentage : " + child.get("percentage");

        //txt_company.setText(str_company);
        student_mark.setText(str_placement);

        ImageView student_image = (ImageView) convertView.findViewById(R.id.bg_img);
        String img = sp.getString("new_profile","")+child.get("student_image");
        Glide.with(context).load(img).dontAnimate().placeholder(R.drawable.daffodils_logo).into(student_image);


        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        if (group1.get(groupPosition).getMenu_items() != null) {
            child = group1.get(groupPosition).getMenu_items();
            return child.size();
        } else
            return 0;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return group1.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        Log.e("size","***"+group1.size());
        return group1.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {

        MenuItemGroup grList = group1.get(groupPosition);
        if (convertView == null) {
            LayoutInflater inf = (LayoutInflater) context
                    .getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView = inf.inflate(R.layout.row_parent, null);
        }
        ExpandableListView mExpandableListView = (ExpandableListView) parent;
        mExpandableListView.expandGroup(groupPosition);

        TextView txt_menu = (TextView) convertView.findViewById(R.id.text_group_name);
        txt_menu.setText(grList.getStudentclass());


        return convertView;
    }


    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

}
