package com.rsl.daffodils.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.rsl.daffodils.R;
import com.rsl.daffodils.activity.skip.ToppersActivity;
import com.rsl.daffodils.model.MenuItemGroup;
import com.rsl.daffodils.model.MenuItemGroupName;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Admin on 6/29/2017.
 */

public class MyListAdapter extends RecyclerView.Adapter<MyListAdapter.PlaceViewHolder> {

    private static ArrayList<MenuItemGroupName> mPlaces;
    private static Activity object1;
    public static String year_pos = "";
    static SharedPreferences sp;
    SharedPreferences.Editor editor;

    public MyListAdapter(ArrayList<MenuItemGroupName> places, Activity object) {
        this.mPlaces = places;
        this.object1 = object;
    }

    @Override
    public PlaceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_header_button, parent, false);
        PlaceViewHolder pvh = new PlaceViewHolder(v);
        sp = object1.getSharedPreferences("daffodils", 0);
        editor = sp.edit();;
        return pvh;
    }

    @Override
    public void onBindViewHolder(PlaceViewHolder holder, int position) {
        String name = mPlaces.get(position).getMenu_item_group_name();
        // String post_date = mPlaces.get(position).getmText2();

        holder.item_name.setText(name);

        if (position == 0) {
            getMainData(mPlaces.get(position).getMenu_item_group_id());
        }


    }

    @Override
    public int getItemCount() {

        return this.mPlaces.size();
    }

    public class PlaceViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView item_name;

        PlaceViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            item_name = (TextView) itemView.findViewById(R.id.txt_item_name);
        }

        @Override
        public void onClick(View v) {
            int pos = getAdapterPosition();
            Log.e("onClick", ":: pos " + mPlaces.get(pos).getMenu_item_group_id());
            getMainData(mPlaces.get(pos).getMenu_item_group_id());


        }


    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }




   public static void getMainData(final String year_id) {

        final ProgressDialog dialog = new ProgressDialog(object1);
        dialog.setCancelable(false);
        dialog.setMessage(object1.getString(R.string.str_requesting));
        dialog.show();
        RequestQueue queue = Volley.newRequestQueue(object1);
        String requestURL = sp.getString("new_link","") + "topper_student_list/"+year_id;
        Log.e("requestURL", "" + requestURL);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", "code=" + response);
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response != null) {
                    if (response.equals("204")) {
                        ToppersActivity.text_msg.setVisibility(View.VISIBLE);
                        ToppersActivity.text_msg.setText("No Records Available");
                        ToppersActivity.expand_topper_list.setVisibility(View.GONE);

                        Log.e("Error message", "No Records Available");
                    } else {
                        ToppersActivity.text_msg.setVisibility(View.GONE);
//                        text_no_data.setVisibility(View.GONE);
//                        adapter = new CarCompanyAdapter(car_company, CarComapnyActivity.this);
//                        recyclerView_company.setAdapter(adapter);
//                        adapter.notifyDataSetChanged();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }


            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                // params.put("",year_id);
                Log.e("param", "" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                    VolleyError error = new VolleyError(new String(volleyError.networkResponse.data));
                    volleyError = error;
                    dialog.dismiss();
                }
                // Toast.makeText(getApplicationContext(),"Poor Internet Coonection",Toast.LENGTH_SHORT).show();
                Log.e("parseNetworkError", "==" + volleyError);
                Log.e("parseNetworkError", "string==" + volleyError.toString());
                Log.e("parseNetworkError", "msg==" + volleyError.getMessage());
                return volleyError;
            }


            @Override
            protected Response<String> parseNetworkResponse(final NetworkResponse response) {
                String responseString = "";
                if (response != null) {
                    responseString = String.valueOf(response.statusCode);
                    // can get more details such as response.headers
                    Log.e("re","----"+response);
                }
                Map<String, String> responseHeaders = response.headers;
                if (response.statusCode == 200) {
                    Response<String> result = Response.success(responseHeaders.get("Content-Type"), HttpHeaderParser.parseCacheHeaders(response));
                    Log.e("Response_yearlist", "" + parseToString(response));
                    ToppersActivity.group1.clear();
//                    ToppersActivity.text_msg.setVisibility(View.GONE);
//                    ToppersActivity.expand_topper_list.setVisibility(View.VISIBLE);

                    object1.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            try {


                                JSONObject jObj_result = new JSONObject(parseToString(response));

                                if (jObj_result.getString("result").equals("success")) {
                                    ToppersActivity.group1.clear();
                                    ToppersActivity.text_msg.setVisibility(View.GONE);
                                    ToppersActivity.expand_topper_list.setVisibility(View.VISIBLE);

                                    // dialog.dismiss();
                                    Object object = jObj_result.get("class");
                                    if (object instanceof JSONObject) {
                                        // It is json object
                                    } else if (object instanceof JSONArray) {

                                        JSONObject jb=null;
                                        if (jObj_result.getJSONArray("class").length() > 0) {
                                            for (int j = 0; j < jObj_result.getJSONArray("class").length(); j++) {
                                                jb= jObj_result.getJSONArray("class").getJSONObject(j);
                                                Log.e("student_class", "" + jb.getString("student_class"));
                                                MenuItemGroup grp_data = new MenuItemGroup();
                                                JSONArray jsonArray = jb.getJSONArray("student");
                                                grp_data.setStudentclass(jb.getString("student_class"));
                                                ArrayList<HashMap<String, String>> menu_items = new ArrayList<HashMap<String, String>>();
                                                for (int m = 0; m < jsonArray.length(); m++) {
                                                    JSONObject jbb = jsonArray.getJSONObject(m);
                                                    Log.e("student_id", "" + jbb.getString("student_id"));
                                                    HashMap<String, String> hash_menu_items = new HashMap<String, String>();
                                                    hash_menu_items.put("student_image", jbb.getString("student_image"));
                                                    hash_menu_items.put("student_id", jbb.getString("student_id"));
                                                    hash_menu_items.put("account_id", jbb.getString("account_id"));
                                                    hash_menu_items.put("name", jbb.getString("name"));
                                                    hash_menu_items.put("email", jbb.getString("email"));
                                                    hash_menu_items.put("mobile", jbb.getString("mobile"));
                                                    hash_menu_items.put("date_of_birth", jbb.getString("date_of_birth"));
                                                    hash_menu_items.put("gender_id", jbb.getString("gender_id"));
                                                    hash_menu_items.put("percentage", jbb.getString("percentage"));
                                                    menu_items.add(hash_menu_items);
                                                }

                                                grp_data.setMenu_items(menu_items);


                                                ToppersActivity.group1.add(grp_data);


                                            }
                                            ToppersActivity.adapter = new MenuItemExpandListAdapter(object1, ToppersActivity.group1);
                                            ToppersActivity.expand_topper_list.setAdapter(ToppersActivity.adapter);


                                        } else {
                                            Log.d("menu_item_leng", "string");
                                        }


//                                    }
                                    }
                                } // success
                                else {
                                    ToppersActivity.text_msg.setVisibility(View.VISIBLE);
                                    ToppersActivity.expand_topper_list.setVisibility(View.GONE);
                                    ToppersActivity.text_msg.setText(jObj_result.getString("msg"));
                                    //  dialog.dismiss();
                                }
                                // dialog.dismiss();


                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                    });

                    return result;
                } else {


                    // text_msg.setText(jObj.getString("msg"));
                    Log.e("Error message", "Login Failed");
                }
                Log.e("responseString", "" + responseString);
                return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
            }

            public String parseToString(NetworkResponse response) {
                String parsed;
                try {
                    parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                } catch (UnsupportedEncodingException e) {
                    parsed = new String(response.data);
                }
                return parsed;
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }//getallmenu



    public static  void alertDialog1(String error) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                object1);
        alertDialog.setTitle(object1.getString(R.string.app_name));
        alertDialog.setIcon(R.drawable.daffodils_logo);
        alertDialog.setMessage(error);
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton(object1.getString(R.string.dlg_ok),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

}
